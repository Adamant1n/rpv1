<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLocationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('locations', function (Blueprint $table) {
            $table->increments('id');

            $table->string('address');

            $table->string('legal_name')->nullable();
            $table->text('legal_address')->nullable();
            $table->string('legal_phone')->nullable();

            $table->integer('price_weekdays')->unsigned()->nullable();
            // $table->foreign('price_weekdays')->references('id')->on('price_lists')->onDelete('set null');

            $table->integer('price_weekends')->unsigned()->nullable();
            // $table->foreign('price_weekends')->references('id')->on('price_lists')->onDelete('set null');

            $table->string('tariff_step')->default('1h')->comment('Минимальный шаг аренды');
            $table->integer('hours_max')->unsigned()->default(6);
            
            $table->string('opens_at')->default('09:00');
            $table->string('closes_at')->default('23:00');
            $table->string('next_day_at')->default('19:00');
            $table->string('return_by')->default('10:20');
            $table->string('weekends')->default('[]');

            $table->string('day_is')->default('opened');
            $table->boolean('all_day')->default(false);

            $table->boolean('active')->default(true);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('locations');
    }
}
