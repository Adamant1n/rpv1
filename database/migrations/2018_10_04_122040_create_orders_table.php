<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('orders', function (Blueprint $table) {
			$table->integer('id')->unsigned();
			$table->primary('id');

			$table->integer('location_id')->unsigned();
			$table->foreign('location_id')->references('id')->on('locations')->onDelete('cascade');

			$table->integer('user_id')->unsigned()->nullable();
			$table->foreign('user_id')->references('id')->on('users')->onDelete('set null');

			$table->integer('agent_id')->unsigned()->nullable();
			$table->foreign('agent_id')->references('id')->on('agents')->onDelete('set null');

			$table->integer('client_id')->unsigned();
			$table->foreign('client_id')->references('id')->on('clients');

			$table->integer('payment_accepted_by')->unsigned()->nullable();
			$table->foreign('payment_accepted_by')->references('id')->on('users')->onDelete('set null');

			$table->string('status')->default('draft');

			$table->string('type')->default('rent');

			$table->string('pledge')->default('cash');

			$table->integer('duration')->unsigned()->default(1);
			$table->string('dimension')->default('h');

			$table->decimal('price', 8, 2)->default(0);
			$table->decimal('full_price', 8, 2)->default(0);
			$table->integer('discount')->unsigned()->default(0);
			$table->decimal('price_extra', 8, 2)->default(0);
            $table->decimal('paid_extra', 8, 2)->default(0);

            $table->text('address')->nullable();

            $table->boolean('kkt')->default(false);
			$table->string('paid_by')->default('cash');
            $table->timestamp('paid_at')->nullable();

			$table->timestamp('starts_at')->nullable();
			$table->timestamp('ends_at')->nullable();
			$table->timestamp('book_at')->nullable();
			
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('orders');
	}
}
