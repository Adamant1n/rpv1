<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('events', function (Blueprint $table) {
			$table->increments('id')->unsigned();
			$table->integer('manager_id')->unsigned()->nullable();
			$table->integer('trainer_id')->unsigned()->nullable();
			$table->integer('client_id')->unsigned()->nullable();
			$table->integer('service_id')->unsigned()->nullable();
			$table->integer('tariff_id')->unsigned()->nullable();
			$table->integer('category_id')->unsigned()->nullable();
			$table->integer('location_id')->unsigned()->nullable();
			$table->date('event_date')->nullable();
			$table->time('start_time')->nullable();
			$table->time('end_time')->nullable();
			$table->date('sale_date')->nullable();
			$table->integer('number_people')->unsigned()->nullable();
			$table->boolean('payment_status')->nullable();
			$table->boolean('online_payment')->nullable();
			$table->integer('cost')->unsigned()->nullable();
			$table->timestamps();
		});

		Schema::table('events', function (Blueprint $table) {
			$table->foreign('manager_id')->references('id')->on('users')->onDelete('set null');
			$table->foreign('trainer_id')->references('id')->on('users')->onDelete('set null');
			$table->foreign('client_id')->references('id')->on('clients')->onDelete('set null');
			$table->foreign('location_id')->references('id')->on('locations')->onDelete('set null');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('events');
	}
}
