<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServiceTariffsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('service_tariffs', function (Blueprint $table) {
			$table->increments('id')->unsigned();
			$table->integer('service_id')->unsigned()->nullable();
			$table->string('name', 100);
			$table->integer('cost')->unsigned();
			$table->timestamps();
		});

		Schema::table('service_tariffs', function (Blueprint $table) {
			$table->foreign('service_id')->references('id')->on('services')->onDelete('set null');
		});

		Schema::table('events', function (Blueprint $table) {
			$table->foreign('tariff_id')->references('id')->on('service_tariffs')->onDelete('set null');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('service_tariffs');
	}
}
