<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTaxationTypeToLocationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('locations', function (Blueprint $table) {
            $table->text('payment_types')->nullable()->before('created_at');
            $table->string('rent_taxation_type')->nullable()->before('created_at');
            $table->string('rent_tax')->nullable()->before('created_at');
            $table->string('sale_taxation_type')->nullable()->before('created_at');
            $table->string('sale_tax')->nullable()->before('created_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('locations', function (Blueprint $table) {
            $table->dropColumn('payment_types');
            $table->dropColumn('rent_taxation_type');
            $table->dropColumn('rent_tax');
            $table->dropColumn('sale_taxation_type');
            $table->dropColumn('sale_tax');
        });
    }
}
