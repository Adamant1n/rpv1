<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReservationsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('reservations', function (Blueprint $table) {
			$table->increments('id');

			$table->integer('order_id')->unsigned();
			$table->foreign('order_id')->references('id')->on('orders')->onDelete('cascade');

			$table->integer('unit_id')->unsigned();
			$table->foreign('unit_id')->references('id')->on('units')->onDelete('cascade');

			$table->integer('given_by')->unsigned()->nullable();
			$table->foreign('given_by')->references('id')->on('users')->onDelete('set null');

			$table->integer('taken_by')->unsigned()->nullable();
			$table->foreign('taken_by')->references('id')->on('users')->onDelete('set null');

			$table->string('status')->default('rent');

			$table->decimal('price', 8, 2)->default(0);
			$table->decimal('full_price', 8, 2)->default(0);
			$table->decimal('price_extra', 8, 2)->default(0);

			$table->timestamp('starts_at')->nullable();
			$table->timestamp('ends_at')->nullable();
			$table->timestamp('returned_at')->nullable();

			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('reservations');
	}
}
