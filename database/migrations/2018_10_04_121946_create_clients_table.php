<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('clients', function (Blueprint $table) {
			$table->integer('id')->unsigned();
			$table->primary('id');
			
			$table->integer('user_id')->unsigned()->nullable();
			$table->foreign('user_id')->references('id')->on('users')->onDelete('set null');

			$table->string('first_name');
			$table->string('last_name');
			$table->string('mid_name');
			
			$table->string('phone')->nullable();
			$table->string('email')->nullable();
			$table->string('passport')->nullable();
			$table->text('description')->nullable();
			$table->date('birthday')->nullable();
			$table->string('photo')->nullable();
			$table->string('passport_scan')->nullable();

			$table->boolean('is_signed')->default(false);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('clients');
	}
}
