<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientFeaturesTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('client_features', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('client_id')->unsigned();
			$table->decimal('height', 3, 0)->nullable();
			$table->decimal('weight', 3, 0)->nullable();
			$table->decimal('shoe_size', 3, 1)->nullable();
			$table->timestamps();
		});

		Schema::table('client_features', function(Blueprint $table)
		{
			$table->foreign('client_id')->references('id')->on('clients')->onDelete('cascade');
		});

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('client_features');
	}
}
