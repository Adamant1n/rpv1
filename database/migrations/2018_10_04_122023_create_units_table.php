<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUnitsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('units', function (Blueprint $table) {
			$table->integer('id')->unsigned();
			$table->primary('id');

			$table->integer('location_id')->unsigned()->nullable();
			$table->foreign('location_id')->references('id')->on('locations')->onDelete('set null');

			$table->integer('main_location')->unsigned()->nullable();
            $table->foreign('main_location')->references('id')->on('locations')->onDelete('cascade');

			$table->integer('product_id')->unsigned();
			$table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');

			$table->integer('condition_id')->unsigned()->nullable();
			$table->foreign('condition_id')->references('id')->on('conditions')->onDelete('set null');

			$table->integer('binding_id')->unsigned()->nullable();
            $table->foreign('binding_id')->references('id')->on('bindings')->onDelete('set null');

			$table->string('size')->nullable();
			$table->string('code');
			$table->string('feature_1')->nullable();
			$table->string('feature_2')->nullable();
			$table->string('feature_3')->nullable();
			$table->string('feature_4')->nullable();
            $table->string('feature_5')->nullable();
			$table->boolean('reserved')->default(false);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('units');
	}
}
