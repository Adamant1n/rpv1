<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('products', function (Blueprint $table) {
			$table->increments('id');

			$table->integer('category_id')->unsigned()->nullable();
			$table->foreign('category_id')->references('id')->on('categories')->onDelete('set null');

			$table->integer('brand_id')->unsigned()->nullable();
			$table->foreign('brand_id')->references('id')->on('brands')->onDelete('set null');

			$table->string('name');
			$table->string('photo')->nullable();

			$table->boolean('for_sale')->default(false);
			$table->decimal('price_buy', 8, 2)->default(0);
            $table->decimal('price_sell', 8, 2)->default(0);

			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('products');
	}
}
