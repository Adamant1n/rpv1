<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventCategoriesTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('event_categories', function (Blueprint $table) {
			$table->increments('id')->unsigned();
			$table->string('name', 100)->unique();
			$table->timestamps();
		});

		Schema::table('users', function (Blueprint $table) {
			$table->foreign('category_id')->references('id')->on('event_categories')->onDelete('set null');
		});

		Schema::table('events', function (Blueprint $table) {
			$table->foreign('category_id')->references('id')->on('event_categories')->onDelete('set null');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('event_categories');
	}
}
