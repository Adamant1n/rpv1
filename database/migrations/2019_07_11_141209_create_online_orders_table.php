<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOnlineOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('online_orders', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('client_id')->unsigned();
            $table->foreign('client_id')->references('id')->on('clients')->onDelete('cascade');

            $table->integer('order_id')->unsigned()->nullable();
            $table->foreign('order_id')->references('id')->on('orders')->onDelete('set null');

            $table->integer('agent_id')->unsigned()->nullable();
            $table->foreign('agent_id')->references('id')->on('agents')->onDelete('set null');

            $table->integer('location_id')->unsigned()->nullable();
            $table->foreign('location_id')->references('id')->on('locations')->onDelete('set null');

            $table->string('external_id')->nullable();

            $table->date('date');
            $table->time('time');
            $table->string('delivery_type');
            $table->string('address')->nullable();
            $table->integer('duration')->unsigned();
            $table->string('dimension')->default('d');
            $table->text('products')->nullable();
            $table->text('additional_products')->nullable();
            $table->string('promocode')->nullable();
            $table->decimal('discount', 9, 2)->default(0);
            $table->decimal('price', 9, 2)->default(0);
            $table->decimal('total', 9, 2)->default(0);
            $table->string('payment_type');
            $table->decimal('paid_amount', 9, 2)->default(0);
            $table->timestamp('paid_at');

            $table->timestamp('starts_at');
            $table->timestamp('ends_at');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('online_orders');
    }
}
