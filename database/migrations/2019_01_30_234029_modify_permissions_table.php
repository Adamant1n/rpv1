<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyPermissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('permissions', function (Blueprint $table) {
            $table->string('display')->nullable()->after('guard_name');
        });

        Schema::table('roles', function (Blueprint $table) {
            $table->string('display')->nullable()->after('name');
            $table->string('color')->nullable()->after('display');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('permissions', function (Blueprint $table) {
            $table->dropColumn('display');
        });

        Schema::table('roles', function (Blueprint $table) {
            $table->dropColumn('display');
            $table->dropColumn('color');
        });
    }
}
