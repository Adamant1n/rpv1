<?php

use Faker\Generator as Faker;

$factory->define(App\ClientFeature::class, function (Faker $faker) {
    return [
        'height' => rand(150, 190),
        'weight' => rand(45, 100),
        'shoe_size' => rand(22, 45)
    ];
});
