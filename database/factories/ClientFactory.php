<?php

use Faker\Generator as Faker;

$factory->define(App\Client::class, function (Faker $faker) {
    return [
        'first_name' => $faker->lastName,
        'last_name' => $faker->firstName,
        'mid_name' => $faker->firstName,
        'phone' => $faker->e164PhoneNumber,
        'email' => $faker->unique()->safeEmail,
        'birthday' => $faker->dateTimeBetween('-40 years', '-20 years')->format('Y-m-d'),
        'is_signed' => rand(0, 1)
    ];
});
