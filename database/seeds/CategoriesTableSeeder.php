<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('categories')->delete();
        
        \DB::table('categories')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Лыжи',
                'params' => '{"1":1,"2":1,"3":0,"4":0,"5":0}',
                'group' => 0,
                'fr_section' => 0,
                'created_at' => '2018-11-13 08:57:59',
                'updated_at' => '2018-11-13 08:57:59',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Ботинки г/л',
                'params' => '{"1":0,"2":0,"3":1,"4":1,"5":0}',
                'group' => 0,
                'fr_section' => 0,
                'created_at' => '2018-11-13 08:57:59',
                'updated_at' => '2018-11-13 08:57:59',
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'Шлемы',
                'params' => '{"1":0,"2":0,"3":0,"4":0,"5":1}',
                'group' => 0,
                'fr_section' => 0,
                'created_at' => '2018-11-13 08:57:59',
                'updated_at' => '2018-11-13 08:57:59',
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'Сноуборды',
                'params' => '{"1":1,"2":1,"3":0,"4":0,"5":0}',
                'group' => 0,
                'fr_section' => 0,
                'created_at' => '2018-11-13 08:57:59',
                'updated_at' => '2018-11-13 08:57:59',
            ),
            4 => 
            array (
                'id' => 5,
                'name' => 'Ботинки с/б',
                'params' => '{"1":0,"2":0,"3":1,"4":0,"5":0}',
                'group' => 0,
                'fr_section' => 0,
                'created_at' => '2018-11-13 08:57:59',
                'updated_at' => '2018-11-13 08:57:59',
            ),
            5 => 
            array (
                'id' => 6,
            'name' => 'Шапочки (продажа)',
                'params' => '{"1":0,"2":0,"3":0,"4":0,"5":1}',
                'group' => 1,
                'fr_section' => 1,
                'created_at' => '2018-11-13 08:57:59',
                'updated_at' => '2019-02-04 21:42:50',
            ),
            6 => 
            array (
                'id' => 7,
                'name' => 'Лыжные палки',
                'params' => '{"1":1,"2":0,"3":0,"4":0,"5":0}',
                'group' => 1,
                'fr_section' => 0,
                'created_at' => '2018-11-19 04:51:53',
                'updated_at' => '2019-07-04 15:19:50',
            ),
        ));
        
        
    }
}