<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('roles')->delete();
        
        \DB::table('roles')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'superadmin',
                'display' => 'Суперадмин',
                'color' => 'danger',
                'guard_name' => 'web',
                'created_at' => NULL,
                'updated_at' => '2019-01-31 00:38:38',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'admin',
                'display' => 'Администратор',
                'color' => 'warning',
                'guard_name' => 'web',
                'created_at' => NULL,
                'updated_at' => '2019-01-31 00:38:54',
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'operator',
                'display' => 'Оператор Колл-Центра',
                'color' => 'primary',
                'guard_name' => 'web',
                'created_at' => NULL,
                'updated_at' => '2019-01-31 00:39:08',
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'manager',
                'display' => 'Менеджер',
                'color' => 'info',
                'guard_name' => 'web',
                'created_at' => NULL,
                'updated_at' => '2019-01-31 00:39:29',
            ),
            4 => 
            array (
                'id' => 5,
                'name' => 'cashier',
                'display' => 'Кассир',
                'color' => 'success',
                'guard_name' => 'web',
                'created_at' => NULL,
                'updated_at' => '2019-01-31 00:39:43',
            ),
            5 => 
            array (
                'id' => 6,
                'name' => 'delivery',
                'display' => 'Курьер',
                'color' => 'outline-primary',
                'guard_name' => 'web',
                'created_at' => NULL,
                'updated_at' => '2019-01-31 00:40:00',
            ),
            6 => 
            array (
                'id' => 7,
                'name' => 'trainer',
                'display' => 'Тренер',
                'color' => 'outline-dark',
                'guard_name' => 'web',
                'created_at' => NULL,
                'updated_at' => '2019-01-31 00:40:16',
            ),
        ));
        
        
    }
}