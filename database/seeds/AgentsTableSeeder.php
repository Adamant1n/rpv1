<?php

use Illuminate\Database\Seeder;

class AgentsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {


        \DB::table('agents')->delete();

        \DB::table('agents')->insert(array(
            0 =>
            array(
                'id' => 1,
                'name' => 'Пятерочка',
                'promocode' => 'P1',
                'discount' => '5',
                'commission' => '6',
                'created_at' => '2018-11-11 00:00:00',
                'updated_at' => '2018-11-11 00:00:00',
                'deleted_at' => NULL,
            ),
            1 =>
            array(
                'id' => 2,
                'name' => 'GonDO Rent',
                'promocode' => '4460185',
                'discount' => '15',
                'commission' => '0',
                'created_at' => '2018-11-19 09:56:06',
                'updated_at' => '2018-11-19 09:56:06',
                'deleted_at' => NULL,
            ),
            2 =>
            array(
                'id' => 3,
                'name' => 'GonDO Rent (15)',
                'promocode' => '2459187',
                'discount' => '0',
                'commission' => '15',
                'created_at' => '2018-11-19 09:56:41',
                'updated_at' => '2018-11-19 09:56:41',
                'deleted_at' => NULL,
            ),
            3 =>
            array(
                'id' => 4,
                'name' => 'Библио глобус',
                'promocode' => '852852',
                'discount' => '10',
                'commission' => '10',
                'created_at' => '2018-11-19 13:17:12',
                'updated_at' => '2018-11-19 13:17:12',
                'deleted_at' => NULL,
            ),
        ));
    }
}
