<?php

use Illuminate\Database\Seeder;

class BindingsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {


        \DB::table('bindings')->delete();

        \DB::table('bindings')->insert(array(
            0 =>
            array(
                'id' => 1,
                'name' => 'LT10
',
                'created_at' => '2018-11-13 11:00:00',
                'updated_at' => '2018-11-13 11:00:00',
            ),
            1 =>
            array(
                'id' => 2,
                'name' => 'SpeedFit',
                'created_at' => '2018-11-13 11:00:00',
                'updated_at' => '2018-11-13 11:00:00',
            ),
            2 =>
            array(
                'id' => 3,
                'name' => 'Z13',
                'created_at' => '2018-11-13 11:00:00',
                'updated_at' => '2018-11-13 11:00:00',
            ),
            3 =>
            array(
                'id' => 4,
                'name' => 'XTR 10 Pro',
                'created_at' => '2018-11-13 11:00:00',
                'updated_at' => '2018-11-13 11:00:00',
            ),
            4 =>
            array(
                'id' => 5,
                'name' => 'Attack 16
',
                'created_at' => '2018-11-13 11:00:00',
                'updated_at' => '2018-11-13 11:00:00',
            ),
            5 =>
            array(
                'id' => 6,
                'name' => 'EL10',
                'created_at' => '2018-11-13 11:00:00',
                'updated_at' => '2018-11-13 11:00:00',
            ),
            6 =>
            array(
                'id' => 7,
                'name' => 'RS10',
                'created_at' => '2018-12-02 08:32:12',
                'updated_at' => '2018-12-02 08:32:12',
            ),
            7 =>
            array(
                'id' => 8,
                'name' => 'EZY7',
                'created_at' => '2018-12-02 08:32:35',
                'updated_at' => '2018-12-02 08:32:35',
            ),
            8 =>
            array(
                'id' => 9,
                'name' => 'FJ7 AC JR',
                'created_at' => '2018-12-02 08:32:35',
                'updated_at' => '2018-12-02 08:32:35',
            ),
            9 =>
            array(
                'id' => 10,
                'name' => 'XTE7',
                'created_at' => '2018-12-02 08:32:35',
                'updated_at' => '2018-12-02 08:32:35',
            ),
        ));
    }
}
