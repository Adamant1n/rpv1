<?php

use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('permissions')->delete();
        
        \DB::table('permissions')->insert(array (
            0 => 
            array (
                'id' => 2,
                'name' => 'manage-roles',
                'guard_name' => 'web',
                'display' => 'Управление ролями',
                'created_at' => '2019-01-31 00:43:56',
                'updated_at' => '2019-01-31 00:43:56',
            ),
            1 => 
            array (
                'id' => 3,
                'name' => 'manage-orders',
                'guard_name' => 'web',
                'display' => 'Управление заказами',
                'created_at' => '2019-01-31 00:48:51',
                'updated_at' => '2019-01-31 00:50:24',
            ),
            2 => 
            array (
                'id' => 4,
                'name' => 'manage-location-orders',
                'guard_name' => 'web',
                'display' => 'Управление заказами филиала',
                'created_at' => '2019-01-31 00:49:06',
                'updated_at' => '2019-01-31 00:50:14',
            ),
            3 => 
            array (
                'id' => 5,
                'name' => 'create-orders',
                'guard_name' => 'web',
                'display' => 'Создание заказов',
                'created_at' => '2019-01-31 00:49:20',
                'updated_at' => '2019-01-31 00:49:20',
            ),
            4 => 
            array (
                'id' => 6,
                'name' => 'manage-clients',
                'guard_name' => 'web',
                'display' => 'Управление клиентами',
                'created_at' => '2019-01-31 00:49:33',
                'updated_at' => '2019-01-31 00:49:33',
            ),
            5 => 
            array (
                'id' => 7,
                'name' => 'manage-stock',
                'guard_name' => 'web',
                'display' => 'Управление складом',
                'created_at' => '2019-01-31 00:49:53',
                'updated_at' => '2019-01-31 00:49:53',
            ),
            6 => 
            array (
                'id' => 8,
                'name' => 'manage-location-stock',
                'guard_name' => 'web',
                'display' => 'Управление складом филиала',
                'created_at' => '2019-01-31 00:50:07',
                'updated_at' => '2019-01-31 00:50:07',
            ),
            7 => 
            array (
                'id' => 9,
                'name' => 'view-reports',
                'guard_name' => 'web',
                'display' => 'Просмотр отчетов',
                'created_at' => '2019-01-31 00:50:55',
                'updated_at' => '2019-01-31 00:50:55',
            ),
            8 => 
            array (
                'id' => 10,
                'name' => 'manage-locations',
                'guard_name' => 'web',
                'display' => 'Управление филиалами',
                'created_at' => '2019-01-31 00:51:11',
                'updated_at' => '2019-01-31 00:51:11',
            ),
            9 => 
            array (
                'id' => 11,
                'name' => 'manage-pricelists',
                'guard_name' => 'web',
                'display' => 'Управление прейскурантами',
                'created_at' => '2019-01-31 00:51:23',
                'updated_at' => '2019-01-31 00:51:23',
            ),
            10 => 
            array (
                'id' => 12,
                'name' => 'manage-users',
                'guard_name' => 'web',
                'display' => 'Управление пользователями',
                'created_at' => '2019-01-31 00:51:35',
                'updated_at' => '2019-01-31 00:51:35',
            ),
            11 => 
            array (
                'id' => 13,
                'name' => 'manage-agents',
                'guard_name' => 'web',
                'display' => 'Управление контрагентами',
                'created_at' => '2019-01-31 00:51:51',
                'updated_at' => '2019-01-31 00:51:51',
            ),
            12 => 
            array (
                'id' => 14,
                'name' => 'manage-properties',
                'guard_name' => 'web',
                'display' => 'Управление характеристиками товаров',
                'created_at' => '2019-01-31 00:52:07',
                'updated_at' => '2019-01-31 00:52:07',
            ),
            13 => 
            array (
                'id' => 15,
                'name' => 'manage-settings',
                'guard_name' => 'web',
                'display' => 'Настройки ПО',
                'created_at' => '2019-01-31 00:52:22',
                'updated_at' => '2019-01-31 00:52:22',
            ),
            14 => 
            array (
                'id' => 16,
                'name' => 'take-payment',
                'guard_name' => 'web',
                'display' => 'Прием оплаты',
                'created_at' => '2019-01-31 00:52:31',
                'updated_at' => '2019-01-31 00:52:31',
            ),
            15 => 
            array (
                'id' => 17,
                'name' => 'edit-reservation-price',
                'guard_name' => 'web',
                'display' => 'Редактирование стоимости позиции в договоре',
                'created_at' => '2019-02-28 15:20:44',
                'updated_at' => '2019-02-28 15:20:44',
            ),
            16 => 
            array (
                'id' => 18,
                'name' => 'edit-pledge-amount',
                'guard_name' => 'web',
                'display' => 'Изменение размера залога',
                'created_at' => '2019-07-15 16:55:03',
                'updated_at' => '2019-07-15 16:55:03',
            ),
            17 => 
            array (
                'id' => 19,
                'name' => 'access-dashboard',
                'guard_name' => 'web',
                'display' => 'Доступ к дешборду филиала',
                'created_at' => '2019-07-15 17:22:33',
                'updated_at' => '2019-07-15 17:22:33',
            ),
            18 => 
            array (
                'id' => 20,
                'name' => 'access-analytics',
                'guard_name' => 'web',
                'display' => 'Доступ к общей аналитике',
                'created_at' => '2019-07-15 17:22:48',
                'updated_at' => '2019-07-15 17:22:48',
            ),
            19 => 
            array (
                'id' => 21,
                'name' => 'cancel-order',
                'guard_name' => 'web',
                'display' => 'Отмена заказа',
                'created_at' => '2019-07-18 00:48:29',
                'updated_at' => '2019-07-18 00:48:29',
            ),
        ));
        
        
    }
}