<?php

use Illuminate\Database\Seeder;

class ConditionsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('conditions')->delete();
        
        \DB::table('conditions')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'ОК',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Сломан',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'Утерян',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
        ));
        
        
    }
}