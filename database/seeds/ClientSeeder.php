<?php

use Illuminate\Database\Seeder;

class ClientSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
	{
	    factory(App\Client::class, 50)->create()->each(function ($client) {
	        $client->feature()->save(factory(App\ClientFeature::class)->make());
	    });
	}
}
