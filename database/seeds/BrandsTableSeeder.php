<?php

use Illuminate\Database\Seeder;

class BrandsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('brands')->delete();
        
        \DB::table('brands')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Nike',
                'created_at' => '2018-10-08 23:14:28',
                'updated_at' => '2018-10-08 23:14:28',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'ATOMIC',
                'created_at' => '2018-10-17 21:52:56',
                'updated_at' => '2018-10-17 21:52:56',
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'TOP SPORT',
                'created_at' => '2018-10-17 21:52:56',
                'updated_at' => '2018-10-17 21:52:56',
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'VIST',
                'created_at' => '2018-10-17 21:52:56',
                'updated_at' => '2018-10-17 21:52:56',
            ),
            4 => 
            array (
                'id' => 5,
                'name' => 'SALOMON',
                'created_at' => '2018-10-17 21:52:56',
                'updated_at' => '2018-10-17 21:52:56',
            ),
            5 => 
            array (
                'id' => 6,
                'name' => 'FISCHER',
                'created_at' => '2018-10-17 21:52:56',
                'updated_at' => '2018-10-17 21:52:56',
            ),
            6 => 
            array (
                'id' => 7,
                'name' => 'ROSSIGNOL',
                'created_at' => '2018-10-17 21:52:57',
                'updated_at' => '2018-10-17 21:52:57',
            ),
            7 => 
            array (
                'id' => 8,
                'name' => 'LAMAR',
                'created_at' => '2018-10-17 21:52:57',
                'updated_at' => '2018-10-17 21:52:57',
            ),
            8 => 
            array (
                'id' => 9,
                'name' => 'SIMS',
                'created_at' => '2018-10-17 21:52:57',
                'updated_at' => '2018-10-17 21:52:57',
            ),
            9 => 
            array (
                'id' => 10,
                'name' => 'NIDECKER',
                'created_at' => '2018-10-17 21:52:57',
                'updated_at' => '2018-10-17 21:52:57',
            ),
            10 => 
            array (
                'id' => 11,
                'name' => 'BUZRUN',
                'created_at' => '2018-10-17 21:52:57',
                'updated_at' => '2018-10-17 21:52:57',
            ),
            11 => 
            array (
                'id' => 12,
                'name' => 'SNOWJAM',
                'created_at' => '2018-10-17 21:52:57',
                'updated_at' => '2018-10-17 21:52:57',
            ),
            12 => 
            array (
                'id' => 13,
                'name' => '540',
                'created_at' => '2018-10-17 21:52:57',
                'updated_at' => '2018-10-17 21:52:57',
            ),
            13 => 
            array (
                'id' => 14,
                'name' => 'HASEGAWA',
                'created_at' => '2018-10-17 21:52:57',
                'updated_at' => '2018-10-17 21:52:57',
            ),
            14 => 
            array (
                'id' => 15,
                'name' => 'VIVE',
                'created_at' => '2018-10-17 21:52:57',
                'updated_at' => '2018-10-17 21:52:57',
            ),
            15 => 
            array (
                'id' => 16,
                'name' => 'HEAD',
                'created_at' => '2018-10-17 21:52:57',
                'updated_at' => '2018-10-17 21:52:57',
            ),
            16 => 
            array (
                'id' => 17,
                'name' => 'NITRO',
                'created_at' => '2018-10-17 21:52:58',
                'updated_at' => '2018-10-17 21:52:58',
            ),
            17 => 
            array (
                'id' => 18,
                'name' => 'Casco',
                'created_at' => '2018-11-06 22:22:36',
                'updated_at' => '2018-11-06 22:22:36',
            ),
            18 => 
            array (
                'id' => 19,
                'name' => 'Alpina',
                'created_at' => '2018-11-06 22:23:09',
                'updated_at' => '2018-11-06 22:23:09',
            ),
            19 => 
            array (
                'id' => 20,
                'name' => 'Scott',
                'created_at' => '2018-11-20 20:51:02',
                'updated_at' => '2018-11-20 20:51:02',
            ),
        ));
        
        
    }
}