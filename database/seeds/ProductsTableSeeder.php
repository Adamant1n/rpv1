<?php

use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('products')->delete();
        
        \DB::table('products')->insert(array (
            0 => 
            array (
                'id' => 1,
                'category_id' => 1,
                'brand_id' => 5,
                'name' => 'X-Pro R',
                'photo' => 'product_1542616899_xmhwcmkz.jpeg',
                'for_sale' => 0,
                'price_buy' => '100.00',
                'price_sell' => '200.00',
                'created_at' => '2018-11-12 21:58:19',
                'updated_at' => '2018-11-19 11:41:39',
            ),
            1 => 
            array (
                'id' => 2,
                'category_id' => 1,
                'brand_id' => 6,
                'name' => 'XTR Viron',
                'photo' => NULL,
                'for_sale' => 0,
                'price_buy' => '100.00',
                'price_sell' => '200.00',
                'created_at' => '2018-11-12 21:58:19',
                'updated_at' => '2018-11-12 21:58:19',
            ),
            2 => 
            array (
                'id' => 3,
                'category_id' => 2,
                'brand_id' => 6,
                'name' => 'Viron XTR9',
                'photo' => NULL,
                'for_sale' => 0,
                'price_buy' => '100.00',
                'price_sell' => '200.00',
                'created_at' => '2018-11-12 21:58:19',
                'updated_at' => '2018-11-12 21:58:19',
            ),
            3 => 
            array (
                'id' => 4,
                'category_id' => 2,
                'brand_id' => 6,
                'name' => 'RS 60 JR',
                'photo' => NULL,
                'for_sale' => 0,
                'price_buy' => '100.00',
                'price_sell' => '200.00',
                'created_at' => '2018-11-12 21:58:19',
                'updated_at' => '2018-11-12 21:58:19',
            ),
            4 => 
            array (
                'id' => 5,
                'category_id' => 3,
                'brand_id' => 18,
                'name' => 'CX-3-Icecube',
                'photo' => NULL,
                'for_sale' => 0,
                'price_buy' => '100.00',
                'price_sell' => '200.00',
                'created_at' => '2018-11-12 21:58:19',
                'updated_at' => '2018-11-12 21:58:19',
            ),
            5 => 
            array (
                'id' => 6,
                'category_id' => 3,
                'brand_id' => 19,
                'name' => 'GRAP 2.0 JR',
                'photo' => NULL,
                'for_sale' => 0,
                'price_buy' => '100.00',
                'price_sell' => '200.00',
                'created_at' => '2018-11-12 21:58:19',
                'updated_at' => '2018-11-12 21:58:19',
            ),
            6 => 
            array (
                'id' => 7,
                'category_id' => 5,
                'brand_id' => 8,
                'name' => 'Dandelion',
                'photo' => NULL,
                'for_sale' => 0,
                'price_buy' => '100.00',
                'price_sell' => '200.00',
                'created_at' => '2018-11-12 21:58:19',
                'updated_at' => '2018-11-12 21:58:19',
            ),
            7 => 
            array (
                'id' => 8,
                'category_id' => 7,
                'brand_id' => 1,
                'name' => 'Палки алюминиевые',
                'photo' => 'product_1542616968_ylypwelz.jpeg',
                'for_sale' => 0,
                'price_buy' => '0.00',
                'price_sell' => '0.00',
                'created_at' => '2018-11-18 17:52:50',
                'updated_at' => '2018-11-19 11:42:49',
            ),
            8 => 
            array (
                'id' => 9,
                'category_id' => 6,
                'brand_id' => 20,
                'name' => 'Шапка Scott',
                'photo' => NULL,
                'for_sale' => 1,
                'price_buy' => '150.00',
                'price_sell' => '250.00',
                'created_at' => '2018-11-20 09:51:24',
                'updated_at' => '2018-11-20 09:51:24',
            ),
        ));
        
        
    }
}