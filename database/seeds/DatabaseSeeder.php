<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(EventCategoriesTableSeeder::class);
        // $this->call(CategoriesTableSeeder::class);
        $this->call(LocationsTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(RolesTableSeeder::class);
        $this->call(ModelHasRolesTableSeeder::class);
        // $this->call(ClientSeeder::class);
        // $this->call(AgentsTableSeeder::class);
        // $this->call(BindingsTableSeeder::class);
        // $this->call(BrandsTableSeeder::class);

        $this->call(ConditionsTableSeeder::class);
        // $this->call(ExpiredPassportsTableSeeder::class);
        $this->call(PriceListsTableSeeder::class);
        // $this->call(ProductsTableSeeder::class);
        // $this->call(UnitsTableSeeder::class);
        $this->call(PermissionsTableSeeder::class);
        $this->call(RoleHasPermissionsTableSeeder::class);
    }
}
