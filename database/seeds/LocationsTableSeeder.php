<?php

use Illuminate\Database\Seeder;

class LocationsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {


        \DB::table('locations')->delete();

        \DB::table('locations')->insert(array(
            0 =>
            array(
                'id' => 1,
                'type' => 'both',
                'address' => 'Филиал #1',
                'legal_name' => 'legal name',
                'legal_address' => 'legal address',
                'legal_phone' => 'phone',
                //'price_weekdays' => 1,
                //'price_weekends' => 2,
                'tariff_step' => '1d',
                'hours_max' => 6,
                'free_time' => 10,
                'opens_at' => '12:00',
                'closes_at' => '19:00',
                'next_day_at' => '23:00',
                'return_by' => '10:20',
                'weekends' => '[6,7]',
                'day_is' => 'opened',
                'all_day' => 0,
                'active' => 1,
                'kkt' => 1,
                'created_at' => '2018-11-13 08:41:50',
                'updated_at' => '2019-09-18 19:59:52',
            ),
        ));
    }
}
