<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        // \DB::table('users')->delete();

        \DB::table('users')->insert(array(
            0 =>
            array(
                'id' => 1,
                'atol_id' => '123654789507',
                'atol_name' => NULL,
                'category_id' => NULL,
                'rate' => NULL,
                'location_id' => 1,
                'first_name' => 'Super',
                'last_name' => 'Admin',
                'mid_name' => '111',
                'barcode' => '1156',
                'phone' => NULL,
                'email' => 'superadmin@test.com',
                'email_verified_at' => NULL,
                'password' => '$2y$10$Q5Ck0Nw5ptq2ntC4iNBk9ee2D98Yci.RrWiFZiaR6VtZZBLw/EDEq',
                'remember_token' => 'jy3jZ3QJna2IE6JbYxEKT6xroPxESbQFsmH5SogR6BjbNh0ZEuZXVnnwmW1F',
                'created_at' => '2018-11-11 19:58:15',
                'updated_at' => '2019-11-20 21:33:39',
                'deleted_at' => NULL,
                'session_id' => NULL,
                'session_updated_at' => '2019-11-20 21:33:39',
            ),
        ));
    }
}
