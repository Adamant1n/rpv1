@extends('layouts.app')

@section('content')

<div class="row">
<div class="col-12">
	<div class="card">
		<div class="card-header">
			<a href="{{ route('orders.show', $order->id) }}" class="btn btn-light">
				<i class="fa fa-angle-left"></i>Назад</a>
		</div>
		<div class="card-body">
			<h4 class="card-title">Принять заказ {{ $order->optional_id }}</h4>

			<input class="form-control col-md-6 mt-3 mb-3" type="text" id="search"
				placeholder="Введите артикул принимаемого товара" autofocus>

			<form method="post" action="{{ route('orders.update', $order->id) }}">
			{{ method_field('PATCH') }}
			@csrf
				<table id="units-listing" class="table">
					<thead>
						<tr>
							<th>Модель</th>
							<th>Бренд</th>
							<th>Категория</th>
							<th>Состояние</th>
							<th>Размер</th>
							<th>Принято</th>
						</tr>
					</thead>
					<tbody>

					@foreach ($order->units as $unit)
					<tr data-code="{{ $unit->code }}">
						<td>{{ $unit->product->name }}</td>
						<td>{{ $unit->product->brand->name }}</td>
						<td>{{ $unit->product->category->name }}</td>
						<td>{{ $unit->condition->name }}</td>
						<td>{{ $unit->size }}</td>
						<td><input type="checkbox" name="units[]"
							value="{{ $unit->id }}" onclick="return false" required></td>
					</tr>
					@endforeach

					</tbody>
				</table>

				<button type="submit" class="btn btn-success mt-4">
					<i class="fa fa-check-circle"></i>Завершить заказ</button>

			</form>

		</div>
	</div>
</div>
</div>

<script>
(function($) {
	'use strict';
	$(function() {
		$('#units-listing').DataTable({
			"paging": false,
			"info": false,
			"searching": false,
			"iDisplayLength": 10,
			"aaSorting": [[0, "asc"]],
			"language": {
				"lengthMenu": "Показывать _MENU_ записей на странице",
				"zeroRecords": "Записей не найдено",
				"loadingRecords": "Загрузка... может занять несколько секунд...",
				"info": "Страница _PAGE_ из _PAGES_",
				"infoEmpty": "Показано с 0 по 0 из 0 записей",
				"search": "",
				"infoFiltered": "(Найдено записей: _TOTAL_)",
				"sInfo": "Показано с _START_ по _END_ из _TOTAL_ записей",
				"paginate":
				{
					"first":    "Первая",
					"last":     "Последняя",
					"next":     "Следующая",
					"previous": "Предыдущая"
				}
			}
		});
		$('#units-listing').each(function() {
			var datatable = $(this);
			var search_input = datatable.closest('.dataTables_wrapper').find('div[id$=_filter] input');
			search_input.attr('placeholder', 'Поиск');
			search_input.removeClass('form-control-sm');
			var length_sel = datatable.closest('.dataTables_wrapper').find('div[id$=_length] select');
			length_sel.removeClass('form-control-sm');
		});
		$('#search').keyup(function() {
			$('#units-listing > tbody > tr').each(function() {
				if ($(this).data('code') == $('#search').val())
				{
					$('td:last input', this).prop('checked', true);
					$('#search').val('');
				}
			});
		});
	});
})(jQuery);
</script>

@endsection
