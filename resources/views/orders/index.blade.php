@extends('layouts.app')

@section('content')

@include('common.validation')
@include('common.notifications')

<div class="card">

	<div class="card-header">
		<!--
		<a href="/orders/create" class="btn btn-success pull-left mr-3">
			<i class="fa fa-shopping-cart"></i>Новый заказ</a>
		-->
		<form>
            <input type="hidden" name="type" value="{{ Input::get('type') }}" />
			@role('superadmin')
			<select class="form-control select2 col-md-6 col-lg-3 mb-2" name="location"
			onchange="this.form.submit()">
				<option value="">Все филиалы</option>
				@foreach ($locations as $location)
				<option value="{{ $location->id }}"
				{{ $location->id == $request->location ? 'selected' : '' }}>
				{{ $location->address }}</option>
				@endforeach
			</select>
			@endrole
			<label class="col-lg-3">
				<input type="checkbox" name="ended" onchange="this.form.submit()" {{ $request->has('ended') ? 'checked' : '' }}>
				Завершенные
			</label>
		</form>
	</div>

	<div class="card-body">
		<h4 class="card-title">Заказы</h4>
		<div class="table-responsive">
			<table id="orders-listing" class="table">
			<thead>
				<tr>
					<th>ID</th>
					<th>Клиент</th>
					<th>Длительность</th>
					<th>К оплате</th>
					@if(request('type') === 'delivery')
					<th>Адрес</th>
					@endif
					<th>Статус</th>
					<th>Начало</th>
					@if(!request('type', false))
					<th>Окончание</th>
					@endif
				</tr>
			</thead>
			<tbody>

			@php $d = ['m'=>'м.','h'=>'ч.','d'=>'д.'] @endphp

			@foreach ($orders as $order)

			<tr>
				<td>{{ $order->optional_id }}</td>
				<td>{{ $order->client->full_name }}</td>
				<td>{{ $order->duration.' '.$d[$order->dimension] }}</td>
				<td>
					{{ $order->price_extra > 0 ? $order->price_extra : ($order->status == 'paid' ? 0 : $order->total_price) }}
					<i class="fas fa-ruble-sign"></i>
				</td>
				@if(request('type') === 'delivery')
				<td>
					{{ $order->address }}
				</td>
				@endif
				<td>
					{!! $order->status_badge !!}
					@if($order->group_id)
					<span class="badge badge-primary">групповой</span>
					@endif
				</td>
				<td>
					@if($order->type === \App\Order::TYPE_BOOK)
					{{ optional($order->book_at)->format('d.m.Y H:i') }}
					@else
					{{ optional($order->starts_at)->format('d.m.Y H:i') }}
					@endif
				</td>
				@if(!request('type', false))
				<td>
					{{ optional($order->ends_at)->format('d.m.Y H:i') }}
				</td>
				@endif
			</tr>
			@endforeach

			</tbody>
			</table>
	</div>
	</div>
</div>

@push('js')
<script>
(function($) {
	'use strict';
	$(function() {
		var table = $('#orders-listing').DataTable({
			"iDisplayLength": 10,
			"aaSorting": [[0, "asc"]],
			"language": {
				"lengthMenu": "Показывать _MENU_ записей на странице",
				"zeroRecords": "Записей не найдено",
				"loadingRecords": "Загрузка... может занять несколько секунд...",
				"info": "Страница _PAGE_ из _PAGES_",
				"infoEmpty": "Показано с 0 по 0 из 0 записей",
				"search": "",
				"infoFiltered": "(Найдено записей: _TOTAL_)",
				"sInfo": "Показано с _START_ по _END_ из _TOTAL_ записей",
				"paginate":
				{
					"first":    "Первая",
					"last":     "Последняя",
					"next":     "Следующая",
					"previous": "Предыдущая"
				}
			},
			"pageLength": {{ Request::get('length') ? Request::get('length') : 10 }},
			initComplete: function () {
      			setTimeout(function () {
        				table.page({{ Request::get('page') ? Request::get('page') - 1 : 0 }})
							.draw(false);
      			}, 10);
    		},
			"drawCallback": function( settings ) {
				var page = Math.ceil(settings._iDisplayStart / settings._iDisplayLength) + 1;

				updateUrlParameter('page', page);
				updateUrlParameter('length', settings._iDisplayLength);
			}
		});
		$('#orders-listing').each(function() {
			var datatable = $(this);
			var search_input = datatable.closest('.dataTables_wrapper').find('div[id$=_filter] input');
			search_input.attr('placeholder', 'Поиск');
			search_input.removeClass('form-control-sm');
			var length_sel = datatable.closest('.dataTables_wrapper').find('div[id$=_length] select');
			length_sel.removeClass('form-control-sm');
		});
		/*
		$('#orders-listing tbody').on('click', 'tr', function () {
			var id = $('td', this).eq(0).text();
			// $(location).attr('href','/orders/'+parseInt(id.replace(/[^-0-9]/gim, '')));
			getOrder(parseInt(id.replace(/[^-0-9]/gim, '')));
		});
		*/
	});
	$(".select2").select2();
})(jQuery);
</script>
@endpush

@endsection
