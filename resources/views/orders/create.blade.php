@extends('layouts.app')

@push('js')
<script type="text/javascript">
	$(document).ready(function () {
		var table = $('#units-listing').DataTable({
			"iDisplayLength": 10,
			"aaSorting": [[0, "asc"]],
			"info": false,
			"lengthChange": false,
			"language": {
				"lengthMenu": "Показывать _MENU_ записей на странице",
				"zeroRecords": "Записей не найдено",
				"loadingRecords": "Загрузка... может занять несколько секунд...",
				"info": "Страница _PAGE_ из _PAGES_",
				"infoEmpty": "Показано с 0 по 0 из 0 записей",
				"search": "",
				"infoFiltered": "(Найдено записей: _TOTAL_)",
				"sInfo": "Показано с _START_ по _END_ из _TOTAL_ записей",
				"paginate":
				{
					"first":    "Первая",
					"last":     "Последняя",
					"next":     "Вперед",
					"previous": "Назад"
				}
			}
		});

		var tariffs = {!! $tariffs !!};
		var units = {!! $product_units !!};
		var selUnits = [];
		var productUnits = [];
		var dim = new Map();
		for (var i = 0; i < Object.keys(units).length; i++) {
			productUnits[units[i].id] = units[i];
		}

		$('#units-listing').each(function() {
			var datatable = $(this);
			var search_input = datatable.closest('.dataTables_wrapper').find('div[id$=_filter] input');
			search_input.attr('placeholder', 'Поиск');
			search_input.removeClass('form-control-sm');
			var length_sel = datatable.closest('.dataTables_wrapper').find('div[id$=_length] select');
			length_sel.removeClass('form-control-sm');
		});

		function addUnit(unitId) {
			console.log('adding');
			if ($.inArray(unitId, selUnits) == -1) {
				$('#orderTable > tbody:last').append(
					'<tr><td><input type="hidden" name="order_units[]" value="'+unitId+'">'
					+productUnits[unitId].product.brand['name']+' '+productUnits[unitId].product['name']
					+'</td><td>'+productUnits[unitId].condition['name']+'</td><td>'+productUnits[unitId].size
					+'</td><td class="unit-price" data-category="'+productUnits[unitId].product['category_id']
					+'"></td><td><i class="fa fa-trash-o del-unit" data-id="'+unitId+'"></i></td></tr>'
				);
				selUnits.push(unitId);
				updatePrices();
				setOrderPrice();
			}
		}

		$('#units-listing tbody').on('click', 'tr', function () {
			if ($('#priceList').val())
			{
				var unitId = $(this).data('id');
				console.log(unitId);
				if ($.inArray(unitId, selUnits) == -1) {
					$('#orderTable > tbody:last').append(
						'<tr><td><input type="hidden" name="order_units[]" value="'+unitId+'">'
						+productUnits[unitId].product.brand['name']+' '+productUnits[unitId].product['name']
						+'</td><td>'+productUnits[unitId].condition['name']+'</td><td>'+productUnits[unitId].size
						+'</td><td class="unit-price" data-category="'+productUnits[unitId].product['category_id']
						+'"></td><td><i class="fa fa-trash-o del-unit" data-id="'+unitId+'"></i></td></tr>'
					);
					selUnits.push(unitId);
					updatePrices();
					setOrderPrice();
				}
			}
			else alert('Установите прейскурант');
		});

		$('#orderTable').on("click", '.del-unit', function() {
			selUnits.splice(selUnits.indexOf($(this).data('id')), 1);
			$($(this).parent()).parent().remove();
			setOrderPrice();
		});

		$('#duration').change(function() {
			updatePrices();
			setOrderPrice();
		});

		$('#agent').change(function() {
			setOrderPrice();
		});

		function updatePrices()
		{
			$('.unit-price').each(function() {
				$(this).html(0);
				for (var i = 0; i < tariffs.length; i++) {
					if ((tariffs[i].category_id == $(this).data('category'))
						&& (tariffs[i].price_list_id == $('#priceList option:selected').val())
						&& (tariffs[i].duration == $('#duration option:selected').data('duration'))
						&& (tariffs[i].dimension == $('#duration option:selected').data('dimension')))
					{
						$(this).html(tariffs[i].price);
						break;
					}
				}
			});
		}

		function setOrderPrice()
		{
			var orderPrice = 0;
			$('.unit-price').each(function(index, value) {
				orderPrice += parseInt(value.innerText);
			});
			if ($('#agent option:selected').data('discount'))
				orderPrice = orderPrice - (orderPrice / 100 * $('#agent option:selected').data('discount'));
			$('#orderPrice').html(orderPrice);
		}

		$('.select2').select2();
		$('.date').datepicker({
			'autoClose': true,
			'dateFormat': 'yyyy-mm-dd',
			'timepicker': true,
			'position': 'top left',
			'minutesStep': 10
		});

		// обработка сканера штрих-кодов
		$(".dataTables_filter").on("keydown", function(event) {
	     	if(event.which == 13) {
	     		units.forEach(function (unit) {
	     			if(unit.code == $(".dataTables_filter :input").val()) {
	     				console.log(unit.code, $(".dataTables_filter :input").val());
	     				addUnit(unit.id);
	     				$(".dataTables_filter :input").val('');
	     				return false;
	     			}
	     		});
	     		// $(this).val();
	     		// addUnit($(this).val());
	     		//if(table.rows( { filter : 'applied'} ).data().length > 0) {
	     			//console.log(table.('tr', { search : 'applied'} ).data());
	     			//addUnit(table('tr', { search : 'applied'} ).data()[0].id);
	     		//}
	     		// console.log();        
	     	}
	    });
});
</script>
@endpush

@section('content')

<!-- данные заказа -->
<div class="row">
	<!-- карточка пользователя -->
	<div class="col-6 grid-margin stretch-card">
		@if ($errors->any())
		<div class="alert alert-danger" role="alert">
			<ul>
				@foreach ($errors->all() as $error)
					<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div>
		@endif

		<div class="card">
			<div class="card-header">
				<h4 class="card-title pull-left">Клиент</h4>

				<a href="{{ route('orders.index') }}" class="btn ml-3 btn-right btn-xs btn-light">
					<i class="fa fa-angle-left"></i>Назад
				</a>

				@if ($request->has('client_id'))
				<a href="{{ route('clients.edit', $client) }}" class="btn btn-info btn-xs pull-right btn-right" target="_blank">Карта клиента</a>
				@endif
			</div>
			<div class="card-body">
				@if ($request->has('client_id'))
				<table class="table table-bordered">
					<tbody>
						<tr>
							<td>Имя</td>
							<td>{{ $client->full_name }}</td>
						</tr>
						<tr>
							<td>Телефон</td>
							<td>{{ $client->phone }}</td>
						</tr>
						<tr>
							<td>Заметка</td>
							<td>{{ $client->description }}</td>
						</tr>
					</tbody>
				</table>
				<table class="table table-bordered mt-3">
					<tbody>
						<tr>
							<td><strong>Рост</strong></td>
							<td><strong>Вес</strong></td>
							<td><strong>Размер</strong></td>
							<td class="text-warning"><strong>Z-число</strong></td>
						</tr>
						<tr>
							<td>{{ $client->feature->height }}</td>
							<td>{{ $client->feature->weight }}</td>
							<td>{{ $client->feature->shoe_size }}</td>
							<td>{{ $client->z_num }}</td>
						</tr>
					</tbody>
				</table>
				@else
				<form>
					<select class="form-control select2" name="client_id"
					onchange="this.form.submit()">
						<option value="">Сделайте выбор...</option>
						@foreach ($clients as $client)
						<option value="{{ $client->id }}">{{ $client->full_name }}</option>
						@endforeach
					</select>
				</form>
				@endif
			</div>
		</div>
	</div>
	<!-- карточка заказа -->
	<div class="col-6 grid-margin stretch-card">
		<div class="card">
			<div class="card-body">
			<h4 class="card-title">Заказ</h4>

			@role('superadmin')
			<div class="row">
				<div class="col-md-12">
					<div class="form-group">
						<form>
						<input type="hidden" name="client_id" value="{{ $request->client_id }}">
						<select class="form-control select2" name="location_id" onchange="this.form.submit()">
							<option value="">Филиал...</option>
							@foreach ($locations as $location)
							@if($location->id !== \App\Location::MAIN)
							<option value="{{ $location->id }}"
							{{ $location->id == $request->location_id ? 'selected' : '' }}>
							{{ $location->address }}</option>
							@endif
							@endforeach
						</select>
						</form>
					</div>
				</div>
			</div>
			@endrole

			<form method="post" action="{{ route('orders.store') }}">
				@csrf
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<select class="form-control select2" id="agent" name="agent_id">
								<option value="">Агент...</option>
								@foreach ($agents as $agent)
								<option value="{{ $agent->id }}" data-discount="{{ $agent->discount }}">
									{{ $agent->name }} ({{ $agent->discount }}%)</option>
								@endforeach
							</select>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<input type="text" name="start_date" class="form-control date" placeholder="Время" required>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<select class="form-control select2" name="price_list" id="priceList" required>
								<option value="">Прейскурант</option>
								@foreach ($price_lists as $price_list)
								<option value="{{ $price_list->id }}">
								{{ $price_list->name }}</option>
								@endforeach
							</select>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<select class="form-control select2" name="duration" id="duration" required>
								<option value="10,m" data-duration="10" data-dimension="m">10 мин</option>
								<option value="20,m" data-duration="20" data-dimension="m">20 мин</option>
								<option value="30,m" data-duration="30" data-dimension="m">30 мин</option>
								<option value="1,h" data-duration="1" data-dimension="h" selected>1 час</option>
								<option value="2,h" data-duration="2" data-dimension="h">2 часа</option>
								<option value="3,h" data-duration="3" data-dimension="h">3 часа</option>
								<option value="4,h" data-duration="4" data-dimension="h">4 часа</option>
								<option value="5,h" data-duration="5" data-dimension="h">5 часов</option>
								<option value="6,h" data-duration="6" data-dimension="h">6 часов</option>
								<option value="1,d" data-duration="1" data-dimension="d">1 день</option>
								<option value="2,d" data-duration="2" data-dimension="d">2 дня</option>
								<option value="3,d" data-duration="3" data-dimension="d">3 дня</option>
								<option value="4,d" data-duration="4" data-dimension="d">4 дня</option>
								<option value="5,d" data-duration="5" data-dimension="d">5 дней</option>
								<option value="6,d" data-duration="6" data-dimension="d">6 дней</option>
								<option value="7,d" data-duration="7" data-dimension="d">7 дней</option>
							</select>
						</div>
					</div>
				</div>

				<table id="orderTable" class="table mt-2">
					<thead>
						<tr>
							<th>Наименование</th>
							<th>Состояние</th>
							<th>Размер</th>
							<th>Цена</th>
							<th></th>
						</tr>
					</thead>
					<tbody>
					</tbody>
				</table>

				<div class="row mt-4">
					<div class="col-md-6">
						<input type="hidden" name="client_id" value="{{ $request->client_id }}">
						<button type="submit" class="btn btn-success">
							<i class="fa fa-shopping-cart"></i>Создать заказ</button>
					</div>
					<div class="col-md-6">
						Сумма заказа: <span id="orderPrice">0</span> руб.
					</div>
				</div>

			</form>

			</div>
		</div>
	</div>
</div>

<!-- номенклатура -->
<div class="row">
	<div class="col-12 grid-margin stretch-card">
		<div class="card">
			<div class="card-body">
				<h4 class="card-title">Подбор номенклатуры</h4>
				<div class="row">
					<div class="col-12">
					<table id="units-listing" class="table">
						<thead>
							<tr>
								<th>Артикул</th>
								<th>Модель</th>
								<th>Бренд</th>
								<th>Состояние</th>
								@foreach(\App\ProductCategory::getAvailableParams() as $param_id => $param_name)
								<th>{{ $param_name }}</th>
								@endforeach
							</tr>
						</thead>
						<tbody>
						@foreach ($product_units as $product_unit)
						<tr data-id="{{ $product_unit->id }}">
							<td>{{ $product_unit->code }}</td>
							<td>{{ $product_unit->product->name }}</td>
							<td>{{ $product_unit->product->brand->name }}</td>
							<td>{{ $product_unit->condition->name }}</td>
							@foreach(\App\ProductCategory::getAvailableParams() as $param_id => $param_name)
								@if($param_id === \App\ProductCategory::PARAM_BINDING)
								<td>{{ optional($product_unit->binding)->name }}</td>
								@else
								<td>{{ $product_unit->{'feature_' . $param_id} }}</td>
								@endif
							@endforeach
						</tr>
						@endforeach
						</tbody>
					</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection