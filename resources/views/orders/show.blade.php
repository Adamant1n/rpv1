@extends('layouts.app')

@section('content')

<div class="row">
<div class="col-12">
	<div class="card">
		<div class="card-header">
			<a href="{{ route('orders.index') }}" class="btn btn-light mr-2">
				<i class="fa fa-angle-left"></i>Назад</a>
			<a href="{{ route('orders.edit', $order->id) }}" class="btn btn-success">
				<i class="fa fa-check-circle"></i>Принять заказ</a>
		</div>
		<div class="card-body">
			<h4 class="card-title">Заказ {{ $order->optional_id }}</h4>

			<div class="row">
				<div class="col-md-6">

					<table class="table table-bordered">
					<tbody>
						<tr>
							<td>Клиент</td>
							<td><a href="{{ route('clients.edit', $order->client->id) }}">{{ $order->client->full_name }}</a></td>
						</tr>
						<tr>
							<td>Сотрудник</td>
							<td>{{ $order->user->full_name }}</td>
						</tr>
						<tr>
							<td>Агент</td>
							<td>{{ $order->agent ? $order->agent->name : '' }}</td>
						</tr>
						<tr>
							<td>Дата создания</td>
							<td>{{ $order->created_at->format('d.m.Y H:i') }}</td>
						</tr>
					</tbody>
					</table>

				</div>
				<div class="col-md-6">

					<table class="table table-bordered">
					<tbody>
						<tr>
							<td>Прейскурант</td>
							<td>{{ $order->price_list->name }}</td>
						</tr>
						<tr>
							<td>Длительность</td>
							@php $d = ['m'=>'м.','h'=>'ч.','d'=>'д.'] @endphp
							<td>{{ $order->duration.' '.$d[$order->dimension] }}</td>
						</tr>
						<tr>
							<td>Полная стоимость</td>
							<td>{{ $order->price }}</td>
						</tr>
						<tr>
							<td>Начало действия</td>
							<td>{{ $order->start_date->format('d.m.Y H:i') }}</td>
						</tr>
					</tbody>
					</table>

				</div>
			</div>

		</div>
	</div>
</div>
</div>

<div class="row mt-3">
<div class="col-12">
	<div class="card">
	<div class="card-body">
		<h4 class="card-title">Состав заказа</h4>
		<table id="units-listing" class="table">
			<thead>
				<tr>
					<th>Артикул</th>
					<th>Модель</th>
					<th>Бренд</th>
					<th>Категория</th>
					<th>Состояние</th>
					<th>Размер</th>
				</tr>
			</thead>
			<tbody>

			@foreach ($order->units as $unit)
			<tr>
				<td>{{ $unit->code }}</td>
				<td>{{ $unit->product->name }}</td>
				<td>{{ $unit->product->brand->name }}</td>
				<td>{{ $unit->product->category->name }}</td>
				<td>{{ $unit->condition->name }}</td>
				<td>{{ $unit->size }}</td>
			</tr>
			@endforeach

			</tbody>
		</table>
	</div>
	</div>
</div>
</div>

<script>
(function($) {
	'use strict';
	$(function() {
		$('#units-listing').DataTable({
			"paging": false,
			"info": false,
			"searching": false,
			"iDisplayLength": 10,
			"aaSorting": [[0, "asc"]],
			"language": {
				"lengthMenu": "Показывать _MENU_ записей на странице",
				"zeroRecords": "Записей не найдено",
				"loadingRecords": "Загрузка... может занять несколько секунд...",
				"info": "Страница _PAGE_ из _PAGES_",
				"infoEmpty": "Показано с 0 по 0 из 0 записей",
				"search": "",
				"infoFiltered": "(Найдено записей: _TOTAL_)",
				"sInfo": "Показано с _START_ по _END_ из _TOTAL_ записей",
				"paginate":
				{
					"first":    "Первая",
					"last":     "Последняя",
					"next":     "Следующая",
					"previous": "Предыдущая"
				}
			}
		});
		$('#units-listing').each(function() {
			var datatable = $(this);
			var search_input = datatable.closest('.dataTables_wrapper').find('div[id$=_filter] input');
			search_input.attr('placeholder', 'Поиск');
			search_input.removeClass('form-control-sm');
			var length_sel = datatable.closest('.dataTables_wrapper').find('div[id$=_length] select');
			length_sel.removeClass('form-control-sm');
		});
	});
})(jQuery);
</script>

@endsection
