<div class="col-6">
    <div class="card stretch-card grid-margin">
        <div class="card-body">
            <h4 class="card-title">Выручка по филиалам</h4>
            <div id="revenueChart"></div>
        </div>
    </div>
</div>

<div class="col-6">
    <div class="card stretch-card grid-margin">
        <div class="card-body">
            <h4 class="card-title">Выручка за сегодня</h4>
            <div id="revenueByPaymentTypeChart"></div>
        </div>
    </div>
</div>

<div class="col-6">
    <div class="card stretch-card grid-margin">
        <div class="card-body">
            <h4 class="card-title">Средний чек</h4>
            <div id="avgChart"></div>
        </div>
    </div>
</div>

<div class="col-6">
    <div class="card stretch-card grid-margin">
        <div class="card-body">
            <h4 class="card-title">Новые договоры</h4>
            <div id="newOrdersChart"></div>
        </div>
    </div>
</div>

<div class="col-6">
    <div class="card stretch-card grid-margin">
        <div class="card-body">
            <h4 class="card-title">Новые занятия</h4>
            <div id="newEventsToday"></div>
        </div>
    </div>
</div>

<script>
    Morris.Bar({
        element: 'revenueChart',
        barColors: ['#63CF72', '#F36368', '#76C1FA', '#FABA66'],
        data: [
            @foreach($revenueByDates as $location)
            {
                y: '{{ $location->address }}',
                @foreach($location->revenue as $date => $value)
                    @if($loop->index == 0)
                    beforeYesterday: {{ $value }},
                    @elseif($loop->index == 1)
                    yesterday: {{ $value }},
                    @else 
                    today: {{ $value }},
                    @endif
                @endforeach
            },
            @endforeach
        ],
        xkey: 'y',
        ykeys: ['beforeYesterday', 'yesterday', 'today'],
        labels: ['Позавчера', 'Вчера', 'Сегодня']
    });

    Morris.Bar({
        element: 'revenueByPaymentTypeChart',
        barColors: ['#63CF72', '#F36368', '#76C1FA', '#FABA66'],
        data: [
            @foreach($revenueByPaymentType as $location)
            {
                y: '{{ $location->address }}',
                @foreach($location->revenue as $paymentType => $value)
                    {{ $paymentType }}: {{ $value }},
                @endforeach
            },
            @endforeach
        ],
        xkey: 'y',
        ykeys: ['cash', 'card', 'invoice'],
        labels: ['Наличные', 'Безнал', 'Счет']
    });

    Morris.Bar({
        element: 'avgChart',
        barColors: ['#63CF72', '#F36368', '#76C1FA', '#FABA66'],
        data: [
            @foreach($avgPayment as $location)
            {
                y: '{{ $location->address }}',
                avg: {{ $location->avg ?? 0 }},
            },
            @endforeach
        ],
        xkey: 'y',
        ykeys: ['avg'],
        labels: ['Средний чек']
    });

    Morris.Bar({
        element: 'newOrdersChart',
        barColors: ['#63CF72', '#F36368', '#76C1FA', '#FABA66'],
        data: [
            @foreach($locationsToday as $location)
            {
                y: '{{ $location->address }}',
                orders: {{ $location->orders->count() }},
            },
            @endforeach
        ],
        xkey: 'y',
        ykeys: ['orders'],
        labels: ['Новые договоры']
    });

    Morris.Bar({
        element: 'newEventsToday',
        barColors: ['#63CF72', '#F36368', '#76C1FA', '#FABA66'],
        data: [
            @foreach($eventsToday as $location)
            {
                y: '{{ $location->address }}',
                events: {{ $location->events->count() }},
            },
            @endforeach
        ],
        xkey: 'y',
        ykeys: ['events'],
        labels: ['Новые занятия']
    });
</script>