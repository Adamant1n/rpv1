@extends('layouts.app')

@push('js')
<script>
    $('#updateDashboard').click(function () {
        $('#analyticsMain').html('');

        $.get('/analytics', {
            ajax: 1
        }, function (response) {
            $('#analyticsMain').html(response);
        })
    });
</script>
@endpush

@section('content')

<button class="btn btn-primary mb-3" id="updateDashboard">Обновить</button>

<div class="row" id="analyticsMain">
    @include('analytics.main')
</div>

@endsection