@extends('layouts.app')

@section('content')

@include('common.validation')
@include('common.notifications')

<div class="card">

	<div class="card-header">
		<a href="{{ route('shifts.open') }}" class="btn btn-success">
			<i class="fa fa-plus"></i>Открыть смену</a>

		<a href="{{ route('shifts.xreport') }}" class="btn btn-outline-warning">
			Распечатать X-Отчет из ККТ
		</a>
	</div>

	<div class="card-body">
		<h4 class="card-title">Клиенты</h4>

		<table id="client-listing" class="table">
			<thead>
				<tr>
					<th>ID</th>
					<th>ФИО</th>
					<th>Статус</th>
					<th>Открыто</th>
					<th>Закрыто</th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				@foreach($shifts as $shift)
				<tr>
					<td>{{ $shift->id }}</td>
					<td>{{ $shift->user->full_name }}</td>
					<td>
						@if($shift->is_closed)
						<span class="badge badge-danger">закрыта</span>
						@else
						<span class="badge badge-success">открыта</span>
						@endif
					</td>
					<td>{{ $shift->opened_at->format('H:i d.m.Y') }}</td>
					<td>{{ optional($shift->closed_at)->format('H:i d.m.Y') ?? '–' }}</td>
					<td>
						@if(!$shift->is_closed)
						<a href="{{ route('shifts.close', $shift) }}" class="btn btn-outline-danger">
							Закрыть
						</a>
						@endif

						<a href="{{ route('shifts.report', $shift) }}" target="_blank" class="btn btn-outline-success">
							Отчет
						</a>
					</td>
				</tr>
				@endforeach
			</tbody>
		</table>

	</div>

</div>

<script>
(function($) {
'use strict';
$(function() {
	$('#client-listing').DataTable({
		"iDisplayLength": 25,
		"aaSorting": [[0, "desc"]],
		"language": {
			"lengthMenu": "Показывать _MENU_ записей на странице",
			"zeroRecords": "Записей не найдено",
			"loadingRecords": "Загрузка... может занять несколько секунд...",
			"info": "Страница _PAGE_ из _PAGES_",
			"infoEmpty": "Показано с 0 по 0 из 0 записей",
			"search": "",
			"infoFiltered": "(Найдено записей: _TOTAL_)",
			"sInfo": "Показано с _START_ по _END_ из _TOTAL_ записей",
			"paginate":
			{
				"first":    "Первая",
				"last":     "Последняя",
				"next":     "Следующая",
				"previous": "Предыдущая"
			}
		}
	});

	$('#client-listing').each(function() {
		var datatable = $(this);
		var search_input = datatable.closest('.dataTables_wrapper').find('div[id$=_filter] input');
		search_input.attr('placeholder', 'Поиск');
		search_input.removeClass('form-control-sm');
		var length_sel = datatable.closest('.dataTables_wrapper').find('div[id$=_length] select');
		length_sel.removeClass('form-control-sm');
	});
});
})(jQuery);
</script>

@endsection
