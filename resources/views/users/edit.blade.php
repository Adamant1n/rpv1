@extends('layouts.app')

@section('content')

@include('common.validation')
@include('common.notifications')

<div class="row">
	<div class="col-md-6">
		<div class="card">
			<div class="card-header">
				<a href="{{ route('users.index') }}" class="btn btn-light pull-left">
					<i class="fa fa-angle-left"></i>Назад</a>

				<button onclick="printImg('{{ route('users.barcode', $user->id) }}')" class="btn ml-2 btn-outline-primary pull-right">
					Карта
				</button>

				@if ($user->deleted_at)
				<a href="{{ route('users.restore', $user->id) }}" class="btn btn-outline-success pull-right">
					<i class="fa fa-trash-o"></i>Восстановить</a>
				@else
				<form method="post" action="{{ route('users.destroy', $user->id) }}">
					@csrf
					{{ method_field('DELETE') }}
					<button type="submit" class="btn btn-outline-danger pull-right">
						<i class="fa fa-trash-o"></i>Уволен</button>
				</form>
				@endif
			</div>
			<div class="card-body">
				<h4 class="card-title">Профиль пользователя</h4>
				<form method="post" action="{{ route('users.update', $user->id) }}">
					@csrf
					{{ method_field('PATCH') }}
					<div class="form-group text-capitalize">
						<label for="role">Роль</label>
						<select class="form-control select1" name="role" id="role" required>
							@foreach ($roles as $role)
							<option value="{{ $role->name }}"
							{{ $role->name == $user->getRoleNames()[0] ? 'selected' : '' }}>{{ $role->name }}</option>
							@endforeach
						</select>
					</div>
					@role('superadmin')
					<div class="form-group" id="location">
						<label for="location_id">Филиал</label>
						<select class="form-control select2" name="location_id" id="location_id">
							<option value="">Сделайте выбор...</option>
							@foreach ($locations as $location)
							<option value="{{ $location->id }}"
							{{ $location->id == $user->location_id ? 'selected' : '' }}>{{ $location->address }}</option>
							@endforeach
						</select>
					</div>
					@endrole
					<div class="form-group">
						<label for="last_name">Фамилия</label>
						<input name="last_name" id="last_name" type="text" class="form-control" placeholder="Фамилия"
							value="{{ $user->last_name }}" maxlength="50" required>
					</div>
					<div class="form-group">
						<label for="first_name">Имя</label>
						<input name="first_name" id="first_name" type="text" class="form-control" placeholder="Имя"
							value="{{ $user->first_name }}" maxlength="50" required>
					</div>
					<div class="form-group">
						<label for="mid_name">Отчество</label>
						<input name="mid_name" id="mid_name" type="text" class="form-control" placeholder="Отчество"
							value="{{ $user->mid_name }}" maxlength="50" required>
					</div>
					<div class="form-group">
						<label for="email">E-mail</label>
						<input name="email" id="email" type="email" class="form-control" placeholder="E-mail"
							value="{{ $user->email }}" maxlength="100" required>
					</div>
					<div class="form-group">
						<label for="phone">Телефон</label>
						<input name="phone" id="phone" type="text" class="form-control" placeholder="Телефон"
							value="{{ $user->phone }}" maxlength="50">
					</div>
					<div class="form-group">
						<label for="password">Изменить пароль</label>
						<input name="password" id="password" type="password" class="form-control" placeholder="Пароль" value="">
					</div>

					<hr />

					<div class="form-group">
						<label for="password">Штрих-код для авторизации</label>
						<input name="barcode" id="barcode" type="text" class="form-control" placeholder="" value="{{ $user->barcode }}">
					</div>

					<hr />

					<div class="form-group">
						<label for="">Атол ID</label>
						<input name="atol_id" id="atol_id" type="text" class="form-control" placeholder=""
							value="{{ $user->atol_id }}" maxlength="50">
					</div>

					<button type="submit" class="btn btn-success"><i class="fa fa-save"></i>Сохранить</button>
				</form>
			</div>
		</div>

	</div>
</div>

<script>
(function($) {
	'use strict';
	$(".select2").select2();
	$(".select1").select2({
		minimumResultsForSearch: -1
	});
	$('#role').on('change', function() {
		if (this.value == 'superadmin'){
			$('#location').hide();
		}
		else {
			$('#location').show();
		}
	});

})(jQuery);
</script>

@endsection
