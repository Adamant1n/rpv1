@extends('layouts.app')

@section('content')

@include('common.validation')
@include('common.notifications')

<div class="card">
	<div class="card-header">
		<a href="{{ route('users.create') }}" class="btn btn-success pull-left mr-4 mb-2">
			<i class="fa fa-plus"></i>Добавить пользователя
		</a>
		<form>
			@role('superadmin')
			<select class="form-control select2 pull-left col-md-6 col-lg-3 mr-6 mb-2" name="location" onchange="this.form.submit()">
				<option value="">Все филиалы</option>
				@foreach ($locations as $location)
				<option value="{{ $location->id }}"
				{{ $location->id == $request->location ? 'selected' : '' }}>{{ $location->address }}</option>
				@endforeach
			</select>
			@endrole
			<label class="col-lg-3">
				<input type="checkbox" name="trashed" onchange="this.form.submit()" {{ $request->has('trashed') ? 'checked' : '' }}>
				Показать уволенных
			</label>
		</form>
	</div>
	<div class="card-body">
		<h4 class="card-title">Пользователи</h4>
		<div class="row">
			<div class="col-12">
				<table id="users-listing" class="table">
				<thead>
					<tr>
						<th>ID</th>
						<th>ФИО</th>
						<th>E-mail</th>
						<th>Роль</th>
						<th>Филиал</th>
						<th>Дата создания</th>
						<th>Дата изменения</th>
					</tr>
				</thead>
				<tbody>

				@foreach ($users as $user)
				<tr>
					<td>{{ $user->id }}</td>
					<td>{{ $user->last_name.' '.$user->first_name.' '.$user->mid_name }}</td>
					<td>{{ $user->email }}</td>
					<td class="text-capitalize">{{ count($user->getRoleNames()) ? $user->getRoleNames()[0] : '' }}</td>
					<td>{{ $user->location_id ? $locations->find($user->location_id)->address : '' }}</td>
					<td>{{ $user->created_at->format('d.m.Y H:i') }}</td>
					<td>{{ $user->updated_at->format('d.m.Y H:i') }}</td>
				</tr>
				@endforeach

				</tbody>
				</table>
			</div>
		</div>
	</div>
</div>

<script>
(function($) {
	'use strict';
	$(function() {
		$('#users-listing').DataTable({
			"iDisplayLength": 10,
			"aaSorting": [[0, "asc"]],
			"language": {
				"lengthMenu": "Показывать _MENU_ записей на странице",
				"zeroRecords": "Записей не найдено",
				"loadingRecords": "Загрузка... может занять несколько секунд...",
				"info": "Страница _PAGE_ из _PAGES_",
				"infoEmpty": "Показано с 0 по 0 из 0 записей",
				"search": "",
				"infoFiltered": "(Найдено записей: _TOTAL_)",
				"sInfo": "Показано с _START_ по _END_ из _TOTAL_ записей",
				"paginate":
				{
					"first":    "Первая",
					"last":     "Последняя",
					"next":     "Следующая",
					"previous": "Предыдущая"
				}
			}
		});
		$('#users-listing').each(function() {
			var datatable = $(this);
			var search_input = datatable.closest('.dataTables_wrapper').find('div[id$=_filter] input');
			search_input.attr('placeholder', 'Поиск');
			search_input.removeClass('form-control-sm');
			var length_sel = datatable.closest('.dataTables_wrapper').find('div[id$=_length] select');
			length_sel.removeClass('form-control-sm');
		});
		$('#users-listing tbody').on('click', 'tr', function () {
			var id = $('td', this).eq(0).text();
			$(location).attr('href','/users/'+id+'/edit');
		});
	});
	$(".select2").select2();
})(jQuery);
</script>

@endsection
