@extends('layouts.app')

@section('content')

@include('common.validation')
@include('common.notifications')

<div class="row">
	<div class="col-md-6">
		<div class="card">
			<div class="card-header">
				<a href="{{ route('users.index') }}" class="btn btn-light">
					<i class="fa fa-angle-left"></i>Назад</a>
			</div>
			<div class="card-body">
				<h4 class="card-title">Новый пользователь</h4>
				<form method="post" action="{{ route('users.store') }}">
					@csrf
					<div class="form-group text-capitalize">
						<label for="role">Роль</label>
						<select class="form-control select1" name="role" id="role" required>
							@foreach ($roles as $role)
							<option value="{{ $role->name }}">{{ $role->name }}</option>
							@endforeach
						</select>
					</div>
					@role('superadmin')
					<div class="form-group" id="location">
						<label for="location_id">Филиал</label>
						<select class="form-control select2" name="location_id" id="location_id">
							<option value="">Сделайте выбор...</option>
							@foreach ($locations as $location)
							<option value="{{ $location->id }}">{{ $location->address }}</option>
							@endforeach
						</select>
					</div>
					@endrole
					<div class="form-group">
						<label for="last_name">Фамилия</label>
						<input name="last_name" id="last_name" type="text" class="form-control"
							placeholder="Фамилия" maxlength="50" required autofocus>
					</div>
					<div class="form-group">
						<label for="first_name">Имя</label>
						<input name="first_name" id="first_name" type="text" class="form-control"
							placeholder="Имя" maxlength="50" required>
					</div>
					<div class="form-group">
						<label for="mid_name">Отчество</label>
						<input name="mid_name" id="mid_name" type="text" class="form-control"
							placeholder="Отчество" maxlength="50" required>
					</div>
					<div class="form-group">
						<label for="email">E-mail</label>
						<input name="email" id="email" type="email" class="form-control"
							placeholder="E-mail" maxlength="100" required>
					</div>
					<div class="form-group">
						<label for="phone">Телефон</label>
						<input name="phone" id="phone" type="text" class="form-control"
							placeholder="Телефон" maxlength="50">
					</div>
					<div class="form-group">
						<label for="password">Пароль</label>
						<input name="password" id="password" type="password" class="form-control"
							placeholder="Пароль" value="" required>
					</div>

					<hr />

					<div class="form-group">
						<label for="password">Штрих-код для авторизации</label>
						<input name="barcode" id="barcode" type="text" class="form-control" placeholder="" value="">
					</div>

					<hr />

					<div class="form-group">
						<label for="">Атол ID</label>
						<input name="atol_id" id="atol_id" type="text" class="form-control" placeholder=""
							value="{{ old('atol_id') }}" maxlength="50">
					</div>

					<button type="submit" class="btn btn-success">
						<i class="fa fa-save"></i>Сохранить</button>
				</form>
			</div>
		</div>

	</div>
</div>

<script>
(function($) {
	'use strict';
	$(".select2").select2();
	$(".select1").select2({
		minimumResultsForSearch: -1
	});
	$('#role').on('change', function() {
		if (this.value == 'superadmin'){
			$('#location').hide();
		}
		else {
			$('#location').show();
		}
	});

})(jQuery);
</script>

@endsection
