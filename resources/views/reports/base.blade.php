@extends('layouts.app')

@push('js')
<script type="text/javascript">
	$(".select2").select2();

	// datatable
	$('#reports-listing').DataTable({
		"bSort": false,
		"iDisplayLength": 100,
		"aaSorting": [[0, "asc"]],
		dom: 'Bfrtip',
        buttons: [
            'excel', 'pdf', 'print'
        ],
		"language": {
			"lengthMenu": "Показывать _MENU_ записей на странице",
			"zeroRecords": "Записей не найдено",
			"loadingRecords": "Загрузка... может занять несколько секунд...",
			"info": "Страница _PAGE_ из _PAGES_",
			"infoEmpty": "Показано с 0 по 0 из 0 записей",
			"search": "",
			"infoFiltered": "(Найдено записей: _TOTAL_)",
			"sInfo": "Показано с _START_ по _END_ из _TOTAL_ записей",
			"paginate":
			{
				"first":    "Первая",
				"last":     "Последняя",
				"next":     "Следующая",
				"previous": "Предыдущая"
			},
			"buttons":
			{
				"pageLength": {
					   _: "%d записей",
					'-1': "Показать все"
				},
				"print": "Печать",
				"excel": "Экспорт"
			}
		}
	});

	// datepicker
	/*
    $('.datepicker').datepicker({
    	enableOnReadonly: true,
    	todayHighlight: false,
    	dateFormat: 'yyyy-mm-dd',
    	onSelect: function (dateText, inst) {
        	$('#form-filters').submit();
      	}
    });
    */

    var date_start = $('#datepicker-start');
    var date_end = $('#datepicker-end');

    date_start.datepicker({
		enableOnReadonly: true,
		todayHighlight: false,
		format: 'yyyy-mm-dd'
	});

    date_end.datepicker({
		enableOnReadonly: true,
		todayHighlight: false,
		format: 'yyyy-mm-dd'
	});

	date_start.datepicker().on('changeDate', function (e) {
		$('#form-filters').submit();
	});

	date_end.datepicker().on('changeDate', function (e) {
		$('#form-filters').submit();
	});
</script>
@endpush

@section('content')

@include('common.validation')
@include('common.notifications')

<div class="card">
	<div class="card-body">
		<h4 class="card-title">Отчеты</h4>

		<form method="get" id="form-filters">
			<div class="row">

				@if(!isset($filters['dates']))
				<div class="form-group col-md-3">
					<label>Начало периода</label>
                    <div id="datepicker-start" class="input-group date datepicker">
	                	<input type="text" class="form-control" name="date_start" value="{{ $date_start_input }}">
	               		<span class="input-group-addon input-group-append border-left">
	                  		<span class="mdi mdi-calendar input-group-text"></span>
	                	</span>
	              	</div>
				</div>

				<div class="form-group col-md-3">
					<label>Окончание периода</label>
                    <div id="datepicker-end" class="input-group date datepicker">
	                	<input type="text" class="form-control" name="date_end" value="{{ $date_end_input }}">
	               		<span class="input-group-addon input-group-append border-left">
	                  		<span class="mdi mdi-calendar input-group-text"></span>
	                	</span>
	              	</div>
				</div>
				@endif

				@foreach($filters as $filter_name => $filter)
				@if($filter_name == 'dates')
				@continue
				@endif
				<div class="form-group col-md-3">
					<label>{{ $filter['label'] }}</label>
					<select name="{{ $filter_name }}" class="form-control select2" onchange="this.form.submit()" @role('admin') {{ ($filter_name == 'location_id') ? 'disabled' : '' }} @endrole>
						<option value="">{{ $filter['label'] }}</option>
						@foreach($filter['options'] as $option)
						@role('admin')
						<option value="{{ $option['id'] }}" {{ ($filter_name == 'location_id' && auth()->user()->location_id == $option['id']) ? 'selected' : '' }}>{{ $option['title'] }}</option>
						@else
						<option value="{{ $option['id'] }}" {{ Input::get($filter_name) == $option['id'] ? 'selected' : '' }}>{{ $option['title'] }}</option>
						@endrole
						@endforeach
					</select>
				</div>
				@endforeach
			</div>
		</form>

		<div class="table-responsive">
			<table id="reports-listing" class="table">
				<thead>
					<tr>
						@foreach($table['head'] as $th)
						<th>{{ $th }}</th>
						@endforeach
					</tr>
				</thead>
				<tbody>

				@foreach ($table['body'] as $tr)
				<tr class="{{ isset($tr['header']) ? 'table-primary' : '' }}">
					@foreach($tr as $key => $td)
						@if($key != 'header')
						<td>{{ $td }}</td>
						@endif
					@endforeach
				</tr>
				@endforeach

				</tbody>
			</table>
		</div>
	</div>
</div>

<!--
<script>
(function($) {
	'use strict';
	$(function() {
		$('#reports').DataTable({
			"iDisplayLength": 100,
			"aaSorting": [[0, "asc"]],
			"language": {
				"lengthMenu": "Показывать _MENU_ записей на странице",
				"zeroRecords": "Записей не найдено",
				"loadingRecords": "Загрузка... может занять несколько секунд...",
				"info": "Страница _PAGE_ из _PAGES_",
				"infoEmpty": "Показано с 0 по 0 из 0 записей",
				"search": "",
				"infoFiltered": "(Найдено записей: _TOTAL_)",
				"sInfo": "Показано с _START_ по _END_ из _TOTAL_ записей",
				"paginate":
				{
					"first":    "Первая",
					"last":     "Последняя",
					"next":     "Следующая",
					"previous": "Предыдущая"
				}
			}
		});

		/*
		$('#agents-listing').each(function() {
			var datatable = $(this);
			var search_input = datatable.closest('.dataTables_wrapper').find('div[id$=_filter] input');
			search_input.attr('placeholder', 'Поиск');
			search_input.removeClass('form-control-sm');
			var length_sel = datatable.closest('.dataTables_wrapper').find('div[id$=_length] select');
			length_sel.removeClass('form-control-sm');
		});
		$('#agents-listing tbody').on('click', 'tr', function () {
			var id = $('td', this).eq(0).text();
			$(location).attr('href','/agents/'+id+'/edit');
		});
		*/
	});
})(jQuery);
</script>
-->
@endsection