@extends('layouts.app')

@push('js')
<script>
(function($) {
	'use strict';
	$(".select2").select2({
		tags: true
	});
	
	$('.dropify').dropify();
})(jQuery);
</script>
@endpush

@section('content')

@include('common.validation')
@include('common.notifications')

@include('common.tabs', ['page' => 'products'])
<div class="row">
	<div class="col-sm-12 col-md-10 col-lg-8">

		@if ($errors->any())
			<div class="alert alert-danger" role="alert">
				<ul>
					@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif

		<div class="card">
			<div class="card-header">
				<a href="{{ route('products.index') }}" class="btn btn-light"><i class="fa fa-angle-left"></i>Назад</a>
			</div>
			<div class="card-body">
				<h4 class="card-title">Новая модель</h4>
				<form method="post" action="{{ route('products.store') }}" enctype="multipart/form-data">
					@csrf

					<div class="row">
						<div class="col-md-9">
							<div class="form-group">
								<label for="name">Наименование</label>
								<input name="name" id="name" type="text" class="form-control" placeholder="Наименование" required autofocus>
							</div>
							<div class="form-group">
								<label for="category_id">Категория</label>
								<select class="form-control select2" name="category_id" id="category_id" required>
									<option value="">Сделайте выбор...</option>
									@foreach ($categories as $category)
									<option value="{{ $category->id }}">{{ $category->name }}</option>
									@endforeach
								</select>
							</div>
							<div class="form-group">
								<label for="brand_id">Бренд</label>
								<div class="row">
									<div class="col-10">
										<select class="form-control select2" name="brand_id" id="brand_id">
											<option value="">Сделайте выбор...</option>
											@foreach ($brands as $brand)
											<option value="{{ $brand->id }}">{{ $brand->name }}</option>
											@endforeach
										</select>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label>Фото</label>
								<input type="file" class="dropify" name="file_photo">
							</div>
						</div>
					</div>

					<hr />

					<label><input type="checkbox" name="for_sale" id="for_sale" value=1> Для продажи</label>

					<div class="row">
						<div class="form-group col-3" id="price_buy">
							<label>Цена закупки</label>
							<input type="number" name="price_buy" step="0,1" value=0 min=0 class="form-control">
						</div>
						<div class="form-group col-3" id="price_sell">
							<label>Стоимость продажи</label>
							<input type="number" name="price_sell" step="0,1" value=0 min=0 class="form-control">
						</div>
					</div>

					<button type="submit" class="btn btn-success"><i class="fa fa-save"></i>Сохранить</button>
				</form>
			</div>
		</div>

	</div>
</div>
@endsection