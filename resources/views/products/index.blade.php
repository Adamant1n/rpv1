@extends('layouts.app')

@section('content')

@include('common.validation')
@include('common.notifications')

@include('common.tabs', ['page' => 'products'])
<div class="card">
	<div class="card-header">
		<a href="{{ route('products.create') }}" class="btn btn-success"><i class="fa fa-plus"></i>Добавить модель</a>
	</div>
	<div class="card-body">
		<h4 class="card-title">Модели товаров</h4>
		<div class="row">
			<div class="col-12">
				<table id="products-listing" class="table">
				<thead>
					<tr>
						<th>ID</th>
						<th>Наименование</th>
						<th>Бренд</th>
						<th>Категория</th>
						<th>Дата создания</th>
						<th>Дата изменения</th>
					</tr>
				</thead>
				<tbody>

				@foreach ($products as $product)
				<tr>
					<td>{{ $product->id }}</td>
					<td>{{ $product->name }}</td>
					<td>{{ optional($product->brand)->name }}</td>
					<td>{{ optional($product->category)->name }}</td>
					<td>{{ $product->created_at->format('d.m.Y H:i') }}</td>
					<td>{{ $product->updated_at->format('d.m.Y H:i') }}</td>
				</tr>
				@endforeach

				</tbody>
				</table>
			</div>
		</div>
	</div>
</div>

<script>
(function($) {
	'use strict';
	$(function() {
		var table = $('#products-listing').DataTable({
			"iDisplayLength": 10,
			"aaSorting": [[0, "asc"]],
			"language": {
				"lengthMenu": "Показывать _MENU_ записей на странице",
				"zeroRecords": "Записей не найдено",
				"loadingRecords": "Загрузка... может занять несколько секунд...",
				"info": "Страница _PAGE_ из _PAGES_",
				"infoEmpty": "Показано с 0 по 0 из 0 записей",
				"search": "",
				"infoFiltered": "(Найдено записей: _TOTAL_)",
				"sInfo": "Показано с _START_ по _END_ из _TOTAL_ записей",
				"paginate":
				{
					"first":    "Первая",
					"last":     "Последняя",
					"next":     "Следующая",
					"previous": "Предыдущая"
				},
			},
			"pageLength": {{ Request::get('length') ? Request::get('length') : 10 }},
			initComplete: function () {
      			setTimeout(function () {
        				table.page({{ Request::get('page') ? Request::get('page') - 1 : 0 }})
							.draw(false);
      			}, 10);
    		},
			"drawCallback": function( settings ) {
				var page = Math.ceil(settings._iDisplayStart / settings._iDisplayLength) + 1;

				updateUrlParameter('page', page);
				updateUrlParameter('length', settings._iDisplayLength);
			}
		});

		$('#products-listing').each(function() {
			var datatable = $(this);
			var search_input = datatable.closest('.dataTables_wrapper').find('div[id$=_filter] input');
			search_input.attr('placeholder', 'Поиск');
			search_input.removeClass('form-control-sm');
			var length_sel = datatable.closest('.dataTables_wrapper').find('div[id$=_length] select');
			length_sel.removeClass('form-control-sm');
		});
		$('#products-listing tbody').on('click', 'tr', function () {
			var id = $('td', this).eq(0).text();
			$(location).attr('href','/products/'+id+'/edit');
		});
	});
})(jQuery);
</script>

@endsection
