<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<title>{{ config('app.name', 'Laravel') }}</title>

	<link rel="stylesheet" href="{{ asset('fonts/Museo/stylesheet.css') }}" />
	<link rel="stylesheet" href="{{ asset('vendors/iconfonts/mdi/css/materialdesignicons.min.css') }}">
	<link rel="stylesheet" href="{{ asset('vendors/iconfonts/puse-icons-feather/feather.css') }}">
	<link rel="stylesheet" href="{{ asset('vendors/css/vendor.bundle.base.css') }}">
	<link rel="stylesheet" href="{{ asset('vendors/css/vendor.bundle.addons.css') }}">
	<link rel="stylesheet" href="{{ asset('vendors/iconfonts/simple-line-icon/css/simple-line-icons.css') }}">
	<link rel="stylesheet" href="{{ asset('vendors/lightgallery/css/lightgallery.css') }}">
	<!--<link rel="stylesheet" href="{{ asset('vendors/iconfonts/font-awesome/css/font-awesome.min.css') }}" />-->
    <link rel="stylesheet" href="{{ asset('vendors/fa/css/all.min.css') }}">


	<link rel="stylesheet" href="{{ asset('css/style.css') }}">
	<link rel="shortcut icon" href="{{ asset('images/favicon.png') }}" />
	<style>.fc-cell-text {font-size:smaller;}</style>
	<script src="{{ asset('vendors/js/vendor.bundle.base.js') }}"></script>
	<script src="{{ asset('vendors/js/vendor.bundle.addons.js') }}"></script>


    <script src="{{ asset('vendors/fa/js/all.min.js') }}"></script>
	@if (Request::is('reports/*','stock','stocktaking/*'))
	<script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
	<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
	<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
	<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>
	@endif

	<script src="{{ asset('js/defiant.min.js') }}"></script>

	<script>
		/*
		// helper function to set cookies
		function setCookie(cname, cvalue, seconds) {
		    var d = new Date();
		    d.setTime(d.getTime() + (seconds * 1000));
		    var expires = "expires="+ d.toUTCString();
		    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
		}

		// helper function to get a cookie
		function getCookie(cname) {
		    var name = cname + "=";
		    var decodedCookie = decodeURIComponent(document.cookie);
		    var ca = decodedCookie.split(';');
		    for(var i = 0; i < ca.length; i++) {
		        var c = ca[i];
		        while (c.charAt(0) == ' ') {
		            c = c.substring(1);
		        }
		        if (c.indexOf(name) == 0) {
		            return c.substring(name.length, c.length);
		        }
		    }
		    return "";
		}

		// Do not allow multiple call center tabs
	    $(window).on('beforeunload onbeforeunload', function(){
	        document.cookie = 'ic_window_id=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;';
	    });

	    function validateCallCenterTab() {
	        var win_id_cookie_duration = 10; // in seconds

	        if (!window.name) {
	            window.name = Math.random().toString();
	        }

	        if (!getCookie('ic_window_id') || window.name === getCookie('ic_window_id')) {
	            // This means they are using just one tab. Set/clobber the cookie to prolong the tab's validity.
	            setCookie('ic_window_id', window.name, win_id_cookie_duration);
	        } else if (getCookie('ic_window_id') !== window.name) {
	            window.close();
	            
	            clearInterval(callCenterInterval);
	            throw 'Multiple call center tabs error. Program terminating.';
	        }
	    }

	    callCenterInterval = setInterval(validateCallCenterTab, 3000);
	    */
	</script> 

	<script src="{{ asset('js/print.min.js') }}"></script>
	<link rel="stylesheet" href="{{ asset('css/print.min.css') }}">

	<script type="text/javascript">
		function printImg(url) {
			var win = window.open('');
			win.document.write('<img src="' + url + '" onload="window.print();setTimeout(window.close, 1000);" />');
			win.focus();
		}

		function printPdf(url) {
			/*
			$('#printContent').val(url);
			$("#printContent").show();
			document.getElementById('printContent').focus();
			document.getElementById('printContent').contentWindow.print();
			*/
			console.log(url);
			printJS({printable: url, type:'pdf', showModal: true})
		}

		function updateUrlParameter(name, value) {
			const params = new URLSearchParams(window.location.search);
			params.set(name, value);
			window.history.replaceState({}, "", decodeURIComponent(`${window.location.pathname}?${params}`));
		}
	</script>

	<style>
		.select2-dropdown {
			z-index: 1080 !important;
		}

		@media (max-width: 960px)
		{
			.navbar  .navbar-brand-wrapper.d-flex {
				display: none !important;
			}

			.card .card-body {
				padding: 0.88rem 0.81rem;
			}

			.content-wrapper {
				padding: 0 !important;
			}
		}
	</style>

	<!-- swal 2.0 -->
	<script src="{{ asset('js/sweetalert2.all.min.js') }}"></script>
</head>
<body class="horizontal-menu">
	<div class="container-scroller">
		<!-- partial:partials/_navbar.html -->
		<nav class="navbar col-lg-12 col-12 p-0 fixed-top d-flex flex-row navbar-success" style="background: #23A425;">
			<div class="text-center navbar-brand-wrapper d-flex align-items-center justify-content-center">
				<a class="navbar-brand brand-logo text-success" href="{{ url('/') }}">
                    <img src="{{ asset('/images/logo/lg.png') }}" style="object-fit:contain">
                </a>
				<a class="navbar-brand brand-logo-mini text-success" href="{{ url('/') }}">
                    <img src="{{ asset('/images/logo/sm.png') }}" style="object-fit:contain">            
                </a>
			</div>
			<div class="navbar-menu-wrapper d-flex align-items-stretch">

				<ul class="navbar-nav navbar-nav-left">
         

					<li class="nav-item">
						<a class="nav-link count-indicator">
							Статус ККТ Атол

							@php
							$atolStatus = \App\AtolStatus::getStatus();
							@endphp

							@if($atolStatus === \App\AtolStatus::OK)
							<div class="badge ml-2 bg-white badge-outline-success">Готов</div>
							@elseif($atolStatus === \App\AtolStatus::ERROR)
							<div class="badge ml-2 bg-white badge-outline-danger">Ошибка</div>
							@else
							<div class="badge ml-2 bg-white badge-outline-info">Ожидание</div>
							@endif
						</a>
					</li>
					 <li class="nav-item">
                        <a class="nav-link count-indicator">
                            {{ auth()->user()->location->address}}, <span id="time">{{ \Carbon\Carbon::now()->toDateTimeLocalString('minute') }}</span>
                        </a>
                    </li>
                </ul>

				<ul class="navbar-nav navbar-nav-right">
					<li class="nav-item dropdown">
						<a class="nav-link dropdown-toggle nav-profile" id="profileDropdown" href="#" data-toggle="dropdown" aria-expanded="false">
							<span class="d-none d-lg-inline">
								<i class="fa fa-user mr-2"></i>
								{{ Auth::user()->full_name }}
							</span>
						</a>
						<div class="dropdown-menu navbar-dropdown w-100" aria-labelledby="profileDropdown">
							<div class="dropdown-divider"></div>
							<a class="dropdown-item" href="{{ route('logout') }}">
								<i class="mdi mdi-logout mr-2 text-primary"></i>
								Выход
							</a>
						</div>
					</li>
				</ul>

				<button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="offcanvas">
					<span class="mdi mdi-menu"></span>
				</button>
			</div>
		</nav>
		<!-- partial -->
		<div class="container-fluid page-body-wrapper">
			<!-- partial -->
			<!-- partial:partials/_sidebar.html -->
			<nav class="sidebar sidebar-offcanvas" id="sidebar">
				<ul class="nav">

                    @can('access-dashboard')
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('dashboard.index') }}">
                            <i class="fas fa-chart-line"></i>
                            <span class="menu-title ml-2">Филиал</span>
                        </a>
                    </li>
                    @endcan
					<!-- Разобраться
                    @can('access-analytics')
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('analytics.index') }}">
                            <i class="fas fa-chart-bar"></i>
                            <span class="menu-title ml-2">Аналитика</span>
                        </a>
                    </li>
                    @endcan
					-->
                    @canany(['manage-orders', 'manage-location-orders'])

                    @if(auth()->user()->role->name != 'delivery')
					<li class="nav-item">
						<a class="nav-link" href="{{ route('orders.index', ['type' => 'rent']) }}">
							<i class="fas fa-cart-arrow-down"></i>
							<span class="menu-title ml-2">Заказы</span>
						</a>
					</li>
					
					<li class="nav-item">
						<a class="nav-link" href="{{ route('orders.index', ['type' => 'book']) }}">
							<i class="far fa-calendar-alt"></i>
							<span class="menu-title ml-2">Брони</span>
						</a>
					</li>
                    @endif

					<li class="nav-item">
						<a class="nav-link" href="{{ route('orders.index', ['type' => 'delivery']) }}">
							<i class="fas fa-truck"></i>
							<span class="menu-title ml-2">Доставка</span>
						</a>
					</li>
					@endcanany

                    @can('take-payment')
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('shifts.index') }}">
                            <i class="fas fa-receipt"></i>
                            <span class="menu-title ml-2">
                                Смены кассы
                            </span>
                        </a>
                    </li>
                    @endcan

					@can('manage-clients')
					<li class="nav-item">
						<a class="nav-link" href="{{ route('clients.index') }}">
							<i class="far fa-id-card"></i>
							<span class="menu-title ml-2">Клиенты</span>
						</a>
					</li>
					@endcan
					<!--
					<li class="nav-item">
						<a class="nav-link" href="{{ route('products.index') }}">
							<i class="icon-basket menu-icon"></i>
							<span class="menu-title ml-2">Модели</span>
						</a>
					</li>
					-->
					@canany(['manage-stock', 'manage-location-stock'])
                    <li class="nav-item">
                        <a class="nav-link">
                            <i class="fas fa-boxes"></i>
                            <span class="menu-title ml-2">Склад</span>
                            <i class="menu-arrow"></i>
                        </a>
                        <div class="collapse" id="settings">
                            <ul class="nav flex-column sub-menu">
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('stock.index') }}">
                                        Склад
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('stocktaking.index') }}">
                                        Инвентаризация
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('massediting.index') }}">
                                        Групповое редактирование
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </li>


					
					@endcanany

					@can('view-reports')
					<li class="nav-item">
						<a class="nav-link">
							<i class="fas fa-chart-bar"></i>
							<span class="menu-title ml-2">Отчеты</span>
							<i class="menu-arrow"></i>
						</a>
						<div class="collapse" id="reports">
							<ul class="nav flex-column sub-menu">
								<li class="nav-item">
									<a class="nav-link" href="{{ route('reports.financial') }}">Выручка</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" href="{{ route('reports.agreements') }}">Договоры</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" href="{{ route('reports.agent_sales_common') }}">Продажи агентов</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" href="{{ route('reports.agent_sales_detailed') }}">Продажи агентов (детально)</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" href="{{ route('reports.inventory_rent') }}">Инвентарь в аренде</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" href="{{ route('reports.inventory_stock') }}">Запас инвентаря</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" href="{{ route('reports.inventory_stock_detailed') }}">Запас инвентаря детально</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" href="{{ route('reports.inventory_sale') }}">Продажи</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" href="{{ route('reports.inventory_products') }}">Запас товаров</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" href="{{ route('reports.bookings') }}">Бронирования</a>
								</li>
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('reports.debt') }}">Задолженность по договорам</a>
                                </li>
								<li class="nav-item">
									<a class="nav-link" href="{{ route('reports.effectivity') }}">Эффективность сотрудников</a>
								</li>
								
							</ul>
						</div>
					</li>
					@endcan

					<li class="nav-item">
						<a class="nav-link">
							<i class="fas fa-cogs"></i>
							<span class="menu-title ml-2">Настройки</span>
							<i class="menu-arrow"></i>
						</a>
						<div class="collapse" id="settings">
							<ul class="nav flex-column sub-menu">

								@can('manage-users')
								<li class="nav-item">
									<a class="nav-link" href="{{ route('users.index') }}">Пользователи</a>
								</li>
								@endcan

                                @can('manage-roles')
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('roles.index') }}">Роли</a>
                                </li>
                                @endcan

                                @can('manage-roles')
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('permissions.index') }}">Права</a>
                                </li>
                                @endcan

                                <hr />

                                @can('manage-locations')
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('locations.index') }}">Филиалы</a>
                                </li>
                                @endcan

                                @can('manage-pricelists')
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('price_lists.index') }}">Прейскуранты</a>
                                </li>
                                @endcan

                                @can('manage-settings')
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('settings.edit') }}">Шаблоны</a>
                                </li>
                                @endcan

                                <hr />

                                @can('manage-agents')
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('agents.index') }}">Скидки и агенты</a>
                                </li>
                                @endcan

                                @can('manage-settings')
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('synchronizations.index') }}">Синхронизация</a>
                                </li>
                                @endcan

                                <hr />

								@can('manage-properties')
								<li class="nav-item">
									<a class="nav-link" href="{{ route('categories.index') }}">Категории товаров</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" href="{{ route('products.index') }}">Модели</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" href="{{ route('bindings.index') }}">Крепления</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" href="{{ route('brands.index') }}">Бренды</a>
								</li>
								@endcan
							</ul>
						</div>
					</li>
				</ul>
			</nav>
			<!-- partial -->
			<div class="main-panel">
				<div class="content-wrapper">
					@section('content')

					@canany(['manage-orders', 'manage-location-orders'])
						@include('modals.order')
					@endcanany

					@can('take-payment')
						@include('modals.cashier')
						@include('modals.payment')
					@endcan

					@yield('content')
				</div>
				<!-- content-wrapper ends -->
				<!-- partial -->
			</div>
			<!-- main-panel ends -->
		</div>
		<!-- page-body-wrapper ends -->
	</div>
	<!-- container-scroller -->

	<script>
	(function($) {
	'use strict';
	$(function() {
		$('title').html($('h4.card-title').html());
	});
	})(jQuery);
	</script>
	<script src="{{ asset('js/hoverable-collapse.js') }}"></script>
	<script src="{{ asset('js/off-canvas.js') }}"></script>
	
	@if (Route::current()->getName() != 'stocktaking.perform' && Route::current()->getName() != 'massediting.index')
	@include('modals.order-js')
	@include('modals.product')
    @include('modals.customer')
	@endif

	@stack('js')

	<script type="text/javascript">
        // init select2
        $(document).ready(function() {
			var options = { year: 'numeric', month: 'long', day: 'numeric', hour: 'numeric', minute: 'numeric', weekday: 'long' };
            $('#time').html(new Date().toLocaleString('ru-RU', options));

            setInterval(function() {
                $('#time').html(new Date().toLocaleString('ru-RU', options));
            }, 1000);
        });
    </script>

	<iframe src="" id="printContent" style="display: none">
</body>
</html>
