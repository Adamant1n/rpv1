@push('css')
<style>
tfoot {
    display: table-header-group !important;
}

.table th, .jsgrid .jsgrid-table th, .table td, .jsgrid .jsgrid-table td {
    padding: 9px 10px;
}

.dataTables_wrapper .dataTable .btn {
	padding: 0.5rem 0.81rem;
}

/*
#listing-{{ $tableName }} {
  table-layout: fixed;
  width: 100% !important;
}
#listing-{{ $tableName }} td,
#listing-{{ $tableName }} th{
  width: auto !important;
  white-space: normal;
  text-overflow: ellipsis;
  overflow: hidden;
}
*/
</style>
@endpush

<div class="table-responsive">
	<table class="table" id="listing-{{ $tableName }}">
		<thead>
			<tr>
				@foreach($columns as $column)
				<th>{{ $column['name'] }}</th>
				@endforeach
			</tr>
		</thead>
		<tfoot>
			<tr>
				@foreach($columns as $column)
				<th></th>
				@endforeach
			</tr>
		</tfoot>
		<tbody>
		</tbody>
	</table>
</div>