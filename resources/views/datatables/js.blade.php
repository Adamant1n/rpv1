<script type="text/javascript">
$(document).ready(function() {
	var tableColumns = {!! json_encode($columns) !!};
    $('#listing-{{ $tableName }}').DataTable({
    	processing: true,
		serverSide: true,
		//searching: false,
		iDisplayLength: 25,
		aaSorting: [[0, 'asc']],
		language: {
			lengthMenu: 'Показывать _MENU_ записей на странице',
			zeroRecords: 'Записей не найдено',
			loadingRecords: 'Загрузка... может занять несколько секунд...',
			info: 'Страница _PAGE_ из _PAGES_',
			infoEmpty: 'Показано с 0 по 0 из 0 записей',
			search: '',
			infoFiltered: '(Найдено записей: _TOTAL_)',
			sInfo: 'Показано с _START_ по _END_ из _TOTAL_ записей',
			processing: "Загрузка",
			paginate:
			{
				first:    'Первая',
				last:     'Последняя',
				next:     'Следующая',
				previous: 'Предыдущая'
			}
		},
        ajax: '{{ $ajaxRoute }}',
        columns: tableColumns,
        initComplete: function () {
            this.api().columns().every(function (k) {
                var column = this;

                if(tableColumns[k].filter != undefined)
                {
                	if(tableColumns[k].filter.type == 'select')
                	{
                		var select = $('<select class="form-control input-sm"><option value=""></option></select>')   
                			.appendTo($(column.footer()).empty())
		                    .on('change', function () { 
		                        column.search($(this).val(), true, false)
		                            .draw();
		                    });

		                for(var i = 0; i < tableColumns[k].filter.options.length; i++)
		                {
		                	select.append(new Option(tableColumns[k].filter.options[i].display, tableColumns[k].filter.options[i].value, false, false));
		                }
                	}
                	else if(tableColumns[k].filter.type == 'input')
                	{
                		var input = $('<input class="form-control input-sm" type="text">')
                			.appendTo($(column.footer()).empty())
                			.on('input', function () { 
		                        column.search($(this).val(), true, false)
		                            .draw();
		                    });
                	}
                }
            });
        }
    });

    $('#listing-{{ $tableName }}').each(function() {
		var datatable = $(this);
		var search_input = datatable.closest('.dataTables_wrapper').find('div[id$=_filter] input');
		search_input.attr('placeholder', 'Поиск');
		search_input.removeClass('form-control-sm');
		var length_sel = datatable.closest('.dataTables_wrapper').find('div[id$=_length] select');
		length_sel.removeClass('form-control-sm');

		$('.dataTables_filter').remove();
    });
});
</script>