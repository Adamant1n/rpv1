@extends('layouts.app')

@section('content')

@include('common.validation')
@include('common.notifications')

@push('js')
<script type="text/javascript">
	var unitsTable = $('#units-listing').DataTable({
		//"iDisplayLength": 100,
		"bSort": false,
		"bFilter": false,
		"paging": false,
		"language": {
			"lengthMenu": "Показывать _MENU_ записей на странице",
			"zeroRecords": "Записей не найдено",
			"loadingRecords": "Загрузка... может занять несколько секунд...",
			"info": "Страница _PAGE_ из _PAGES_",
			"infoEmpty": "Показано с 0 по 0 из 0 записей",
			"search": "",
			"infoFiltered": "(Найдено записей: _TOTAL_)",
			"sInfo": "Показано с _START_ по _END_ из _TOTAL_ записей",
			"bInfo": false,
			"paginate":
			{
				"first":    "Первая",
				"last":     "Последняя",
				"next":     "Следующая",
				"previous": "Предыдущая"
			}
		}
	});

	var codes = [];

	$('#codeFile').change(function () {
		var file_data = $('#codeFile').prop('files')[0];
	    var form_data = new FormData();                  
	    form_data.append('file', file_data);

	    $.ajax({
	        url: '{{ route('massediting.get_units') }}', 
	        data: form_data,                         
	        type: 'post',
	        contentType: false,
	        processData: false,
	        success: function(response){
	        	unitsTable.clear().draw();
	        	addRows(response);   
	        }
	    });
	});

	function addRows(units)
	{
		for(var i = 0; i < units.length; i++)
		{
			codes.push(units[i][0]);
			$('#codes').val(JSON.stringify(codes));
			var rowNode = unitsTable.row.add(units[i]).draw(false).node();
		}

		// scan();
	}

	function addUnit(code)
	{
		var filteredData = unitsTable.rows()
		    .indexes()
		    .filter(function (value, index) {
		       return unitsTable.row(value).data()[0] == code; 
		    });

		if(filteredData.length > 0) return false;

		$.ajax({
	        url: '{{ route('massediting.get_unit') }}', 
	        data: {
	        	code: code
	        },                         
	        type: 'post',
	        success: function(response){
	        	addRows(response);   
	        	$('#codeText').val('');
	        }
	    });

		/*
		var filteredData = unitsTable.rows()
		    .indexes()
		    .filter(function (value, index) {
		       return unitsTable.row(value).data()[0] == code; 
		    });

		unitsTable.rows(filteredData)
			.remove()
			.draw();
		*/
	}

	// штрихсканнер
    var typingBarcode = '';
    var barcode = '';
    var prevKey;

	$(document).on('keyup', function (e) {
        clearInterval(timer);

        var code = e.originalEvent.code;

        // letter or digit
        if(code.indexOf('Key') != -1) {
            typingBarcode += code.substring(3);
        }

        else if(code.indexOf('Digit') != -1) {
            typingBarcode += code.substring(5);
        }

        // $('#client_popup_scan').html(typingBarcode);

        if(code == 'ArrowDown' && prevKey == 'Enter') {
        	finishBarcodeEnter();
            
            return true;
        }

        prevKey = code;
        timer = setInterval(ticker, 500);
    });

    var timer = setInterval(ticker, 500);

    var ticker = function () {
        resetValues();
        clearInterval(timer);
    }

    function resetValues() {
        typingBarcode = '';
        prevKey = '';
    }

    function finishBarcodeEnter() {
        barcode = typingBarcode;
        addUnit(barcode);

        barcode = '';
        resetValues();
    }

    // подтверждение
    $('#store').click(function (event) {
		 event.preventDefault();
		 swal({   
            title: "Подтвердите действие",   
            text: "Изменить свойства " + codes.length + " единиц?",   
            type: "warning",   
            showCancelButton: true,   
            confirmButtonColor: "#DD6B55",   
            confirmButtonText: "Да",   
            cancelButtonText: "Отмена",   
        }).then(function(result){   
        	console.log(result);
        	if(result.value)
        	{
        		$('#massediting-form').submit();
        	}
        });
	});

    $(document).ready(function() {
		$(window).keydown(function(event){
			if(event.keyCode == 13) {
				if($('#codeText').val())
				{
					addUnit($('#codeText').val());
				}
				else
				{
					event.preventDefault();
				}
				return false;
			}
		});
	});

</script>
@endpush

@include('common.tabs', ['page' => 'massediting'])

<div class="row">
	<div class="col-md-12">
		<div class="card">
			<div class="card-body">
				<h4 class="card-title">
					Изменение признаков товаров
				</h4>
				<form id="massediting-form" method="POST" action="{{ route('massediting.store') }}">
					@csrf

					<input type="hidden" name="codes" id="codes">

					<div class="row">
						<div class="form-group col-6">
							<label>
								Список артикулов
							</label>
							<input class="form-control" id="codeFile" type="file" accept=".txt">
						</div>

						<div class="form-group col-6">
							<label>
								Ввод вручную
							</label>
							<input class="form-control" id="codeText" type="text">
						</div>
					</div>

					<div class="row">
						<div class="form-group col-4">
							<label for="location_id">Текущий склад</label>
							<select class="form-control select2" name="location_id" id="location_id" required>
								@foreach ($locations as $location)
								<option value="{{ $location->id }}">{{ $location->address }}</option>
								@endforeach
							</select>
						</div>

						<div class="form-group col-4">
							<label for="main_location">Основной склад</label>
							<select class="form-control select2" name="main_location" id="main_location" required>
								@foreach ($locations as $location)
								<option value="{{ $location->id }}">{{ $location->address }}</option>
								@endforeach
							</select>
						</div>

						<div class="form-group col-4">
							<label for="condition">Состояние</label>
							<select class="form-control select2" name="condition_id" id="condition_id" required>
								@foreach ($conditions as $condition)
								<option value="{{ $condition->id }}">{{ $condition->name }}</option>
								@endforeach
							</select>
						</div>
					</div>

					<hr>

					<table id="units-listing" class="table">
						<thead>
							<tr>
								<th>Артикул</th>
								<th>Категория</th>
								<th>Бренд</th>
								<th>Наименование</th>
								<th>Состояние</th>
								<th>Кол-во</th>
								<th>Статус</th>
							</tr>
						</thead>
						<tbody>

						</tbody>
					</table>

					<hr>

					<button type="button" id="store" class="btn btn-success">
						<i class="fa fa-save"></i>Далее
					</button>
				</form>
			</div>
		</div>

	</div>
</div>

@endsection
