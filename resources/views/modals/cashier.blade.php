<div class="modal fade" id="photoPopup_cashier" tabindex="1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content bg-white">
            <div class="modal-body text-center">
                <img style="z-index: 1060; position: absolute;top: 35px;left: 26px;" src="https://images.sftcdn.net/images/t_app-cover-l,f_auto/p/befbcde0-9b36-11e6-95b9-00163ed833e7/260663710/the-test-fun-for-friends-screenshot.jpg" id="previewPhoto_cashier">
                <div id="camera_cashier"></div>
                <button type="button" class="btn btn-info mt-2" id="takePhoto_cashier">Фото</button>
                <button type="button" class="btn btn-primary mt-2" id="savePhoto_cashier">Сохранить</button>
            </div>
        </div>
    </div>
</div>

<!-- barcode modal -->
<div class="modal fade" id="cashierPopup" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content bg-white">
            <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6 cashierPopupFirstColumn">
                            <table class="table table-bordered">
                                <tbody>
                                    <tr>
                                        <td>Имя</td>
                                        <td id="cashier_popup_full_name"></td>
                                    </tr>
                                    <tr>
                                        <td>Телефон</td>
                                        <td id="cashier_popup_phone"></td>
                                    </tr>
                                    <tr>
                                        <td>Договор</td>
                                        <td>
                                            <div class="badge badge-primary float-right ml-1 p-2" id="cashier_popup_group"></div>
                                            <div class="badge float-right p-2" id="cashier_popup_status"></div>
                                            <select class="form-control form-control-sm" id="cashier_popup_order_id" style="max-width: 140px;">
                                            </select> 
                                        </td>
                                    </tr>
                                </tbody>
                            </table>       
                        </div>

                        <div class="col-md-6 col-sm-12 cashierPopupSecondColumn">
                            <div class="row">
                                <div class="col-6">
                                    <button class="btn-block btn btn-light mb-2" id="webcam">Вебкамера</button>
                                    <a href="#" target="_blank">
                                        <img src="" id="cashier_popup_client_photo" class="img-fluid">
                                    </a>
                                </div>
                                <div class="col-6">
                                    <span class="mb-2">&nbsp;</button>
                                    <a href="#" target="_blank">
                                        <img src="" id="cashier_popup_client_passport" class="img-fluid">
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <hr />

                    <form name="cashier">
                        <div class="row">
                            <div class="form-group col-3" id="cashierPopupPledgeContainer">
                                <label>Залог</label>
                                <select class="form-control" id="cashier_popup_pledge">
                                    @foreach(\App\Order::$pledges as $pledge_id => $pledge)
                                    <option value="{{ $pledge_id }}">{{ $pledge }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group col-3" id="pledgeAmountContainer">
                                <label>Размер залога</label>
                                <input type="text" disabled class="form-control" id="cashier_popup_pledge_amount">
                            </div>

                            <!--
                            <div class="form-group col-3">
                                <label>Способ оплаты</label>
                                <select class="form-control" id="cashier_popup_paid_by">
                                    <option value="cash">Наличные</option>
                                    <option value="card">Карта</option>
                                </select>
                            </div>
                            -->

                            <div class="form-group col-3" id="cashierPopupPromocodeContainer">
                                <label>Промокод</label>
                                <div class="input-group col-xs-12">
                                    <input type="text" id="cashier_popup_promocode" class="form-control">
                                    <span class="input-group-append">
                                        <button class="btn btn-info" id="cashier_popup_apply_promocode" type="button">Применить</button>
                                    </span>
                                </div>
                            </div>

                            <div class="form-group col-3" id="cashierPopupAgentContainer">
                                <label>Агент</label>
                                <input type="text" id="cashier_popup_agent" class="form-control" disabled>
                            </div>
                        </div>
                    </form>


                    <div class="row">
                        <div class="form-group col-2">
                            <label>Срок аренды</label>
                            <select class="form-control" id="cashier_popup_duration">
                                @foreach(\App\PriceList::$timeline as $time)
                                <option value="{{ $time['duration'] }}{{ $time['dimension'] }}" data-dimension="{{ $time['dimension'] }}" data-duration="{{ $time['duration'] }}">{{$time['duration']}} {{ $time['ru'] }}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group col-2" id="cashierPopupStartsAtContainer">
                            <label>Начало аренды</label>
                            <input type="text" disabled class="form-control" id="cashier_popup_starts_at">
                        </div>

                        <div class="form-group col-2" id="cashierPopupEndsAtContainer">
                            <label>Окончание аренды</label>
                            <input type="text" disabled class="form-control" id="cashier_popup_ends_at">
                        </div>

                        <div class="form-group col-6">
                            <label>Заметка</label>
                            <div class="input-group">
                                <input type="text" id="cashier_popup_description" class="form-control">
                                <span class="input-group-append">
                                    <button class="file-upload-browse btn btn-primary" id="cashier_popup_save_description" type="button">
                                        Сохранить
                                    </button>
                                </span>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-6 col-md-3">
                            <div class="card border-primary border">
                            	<div class="row">
                            		<div class="col-12">
		                                <div class="card-body p-2 text-center">
		                                    <span id="cashier_popup_price_label">К оплате:</span>
		                                    <div id="cashier_popup_price">0</div>
		                                </div>
		                            </div>

		                            <!--
		                            <div class="col-6">
		                            	<div class="card-body p-2 text-center" id="cashierGroupPriceContainer">
	                                        Группа:
	                                        <div id="cashier_popup_group_price">–</div>
	                                    </div>
		                            </div>
		                        	-->
	                            </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6" id="cashier_popup_extra_card">
                            <div class="card border-danger border">
                                <div class="card-body p-3 text-center">
                                    Задолженность:
                                    <div id="cashier_popup_extra">0</div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-3 col-sm-12" id="cashierPopupTotalContainer">
	                        <div class="card border-success border">
	                            <div class="row">
	                                <div class="col-md-6 col-sm-12">
	                                    <div class="card-body p-2 text-center">
	                                        В прокате:
	                                        <div id="cashier_popup_units_count">–</div>
	                                    </div>
	                                </div>
	                                <div class="col-md-6 col-sm-12">
	                                    <div class="card-body p-2 text-center" id="cashierGroupUnitsCountContainer">
	                                        Группа:
	                                        <div id="cashier_popup_group_units_count">–</div>
	                                    </div>
	                                </div>
	                            </div>
	                        </div>
	                    </div>
                    </div>

                    <hr />

                    <button id="cashier_popup_pay" type="button" class="btn btn-outline-primary" style="min-width: 200px;">Оплатить</button>
                    <button id="cashier_popup_force_pay" type="button" class="btn btn-outline-primary" style="min-width: 200px;">Оплачен (принудительно)</button>
                    @if(currentUser()->location->kkt)
                    <button id="cashier_popup_receipt" type="button" class="btn btn-outline-warning">Печать чека</button>
                    @endif
                    <button id="cashier_popup_agreement" type="button" class="btn btn-outline-info">Договор</button>
                    <button id="cashier_popup_pledge_agreement" type="button" class="btn btn-outline-info">Договор на залог</button>
                    <button id="cashier_popup_end" type="button" class="btn btn-outline-success">Завершить</button>
                    <button id="cashier_popup_pay_extra" type="button" class="btn btn-outline-danger">Оплатить задолженность</button>

                    <button id="cashier_popup_refund" type="button" class="btn btn-outline-danger">Возврат</button>
                    <hr />

                    <h5>
                    Текущий заказ
	                </h5>

                    <div class="row" id="cashierInputContainer" style="display: none">
                        <div class="form-group col-md-6 col-sm-12">
                            <label>Ввод</label>
                            <div class="input-group">
                                <input style="font-size: 1.5rem; background: #f79700; letter-spacing: 4px;" type="text" name="cashierCodeInput" id="cashierCodeInput" class="form-control">
                                <span class="input-group-append">
                                    <button class="btn btn-success" type="button" id="cashierCodeInputButton">Добавить</button>
                                </span>
                            </div>
                        </div>
                    </div>

                    <div class="row" id="deliveryInputContainer" style="display: none">
                        <div class="form-group col-md-6 col-sm-12">
                            <label>Ввод</label>
                            <div class="input-group">
                                <input type="text" name="deliveryCodeInput" id="deliveryCodeInput" class="form-control">
                                <span class="input-group-append">
                                    <button class="btn btn-success" type="button" id="deliveryCodeInputButton">Добавить</button>
                                </span>
                            </div>
                        </div>
                    </div>

                    <div class="table-responsive">
                        <table id="order_table_cashier" class="table table-bordered">
                            <thead>
                                @role('delivery')
                                <tr>
                                    <th>Артикул</th>
                                    <th>Категория</th>
                                    <th>Наименование</th>
                                    <th>Стоимость</th> 
                                    <th>Длина</th>
                                    <th>Размер</th>
                                    <th>Колодка</th>
                                    <th></th>
                                </tr>
                                @else
                                <tr>
                                    <th>Артикул</th>
                                    <th>Категория</th>
                                    <th>Наименование</th>
                                    <th>Начало</th>
                                    <th>Стоимость</th> 
                                    <th>Задолженность</th> 
                                    <th>Срок</th>
                                    <th>Длина</th>
                                    <th>Размер</th>
                                    <th>Колодка</th>
                                    <th>Сумма возврата</th>
                                    <th>Возврат</th>
                                </tr>
                                @endrole
                            </thead>
                        </table>
                    </div>

	                <hr />

                    <div class="row" id="payments">
                    	<div class="col-6">
                            <label>История оплат</label>
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>Дата</th>
                                            <th>Сумма</th>
                                            <th>Тип</th>
                                        </tr>
                                    </thead>
                                    <tbody id="paymentsBody">

                                    </tbody>
                                </table>
                            </div>
	                    </div>
                    </div>

            </div>
        </div>
    </div>
</div>
<!-- endbarcode modal -->