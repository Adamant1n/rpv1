<!-- barcode modal -->
@push('js')
<script>

</script>
@endpush

<div class="modal fade" id="paymentPopup" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-md" role="document" style="max-width: 800px;">
        <div class="modal-content bg-white">
            <div class="modal-body">

            	<div class="form-group">
	            	<table id="payment_table" class="table table-bordered">
	                    <thead>
	                        <tr>
	                            <th>Артикул</th>
	                            <th>Категория</th>
	                            <th>Наименование</th>
	                            <th>Стоимость</th> 
	                        </tr>
	                    </thead>
	                </table>
	            </div>

                <div class="row mb-2">
                	<div class="col-6 text-center">
                		<h3 style="line-height: 5rem;">
                			СУММА К ОПЛАТЕ
                		</h3>
                	</div>

                	<div class="col-6">
	                	<div class="card border-primary border">
	                        <div class="card-body p-3 text-center">
	                            <h1 id="payment_popup_price" class="mb-0">0</h1>
	                        </div>
	                    </div>
	                </div>
                </div>

                <div class="row mb-4">
					@if(in_array(\App\Order::PAID_CASH, currentUser()->location->payment_types))
                	<div class="col-4">
                		<button class="btn btn-primary btn-block" id="payment_popup_paid_by_cash">Наличные</button>
                	</div>
					@endif

					@if(in_array(\App\Order::PAID_CARD, currentUser()->location->payment_types))
                	<div class="col-4">
                		<button class="btn btn-outline-success btn-block" id="payment_popup_paid_by_card">Банковская карта</button>
                	</div>
					@endif

					@if(in_array(\App\Order::PAID_INVOICE, currentUser()->location->payment_types))
					<div class="col-4">
                		<button class="btn btn-outline-warning btn-block" id="payment_popup_paid_by_invoice">Счет</button>
                	</div>
					@endif
                </div>

                <input type="hidden" name="payment_popup_paid_by" value="cash" id="payment_popup_paid_by">
                <input type="hidden" id="payment_popup_kkt"> 

                <div class="row" id="amountContainer">
                	<div class="form-group col-6">
                		<label><h4>ВНЕСЕНО</h4></label>
                		<input type="text" class="form-control" id="payment_popup_amount">
                	</div>
                	<div class="form-group col-6">
                		<label><h4>СДАЧА</h4></label>
                		<input type="text" class="form-control" id="payment_popup_change" disabled>
                	</div>
                </div>

                <hr />

                <button id="payment_popup_pay" type="button" class="btn btn-outline-primary">Оплатить</button>

            </div>
        </div>
    </div>
</div>
<!-- endbarcode modal -->