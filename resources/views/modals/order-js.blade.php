<script type="text/javascript">
    $(document).ready(function() {
        var inputStart;
        var inputEnd;
        var typingBarcode = '';
        var barcode = '';
        var prevKey;
        var clientData;
        var client_id;
        var order_id;
        var orderData;
        var atolStatusUuid = -1;
        var orderTable = $('#order_table').DataTable({
            "bSort": false,
            "bPaginate": false,
            "bFilter": false,
            "bInfo": false,
        });

        var paymentTable = $('#payment_table').DataTable({
        	"bSort": false,
        	"bPaginate": false,
        	"bFilter": false,
        	"bInfo": false
        });

        var orderTableCashier = $('#order_table_cashier').DataTable({
            "bSort": false,
            "bPaginate": false,
            "bFilter": false,
            "bInfo": false,
        });

        /** customer js **/
        function getFormData($form){
            var unindexed_array = $form.serializeArray();
            var indexed_array = {};

            $.map(unindexed_array, function(n, i){
                indexed_array[n['name']] = n['value'];
            });

            return indexed_array;
        }
        
        var customer_id = -1;
        var customer_code = -1;

        var atolActive = false;

        function setCustomerFormData(data)
        {
            var $form = $("#customerForm");
            customer_id = data.id;
            customer_code = data.optional_id;
            $form.find('input[name="first_name"]').val(data.first_name);
            $form.find('input[name="last_name"]').val(data.last_name);
            $form.find('input[name="mid_name"]').val(data.mid_name);
            $form.find('input[name="phone"]').val(data.phone);
            $form.find('input[name="email"]').val(data.email);
            $form.find('input[name="passport"]').val(data.passport);
            $form.find('input[name="birthday"]').val(data.birthday_short);
            $form.find('input[name="description"]').html(data.description);
            $form.find('input[name="weight"]').val(data.feature.weight);
            $form.find('input[name="height"]').val(data.feature.height);
            $form.find('input[name="shoe_size"]').val(data.feature.shoe_size);
            if(data.is_signed)
            {
                $form.find('input[name="is_signed"]').attr('checked', 'checked');
            }
            else
            {
                $form.find('input[name="is_signed"]').removeAttr('checked');
            }
        }

        $('#customerForm').on('submit', function(e) {
            e.preventDefault();
            saveCustomerData();
        })

        function saveCustomerData()
        {
            var $form = $("#customerForm");
            var data = getFormData($form);

            $.post('/clients/' + customer_id + '/ajax', data, function (response) {
                console.log(response);
                $('#customerPopup').modal('hide');
                barcode = customer_code;
                getClient();
            });
        }

        /***/

        var canEditPrice = {{ auth()->user()->can('edit-reservation-price') ? 'true' : 'false' }};
        var canEditPledgeAmount = {{ auth()->user()->can('edit-pledge-amount') ? 'true' : 'false' }};
        var canCancelOrder = {{ auth()->user()->can('cancel-order') ? 'true' : 'false' }};

        if(canEditPledgeAmount == true)
        {
            $('#clientPledgeAmountContainer').show();

            $('#savePledgeAmount').click(function () {
                $.ajax({
                    url: "{{ route('scanner.update') }}",
                    type: "get", 
                    data: { 
                        order_id: order_id,
                        pledge_amount: $('#client_popup_pledge_amount').val()
                    },
                })
                .done(function(data) {
                    clientData = data;

                    updateOrderData();
                });
            })

        }
        else
        {
            $('#clientPledgeAmountContainer').hide();
        }

        var htmlRub = ' <i class="fas fa-ruble-sign"></i>';

        $.fn.editableform.buttons =
		  '<button type="submit" class="btn btn-primary btn-sm editable-submit" style="height: 2rem;">'+
		    '<i class="fa fa-fw fa-check"></i>'+
		  '</button>'+
		  '<button type="button" class="btn btn-default btn-sm editable-cancel" style="height: 2rem;">'+
		    '<i class="fa fa-fw fa-times"></i>'+
		  '</button>';

        // загрузка списка клиентов
        var clientList = {};

        $(document).on('click', '.open-customer-popup', function () {
            $('#customerPopup').modal('show');
            setCustomerFormData(clientData);
        });

        // hack for modal focus
        jQuery.fn.modal.Constructor.prototype.enforceFocus = function () { };
        jQuery('#clientPopup').on('shown.bs.modal', function() {
            jQuery(document).off('focusin.modal');
        });

        jQuery('#customerPopup').on('shown.bs.modal', function() {
            jQuery(document).off('focusin.modal');
        });

        jQuery('#productPopup').on('shown.bs.modal', function() {
            jQuery(document).off('focusin.modal');
        });

        jQuery('#clientPopup').on('hidden.bs.modal', function () {
        	document.location.reload(true);
        });

        jQuery('#cashierPopup').on('hidden.bs.modal', function () {
            document.location.reload(true);
        });

        jQuery('#cashierPopup').on('shown.bs.modal', function() {
        	//initDatePickers();
            jQuery(document).off('focusin.modal');
        });

        var userRole = "{{ auth()->user()->getRoleNames()[0] }}";
        //console.log(userRole);

        if(userRole == 'delivery')
        {
            $('.cashierPopupSecondColumn').hide();
            $('.cashierPopupFirstColumn').removeClass('col-6');
            $('.cashierPopupFirstColumn').addClass('col-12');
            $('#cashierPopupPromocodeContainer').hide();
            $('#cashierPopupAgentContainer').hide();
            $('#cashierPopupPledgeContainer').removeClass('col-3');
            $('#cashierPopupPledgeContainer').addClass('col-6');
            $('#pledgeAmountContainer').removeClass('col-3');
            $('#pledgeAmountContainer').addClass('col-6');

            $('#cashierPopupStartsAtContainer').removeClass('col-3');
            $('#cashierPopupStartsAtContainer').addClass('col-6');

            $('#cashierPopupEndsAtContainer').removeClass('col-3');
            $('#cashierPopupEndsAtContainer').addClass('col-6');

            $('#cashierPopupTotalContainer').removeClass('col-3');
            $('#cashierPopupTotalContainer').addClass('col-6');      

            $('#orderTableReturnColumn').html('');  

            $('#deliveryInputContainer').show(); 
        }

        if(userRole == 'cashier')
        {
            $('#cashierInputContainer').show();
        }

        var location = {!! json_encode(auth()->user()->location) !!};

        var waitAfterScanning = false;

        $(document).on('keyup', function (e) {
            clearInterval(timer);

            var code = e.originalEvent.code;

            if(code == 'F8')
            {
            	showReplaceUnitModal();
            }

            if(waitAfterScanning) {
                console.log('задержка');
            }
            else {
                // letter or digit
                if(code.indexOf('Key') != -1) {
                    typingBarcode += code.substring(3);
                }

                else if(code.indexOf('Digit') != -1) {
                    typingBarcode += code.substring(5);
                }

                $('#client_popup_scan').html(typingBarcode);

                if(code == 'ArrowDown' && prevKey == 'Enter') {
                    if(!replaceMode)
                    {
                        finishBarcodeEnter();
                    }
                    
                    return true;
                }

                prevKey = code;
            }
            timer = setInterval(ticker, 500);
        });

        var timer = setInterval(ticker, 500);

        var ticker = function () {
            resetValues();
            clearInterval(timer);
        }

        // сброс баркода
        function resetValues() {
            typingBarcode = '';
            prevKey = '';
            $('#client_popup_scan').html('–');
        }

        var replaceMode = false;

        // модалка на замену инвентаря
        function showReplaceUnitModal()
        {	
        	replaceMode = true;
        	swal({
			 	title: "Введите артикулы заменяемого и нового инвентаря",
				html:
			    	'<input id="swal-input1" class="swal2-input">' +
			    	'<input id="swal-input2" class="swal2-input">',
			  	preConfirm: function () {
			    	return new Promise(function (resolve) {
			      		resolve([
			        		$('#swal-input1').val(),
			        		$('#swal-input2').val()
			      		])
			    	})
			  	},
			  	onOpen: function () {
			    	$('#swal-input1').focus()
			  	}
			}).then(function (result) {
				codes = result.value;
				console.log(result.value);

				$.ajax({
	                url: "{{ route('scanner.replace') }}",
	                type: "get", 
	                data: { 
	                    order_id: order_id,
	                    unit_1: codes[0],
	                    unit_2: codes[1]
	                },
	            })
	            .done(function(data) {
	            	//swal(data.message);
	            	if(data.success)
	            	{
	            		clientData.current_order = data.clientData;
	                	updateOrderData();
	            	}
	            });

				replaceMode = false;
				// swal(JSON.stringify(result))
			}).catch(swal.noop)
        }

        // окно выбора количества неуникальных товаров
        function chooseCount(code, client_id, order_id)
        {
            var mCode = code;
            var mClient_id = client_id;
            var mOrder_id = order_id;

            //var count = prompt('Выберите количество единиц инвентаря', 1);
            //toggleUnitCount(count, mCode, mClient_id, mOrder_id);

            var count = 1;

            swal({
                title: "Выберите количество единиц инвентаря",
                input: 'number',
                inputAttributes: {
                    value: 1,
                    min: 1,
                },
                showCancelButton: true,
                confirmButtonText: 'Добавить',
                cancelButtonText: 'Удалить',
                onOpen: function(swal) {
                    $(swal).find('.swal2-input').val(1);
                    $(swal).find('.swal2-cancel').off().click(function(e) {
                        count = $('.swal2-input');
                    });
                }
            })
            .then((result) => {
                if(result.dismiss)
                {
                    toggleUnitCount('remove', $('.swal2-input').val(), mCode, mClient_id, mOrder_id);
                }
                else 
                {
                    toggleUnitCount('add', result.value, mCode, mClient_id, mOrder_id);
                }
            });
        }

        $(document).on('click', '.btn-remove', function () {
            toggleUnitCount('remove', 1, $(this).data('unit'), clientData.id, clientData.current_order.id);
        });

        // toggle unit with count
        function toggleUnitCount(action, count, code, client_id, order_id)
        {
            $.ajax({
                url: "{{ route('scanner.unit') }}",
                type: "get", 
                data: { 
                    barcode: code, 
                    client_id: client_id,
                    order_id: order_id,
                    count: count,
                    action: action
                },
            })
            .done(function(data) {
                clientData = data;
                updateOrderData();
            });
        }

        // окно подтверждения создания группы
        function confirmGroup()
        {
            // если групповой - проверка на наличие человека в группе
            if(clientData.current_order.group)
            {
                c_id = parseInt(barcode.replace(/[^-0-9]/gim, ''));
                for(var i = 0; i < clientData.current_order.group.orders.length; i++)
                {
                    if(clientData.current_order.group.orders[i].client_id == c_id)
                    {
                        getClient();

                        barcode = '';
                        resetValues();
                        return true;
                        break;
                    }
                }

            }

            swal({
                title: "Добавить клиента в группу договоров?",
                showCancelButton: true,
            })
            .then((result) => {

                if (!result.dismiss) {
                    addToGroup(barcode);
                } else {
                    // console.log('cancel group');
                    getClient();
                }

                barcode = '';
                resetValues();
            });
        }

        // добавление клиента в группу договоров
        function addToGroup(code)
        {
            $.ajax({
                url: "{{ route('scanner.add_to_group') }}",
                type: "get", 
                data: { 
                    client_id: code,
                    order_id: order_id
                },
            })
            .done(function(data) {
                console.log(data);
                if(data != false) {
                    clientData = data;
                    client_id = data.id;
                    order_id = data.current_order.id;
                    updateClientData();
                    updateOrderData();
                }
                else
                {
                    swal('Клиент уже находится в группе');
                }
            });
        }

        // окончание ввода кода
        function finishBarcodeEnter() {
            waitAfterScanning = true;

            barcode = typingBarcode;
            console.log('barcode: ' + barcode);

            // открыть клиента
            if(barcode.substring(0, 1) === 'L')
            {
                // если окно закрыто, открыть клиента
                if(!$('#clientPopup').is(':visible')) {
                    getClient();
                }
                // если окно открыто, предложить добавить в договор
                else {
                    //confirmGroup();
                    getClient();
                    barcode = '';
                	resetValues();
                    return true;
                }
            }

            // применить промокод
            else if(barcode.substring(0, 1) === 'P')
            {
                getPromocode(barcode);
            }

            // открыть заказ
            else if(barcode.substring(0, 1) === 'D' || barcode.substring(0, 1) === 'R' || barcode.substring(0, 1) === 'S') {
                getOrder(parseInt(barcode.replace(/[^-0-9]/gim, '')));
            }

            // открыть заказ по групповому ID ???
            else if(barcode.substring(0, 2) === 'GD') {
                getOrder(parseInt(barcode.replace(/[^-0-9]/gim, '')));
            }

            // аттач детач единицы товара
            else {
                // загрузка заказа по юниту если не открыто модальное окно
                if(!$('#clientPopup').is(':visible') && !$('#cashierPopup').is(':visible')) {
                	console.log('get order by unit');
                    getOrderByUnit(barcode);
                }
                else {
                    toggleUnit();
                }
            }

            barcode = '';
            resetValues();

            setTimeout(() => {
                waitAfterScanning = false;
            }, 1000);
        }

        // открыть карту заказа по инвентарю
        function getOrderByUnit(code) {
            $.ajax({
                url: "{{ route('scanner.client') }}",
                type: "get", 
                data: { 
                    unit_id: code
                },
            })
            .done(function(data) {
                if(data != false) {
                    clientData = data;
                    client_id = data.id;
                    order_id = data.current_order.id;
                    updateClientData();
                    updateOrderData();
                    
                    if(userRole == 'cashier' || userRole == 'delivery') $('#cashierPopup').modal('show');
                    else $('#clientPopup').modal('show');
                }
            });

            // getOrder(parseInt(id.replace(/[^-0-9]/gim, '')));
        }

        // открыть карту заказа
        function getOrder(id) {
            order_id = id;
            $.ajax({
                url: "{{ route('scanner.client') }}",
                type: "get", 
                data: { 
                    order_id: order_id,
                    cashier: (userRole == 'cashier' || userRole == 'delivery') ? 1 : 0
                },
            })
            .done(function(data) {
                clientData = data;
                client_id = data.id;
                

                updateClientData();
                updateOrderData();
                
                if(userRole == 'cashier' || userRole == 'delivery') {
                    getAtolStatus();
                    $('#cashierPopup').modal('show');
                }
                else $('#clientPopup').modal('show');
            });
        }

        // открыть карту заказа
        if($('#order_table tbody').length) {
            $('#order_table tbody').on('click', 'td', function (e) {
                if(e.target != this) return;
                // var id = $('td', this).eq(0).text();

                var id = $(this).closest('tr').find("td:first").text();

                if($(this).parent().hasClass('table-success'))
                {
                	swal({
					 	title: "Выберите состояние инвентаря",
					 	input: 'select',
						inputOptions: {
						    {{ \App\Unit::CONDITION_OK }}: 'ОК',
						    {{ \App\Unit::CONDITION_BROKEN }}: 'Сломан',
						    {{ \App\Unit::CONDITION_LOST }}: 'Утерян'
						},
						showCancelButton: true,
                        confirmButtonText: 'Подтвердить',
                        cancelButtonText: 'Отмена'
					}).then(function (result) {

                        if(result.dismiss) {
                            return;
                        }

						var status_ru = 'OK';
						if(result.value == '{{ \App\Unit::CONDITION_OK }}')
						{
							status_ru = 'OK';
						}
						else if(result.value == '{{ \App\Unit::CONDITION_BROKEN }}')
						{
							status_ru = 'Сломан';
						}
						else
						{
							status_ru = 'Утерян';
						}

						var condition_id = result.value;

						swal({
			                title: "Вы уверены в смене статуса инвентаря " + id + " на " + status_ru +  "?",
			                showCancelButton: true,
                            confirmButtonText: 'Подтвердить',
                            cancelButtonText: 'Отмена'
			            })
			            .then((result) => {

                            console.log('code 2', id);

			                if (result.dismiss) {
                                return
			                }

                            $.ajax({
                                url: "{{ route('scanner.update_condition') }}",
                                type: "get", 
                                data: { 
                                    code: id,
                                    condition_id: condition_id
                                },
                            })
                            .done(function(data) {
                                console.log(data);
                            });
			                
			            });

						
					}).catch(swal.noop)
                }
            });
        }

        // изменить статус возвращенного инвентаря
        $('#orders-listing tbody').on('click', 'tr', function () {
			var id = $('td', this).eq(0).text();
			// $(location).attr('href','/orders/'+parseInt(id.replace(/[^-0-9]/gim, '')));
			getOrder(parseInt(id.replace(/[^-0-9]/gim, '')));
		});

        // карта заказа из меню клиента
        if($('#client_current_order').length) {
            $('#client_current_order').on('click', function () {
                var id = $(this).data('id');
                getOrder(parseInt(id.replace(/[^-0-9]/gim, '')));
            });
        }

        // новый заказ из меню клиента
        if($('#client_new_order').length) {
            $('#client_new_order').on('click', function () {
                barcode = $(this).data('id');
                getClient();
                barcode = '';
            });
        }

        // новая доставка
        if($('#client_new_delivery').length) {
	        $("#client_new_delivery").click(function () {
	        	client_id = $(this).data('id');
	            createDelivery();
	        });
	    }

        // новая бронь
        if($('#client_new_booking').length) {
	        $("#client_new_booking").click(function () {
	        	client_id = $(this).data('id');
	            createBooking();
	        });
	    }

	    // сохранение адреса
	    $('#client_popup_save_address').click(function () {
	    	$.ajax({
                url: "{{ route('scanner.update') }}",
                type: "get", 
                data: { 
                    order_id: order_id,
                    address: $('#client_popup_address').val()
                },
            })
            .done(function(data) {
                clientData = data;

                updateOrderData();
            });
	    });

	    var timepicker = $('#timepicker-example');
	    var datepicker = $('#datepicker-popup');

	    initDatePickers();

	    var timeUpdatedManually = true;

	    function initDatePickers()
	    {
			timepicker.datetimepicker({
		    	use24hours: true,
		        format: 'HH:mm',
		        stepping: 10,
		    });

		    timepicker.on('change.datetimepicker', function(e) {
		    	console.log(e);
		    	if(timeUpdatedManually)
		    	{
		    		updateBookDate();
		    	}
		        
		        timeUpdatedManually = true;
		        console.log('d upd2');
		    });

		    datepicker.datepicker({
				enableOnReadonly: true,
				todayHighlight: false,
				format: 'dd.mm.yyyy'
			});

			datepicker.datepicker().on('changeDate', function (e) {
				updateBookDate();
				console.log('d upd1');
			});
	    }

		function updateBookDate()
		{
			$.ajax({
                url: "{{ route('scanner.update') }}",
                type: "get", 
                data: { 
                    order_id: order_id,
                    book_at: $('#book_at_date').val() + ' ' + timepicker.datetimepicker('viewDate').format('HH:mm')
                },
            })
            .done(function(data) {
                clientData = data;

                updateOrderData();
            });
		}

        $(document).on('click', '.product-link', function () {
            console.log($(this).data('photo'));
            $('#product-popup-photo').attr('src', $(this).data('photo'));
            $('#productPopup').modal('show');
        });

        // создание нового договора брони
        function createBooking()
        {
            $.ajax({
                url: "{{ route('scanner.create_order') }}",
                type: "get", 
                data: { 
                    client_id: client_id,
                    booking: true
                },
            })
            .done(function(data) {
                $('#clientPopup').modal('show');
                clientData = data;
                order_id = data.current_order.id;
                updateOrderData();      
                updateClientData();        
            });
        }

        // создание новой доставки
        function createDelivery()
        {
            $.ajax({
                url: "{{ route('scanner.create_order') }}",
                type: "get", 
                data: { 
                    client_id: client_id,
                    delivery: true
                },
            })
            .done(function(data) {
                $('#clientPopup').modal('show');
                clientData = data;
                order_id = data.current_order.id;
                updateOrderData();      
                updateClientData();        
            });
        }

        // статус укомплектован
        $('#client_popup_staffed').click(function () {
        	$.ajax({
                url: "{{ route('scanner.update') }}",
                type: "get", 
                data: { 
                    order_id: order_id,
                    status: 'staffed'
                },
            })
            .done(function(data) {
                clientData = data;

                updateOrderData();
            });
        });

        // открыть заказ из меню юнита ???
        if($('#unit_order').length) {
            $('#unit_order').on('click', function () {
                var id = $(this).data('order');
                getOrder(id);
            });
        }

        // ручной ввод артикула
        $('#codeInput').on('keypress', function (e) {
            if(e.which === 13){
                barcode = $('#codeInput').val();
                typingBarcode = barcode;
                toggleUnit();
                $('#codeInput').val('');
            }
        });

        $('#deliveryCodeInputButton').click(function () {
            barcode = $('#deliveryCodeInput').val();
            typingBarcode = barcode;
            // toggleUnit();
            toggleUnitCount('add', 1, barcode, clientData.id, clientData.current_order.id);
            $('#deliveryCodeInput').val('');
        });

        $('#cashierCodeInputButton').click(function () {
            barcode = $('#cashierCodeInput').val();
            typingBarcode = barcode;
            // toggleUnit();
            // toggleUnitCount('add', 1, barcode, clientData.id, clientData.current_order.id);
            finishBarcodeEnter();
            // $('#cashierCodeInput').val('');
        });

        // получение промокода
        function getPromocode (promocode) {
            $.ajax({
                url: "{{ route('scanner.promocode') }}",
                type: "get", 
                data: { 
                    order_id: order_id,
                    promocode: promocode, 
                },
            })
            .done(function(data) {
                clientData.current_order = data;
                updateOrderData();
            });
        }

        // получение клиента
        function getClient() {
            $.ajax({
                url: "{{ route('scanner.client') }}",
                type: "get", 
                data: { 
                    barcode: barcode, 
                    cashier: (userRole == 'cashier' || userRole == 'delivery') ? 1 : 0
                },
            })
            .done(function(data) {
                clientData = data;
                client_id = data.id;
                order_id = data.current_order.id;
                updateClientData();
                updateOrderData();
                
                if(userRole == 'cashier' || userRole == 'delivery') $('#cashierPopup').modal('show');
                else $('#clientPopup').modal('show');
            });
        }

        // update client data
        function updateClientData() {
            // $('#client_popup_full_name').html(clientData.full_name);
            //$('#client_popup_phone').html(clientData.phone);
            //$('#client_popup_description').html(clientData.description);

            $('#cashier_popup_client_photo').attr('src', clientData.images.photo);
            $('#cashier_popup_client_photo').parent().attr('href', clientData.images.photo);
            $('#cashier_popup_client_passport').attr('src', clientData.images.passport);
            $('#cashier_popup_client_passport').parent().attr('href', clientData.images.passport);

            if(clientData.images.photo != '')
            {
                $('#cashier_popup_client_photo').show();
            }
            else
            {
                $('#cashier_popup_client_photo').hide();
            }

            if(clientData.images.passport != '')
            {
                $('#cashier_popup_client_passport').show();
            }
            else
            {
                $('#cashier_popup_client_passport').hide();
            }

            $('#client_popup_feature_height').html(clientData.feature.height);
            $('#client_popup_feature_weight').html(clientData.feature.weight);
            $('#client_popup_feature_shoe_size').html(clientData.feature.shoe_size);

            $('#client_popup_order_id').find('option').remove();
            $('#cashier_popup_order_id').find('option').remove();

            // cashier popup
            $('#cashier_popup_full_name').html('<a data-customer=\'' + JSON.stringify(clientData) + '\' onclick="openCustomerPopup()" href="#">' + clientData.full_name + '</a>');
            $('#cashier_popup_phone').html(clientData.phone);
            $('#cashier_popup_description').html(clientData.description);

            for(var i = 0; i < clientData.orders.length; i++) {
                var selected = clientData.orders[i].id == order_id;
                $('#client_popup_order_id').append(new Option(clientData.orders[i].optional_id, clientData.orders[i].id, selected, selected));

                console.log(clientData.orders[i].optional_id)

                $('#cashier_popup_order_id').append(new Option(clientData.orders[i].optional_id, clientData.orders[i].id, selected, selected));
            }
        }

        // update order data
        function updateOrderData() {

            // текущий тариф
            $('#client_popup_pricelist').val(clientData.current_order.current_price_list.name);

            // примечание
            $('#client_popup_description').val(clientData.current_order.description);
            $('#cashier_popup_description').val(clientData.current_order.description);

        	// селектор договоров для группового
        	$('#client_popup_select_client').find('option').remove();
            // $('#client_popup_select_client_container').hide();
            $('#client_popup_full_name').hide();

            console.log('update order data');

            // $('#client_popup_units_count').html(clientData.current_order.reservations.length);
            $('#client_popup_units_count').html(clientData.current_order.active_reservations_count);
            $('#cashier_popup_units_count').html(clientData.current_order.active_reservations_count);

            if(clientData.current_order.group_id != undefined)
            {
                $('#client_popup_pledge_amount').val(clientData.current_order.pledge_amount);
            	// оплаты
            	if(clientData.current_order.group.payments.length > 0)
            	{
            		$('#payments').show();
            		$('#paymentsBody').html('');
            		console.log()
            		$.each(clientData.current_order.group.payments, function (index, payment) {
            			$('#paymentsBody').append('<tr>');

            			$('#paymentsBody').append('<td>' + payment.created_at + '</td>');
            			$('#paymentsBody').append('<td>' + payment.sum + 'руб. </td>');
            			$('#paymentsBody').append('<td>' + payment.type_info + ' (' + payment.optional_id + ')</td>');

            			$('#paymentsBody').append('</tr>');
            		});
            	}
            	else
            	{
            		$('#payments').hide();
            	}

            	//
                console.log('is group');

                $('#groupPriceContainer').show();
                $('#groupUnitsCountContainer').show();
                $('#cashierPriceContainer').show();
                $('#cashierGroupUnitsCountContainer').show();

                $('#client_popup_group_price').html(clientData.current_order.group.price + htmlRub);
                $('#cashier_popup_group_price').html(clientData.current_order.group.price + htmlRub);
                $('#client_popup_group_units_count').html(clientData.current_order.group.active_reservations_count);
                $('#cashier_popup_group_units_count').html(clientData.current_order.group.active_reservations_count);

            	$('#client_popup_remove_client').show();
            	$('#client_popup_full_name').hide();
            	$('#client_popup_select_client_container').show();
                $('#clientsTableBody').html('');

            	for(var i = 0; i < clientData.current_order.group.orders.length; i++)
            	{
            		var selected = false;

            		if(clientData.current_order.group.orders[i].client.id == clientData.current_order.client.id){
            			selected = true;
            		}

            		$('#client_popup_select_client').append(new Option(
            			clientData.current_order.group.orders[i].client.optional_id,
            			clientData.current_order.group.orders[i].id,
            			selected, selected));

                    console.log('add row for' + clientData.current_order.group.orders[i].client.full_name);

                    if(clientData.current_order.group.orders[i].client.id == clientData.current_order.client.id)
                        var row = '<tr> <td><strong class="text-primary">';
                    else if(clientData.current_order.group.client_id == clientData.current_order.group.orders[i].client.id)
                        var row = '<tr> <td><strong class="text-danger">';
                    else 
                        var row = '<tr> <td><strong>';

                    row += '<a class="open-customer-popup" data-customer=\'' + JSON.stringify(clientData.current_order.group.orders[i].client) + '\' href="#">';

                    row += clientData.current_order.group.orders[i].client.full_name + '</a>';
                    row += '</strong></td><td>Рост</td><td>Вес</td><td>Размер обуви</td>';
                    row += '<td><div class="badge badge-primary float-right ml-1">';
                    row += clientData.current_order.group.orders[i].client.optional_id;
                    row += '</div></td>';
                    row += '<td></td>';
                    row += '<td><button data-id="' + clientData.current_order.group.orders[i].client.id + '" type="button" class="btn btn-outline-danger py-1 removeClient"><i class="fa fa-times m-0"></i></button></td>';
                    row += '</tr>';

                    $('#clientsTableBody').append(row);

                    var row = '<tr> <td>';
                    row += clientData.current_order.group.orders[i].client.phone;
                    row += '</td><td>';
                    row += clientData.current_order.group.orders[i].client.feature.height;
                    row += '</td><td>';
                    row += clientData.current_order.group.orders[i].client.feature.weight;
                    row += '</td><td>';
                    row += clientData.current_order.group.orders[i].client.feature.shoe_size + ' (' + clientData.current_order.group.orders[i].client.feature.foot_length + ')';
                    row += '</td>';
                    row += '<td>';
                    row += '';
                    row += '</td></tr>';

                    $('#clientsTableBody').append(row);

            	}
            }
            else
            {
            	// оплаты
            	if(clientData.current_order.payments.length > 0)
            	{
            		$('#payments').show();
            		$('#paymentsBody').html('');
            		$.each(clientData.current_order.payments, function (index, payment) {
            			$('#paymentsBody').append('<tr>');

            			$('#paymentsBody').append('<td>' + payment.created_at + '</td>');
            			$('#paymentsBody').append('<td>' + payment.sum + 'руб. </td>');
            			$('#paymentsBody').append('<td>' + payment.type_info + '</td>');

            			$('#paymentsBody').append('</tr>');
            		});
            	}
            	else
            	{
            		$('#payments').hide();
            	}

            	//
                $('#client_popup_group_units_count').html('–');
                $('#cashier_popup_group_units_count').html('–');
                $('#groupPriceContainer').hide();
                $('#cashierGroupPriceContainer').hide();
                $('#groupUnitsCountContainer').hide();
                $('#cashierGroupUnitsCountContainer').hide();

                console.log('is not a group');
            	$('#client_popup_remove_client').hide();
            	var selected = false;
            	$('#client_popup_select_client').append(new Option(
            			clientData.full_name,
            			clientData.id,
            			selected, selected));

                $('#clientsTableBody').html('');

                var row = '<tr> <td><strong class="text-primary">';
                row += '<a data-customer=\'' + JSON.stringify(clientData) + '\' class="open-customer-popup" href="#">';
                row += clientData.full_name + '</a>';
                row += '</strong></td><td>Рост</td><td>Вес</td><td>Размер обуви</td>';
                row += '<td><div class="badge badge-primary float-right ml-1">';
                row += clientData.optional_id;
                row += '</div></td>';
                row += '<td></td>';
                row += '<td></td>';
                row += '</tr>';

                $('#clientsTableBody').append(row);

                var row = '<tr> <td>';
                row += clientData.phone;
                row += '</td><td>';
                row += clientData.feature.height;
                row += '</td><td>';
                row += clientData.feature.weight;
                row += '</td><td>';
                row += clientData.feature.shoe_size + ' (' + clientData.feature.foot_length + ')';;
                row += '</td>';
                row += '<td>';
                row += '';
                row += '</td></tr>';

                $('#clientsTableBody').append(row);
            }

            var totalPrice = clientData.current_order.price;

            if(clientData.current_order.online_order)
            {
                totalPrice -= clientData.current_order.online_order.paid_amount;
            }

        	// z num
            $('#client_popup_pledge_amount').val(clientData.current_order.pledge_amount);
        	$('#client_popup_z_num').html(clientData.current_order.z_num);
            // client popup
            $('#client_popup_type').val(clientData.current_order.type);
            // $('#client_popup_paid_by').val(clientData.current_order.paid_by);
            $('#client_popup_duration').val(clientData.current_order.duration + clientData.current_order.dimension);
            $('#cashier_popup_duration').val(clientData.current_order.duration + clientData.current_order.dimension);
            // $('#client_popup_pledge').val(clientData.current_order.pledge);
            $('#client_popup_starts_at').val(clientData.current_order.starts_at);
            $('#client_popup_ends_at').val(clientData.current_order.ends_at);
            $('#cashier_popup_ends_at').val(clientData.current_order.ends_at);
            $('#client_popup_price').html(totalPrice + htmlRub);

            // cashier popup
            $('#cashier_popup_paid_by').val(clientData.current_order.paid_by);
            $('#cashier_popup_pledge').val(clientData.current_order.pledge);

            $('#cashier_popup_pledge_amount').val(clientData.current_order.pledge_amount);

            if(clientData.current_order.pledge == 'cash')
            {
            	$('#pledgeAmountContainer').show();
            }
            else
            {
            	$('#pledgeAmountContainer').hide();
            }

            $('#cashier_popup_starts_at').val(clientData.current_order.starts_at);
            $('#cashier_popup_ends_at').val(clientData.current_order.ends_at);

            $('#cashier_popup_price').html(totalPrice + htmlRub);
            $('#client_popup_address').val(clientData.current_order.address);

            /*
            if(clientData.current_order.kkt){
                $("#cashier_popup_kkt").prop('checked', 1);
            }
            else {
                $("#cashier_popup_kkt").prop('checked', 0);
            }
            */

            // hide pay button if price = 0

            // promocode
            if(clientData.current_order.agent != undefined) {
                //$('#client_popup_promocode').val(clientData.current_order.agent.promocode);
                $('#client_popup_agent').val(
                    clientData.current_order.agent.name + ', ' +
                    clientData.current_order.discount + '%'
                );

                //$('#cashier_popup_promocode').val(clientData.current_order.agent.promocode);
                $('#cashier_popup_agent').val(
                    clientData.current_order.agent.name + ', ' +
                    clientData.current_order.discount + '%'
                );
            }
            else {
                $('#cashier_popup_promocode').val('');
                $('#cashier_popup_agent').val('');
            }

            $('#client_popup_promocode').val('');

            orderTable.clear().draw();
            orderTableCashier.clear().draw();

            $('#onlineOrderData').hide();

            if(clientData.current_order.online_order)
            {
                $('#onlineOrderData').show();
                $('#onlineOrderItems').html(clientData.current_order.online_order.assignedProductsHtml);
            }

            for(var i = 0; i < clientData.current_order.reservations.length; i++)
            {
                if(!clientData.current_order.reservations[i].unit.feature_1) clientData.current_order.reservations[i].unit.feature_1 = '';
                if(!clientData.current_order.reservations[i].unit.feature_4) clientData.current_order.reservations[i].unit.feature_4 = '';
                if(!clientData.current_order.reservations[i].unit.feature_3) clientData.current_order.reservations[i].unit.feature_3 = '';
                if(!clientData.current_order.reservations[i].returned_at) clientData.current_order.reservations[i].returned_at = '';

                var binding = clientData.current_order.reservations[i].unit.binding ?  clientData.current_order.reservations[i].unit.binding.name : '';
                var size = clientData.current_order.reservations[i].unit.feature_5 ? clientData.current_order.reservations[i].unit.feature_5 : clientData.current_order.reservations[i].unit.feature_3;

                var rowNode = orderTable.row.add([
                        clientData.current_order.reservations[i].unit.code,
                        clientData.current_order.reservations[i].unit.product.category.name,
                        '<a href="#" class="product-link" data-photo="' + clientData.current_order.reservations[i].unit.product.photo_url + '">' + clientData.current_order.reservations[i].unit.product.name + '</a>',
                        clientData.current_order.reservations[i].starts_at,
                        '<a href="#" id="xeditable-' + i + '">' + clientData.current_order.reservations[i].price + '</a>',
                        '<a href="#" id="debt-xeditable-' + i + '">' + clientData.current_order.reservations[i].price_extra + '</a>',
                        clientData.current_order.reservations[i].term,
                        clientData.current_order.reservations[i].unit.feature_1,
                        size,
                        clientData.current_order.reservations[i].unit.feature_4,
                        clientData.current_order.reservations[i].returned_at,
                    ]).draw(false).node();

                if(canEditPrice)
                {
	                $('#xeditable-' + i).editable({
				        type: 'text',
				        title: 'Введите сумму',
				        pk: clientData.current_order.reservations[i].id,
				        url: '/scan/set_price',
				        ajaxOptions:{
	                        type:'get',
	                        // dataType: 'json'
	                    },
	                    success: function (data, value) {
	                    	clientData.current_order = data;
	                    	updateOrderData();
	                    	// return value;
	                    },
	                    error: function (data, value) {
	                    	return false;
	                    }
			      	});

                      $('#debt-xeditable-' + i).editable({
				        type: 'text',
				        title: 'Введите сумму',
				        pk: clientData.current_order.reservations[i].id,
				        url: '/scan/set_price_extra',
				        ajaxOptions:{
	                        type:'get',
	                        // dataType: 'json'
	                    },
	                    success: function (data, value) {
	                    	clientData.current_order = data;
	                    	updateOrderData();
	                    	// return value;
	                    },
	                    error: function (data, value) {
	                    	return false;
	                    }
			      	});
	            }

                // returned
                if (clientData.current_order.reservations[i].replaced)
                {
                	$(rowNode).addClass('table-warning');
                }
                else if (clientData.current_order.reservations[i].status == 'returned') {
                    $(rowNode).addClass('table-success');
                }
                else if (clientData.current_order.reservations[i].status == 'cancelled') {
                    $(rowNode).addClass('table-danger');
                }
                else if(clientData.current_order.reservations[i].starts_at == undefined) {
                    $(rowNode).addClass('table-primary');
                }
                else if (clientData.current_order.reservations[i].status == 'rent' && clientData.current_order.status == 'paid' || clientData.current_order.status == 'ended')
                {
                    $(rowNode).addClass('table-danger');
                }
                else
                {
                	$(rowNode).addClass('table-danger');
                }

                // cashier table

                if(userRole == 'delivery')
                {
                    var newRow = [
                        clientData.current_order.reservations[i].unit.code,
                        clientData.current_order.reservations[i].unit.product.category.name,
                        clientData.current_order.reservations[i].unit.product.name,
                        clientData.current_order.reservations[i].price + htmlRub,
                        clientData.current_order.reservations[i].unit.feature_1,
                        size,
                        clientData.current_order.reservations[i].unit.feature_4,
                        clientData.current_order.reservations[i].status != 'returned' ? '<button type="button" class="btn btn-sm btn-remove btn-danger" data-unit="' + clientData.current_order.reservations[i].unit.code + '">Удалить</button>' : '',
                    ]
                }
                else
                {
                    var newRow = [
                    //console.log([
                        clientData.current_order.reservations[i].unit.code,
                        clientData.current_order.reservations[i].unit.product.category.name,
                        clientData.current_order.reservations[i].unit.product.name,
                        //clientData.current_order.reservations[i].starts_at,
                        clientData.current_order.reservations[i].starts_at,
                        clientData.current_order.reservations[i].price + htmlRub,
                        clientData.current_order.reservations[i].price_extra + htmlRub,
                        
                        clientData.current_order.reservations[i].term,
                        clientData.current_order.reservations[i].unit.feature_1,
                        size,
                        clientData.current_order.reservations[i].unit.feature_4,
                        clientData.current_order.reservations[i].refund + htmlRub,
                        clientData.current_order.reservations[i].returned_at,
                    ];
                }

                var rowNode = orderTableCashier.row.add(newRow).draw(false).node();

                // returned
                if (clientData.current_order.reservations[i].replaced)
                {
                	$(rowNode).addClass('table-warning');
                }
                else if (clientData.current_order.reservations[i].status == 'returned') {
                    $(rowNode).addClass('table-success');
                }
                else if (clientData.current_order.reservations[i].status == 'cancelled') {
                    $(rowNode).addClass('table-danger');
                }
                else if(clientData.current_order.reservations[i].starts_at == undefined) {
                    $(rowNode).addClass('table-primary');
                }
                else if (clientData.current_order.reservations[i].status == 'rent' && clientData.current_order.status == 'paid' || clientData.current_order.status == 'ended')
                {
                    $(rowNode).addClass('table-danger');
                }
                else
                {
                	$(rowNode).addClass('table-danger');
                }
            }

            // обновление времени начала бронирования

            if(clientData.current_order.type == 'book' || clientData.current_order.type == 'delivery')
            {
            	var book_at = moment(clientData.current_order.book_at);
            	
            	if(clientData.current_order.type == 'book')
	            {
	            	$('#client_popup_book_at_container').show();
	            }

	            if(clientData.current_order.type == 'delivery')
	            {
	            	$('#client_popup_book_at_container').show();
	            }

	            var date = book_at.format('DD.MM.YYYY');
	            var time = book_at.format('HH:mm');

	            timeUpdatedManually = false;

            	datepicker.datepicker('update', date);
            	timepicker.datetimepicker('date', time);
            	
            }
            else{
            	$('#client_popup_book_at_container').hide();
            }

            updateStatus();
        }

        // добавление юнита
        function toggleUnit() {
            var code = barcode;
            $.ajax({
                url: "{{ route('scanner.unit') }}",
                type: "get", 
                data: { 
                    barcode: barcode, 
                    client_id: client_id,
                    order_id: order_id
                },
            })
            .done(function(data) {
                if(data.is_unique == undefined) 
                {
                	if(data.success == false)
                	{
                		swal(data.message)
                	}
                	else
                	{
                		clientData = data;
                    	updateOrderData();
                	}
                }
                else 
                {
                    chooseCount(code, client_id, order_id);
                }
                
                // $('#clientPopup').modal('show');
            });
        }

        // сохранение примечания
        $('#client_popup_save_description').on('click', function(){
            $.ajax({
                url: "{{ route('scanner.update') }}",
                type: "get", 
                data: { 
                    order_id: clientData.current_order.id,
                    description: $('#client_popup_description').val()
                },
            })
            .done(function(data) {
                clientData = data;

                updateOrderData();
            });
        });

        $('#cashier_popup_save_description').on('click', function(){
            $.ajax({
                url: "{{ route('scanner.update') }}",
                type: "get", 
                data: { 
                    order_id: clientData.current_order.id,
                    description: $('#cashier_popup_description').val()
                },
            })
            .done(function(data) {
                clientData = data;

                updateOrderData();
            });
        });

        $('#cashier_popup_duration').change(function () {
            $.ajax({
                url: "{{ route('scanner.update') }}",
                type: "get", 
                data: { 
                    order_id: order_id,
                    duration: $('#cashier_popup_duration').find(':selected').data('duration'),
                    dimension: $('#cashier_popup_duration').find(':selected').data('dimension'),
                },
            })
            .done(function(data) {
                clientData = data;

                updateOrderData();
            });
        });

        // изменение данных формы
        $('form[name=order]').on('change paste', 'input, select, textarea', function(){
        	console.log($(this).prop('id'));
        	if($(this).prop('id') == 'client_popup_select_client')
        	{
        		return false;
        	}

            $.ajax({
                url: "{{ route('scanner.update') }}",
                type: "get", 
                data: { 
                    order_id: order_id,
                    duration: $('#client_popup_duration').find(':selected').data('duration'),
                    dimension: $('#client_popup_duration').find(':selected').data('dimension'),
                    // type: $('#client_popup_type').val()
                },
            })
            .done(function(data) {
                clientData = data;

                updateOrderData();
            });
        });

        // изменение данных формы оплаты
        $('form[name=cashier]').on('keyup change paste', 'input, select, textarea', function(){

            if($(this).prop('id') == 'cashier_popup_promocode' || $(this).prop('id') == 'cashier_popup_agent')
        	{
        		return false;
        	}

            $.ajax({
                url: "{{ route('scanner.update') }}",
                type: "get", 
                data: { 
                    order_id: order_id,
                    pledge: $('#cashier_popup_pledge').val(),
                    paid_by: $('#cashier_popup_paid_by').val(),
                    //kkt: $("#cashier_popup_kkt").is(':checked') ? 1 : 0
                },
            })
            .done(function(data) {
                clientData = data;

                updateOrderData();
            });
        });

        // загрузка договора
        $('#client_popup_order_id').change(function () {
            $.ajax({
                url: "{{ route('scanner.order') }}",
                type: "get", 
                data: { 
                    order_id: $(this).val() 
                },
            })
            .done(function(data) {
                clientData.current_order = data;
                order_id = data.id;
                updateOrderData();              
            });
        });

        // загрузка договора из группового
        $('#client_popup_select_client').change(function () {
            $.ajax({
                url: "{{ route('scanner.order') }}",
                type: "get", 
                data: { 
                    order_id: $(this).val() 
                },
            })
            .done(function(data) {
            	clientData = data.client;
                clientData.current_order = data;

                order_id = data.id;
                updateOrderData();      
                updateClientData(); 
            });
        });

        // удалить клиента из группового договора
        $('#client_popup_remove_client').click(function () {
        	$.ajax({
                url: "{{ route('scanner.remove_from_group') }}",
                type: "get", 
                data: { 
                    client_id: clientData.id,
                    order_id: clientData.current_order.id
                },
            })
            .done(function(data) {
                if(data != false) {
                    clientData = data;
                    client_id = data.id;
                    //order_id = data.current_order.id;
                    updateClientData();
                    updateOrderData();
                }
            });
        });

        $(document). on('click', '.removeClient', function () {
            var clientId = $(this).data('id');
            $.ajax({
                url: "{{ route('scanner.remove_from_group') }}",
                type: "get", 
                data: { 
                    client_id: clientId,
                    order_id: clientData.current_order.id
                },
            })
            .done(function(data) {
                if(data != false) {
                    clientData = data;
                    client_id = data.id;
                    //order_id = data.current_order.id;
                    updateClientData();
                    updateOrderData();
                }
            });
        });

        // добавить клиента в групповой договор
        $('#client_popup_add_client').click(function () {
                swal({
                    title: "Выберите клиента",
                    input: 'text',
                    showCancelButton: true,
                    showConfirmButton: false,
                    onOpen: function () {
                        /*
                        $('.sw-select').select2({
                            minimumResultsForSearch: 15,
                            width: '100%',
                        });
                        */
                    },
                }).then(function (result) {
                    if(!result.dismiss)
                    {
                        addToGroup(result.value);
                    }
                    
                }).catch(swal.noop)

            /*
        	$.ajax({
	            url: "{{ route('scanner.get_clients') }}",
	            data: {
	            	order_id: clientData.current_order.id
	            },
	            type: "get", 
	        })
	        .done(function(data) {
	        	clientList = data;

	        	html = '<select class="sw-select">';
	        	for(var i = 0; i < clientList.length; i++)
	        	{
	        		html += '<option value="' + clientList[i]['id'] + '">' + clientList[i]['name'] + '</option>';
	        	}

	        	html += '</select>';

	        	swal({
				 	title: "Выберите клиента",
				 	html: html,
				 	//input: 'select',
					//inputOptions: clientList,
					showCancelButton: true,
					onOpen: function () {
                        $('.sw-select').select2({
                            minimumResultsForSearch: 15,
                            width: '100%',
                        });
                    },
				}).then(function (result) {
                    console.log(result);
                    if(result)
                    {
                        addToGroup($('.sw-select').val());
                    }
					
				}).catch(swal.noop)
	        });
            */
        });

        // проставить стоимость договора вручную
        $('#client_popup_set_price_manual').on('click', function () {
            var price = clientData.current_order.price;
            swal({
                title: "Введите сумму договора",
                input: 'number',
                inputAttributes: {
                    min: 0,
                    value: price,
                },
                showCancelButton: true,
                confirmButtonText: 'Подтвердить',
                cancelButtonText: 'Отменить',
            }).then(function (result) {
                console.log(result);
                if(result.dismiss)
                {
                    console.log('cancel');
                }
                else if(result.value)
                {
                    $.ajax({
                        url: "{{ route('scanner.set_price') }}",
                        type: "get", 
                        data: { 
                            order_id: clientData.current_order.id,
                            price: result.value
                        },
                    })
                    .done(function(data) {
                        console.log(data);
                        clientData.current_order = data;
                        order_id = data.id;
                        updateOrderData();              
                    });
                }
            }).catch(swal.noop)
        })

        // загрузка договора кассиром
        $('#cashier_popup_order_id').change(function () {
            $.ajax({
                url: "{{ route('scanner.order') }}",
                type: "get", 
                data: { 
                    order_id: $(this).val(),
                    cashier: (userRole == 'cashier' || userRole == 'delivery') ? 1 : 0
                },
            })
            .done(function(data) {
                clientData.current_order = data;
                order_id = data.id;
                updateOrderData();              
            });
        });

        // создание нового договора
        function createOrder()
        {
            $.ajax({
                url: "{{ route('scanner.create_order') }}",
                type: "get", 
                data: { 
                    client_id: client_id
                },
            })
            .done(function(data) {
                clientData = data;
                order_id = data.current_order.id;
                updateOrderData();      
                updateClientData();        
            });
        }

        $('#cashier_popup_pay_extra').click(function () {
            // payExtra();
            $('#paymentPopup').modal('show');
            $('#payment_popup_price').html($('#cashier_popup_price').html());
            updatePaymentTable();
        });

        // отмена заказа
        function cancelOrder()
        {
            $.ajax({
                url: "{{ route('scanner.cancel_order') }}",
                type: "get", 
                data: { 
                    order_id: order_id
                },
            })
            .done(function(data) {
                clientData.current_order = data;

                updateOrderData();      
            });
        }

        function updateStatus()
        {
            // групповой договор
            $('#client_popup_group').hide();
            $('#cashier_popup_group').hide();

            if(clientData.current_order.group_id != undefined)
            {
                $('#client_popup_group').html(clientData.current_order.group.optional_id);
                $('#client_popup_group').show();

                $('#cashier_popup_group').html(clientData.current_order.group.optional_id);
                $('#cashier_popup_group').show();
            }

            $('#client_popup_staffed_container').hide();
            $('#client_popup_address_container').hide();

            if(clientData.current_order.type == 'delivery')
            {
	            $('#client_popup_address_container').show();
            }



            if(clientData.current_order.type == 'book' || clientData.current_order.type == 'delivery' && clientData.current_order.status == 'draft')
            {
            	$('#client_popup_staffed_container').show();
            }


            // сюда же закинуть обработку полей промокод, дисейбл полей и тд
            $('#client_popup_status').removeClass('badge-primary');
            $('#client_popup_status').removeClass('badge-outline-primary');
            $('#client_popup_status').removeClass('badge-danger');
            $('#client_popup_status').removeClass('badge-success');
            $('#client_popup_status').removeClass('badge-primary');

            // cashier status badge
            $('#cashier_popup_status').removeClass('badge-primary');
            $('#cashier_popup_status').removeClass('badge-outline-primary');
            $('#cashier_popup_status').removeClass('badge-danger');
            $('#cashier_popup_status').removeClass('badge-success');
            $('#cashier_popup_status').removeClass('badge-primary');

            $('#cashier_popup_agreement').hide();
            $('#client_popup_cancel_order').hide();
            $('#cashier_popup_receipt').hide();
            console.log('HIDE');

            $('#cashier_popup_pay_extra').hide();
            $('#cashier_popup_refund').hide();
            $('#cashier_popup_pay').hide();
            $('#cashier_popup_force_pay').hide();
            $('#cashier_popup_end').hide();
            $('#cashier_popup_pledge_agreement').hide();
            $('#cashier_popup_extra_card').hide();
            $('#cashier_popup_duration').attr('disabled', 'disabled');
            // $('#client_popup_paid_by').hide();
            $('#cashier_popup_duration option').each(function () {
                $(this).show();
            });

            $('#client_popup_paid_by').attr('disabled', 'disabled');
            $('#client_popup_pledge').attr('disabled', 'disabled');
            $('#client_popup_duration').attr('disabled', 'disabled');
            $('#client_popup_pledge').attr('disabled', 'disabled');
            $('#client_popup_type').attr('disabled', 'disabled');

            $('#client_popup_duration option').each(function () {
                $(this).show();
            });

            $('#cashier_popup_price_label').html('К оплате:');
            $('#client_popup_price_label').html('К оплате:');

            $('#client_popup_set_price_manual').hide();

            if(clientData.current_order.pledge == 'cash')
            {
                if(userRole != 'delivery')
                {
                    $('#cashier_popup_pledge_agreement').show();
                }
            }

            if(clientData.current_order.status == 'draft') {
                $('#client_popup_set_price_manual').show();
                $('#client_popup_paid_by').removeAttr('disabled');
                $('#client_popup_pledge').removeAttr('disabled');
                $('#client_popup_duration').removeAttr('disabled');
                $('#client_popup_pledge').removeAttr('disabled');
                $('#client_popup_type').removeAttr('disabled');
                $('#cashier_popup_duration').removeAttr('disabled');

                $('#client_popup_status').addClass('badge-outline-primary');
                $('#client_popup_status').html('Новый');

                $('#cashier_popup_status').addClass('badge-outline-primary');
                $('#cashier_popup_status').html('Новый');
                
                if(clientData.current_order.price == 0) {
                    $('#cashier_popup_pay').hide();
                    $('#cashier_popup_force_pay').hide();
                    // $('#client_popup_paid_by').hide();
                }
                else {
                    $('#cashier_popup_pay').show();

                    if(clientData.current_order.uuid) {
                        $('#cashier_popup_force_pay').show();
                    }
                    // $('#client_popup_paid_by').show();
                }

                // убрать ненужные тарифы

                $('#cashier_popup_duration option').each(function () {
                    if($(this).data('dimension') == 'm' & location.tariff_step != '10m') {
                        $(this).hide();
                    }
                    if($(this).data('dimension') == 'h' & location.tariff_step == '1d') {
                        $(this).hide();
                    }
                });

                $('#client_popup_duration option').each(function () {
                    if($(this).data('dimension') == 'm' & location.tariff_step != '10m') {
                        $(this).hide();
                    }
                    if($(this).data('dimension') == 'h' & location.tariff_step == '1d') {
                        $(this).hide();
                    }
                });

                if(canCancelOrder)
                {
                    $('#client_popup_cancel_order').show();
                }
            }
            if(clientData.current_order.status == 'staffed') {
                $('#client_popup_status').addClass('badge-info');
                $('#client_popup_status').html('Укомплектован');

                $('#cashier_popup_status').addClass('badge-info');
                $('#cashier_popup_status').html('Укомплектован');
                $('#client_popup_type').removeAttr('disabled');

                if(clientData.current_order.price == 0) {
                    $('#cashier_popup_pay').hide();
                    $('#cashier_popup_force_pay').hide();
                    // $('#client_popup_paid_by').hide();
                }
                else {
                    $('#cashier_popup_pay').show();

                    if(clientData.current_order.uuid) {
                        $('#cashier_popup_force_pay').show();
                    }
                    // $('#client_popup_paid_by').show();
                }
            }
            console.log('STATUS')
            console.log(clientData.current_order.status);
            if(clientData.current_order.status == 'paid') {
                $('#cashier_popup_price_label').html('К оплате:');
                $('#client_popup_price_label').html('К оплате:');

                // $('#cashier_popup_price').html(clientData.current_order.price_paid + htmlRub);
                // $('#client_popup_price').html(clientData.current_order.price_paid + htmlRub);
                $('#cashier_popup_price').html('0' + htmlRub);
                $('#client_popup_price').html('0' + htmlRub);
                /*
                if(clientData.current_order.price_extra != 0)
                {
                	$('#cashier_popup_price_label').html('Задолженность:');
                	$('#client_popup_price_label').html('Задолженность:');

                	$('#cashier_popup_price').html(clientData.current_order.price_extra + htmlRub);
                	$('#client_popup_price').html(clientData.current_order.price_extra + htmlRub);
                }
                */

                $('#client_popup_status').addClass('badge-success');
                $('#client_popup_status').html('Аренда');

                if(userRole != 'delivery')
                {
                    $('#cashier_popup_agreement').show();
                    //$('#cashier_popup_receipt').show();
                }
                console.log('SHOW');

                $('#cashier_popup_status').addClass('badge-success');
                $('#cashier_popup_status').html('Аренда');

                // проверка на возвращенные товары
                var returned = true;
                for(var i = 0; i < clientData.current_order.reservations.length; i++)
                {  
                    if(clientData.current_order.reservations[i].status == 'rent') returned = false;
                }

                // проверка группы
                if(clientData.current_order.group != undefined)
                {
                    for(var i = 0; i < clientData.current_order.group.reservations.length; i++)
                    {  
                        if(clientData.current_order.group.reservations[i].status == 'rent') returned = false;
                    }
                }

                if(returned && clientData.current_order.price_extra == 0)
                {
                    if(userRole != 'delivery')
                    {
                        $('#cashier_popup_end').show();
                    }
                }
                else if(clientData.current_order.price_extra != 0)
                {
                    if(userRole != 'delivery')
                    {
                        $('#cashier_popup_pay_extra').show();
                    }
                }

                if(clientData.current_order.refund > 0)
                {
                    if(userRole != 'delivery')
                    {   
                        $('#cashier_popup_refund').show();
                        $('#cashier_popup_refund').html('Возврат (' + clientData.current_order.refund + ' руб.)');
                    }
                }

                console.log('extra', clientData.current_order.price_extra);

                if(clientData.current_order.price_extra > 0) {
                	/*
                    $('#cashier_popup_extra_card').show();
                    $('#cashier_popup_extra').html(clientData.current_order.price_extra + htmlRub);
                    */

                    $('#cashier_popup_price_label').html('К оплате:');
	                $('#cashier_popup_price').html(clientData.current_order.price_extra + htmlRub);

                    $('#client_popup_price_label').html('К оплате:');
                    $('#client_popup_price').html(clientData.current_order.price_extra + htmlRub);
                }
            }
            if(clientData.current_order.status == 'cancelled') {
                $('#client_popup_status').addClass('badge-danger');
                $('#client_popup_status').html('Отменен');

                $('#cashier_popup_status').addClass('badge-danger');
                $('#cashier_popup_status').html('Отменен');
            }
            if(clientData.current_order.status == 'ended') {
                $('#cashier_popup_price_label').html('Оплачено:');
                $('#client_popup_price_label').html('Оплачено:');
                $('#cashier_popup_price').html(clientData.current_order.price_paid + htmlRub);
                $('#client_popup_price').html(clientData.current_order.price_paid + htmlRub);

                $('#client_popup_status').addClass('badge-primary');
                $('#client_popup_status').html('Завершен');

                $('#cashier_popup_status').addClass('badge-primary');
                $('#cashier_popup_status').html('Завершен');
            }
        }

        // создать заказ
        $("#client_popup_new_order").click(function () {
            createOrder();
        });

        // отменить заказ
        $('#client_popup_cancel_order').click(function () {
            cancelOrder();
        });

        // оплатить заказ
        function pay()
        {
            $.ajax({
                url: "{{ route('scanner.pay') }}",
                type: "get", 
                data: { 
                    order_id: order_id,
                    role: userRole,
                },
            })
            .done(function(data) {
                clientData.current_order = data;
                clientData.feature = data.client.feature;

                updateOrderData();      
            });
        }

        // принудительно оплатить заказ
        function forcePay()
        {
            $.ajax({
                url: "{{ route('scanner.force_pay') }}",
                type: "get", 
                data: { 
                    order_id: order_id,
                    role: userRole,
                },
            })
            .done(function(data) {
                clientData.current_order = data;
                // clientData.feature = data.client.feature;

                updateOrderData();      
            });
        }

        function refund()
        {
            $.ajax({
                url: "{{ route('scanner.refund') }}",
                type: "get", 
                data: { 
                    order_id: order_id
                },
            })
            .done(function(data) {
                clientData.current_order = data;
                clientData.feature = data.client.feature;

                updateOrderData();      
            });
        }

        function payExtra()
        {
            $.ajax({
                url: "{{ route('scanner.pay_extra') }}",
                type: "get", 
                data: { 
                    order_id: order_id
                },
            })
            .done(function(data) {
                clientData.current_order = data;
                clientData.feature = data.client.feature;

                updateOrderData();      
            });
        }

        $('#cashier_popup_force_pay').click(function () {
            forcePay();
        });

        function getAtolStatus() {
            $.ajax({
                url: "{{ route('atol.status.request') }}",
                type: "get", 
            })
            .done(function(data) {
                console.log(data);
                atolStatusUuid = data.uuid;
                pingAtolStatus();
            });
        }

        function pingAtolStatus() {
            var pingAtolInterval = setInterval(() => {
                $.ajax({
                url: "/atol_status/" + atolStatusUuid,
                type: "get", 
            })
            .done(function(data) {
                atolActive = data.active;

                if (!atolActive) {
                    console.log('inactive', data);
                }
                else {
                    console.log('active', data);
                    $('#payment_popup_pay').removeAttr('disabled');
                    $('#cashier_popup_refund').removeAttr('disabled');
                    $('#cashier_popup_receipt').removeAttr('disabled');
                    clearInterval(pingAtolInterval);
                }
            });
            }, 2000);
        }

        $('#cashier_popup_refund').prop('disabled', true);
        $('#cashier_popup_receipt').prop('disabled', true);

        $('#cashier_popup_pay').click(function () {
            $('#payment_popup_pay').prop('disabled', true);
            //pay();

            // getAtolStatus();

            $('#paymentPopup').modal('show');
            $('#payment_popup_price').html($('#cashier_popup_price').html());
            updatePaymentTable();

        });

        $('#payment_popup_pay').click(function () {
            if(clientData.current_order.status == 'paid') {
                payExtra();
            }
            else {
        	    pay();
            }
        	$('#paymentPopup').modal('hide');
        });

        $('#cashier_popup_refund').click(function () {
            refund();
        });

        // печать договора
        $('#cashier_popup_agreement').click(function () {
            var url = "/orders/" + order_id + "/agreement";
            // printPdf(url);
            let w = window.open(url);
            //w.print();
            //w.close();
        });

        $('#cashier_popup_pledge_agreement').click(function () {
            var url = "/orders/" + order_id + "/pledge";
            let w = window.open(url);
        });

        // печать чека
        $('#cashier_popup_receipt').click(function () {
            $.ajax({
                url: "{{ route('scanner.print_receipt') }}",
                type: "get", 
                data: { 
                    order_id: order_id
                },
            })
            .done(function(data) {
                console.log(data);
                clientData.current_order = data;

                updateOrderData();      
            });
        });

        // стикер
        $('#client_popup_sticker').click(function () {
        	var url = "/orders/" + order_id + "/sticker";
        	/*
        	

            window.open(url, '_blank');
            */

            printImg(url);
        });

        // закрыть заказ
        $('#cashier_popup_end').click(function () {
            $.ajax({
                url: "{{ route('scanner.end') }}",
                type: "get", 
                data: { 
                    order_id: order_id
                },
            })
            .done(function(data) {
                clientData.current_order = data;

                updateOrderData();      
            });
        });

        // применить промокод из формы
        $('#client_popup_apply_promocode').click(function () {
            getPromocode($('#client_popup_promocode').val());
            $('#client_popup_promocode').val('');
        });

        $('#cashier_popup_apply_promocode').click(function () {
            getPromocode($('#cashier_popup_promocode').val());
            $('#cashier_popup_promocode').val('');
        });

        var width = 448;
        var height = 330;
        var photo;

        $('#webcam').click(function () {
            $('#photoPopup_cashier').modal('show');
        });

        $('#previewPhoto_cashier').attr('width', width);
        $('#previewPhoto_cashier').attr('height', height);
        $('#previewPhoto_cashier').hide();

        jQuery('#photoPopup_cashier').on('shown.bs.modal', function() {
            Webcam.set({
                width: width,
                height: height,
                image_format: 'png',
                jpeg_quality: 100
            });

            Webcam.attach('#camera_cashier');
        });

        $('#takePhoto_cashier').on('click', function () {
            Webcam.snap( function(data_uri) {
                $('#previewPhoto_cashier').attr('src', data_uri);
                $('#previewPhoto_cashier').show();
                photo = data_uri;
                /*
                document.getElementById('results').innerHTML = 
                    '<h2>Here is your image:</h2>' + 
                    '<img src="'+data_uri+'"/>';
                */
            });
        });

        $('#previewPhoto_cashier').click(function () {
            $(this).hide();
        });

        $('#savePhoto_cashier').on('click', function () {
            $('#photoPopup_cashier').modal('hide');
            // $('#photo_webcam').val(photo)
            Webcam.upload(photo, 'clients/webcam/' + clientData.id, function(code, text) {
                $('#cashier_popup_client_photo').attr('src', JSON.parse(text).path);
                $('#cashier_popup_client_photo').show();
            });
        });

        $(document).on('show.bs.modal', '.modal', function (event) {
            var zIndex = 1040 + (10 * $('.modal:visible').length);
            $(this).css('z-index', zIndex);
            console.log('set', zIndex);
            setTimeout(function() {
                $('.modal-backdrop').not('.modal-stack').css('z-index', zIndex - 1).addClass('modal-stack');
            }, 0);
        });

        function setPaymentMethod(type)
        {
        	if(type == 'cash')
        	{
        		$('#payment_popup_paid_by_cash').removeClass('btn-outline-primary');
	        	$('#payment_popup_paid_by_cash').addClass('btn-primary');
	        	$('#payment_popup_paid_by_card').removeClass('btn-success');
	        	$('#payment_popup_paid_by_card').addClass('btn-outline-success');
                $('#payment_popup_paid_by_invoice').removeClass('btn-warning');
	        	$('#payment_popup_paid_by_invoice').addClass('btn-outline-warning');
	        	$('#payment_popup_paid_by').val('cash');
        	}
        	else if(type == 'card')
        	{
        		$('#payment_popup_paid_by_cash').addClass('btn-outline-primary');
	        	$('#payment_popup_paid_by_cash').removeClass('btn-primary');
	        	$('#payment_popup_paid_by_card').addClass('btn-success');
	        	$('#payment_popup_paid_by_card').removeClass('btn-outline-success');
                $('#payment_popup_paid_by_invoice').removeClass('btn-warning');
	        	$('#payment_popup_paid_by_invoice').addClass('btn-outline-warning');
	        	$('#payment_popup_paid_by').val('card');
        	}
            else if(type == 'invoice')
        	{
        		$('#payment_popup_paid_by_cash').addClass('btn-outline-primary');
	        	$('#payment_popup_paid_by_cash').removeClass('btn-primary');
	        	$('#payment_popup_paid_by_card').addClass('btn-outline-success');
	        	$('#payment_popup_paid_by_card').removeClass('btn-success');
                $('#payment_popup_paid_by_invoice').removeClass('btn-outline-warning');
	        	$('#payment_popup_paid_by_invoice').addClass('btn-warning');
	        	$('#payment_popup_paid_by').val('invoice');


        	}
        }

        function updatePaymentMethod()
        {
        	/*
        	$.ajax({
                url: "{{ config('app.rent') . '/scan/update' }}",
                type: "get", 
                data: { user_id: {{ auth()->user()->id }},
                    order_id: order_id,
                    paid_by: $('#cashier_popup_paid_by').val(),
                },
            })
            .done(function(data) {
                clientData = data;

                updateOrderData();
            });
            */

            $.ajax({
                url: "{{ route('scanner.update') }}",
                type: "get", 
                data: { 
                    order_id: order_id,
                    paid_by: $('#payment_popup_paid_by').val(),
                },
            })
            .done(function(data) {
                clientData = data;

                updateOrderData();
            });
        }

        $('#payment_popup_paid_by_cash').click(function () {
        	setPaymentMethod('cash');
        	updatePaymentMethod();
        });

        $('#payment_popup_paid_by_card').click(function () {
        	setPaymentMethod('card');
        	updatePaymentMethod();
        });

        $('#payment_popup_paid_by_invoice').click(function () {
            $('#payment_popup_amount').val(clientData.current_order.price);
            $('#payment_popup_change').val(0);
        	setPaymentMethod('invoice');
        	updatePaymentMethod();
        });

        $('#payment_popup_amount').on('input change input paste', function () {
        	var amount = $(this).val();
        	var change = amount - clientData.current_order.price;
        	if(change < 0) change = 0;
            
            console.log('change', amount, clientData.current_order.price)

        	$('#payment_popup_change').val(change);
        });

        function updatePaymentTable()
        {
        	paymentTable.clear().draw();

            for(var i = 0; i < clientData.current_order.reservations.length; i++)
            {
                if(!clientData.current_order.reservations[i].unit.feature_1) clientData.current_order.reservations[i].unit.feature_1 = '';
                if(!clientData.current_order.reservations[i].unit.feature_4) clientData.current_order.reservations[i].unit.feature_4 = '';
                if(!clientData.current_order.reservations[i].unit.feature_3) clientData.current_order.reservations[i].unit.feature_3 = '';
                if(!clientData.current_order.reservations[i].returned_at) clientData.current_order.reservations[i].returned_at = '';

                var binding = clientData.current_order.reservations[i].unit.binding ?  clientData.current_order.reservations[i].unit.binding.name : '';
                var size = clientData.current_order.reservations[i].unit.feature_5 ? clientData.current_order.reservations[i].unit.feature_5 : clientData.current_order.reservations[i].unit.feature_3;

                // cashier table
                var rowNode = paymentTable.row.add([
                //console.log([
                    clientData.current_order.reservations[i].unit.code,
                    clientData.current_order.reservations[i].unit.product.category.name,
                    clientData.current_order.reservations[i].unit.product.name,
                    clientData.current_order.reservations[i].price + htmlRub,

                ]).draw(false).node();

                // returned
                if (clientData.current_order.reservations[i].replaced)
                {
                	$(rowNode).addClass('table-warning');
                }
                else if (clientData.current_order.reservations[i].status == 'returned') {
                    $(rowNode).addClass('table-success');
                }
                else if (clientData.current_order.reservations[i].status == 'cancelled') {
                    $(rowNode).addClass('table-danger');
                }
                else if(clientData.current_order.reservations[i].starts_at == undefined) {
                    $(rowNode).addClass('table-primary');
                }
                else if (clientData.current_order.reservations[i].status == 'rent' && clientData.current_order.status == 'paid' || clientData.current_order.status == 'ended')
                {
                    $(rowNode).addClass('table-danger');
                }
                else
                {
                	$(rowNode).addClass('table-danger');
                }
            }
        }
    });
</script>

<script src="{{ asset('js/webcam.js') }}"></script>