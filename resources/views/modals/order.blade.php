<style>
.modal .table th, .modal .table td{
	padding: 5px;
}

.modal-body {
	padding: 15px !important;
}

.modal .modal-dialog {
	margin-top: 20px !important;
}
</style>

<!-- barcode modal -->
<div class="modal fade" id="clientPopup" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content bg-white">
            <div class="modal-body">

                <div class="row">
                    <!-- левый блок -->

                    <div class="col-8">
                        <div class="row">
                            
                            <!-- current order -->
                            <div class="form-group col-4">
                                <label>Договор</label>
                                
                                <div class="badge badge-primary float-right ml-1" id="client_popup_group"></div>
                                <div class="badge float-right" id="client_popup_status"></div>
                                <select class="form-control" id="client_popup_order_id">
                                </select>
                            </div>

                            <!-- new order -->
                            <div class="form-group col-2">
                                <button id="client_popup_new_order" type="button" class="mt-4 px-2 btn btn-outline-success">Новый договор</button>
                            </div>

                            <!-- cancel order -->
                            <div class="form-group col-2">
                                <button id="client_popup_cancel_order" type="button" class="mt-4 btn btn-outline-danger">
                                    <i class="fa fa-times"></i>
                                </button>
                            </div>

                            <div class="form-group col-3">
                                <button class="btn btn-primary mt-4" type="button" id="client_popup_sticker">Стикер</button>
                            </div>

                        </div>

                        <div class="row">
                            <!-- клиент -->
                            <div class="form-group col-6">
                                <div class="input-group">
                                    <select id="client_popup_select_client" class="form-control"></select>
                                    <span class="input-group-append">
                                        <button class="file-upload-browse btn btn-primary" id="client_popup_add_client" type="button">
                                            <i class="fa fa-plus"></i>
                                        </button>
                                        <!--
                                        <button class="file-upload-browse btn btn-danger" id="client_popup_remove_client" style="display: none" type="button">Удалить</button>
                                        -->
                                    </span>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <!-- клиенты -->
                            <div class="col-10">
                                <table class="table" id="clientsTable">
                                    <tbody id="clientsTableBody">
                                        <tr>
                                            <td>
                                                <strong class="text-primary">Иванов Иван Иванович</strong>
                                            </td>
                                            <td>Рост</td>
                                            <td>Вес</td>
                                            <td>Размер обуви</td>
                                            <td>
                                                <div class="badge badge-primary float-right ml-1">D001234</div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>+7 (911) 215-01-120</td>
                                            <td>195 см</td>
                                            <td>65 кг</td>
                                            <td>46</td>
                                            <td></td>
                                        </tr>
                                    </tbody>
                                </table> 
                            </div>
                        </div>
                    </div>

                    <!-- правый блок -->
                    <div class="col-4">

                        <div class="form-group">
                            <label>Промокод</label>
                            <div class="input-group col-xs-12">
                                <input type="text" id="client_popup_promocode" class="form-control">
                                <span class="input-group-append">
                                    <button class="btn btn-info" id="client_popup_apply_promocode" type="button">Применить</button>
                                </span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label>Агент</label>
                            <input type="text" id="client_popup_agent" class="form-control" disabled>
                        </div>

                        <div class="row">
                            <div class="col-12">
                                <div class="card border-primary border">
                                    <div class="row">

                                        <div class="col-6">
                                            <div class="card-body p-3 text-center">
                                                <span id="client_popup_price_label">К оплате:</span>
                                                <div id="client_popup_price">0</div>
                                                <!--
                                                <button class="btn btn-xs py-1 btn-outline-primary" id="client_popup_set_price_manual">Изменить</button>
                                            	-->
                                            </div>
                                        </div>

                                        <div class="col-6" id="groupPriceContainer">
                                            <div class="card-body p-3 text-center">
                                                Группа:
                                                <div id="client_popup_group_price">0</div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <!--
                            <div class="col-6">
                                <div class="card border-info border">
                                    <div class="card-body p-3 text-center">
                                        Сканирование:
                                        <div id="client_popup_scan">–</div>
                                    </div>
                                </div>
                            </div>
                            -->
                        </div>

                    </div>
                </div>

                <!--
                <hr />
            	-->

                <div class="row">
                    <div class="form-group col-3">
                        <label>Ввод</label>
                        <input type="text" name="codeInput" id="codeInput" class="form-control">
                    </div>

                    <div class="form-group col-3">
                        <label>Прейскурант</label>
                        <input type="text" id="client_popup_pricelist" disabled class="form-control">
                    </div>

                    <div class="form-group col-6">
                    	<label>Заметка</label>
                        <div class="input-group">
                            <input type="text" id="client_popup_description" class="form-control">
                            <span class="input-group-append">
                                <button class="file-upload-browse btn btn-primary" id="client_popup_save_description" type="button">
                                    Сохранить
                                </button>
                            </span>
                        </div>
                    </div>
                </div>

                <form name="order">

                <div class="row">
                    <div class="form-group col-2">
                        <label>Срок аренды</label>
                        <select class="form-control" id="client_popup_duration">
                            @foreach(\App\PriceList::$timeline as $time)
                            <option value="{{ $time['duration'] }}{{ $time['dimension'] }}" data-dimension="{{ $time['dimension'] }}" data-duration="{{ $time['duration'] }}">{{$time['duration']}} {{ $time['ru'] }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-2">
                        <label>Начало аренды</label>
                        <input type="text" disabled class="form-control" id="client_popup_starts_at">
                    </div>
                    <div class="form-group col-2">
                        <label>Окончание аренды</label>
                        <input type="text" disabled class="form-control" id="client_popup_ends_at">
                    </div>

                    <div class="col-3">
                        <div class="card border-success border">
                            <div class="row">
                                <div class="col-6">
                                    <div class="card-body p-2 text-center">
                                        В прокате:
                                        <div id="client_popup_units_count">–</div>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="card-body p-2 text-center" id="groupUnitsCountContainer">
                                        Группа:
                                        <div id="client_popup_group_units_count">–</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-3">
                        <div class="card border-success border">
                            <div class="card-body p-2 text-center">
                                Z-число:
                                <div id="client_popup_z_num">–</div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="form-group col-3" id="clientPledgeAmountContainer"  style="display: hidden">
                        <label>Размер залога</label>
                        <div class="input-group">
                            <input type="text" class="form-control" id="client_popup_pledge_amount">
                            <span class="input-group-append">
                                <button class="btn btn-info" id="savePledgeAmount" type="button">Сохранить</button>
                            </span>
                        </div>
                    </div>
                </div>

                <!-- старая форма -->

                
                    <div class="row">
                    	<div class="col-6" id="client_popup_book_at_container">
                    		<div class="row">
		                    	<div class="form-group col-6">
		                    		<label>Дата бронирования</label>
		                    		<div id="datepicker-popup" class="input-group date datepicker">
			                        	<input type="text" class="form-control" id="book_at_date">
			                       		<span class="input-group-addon input-group-append border-left">
			                          		<span class="mdi mdi-calendar input-group-text"></span>
			                        	</span>
			                      	</div>
		                    	</div>

		                    	<div class="form-group col-6">
		                    		<label>Время</label>
		                    		<div class="input-group date" id="timepicker-example" data-target-input="nearest">
                        				<div class="input-group" data-target="#timepicker-example" data-toggle="datetimepicker">
                          					<input type="text" class="form-control datetimepicker-input" data-target="#timepicker-example" id="book_at_time" />
                          					<div class="input-group-addon input-group-append"><i class="mdi mdi-clock input-group-text"></i></div>
                        				</div>
                      				</div>
		                    	</div>
		                    </div>
	                    </div>

                        <div class="form-group col-3" style="display:none">
                            <label>Тип договора</label>
                            <select class="form-control" name="group" id="client_popup_type">
                                <option value="rent">Аренда</option>
                                <option value="book">Бронь</option>
                            </select>
                        </div>

                        <div class="form-group col-3" id="client_popup_staffed_container">
                        	<button id="client_popup_staffed" class="mt-4 btn btn-outline-primary" type="button">Укомплектован</button>
                        </div>

                        <div class="col-12" id="client_popup_address_container">
                        	<div class="row">
		                    	<div class="form-group col-10">
		                    		<label>Адрес</label>
		                    		<input type="text" class="form-control" id="client_popup_address">
		                    	</div>

		                    	<div class="form-group col-2">
		                    		<button type="button" class="mt-4 btn btn-outline-info" id="client_popup_save_address">Сохранить</button>
		                    	</div>
		                    </div>
	                    </div>
                    </div>
                </form>

                <div style="display: none" id="onlineOrderData">
                    <h5>Забронировано:</h5>
                    <div id="onlineOrderItems">
                    </div>
                </div>

                <h5>
                    Текущий заказ
                </h5>
                <table id="order_table" class="table table-bordered mt-0 mb-0">
                    <thead>
                        <tr>
                            <th>Артикул</th>
                            <th>Категория</th>
                            <th>Наименование</th>
                            <th>Начало</th>
                            <th>Стоимость</th> 
                            <th>Задолженность</th> 
                            <th>Срок</th>
                            <th>Длина</th>
                            <th>Размер</th>
                            <th>Колодка</th> 
                            <th>Возврат</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>
<!-- endbarcode modal -->