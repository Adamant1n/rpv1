@push('js')
<script src="{{ asset('js/webcam.js') }}"></script>

<script type="text/javascript">
	var width = 448;
	var height = 330;
	var photo;

	$('#previewPhoto').attr('width', width);
	$('#previewPhoto').attr('height', height);
	$('#previewPhoto').hide();

	jQuery('#photoPopup').on('shown.bs.modal', function() {
        Webcam.set({
			width: width,
			height: height,
			image_format: 'png',
			jpeg_quality: 100
		});

		Webcam.attach('#camera');
    });

	function takePhoto() {
		Webcam.snap( function(data_uri) {
			$('#previewPhoto').attr('src', data_uri);
			$('#previewPhoto').show();
			photo = data_uri;
			/*
			document.getElementById('results').innerHTML = 
				'<h2>Here is your image:</h2>' + 
				'<img src="'+data_uri+'"/>';
			*/
		});
	}

	$('#previewPhoto').click(function () {
		$(this).hide();
	});

	function savePhoto() {
		$('#photoPopup').modal('hide');
		// $('#photo_webcam').val(photo)
		Webcam.upload(photo, '/webcam', function(code, text) {
			console.log(text);
			$('#photo_webcam').val(JSON.parse(text).path)
		});
	}
</script>

@endpush

<div class="modal fade" id="photoPopup" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content bg-white">
            <div class="modal-body text-center">
            	<img style="z-index: 1060; position: absolute;top: 35px;left: 26px;" src="https://images.sftcdn.net/images/t_app-cover-l,f_auto/p/befbcde0-9b36-11e6-95b9-00163ed833e7/260663710/the-test-fun-for-friends-screenshot.jpg" id="previewPhoto">
                <div id="camera"></div>
                <button type="button" class="btn btn-info mt-2" onClick="takePhoto()">Фото</button>
                <button type="button" class="btn btn-primary mt-2" onClick="savePhoto()">Сохранить</button>
            </div>
        </div>
    </div>
</div>