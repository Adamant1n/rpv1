<div class="modal fade" id="productPopup" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-md" role="document" style="max-width: 700px;">
        <div class="modal-content bg-white">
            <div class="modal-body">
                <img class="img-fluid" style="width: 100%" src="" id="product-popup-photo" />
            </div>
        </div>
    </div>
</div>