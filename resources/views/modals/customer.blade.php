@push('js')
<script type="text/javascript">

</script>

@endpush

<div class="modal fade" id="customerPopup" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-md" role="document" style="max-width: 700px;">
        <div class="modal-content bg-white">
            <div class="modal-body">
                <form id="customerForm">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Фамилия</label>
                                <input type="text" class="form-control" name="last_name" value="" required autofocus>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Имя</label>
                                <input type="text" class="form-control" name="first_name" value="" required>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Отчество</label>
                                <input type="text" class="form-control" name="mid_name" value="">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Номер телефона</label>
                                <input type="text" class="form-control" name="phone" value="">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>E-mail</label>
                                <input type="text" class="form-control" name="email" value="">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Номер паспорта</label>

                                <div class="input-group col-xs-12">
                                    <input type="text" class="form-control passport input-dark" name="passport" value="">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Дата рождения</label>
                                <input type="text" class="form-control date" name="birthday" value="">
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label>Заметка</label>
                        <textarea class="form-control" name="description"></textarea>
                    </div>

                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Вес</label>
                                <input type="number" class="form-control" name="weight"
                                    value="" maxlength="3">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Рост</label>
                                <input type="number" class="form-control" name="height"
                                    value="" maxlength="3">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Размер обуви</label>
                                <input type="number" class="form-control" name="shoe_size"
                                    value="" maxlength="3">
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label>
                            <input type="checkbox" name="is_signed" value="1">
                            Договор подписан
                        </label>
                    </div>

                    <button class="btn btn-outline-info" type="submit">Сохранить</button>
                </form>
            </div>
        </div>
    </div>
</div>