@extends('layouts.app')

@push('js')
<script>
    $(document).ready(function () {
        //$('.repeater').find('select').select2({ width: '100%' });

        $('.repeater').repeater({
            initEmpty: false,
            defaultValues: {
                'text-input': 'foo'
            },
            show: function () {
                //$('.repeater').find('select').select2({ width: '100%' });
                $(this).slideDown();
            },
            hide: function (deleteElement) {
                $(this).slideUp(deleteElement);
            },
            ready: function (setIndexes) {
                // $dragAndDrop.on('drop', setIndexes);
            },
            isFirstItemUndeletable: false
        });
    });
</script>

@endpush

@section('content')

@include('common.validation')
@include('common.notifications')

<style>
.select2{
    max-width: 100%;
}

.select2-selection {
    width: 100% !important !important;
}

.select2-selection--single {
    width: 100% !important;
}
</style>

<div class="row">
	<div class="col-md-6">
		<div class="card">
			<div class="card-header">
				<form method="post" action="{{ route('agents.destroy', $agent->id) }}">
					@csrf
					{{ method_field('DELETE') }}

                    <a href="{{ route('agents.index') }}" class="btn btn-light pull-left">
                        <i class="fa fa-angle-left"></i>Назад
                    </a>
                    @if ($agent->deleted_at)
                    <a href="{{ route('agents.restore', $agent->id) }}" class="btn btn-outline-success pull-right">
                        <i class="fa fa-trash-o"></i>Восстановить
                    </a>
                    @else

					<button type="submit" class="btn btn-outline-danger float-right">
						<i class="fa fa-trash-o"></i>Заблокировать
					</button>
                    @endif
				</form>
				
			</div>
			<div class="card-body">
				<h4 class="card-title">Редактирование агента</h4>
				<form method="post" action="{{ route('agents.update', $agent->id) }}">
					{{ method_field('PATCH') }}
					@csrf
					<div class="form-group">
						<label>Агент</label>
						<input name="name" type="text" class="form-control" placeholder="Агент"
							value="{{ $agent->name }}" required>
					</div>
					<div class="form-group">
						<label>Промокод</label>
						<input name="promocode" type="text" class="form-control" placeholder="Промокод"
							value="{{ $agent->promocode }}" required>
					</div>
					<div class="form-group">
						<label>Комиссия, %</label>
						<input name="commission" type="number" class="form-control" placeholder="Комиссия"
							value="{{ $agent->commission }}" required>
					</div>
					<div class="form-group">
						<label>Общая скидка, %</label>
						<input name="discount" type="number" class="form-control" placeholder="Скидка"
							value="{{ $agent->discount }}" required>
					</div>

                    <div class="form-group">
                        <label>Скидка на товары, %</label>
                        <input name="sell_discount" type="number" class="form-control" placeholder="Скидка"
                            value="{{ $agent->sell_discount }}" required>
                    </div>

                    @if(false)
                    <hr />

                    <h5>Товары</h5>

                    <div class="repeater">
                        <div class="form-inline">
                            <div data-repeater-list="products" style="width: 100%">

                                @if(!$agent->products->count())
                                <div data-repeater-item class="row mb-2">
                                    <div class="form-group col-7">
                                        <select name="id" class="form-control" style="width: 100%">
                                            @foreach($categories as $category)
                                            <optgroup label="{{ $category->name }}">
                                                @foreach($category->products as $product)
                                                <option value="{{ $product->id }}">{{ $product->name }}</option>
                                                @endforeach
                                            </optgroup>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group col-3">
                                        <input type="number" class="form-control" name="discount" value="0">
                                    </div>
                                    <div class="form-group col-2">
                                        <input data-repeater-delete type="button" class="btn btn-outline-danger" value="Удалить"/>
                                    </div>
                                </div>
                                @endif

                                @foreach($agent->products as $_product)
                                <div data-repeater-item class="row mb-2">
                                    <div class="form-group col-7">
                                        <select name="id" class="form-control" style="width: 100%">
                                            @foreach($categories as $category)
                                            <optgroup label="{{ $category->name }}">
                                                @foreach($category->products as $product)
                                                <option value="{{ $product->id }}" {{ $product->id == $_product->id ? 'selected' : '' }}>{{ $product->name }}</option>
                                                @endforeach
                                            </optgroup>
                                            @endforeach
                                        </select>
                                    </div>
                                     <div class="form-group col-3">
                                        <input type="number" class="form-control" name="discount" value="{{ $_product->pivot->discount }}">
                                    </div>
                                    <div class="form-group col-2">
                                        <input data-repeater-delete type="button" class="btn btn-outline-danger" value="Удалить"/>
                                    </div>
                                </div>
                                @endforeach 

                            </div>
                        </div>

                        <button data-repeater-create class="btn btn-outline-info" type="button">Добавить</button>
                    </div>

                    <hr />
                    @endif

					<button type="submit" class="btn btn-success">
						<i class="fa fa-save"></i>Сохранить
					</button>
				</form>

			</div>
		</div>

	</div>
</div>

@endsection
