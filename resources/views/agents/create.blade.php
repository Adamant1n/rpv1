@extends('layouts.app')

@section('content')

@include('common.validation')
@include('common.notifications')

<div class="row">
	<div class="col-md-6">
		<div class="card">
			<div class="card-header">
				<a href="{{ route('agents.index') }}" class="btn btn-light"><i class="fa fa-angle-left"></i>Назад</a>
			</div>
			<div class="card-body">
				<h4 class="card-title">Новый агент</h4>
				<form method="post" action="{{ route('agents.store') }}">
					@csrf
					<div class="form-group">
						<label>Агент</label>
						<input name="name" type="text" class="form-control" placeholder="Агент" required autofocus>
					</div>
					<div class="form-group">
						<label>Промокод</label>
						<input name="promocode" type="text" class="form-control" placeholder="Промокод" required>
					</div>
					<div class="form-group">
						<label>Комиссия, %</label>
						<input name="commission" type="number" class="form-control" placeholder="Комиссия" required>
					</div>
					<div class="form-group">
						<label>Скидка, %</label>
						<input name="discount" type="number" class="form-control" placeholder="Скидка" required>
					</div>
					<button type="submit" class="btn btn-success"><i class="fa fa-save"></i>Сохранить</button>
				</form>
			</div>
		</div>

	</div>
</div>

@endsection
