@extends('public.layout')

@section('content')
    <div class="row w-100">
        <div class="col-lg-8 mx-auto">
            <div class="auth-form-light text-left py-4 px-5">
                <h1>Заказ #{{ $onlineOrder->id }} успешно оплачен</h1>
            </div>
        </div>
    </div>
@endsection