@extends('layouts.app')

@section('content')

@include('common.validation')
@include('common.notifications')

<div class="row">
	<div class="col-md-6">
		<div class="card">
			<div class="card-header">
				<a href="{{ route('price_lists.index') }}" class="btn btn-light">
					<i class="fa fa-angle-left"></i>Назад</a>
			</div>
			<div class="card-body">
				<h4 class="card-title">Новый прейскурант</h4>
				<form method="post" action="{{ route('price_lists.store') }}">
					@csrf

					@role('superadmin')
					<div class="form-group">
						<label for="location_id">Филиал</label>
						<select class="form-control select2" name="location_id" id="location_id" required>
							<option value="">Сделайте выбор...</option>
							@foreach ($locations as $location)
							<option value="{{ $location->id }}">{{ $location->address }}</option>
							@endforeach
						</select>
					</div>
					@endrole

					<div class="form-group">
						<label for="name">Название прейскуранта</label>
						<input name="name" id="name" type="text" class="form-control" placeholder="Название прейскуранта" required autofocus>
					</div>

					<button type="submit" class="btn btn-success">
						<i class="fa fa-save"></i>Сохранить
					</button>
				</form>
			</div>
		</div>

	</div>
</div>

<script>
(function($) {
	'use strict';
	$(".select2").select2();
})(jQuery);
</script>

@endsection
