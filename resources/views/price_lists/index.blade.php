@extends('layouts.app')

@section('content')

@include('common.validation')
@include('common.notifications')

<div class="card">
	<div class="card-header">
		<a href="{{ route('price_lists.create') }}" class="btn btn-success pull-left mr-4 mb-2"><i class="fa fa-plus"></i>Добавить прейскурант</a>
		@role('superadmin')
		<form>
			<select class="form-control select2 col-md-6 col-lg-3" name="location" onchange="this.form.submit()">
				<option value="">Все филиалы</option>
				@foreach ($locations as $location)
				<option value="{{ $location->id }}"
				{{ $location->id == $request->location ? 'selected' : '' }}>
				{{ $location->address }}</option>
				@endforeach
			</select>
		</form>
		@endrole
	</div>
	<div class="card-body">
		<h4 class="card-title">Прейскуранты</h4>
		<div class="row">
			<div class="col-12">
				<table id="prices-listing" class="table">
				<thead>
					<tr>
						<th>ID</th>
						<th>Название</th>
						<th>Филиал</th>
						<th>Дата создания</th>
						<th>Дата изменения</th>
					</tr>
				</thead>
				<tbody>

				@foreach ($price_lists as $price_list)
				<tr>
					<td>{{ $price_list->id }}</td>
					<td>{{ $price_list->name }}</td>
					<td>{{ optional($locations->find($price_list->location_id))->address }}</td>
					<td>{{ $price_list->created_at->format('d.m.Y H:i') }}</td>
					<td>{{ $price_list->updated_at->format('d.m.Y H:i') }}</td>
				</tr>
				@endforeach

				</tbody>
				</table>
			</div>
		</div>
	</div>
</div>

<script>
(function($) {
	'use strict';
	$(function() {
		$('#prices-listing').DataTable({
			"iDisplayLength": 10,
			"aaSorting": [[0, "asc"]],
			"language": {
				"lengthMenu": "Показывать _MENU_ записей на странице",
				"zeroRecords": "Записей не найдено",
				"loadingRecords": "Загрузка... может занять несколько секунд...",
				"info": "Страница _PAGE_ из _PAGES_",
				"infoEmpty": "Показано с 0 по 0 из 0 записей",
				"search": "",
				"infoFiltered": "(Найдено записей: _TOTAL_)",
				"sInfo": "Показано с _START_ по _END_ из _TOTAL_ записей",
				"paginate":
				{
					"first":    "Первая",
					"last":     "Последняя",
					"next":     "Следующая",
					"previous": "Предыдущая"
				}
			}
		});
		$('#prices-listing').each(function() {
			var datatable = $(this);
			var search_input = datatable.closest('.dataTables_wrapper').find('div[id$=_filter] input');
			search_input.attr('placeholder', 'Поиск');
			search_input.removeClass('form-control-sm');
			var length_sel = datatable.closest('.dataTables_wrapper').find('div[id$=_length] select');
			length_sel.removeClass('form-control-sm');
		});
		$('#prices-listing tbody').on('click', 'tr', function () {
			var id = $('td', this).eq(0).text();
			$(location).attr('href','/price_lists/'+id+'/edit');
		});
	});
	$(".select2").select2();
})(jQuery);
</script>

@endsection
