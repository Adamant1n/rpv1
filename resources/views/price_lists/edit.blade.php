@extends('layouts.app')

@section('content')

@include('common.validation')
@include('common.notifications')

<div class="row">
	<div class="col-md-6 grid-margin stretch-card">
		<div class="card">
			<div class="card-header">
				<a href="{{ route('price_lists.index') }}" class="btn btn-light pull-left mr-2"><i class="fa fa-angle-left"></i>Назад</a>
				<form method="post" action="{{ route('price_lists.destroy', $price_list->id) }}">
					@csrf
					{{ method_field('DELETE') }}
					<button type="submit" class="btn btn-outline-danger pull-right"><i class="fa fa-trash-o"></i>Удалить</button>
				</form>
			</div>
			<div class="card-body">
				<h4 class="card-title">Изменить прейскурант</h4>
				<form method="post" action="{{ route('price_lists.update', $price_list->id) }}">
					{{ method_field('PATCH') }}
					@csrf
					@role('superadmin')
					<div class="form-group">
						<label for="location_id">Филиал</label>
						<select class="form-control select2" name="location_id" id="location_id">
							@foreach ($locations as $location)
							<option value="{{ $location->id }}"
							{{ $location->id == $price_list->location_id ? 'selected' : '' }}>{{ $location->address }}</option>
							@endforeach
						</select>
					</div>
					@endrole
					<div class="form-group">
						<label for="name">Название прейскуранта</label>
						<input name="name" id="name" type="text" class="form-control" placeholder="Название прейскуранта" value="{{ $price_list->name }}" required>
					</div>
					<button type="submit" class="btn btn-success"><i class="fa fa-save"></i>Сохранить</button>
				</form>
			</div>
		</div>

	</div>
</div>

<div class="row">
	<div class="col-12 grid-margin stretch-card">
		<div class="card">
			<div class="card-body">
				<h4 class="card-title">Тарифная сетка</h4>
				<form method="post" action="{{ route('price_lists.timeline', $price_list->id) }}">
					@csrf
					<div class="table-responsive">
						<table class="table">
							<thead>
								<th width="200px"></th>
								@foreach($timeline as $dimension => $durations)
									@foreach($durations as $duration => $value)
									<th width="200px">{{ $duration }} {{ $casts[$dimension] }}</th>
									@endforeach
								@endforeach
							</thead>
							<tbody>
								@foreach($table as $category_id => $category)
								<tr>
									<td style="width: 100%">{{ $category['name'] }}</td>
									@foreach($category['timeline'] as $dimension => $durations)
										@foreach($durations as $duration => $value)
										<td>
											<input style="min-width: 100px" name="tariffs[{{ $category_id }}][{{ $dimension }}][{{ $duration }}]" type="number" step="0.1" class="form-control" value="{{ $value }}">
										</td>
										@endforeach
									@endforeach
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>

					<button class="btn btn-success">Сохранить</button>
				</form>
			</div>
		</div>
	</div>
</div>
@endsection