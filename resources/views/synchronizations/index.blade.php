@extends('layouts.app')

@section('content')

<div class="card">

	<div class="card-header">
		Синхронизация с облачным сервером
	</div>

	<div class="card-body">
		<h4 class="card-title">Синхронизации</h4>

		<div class="alert alert-{{ $needUpdate ? 'danger' : 'success' }}">
			<p>
				Текущая версия: <b>{{ $version }}</b>
			</p>
			<p class="mb-0">
				Последняя версия: <b>{{ $latestVersion }}</b>
			</p>
		</div>

		<table id="client-listing" class="table">
			<thead>
				<tr>
					<th>ID</th>
					<th>Дата начала</th>
					<th>Дата окончания</th>
					<th>Версия ПО</th>
				</tr>
			</thead>
			<tbody>
				@foreach($synchronizations as $synchronization)
				<tr>
					<td>{{ $synchronization->id }}</td>
					<td>{{ $synchronization->started_at->format('H:i d.m.Y') }}</td>
					<td>{{ optional($synchronization->ended_at)->format('H:i d.m.Y') ?? '–' }}</td>
					<td>{{ $synchronization->version }}</td>
				</tr>
				@endforeach
			</tbody>
		</table>

	</div>

</div>

<script>
(function($) {
'use strict';
$(function() {
	$('#client-listing').DataTable({
		"iDisplayLength": 25,
		"aaSorting": [[0, "desc"]],
		"language": {
			"lengthMenu": "Показывать _MENU_ записей на странице",
			"zeroRecords": "Записей не найдено",
			"loadingRecords": "Загрузка... может занять несколько секунд...",
			"info": "Страница _PAGE_ из _PAGES_",
			"infoEmpty": "Показано с 0 по 0 из 0 записей",
			"search": "",
			"infoFiltered": "(Найдено записей: _TOTAL_)",
			"sInfo": "Показано с _START_ по _END_ из _TOTAL_ записей",
			"paginate":
			{
				"first":    "Первая",
				"last":     "Последняя",
				"next":     "Следующая",
				"previous": "Предыдущая"
			}
		}
	});

	$('#client-listing').each(function() {
		var datatable = $(this);
		var search_input = datatable.closest('.dataTables_wrapper').find('div[id$=_filter] input');
		search_input.attr('placeholder', 'Поиск');
		search_input.removeClass('form-control-sm');
		var length_sel = datatable.closest('.dataTables_wrapper').find('div[id$=_length] select');
		length_sel.removeClass('form-control-sm');
	});
});
})(jQuery);
</script>

@endsection
