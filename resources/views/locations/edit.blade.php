@extends('layouts.app')

@push('js')
<script type="text/javascript">
	$(document).ready(function () {
		var timepicker_options = {
		    use24hours: true,
		    format: 'HH:mm',
		    stepping: 10
		};

		$('#return_by').datetimepicker(timepicker_options);
		$('#next_day_at').datetimepicker(timepicker_options);
		$('#opens_at').datetimepicker(timepicker_options);
		$('#closes_at').datetimepicker(timepicker_options);

		@if($location->all_day)
		$('#opens_closes').hide();
		@endif

		$('input[type=radio][name=all_day]').change(function() {
			if (this.value == 1) {
				$('#opens_closes').hide();
			}
			else {
				$('#opens_closes').show();
			}
		});

		/*
		$('#all_day').on('change', function () {
	        if($(this).is(':checked')) {
	            $('#opens_closes').hide();
	        }
	        else {
	            $('#opens_closes').show();
	        }
	    });
	    */
	});
</script>
<style>
.col-add-7 {
	position: relative;
    width: 100%;
    min-height: 1px;
    padding-right: 15px;
    padding-left: 15px;
	flex: 0 0 14.28571%;
    max-width: 14.28571%;
}
</style>
@endpush

@section('content')

@include('common.validation')
@include('common.notifications')

<div class="row">
	<div class="col-md-7">
		<div class="card">
			<div class="card-header">
				<form method="post" action="{{ route('locations.destroy', $location->id) }}">
					@csrf
					{{ method_field('DELETE') }}
                    <a href="{{ route('locations.index') }}" class="btn btn-light"><i class="fa fa-angle-left"></i>Назад</a>
					<button type="submit" class="btn btn-outline-danger pull-right"><i class="fa fa-trash-o"></i>Удалить</button>
				</form>
			</div>
			<div class="card-body">
				<h4 class="card-title">Настройки филиала</h4>
				<form method="post" action="{{ route('locations.update', $location->id) }}">
					{{ method_field('PATCH') }}
					@csrf
					<div class="form-group">
						<input name="address" type="text" class="form-control" placeholder="Адрес филиала" value="{{ $location->address }}" required>
					</div>

					<hr />

                    <div class="form-group row align-items-center">
                        <label class="col-3 col-form-label">Тип филиала</label>
                        <div class="col-7">
                            <select class="form-control" name="type">
                                <option value="both" {{ $location->type == 'both' ? 'selected' : '' }}>Прокат и инструкторы</option>
                                <option value="rent" {{ $location->type == 'rent' ? 'selected' : '' }}>Прокат</option>
                                <option value="instructors" {{ $location->type == 'instructors' ? 'selected' : '' }}>Инструкторы</option>
                            </select>
                        </div>

						<div class="form-group col-2 mb-0 d-flex">
							<label class="mb-0">
								<input value="1" name="active" type="checkbox" {{ $location->active ? 'checked' : '' }}>
								Активен
							</label>
						</div>
                    </div>

                    <hr />

					<!-- тарификация -->

					<h5>Тарификация</h5>

					<div class="form-group row">
						<label class="col-4 col-form-label">Прейскурант будни</label>
						<div class="col-8">
							<select class="form-control" name="price_weekdays">
								@foreach($location->price_lists as $price_list)
								<option value="{{ $price_list->id }}" {{ $location->price_weekdays === $price_list->id ? 'selected' : '' }}>{{ $price_list->name }}</option>
								@endforeach
							</select>
						</div>
					</div>

					<div class="form-group row">
						<label class="col-4 col-form-label">Прейскурант выходные</label>
						<div class="col-8">
							<select class="form-control" name="price_weekends">
								@foreach($location->price_lists as $price_list)
								<option value="{{ $price_list->id }}" {{ $location->price_weekends === $price_list->id ? 'selected' : '' }}>{{ $price_list->name }}</option>
								@endforeach
							</select>
						</div>
					</div>

					<div class="form-group row">
						<label class="col-4 col-form-label">Минимальный срок</label>
						<div class="col-8">
							<div class="btn-group btn-group-toggle btn-group-justified w-100" data-toggle="buttons">
								@foreach(Location::$steps as $tariff_step => $label)
								<label class="btn btn-info w-100 {{ $location->tariff_step === $tariff_step ? 'active' : '' }}">
	                                <input type="radio" name="tariff_step" value="{{ $tariff_step }}" {{ $location->tariff_step === $tariff_step ? 'checked' : '' }}> {{ $label }}
	                            </label>
								@endforeach
	                        </div>
						</div>
					</div>

					<div class="form-group row">
						<label class="col-8 col-form-label">Бесплатный возврат, до</label>
						<div class="col-4">
							<div class="input-group date" id="return_by" data-target-input="nearest">
		                        <div class="input-group" data-target="#return_by" data-toggle="datetimepicker">
		                            <input type="text" class="form-control datetimepicker-input" value="{{ $location->return_by }}" name="return_by" data-target="#return_by"/>
		                            <div class="input-group-addon input-group-append"><i class="mdi mdi-clock input-group-text"></i></div>
		                        </div>
		                    </div>
						</div>
					</div>

					<div class="form-group row">
						<label class="col-8 col-form-label">Выдача на следующий день, с</label>
						<div class="col-4">
							<div class="input-group date" id="next_day_at" data-target-input="nearest">
		                        <div class="input-group" data-target="#next_day_at" data-toggle="datetimepicker">
		                            <input type="text" class="form-control datetimepicker-input" value="{{ $location->next_day_at }}" name="next_day_at" data-target="#next_day_at"/>
		                            <div class="input-group-addon input-group-append"><i class="mdi mdi-clock input-group-text"></i></div>
		                        </div>
		                    </div>
						</div>
					</div>

					<div class="form-group row">
						<label class="col-8 col-form-label">Бесплатное время, мин.</label>
						<div class="col-4">
							<div class="input-group date" id="free_time" data-target-input="nearest">
		                        <div class="input-group" data-target="#free_time" data-toggle="datetimepicker">
		                            <input type="number" min="0" class="form-control datetimepicker-input" value="{{ $location->free_time }}" name="free_time" data-target="#free_time"/>
		                            <div class="input-group-addon input-group-append"><i class="mdi mdi-clock input-group-text"></i></div>
		                        </div>
		                    </div>
						</div>
					</div>

					<hr />

					<h5>
						График работы
						<!--
						<label class="float-right">
							<input type="checkbox" id="all_day" name="all_day" {{ $location->all_day ? 'checked' : '' }}>
							Круглосуточный
						</label>
						-->
					</h5>

					<!-- график работы -->
					<div class="form-group row">
						<label class="col-4 col-form-label">1 день это</label>
						<div class="col-8">
							<div class="btn-group btn-group-toggle btn-group-justified w-100" data-toggle="buttons">
								<!-- часы работы -->
								<label class="btn btn-info w-100 {{ !$location->all_day ? 'active' : '' }}">
	                                <input type="radio" name="all_day" value=0 {{ !$location->all_day ? 'checked' : '' }}> Часы работы
	                            </label>
	                            <!-- сутки 24 -->
	                            <label class="btn btn-info w-100 {{ $location->all_day ? 'active' : '' }}">
	                                <input type="radio" name="all_day" value=1 {{ $location->all_day ? 'checked' : '' }}> Сутки 24 часа
	                            </label>
	                        </div>
						</div>
					</div>

					<div class="form-group row" id="opens_closes">
						<label class="col-1 col-form-label">С: </label>
						<div class="col-4">
							<div class="input-group date" id="opens_at" data-target-input="nearest">
		                        <div class="input-group" data-target="#opens_at" data-toggle="datetimepicker">
		                            <input type="text" class="form-control datetimepicker-input" value="{{ $location->opens_at }}" name="opens_at" data-target="#opens_at"/>
		                            <div class="input-group-addon input-group-append"><i class="mdi mdi-clock input-group-text"></i></div>
		                        </div>
		                    </div>
						</div>

						<label class="col-3 text-right col-form-label">До</label>
						<div class="col-4">
							<div class="input-group date" id="closes_at" data-target-input="nearest">
		                        <div class="input-group" data-target="#closes_at" data-toggle="datetimepicker">
		                            <input type="text" class="form-control datetimepicker-input" value="{{ $location->closes_at }}" name="closes_at" data-target="#closes_at"/>
		                            <div class="input-group-addon input-group-append"><i class="mdi mdi-clock input-group-text"></i></div>
		                        </div>
		                    </div>
						</div>
					</div>

					<div class="form-group row">
						<label class="col-12">Настройка выходных дней</label>
						<label class="col-add-7">
	                        <input type="checkbox" name="weekends[]" value=1 {{ in_array(1, $location->weekends) ? 'checked' : '' }}> Пн
	                    </label>
	                    <label class="col-add-7">
	                        <input type="checkbox" name="weekends[]" value=2 {{ in_array(2, $location->weekends) ? 'checked' : '' }}> Вт
	                    </label>
	                    <label class="col-add-7">
	                        <input type="checkbox" name="weekends[]" value=3 {{ in_array(3, $location->weekends) ? 'checked' : '' }}> Ср
	                    </label>
	                    <label class="col-add-7">
	                        <input type="checkbox" name="weekends[]" value=4 {{ in_array(4, $location->weekends) ? 'checked' : '' }}> Чт
	                    </label>
	                    <label class="col-add-7">
	                        <input type="checkbox" name="weekends[]" value=5 {{ in_array(5, $location->weekends) ? 'checked' : '' }}> Пт
	                    </label>
	                    <label class="col-add-7">
	                        <input type="checkbox" name="weekends[]" value=6 {{ in_array(6, $location->weekends) ? 'checked' : '' }}> Сб
	                    </label>
	                    <label class="col-add-7">
	                        <input type="checkbox" name="weekends[]" value=7 {{ in_array(7, $location->weekends) ? 'checked' : '' }}> Вс
	                    </label>
					</div>

					<hr />

					<!-- юридические данные -->
					<div class="form-group">
						<label>Юридическое лицо</label>
						<input value="{{ $location->legal_name }}" name="legal_name" type="text" class="form-control" placeholder="Юридическое лицо">
					</div>
					<div class="form-group">
						<label>Юридический адрес</label>
						<textarea name="legal_address" class="form-control">{{ $location->legal_address }} </textarea>
					</div>
					<div class="form-group">
						<label>Номер телефона</label>
						<input value="{{ $location->legal_phone }}" name="legal_phone" type="text" class="form-control" placeholder="Номер телефона">
					</div>

					<hr />

					<h5>Используемые типы платежей</h5>
					<div class="row">
						<div class="form-group col-3">
							<label>
								<input name="payment_types[]" type="checkbox" value="{{ \App\Order::PAID_CASH }}" {{ in_array(\App\Order::PAID_CASH, $location->payment_types) ? 'checked' : '' }}>
								Наличные
							</label>
						</div>

						<div class="form-group col-3">
							<label>
								<input name="payment_types[]" type="checkbox" value="{{ \App\Order::PAID_CARD }}" {{ in_array(\App\Order::PAID_CARD, $location->payment_types) ? 'checked' : '' }}>
								Банк. карты
							</label>
						</div>

						<div class="form-group col-3">
							<label>
								<input name="payment_types[]" type="checkbox" value="{{ \App\Order::PAID_INVOICE }}" {{ in_array(\App\Order::PAID_INVOICE, $location->payment_types) ? 'checked' : '' }}>
								По счету
							</label>
						</div>
					</div>

					<hr />

					<div class="form-group">
						<label>
							<input name="kkt" type="checkbox" {{ $location->kkt ? 'checked' : '' }}>
							Использовать ККТ Атол
						</label>
					</div>

					<div class="row align-items-center mb-3">
						<span class="col-2" style='font-size: 0.8rem;'>Прокат (услуга)</span>

						<div class="col-5 d-flex align-items-center">

							<span style='font-size: 0.8rem;' class='text-nowrap'>Налогообложение</span>
							<select class="form-control ml-2" name="rent_taxation_type">
								<option value="{{ \App\Location::TAXATION_TYPE_OSN }}" {{ $location->rent_taxation_type == \App\Location::TAXATION_TYPE_OSN ? 'selected' : '' }}>ОСН</option>
								<option value="{{ \App\Location::TAXATION_TYPE_USNINCOME }}" {{ $location->rent_taxation_type == \App\Location::TAXATION_TYPE_USNINCOME ? 'selected' : '' }}>УСН Доход</option>
								<option value="{{ \App\Location::TAXATION_TYPE_USNINCOMEOUTCOME }}" {{ $location->rent_taxation_type == \App\Location::TAXATION_TYPE_USNINCOMEOUTCOME ? 'selected' : '' }}>УСН Доход - Расход</option>
								<option value="{{ \App\Location::TAXATION_TYPE_ENVD }}" {{ $location->rent_taxation_type == \App\Location::TAXATION_TYPE_ENVD ? 'selected' : '' }}>ЕНВД</option>
								<option value="{{ \App\Location::TAXATION_TYPE_PATENT }}" {{ $location->rent_taxation_type == \App\Location::TAXATION_TYPE_PATENT ? 'selected' : '' }}>Патент</option>
							</select>
						</div>

						<div class="col-5 d-flex align-items-center">
							<span style='font-size: 0.8rem;' class="text-nowrap">Параметры НДС</span>
							<select class="form-control ml-2" name="rent_tax">
								<option value="{{ \App\Location::TAX_NONE }}" {{ $location->rent_tax == \App\Location::TAX_NONE ? 'selected' : '' }}>Без НДС</option>
								<option value="{{ \App\Location::TAX_VAT0 }}" {{ $location->rent_tax == \App\Location::TAX_VAT0 ? 'selected' : '' }}>НДС 0%</option>
								<option value="{{ \App\Location::TAX_VAT10 }}" {{ $location->rent_tax == \App\Location::TAX_VAT10 ? 'selected' : '' }}>НДС 10%</option>
								<option value="{{ \App\Location::TAX_VAT18 }}" {{ $location->rent_tax == \App\Location::TAX_VAT18 ? 'selected' : '' }}>НДС 18%</option>
								<option value="{{ \App\Location::TAX_VAT20 }}" {{ $location->rent_tax == \App\Location::TAX_VAT20 ? 'selected' : '' }}>НДС 20%</option>
							</select>
						</div>
					</div>

					<div class="row align-items-center mb-3">
						<span class="col-2" style='font-size: 0.8rem;'>Реализация (товар)</span>
						
						<div class="col-5 d-flex align-items-center">

							<span class="text-nowrap" style='font-size: 0.8rem;'>Налогообложение</span>
							<select class="form-control ml-2" name="sale_taxation_type">
								<option value="{{ \App\Location::TAXATION_TYPE_OSN }}" {{ $location->sale_taxation_type == \App\Location::TAXATION_TYPE_OSN ? 'selected' : '' }}>ОСН</option>
								<option value="{{ \App\Location::TAXATION_TYPE_USNINCOME }}" {{ $location->sale_taxation_type == \App\Location::TAXATION_TYPE_USNINCOME ? 'selected' : '' }}>УСН Доход</option>
								<option value="{{ \App\Location::TAXATION_TYPE_USNINCOMEOUTCOME }}" {{ $location->sale_taxation_type == \App\Location::TAXATION_TYPE_USNINCOMEOUTCOME ? 'selected' : '' }}>УСН Доход - Расход</option>
								<option value="{{ \App\Location::TAXATION_TYPE_ENVD }}" {{ $location->sale_taxation_type == \App\Location::TAXATION_TYPE_ENVD ? 'selected' : '' }}>ЕНВД</option>
								<option value="{{ \App\Location::TAXATION_TYPE_PATENT }}" {{ $location->sale_taxation_type == \App\Location::TAXATION_TYPE_PATENT ? 'selected' : '' }}>Патент</option>
							</select>
						</div>

						<div class="col-5 d-flex align-items-center">
							<span class="text-nowrap" style='font-size: 0.8rem;'>Параметры НДС</span>
							<select class="form-control ml-2" name="sale_tax">
								<option value="{{ \App\Location::TAX_NONE }}" {{ $location->sale_tax == \App\Location::TAX_NONE ? 'selected' : '' }}>Без НДС</option>
								<option value="{{ \App\Location::TAX_VAT0 }}" {{ $location->sale_tax == \App\Location::TAX_VAT0 ? 'selected' : '' }}>НДС 0%</option>
								<option value="{{ \App\Location::TAX_VAT10 }}" {{ $location->sale_tax == \App\Location::TAX_VAT10 ? 'selected' : '' }}>НДС 10%</option>
								<option value="{{ \App\Location::TAX_VAT18 }}" {{ $location->sale_tax == \App\Location::TAX_VAT18 ? 'selected' : '' }}>НДС 18%</option>
								<option value="{{ \App\Location::TAX_VAT20 }}" {{ $location->sale_tax == \App\Location::TAX_VAT20 ? 'selected' : '' }}>НДС 20%</option>
							</select>
						</div>
					</div>

					<hr />

					<button type="submit" class="btn btn-success"><i class="fa fa-save"></i>Сохранить</button>
				</form>
			</div>
		</div>

	</div>
</div>

@endsection
