@extends('layouts.app')

@section('content')

@include('common.validation')
@include('common.notifications')

<div class="row">
	<div class="col-md-6">
		<div class="card">
			<div class="card-header">
				<a href="{{ route('locations.index') }}" class="btn btn-light"><i class="fa fa-angle-left"></i>Назад</a>
			</div>
			<div class="card-body">
				<h4 class="card-title">Новый филиал</h4>
				<form method="post" action="{{ route('locations.store') }}">
					@csrf
					<div class="form-group">
						<input name="address" type="text" class="form-control" placeholder="Адрес филиала" required autofocus>
					</div>

                    <div class="form-group row">
                        <label class="col-4 col-form-label">Тип филиала</label>
                        <div class="col-8">
                            <select class="form-control" name="type">
                                <option value="both">Прокат и инструкторы</option>
                                <option value="rent">Прокат</option>
                                <option value="instructors">Инструкторы</option>
                            </select>
                        </div>
                    </div>

                    <hr />

					<div class="form-group">
						<label>Юридическое лицо</label>
						<input name="legal_name" type="text" class="form-control" placeholder="Юридическое лицо">
					</div>
					<div class="form-group">
						<label>Юридический адрес</label>
						<textarea name="legal_address" class="form-control"></textarea>
					</div>
					<div class="form-group">
						<label>Номер телефона</label>
						<input name="legal_phone" type="text" class="form-control" placeholder="Номер телефона">
					</div>

					<div class="form-group">
						<label>
							<input name="kkt" type="checkbox">
							ККТ
						</label>
					</div>

					<button type="submit" class="btn btn-success">
						<i class="fa fa-save"></i>Сохранить
					</button>
				</form>
			</div>
		</div>

	</div>
</div>

@endsection
