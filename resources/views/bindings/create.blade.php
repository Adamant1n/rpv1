@extends('layouts.app')

@section('content')

@include('common.validation')
@include('common.notifications')

@include('common.tabs', ['page' => 'bindings'])
<div class="row">
	<div class="col-md-6">
		<div class="card">
			<div class="card-header">
				<a href="{{ route('bindings.index') }}" class="btn btn-light"><i class="fa fa-angle-left"></i>Назад</a>
			</div>
			<div class="card-body">
				<h4 class="card-title">Добавить крепление</h4>
				<form method="post" action="{{ route('bindings.store') }}">
					@csrf
					<div class="form-group">
						<input name="name" type="text" class="form-control" placeholder="Название крепления" required autofocus>
					</div>
					<button type="submit" class="btn btn-success"><i class="fa fa-save"></i>Сохранить</button>
				</form>
			</div>
		</div>

	</div>
</div>

@endsection
