@extends('layouts.app')

@section('content')

@include('common.validation')
@include('common.notifications')

@include('common.tabs', ['page' => 'bindings'])
<div class="card">
	<div class="card-header">
		<a href="{{ route('bindings.create') }}" class="btn btn-success"><i class="fa fa-plus"></i>Добавить крепление</a>
	</div>
	<div class="card-body">
		<h4 class="card-title">Крепления</h4>
		<div class="row">
			<div class="col-12">
				<table id="bindings-listing" class="table">
				<thead>
					<tr>
						<th>ID</th>
						<th>Название</th>
						<th>Дата создания</th>
						<th>Дата изменения</th>
					</tr>
				</thead>
				<tbody>

				@foreach ($bindings as $binding)
				<tr>
					<td>{{ $binding->id }}</td>
					<td>{{ $binding->name }}</td>
					<td>{{ $binding->created_at->format('d.m.Y H:i') }}</td>
					<td>{{ $binding->updated_at->format('d.m.Y H:i') }}</td>
				</tr>
				@endforeach

				</tbody>
				</table>
			</div>
		</div>
	</div>
</div>

<script>
(function($) {
	'use strict';
	$(function() {
		$('#bindings-listing').DataTable({
			"iDisplayLength": 10,
			"aaSorting": [[0, "asc"]],
			"language": {
				"lengthMenu": "Показывать _MENU_ записей на странице",
				"zeroRecords": "Записей не найдено",
				"loadingRecords": "Загрузка... может занять несколько секунд...",
				"info": "Страница _PAGE_ из _PAGES_",
				"infoEmpty": "Показано с 0 по 0 из 0 записей",
				"search": "",
				"infoFiltered": "(Найдено записей: _TOTAL_)",
				"sInfo": "Показано с _START_ по _END_ из _TOTAL_ записей",
				"paginate":
				{
					"first":    "Первая",
					"last":     "Последняя",
					"next":     "Следующая",
					"previous": "Предыдущая"
				}
			}
		});
		$('#bindings-listing').each(function() {
			var datatable = $(this);
			var search_input = datatable.closest('.dataTables_wrapper').find('div[id$=_filter] input');
			search_input.attr('placeholder', 'Поиск');
			search_input.removeClass('form-control-sm');
			var length_sel = datatable.closest('.dataTables_wrapper').find('div[id$=_length] select');
			length_sel.removeClass('form-control-sm');
		});
		$('#bindings-listing tbody').on('click', 'tr', function () {
			var id = $('td', this).eq(0).text();
			$(location).attr('href','/bindings/'+id+'/edit');
		});
	});
})(jQuery);
</script>

@endsection
