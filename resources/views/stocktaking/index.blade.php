@extends('layouts.app')

@push('js')
<script type="text/javascript">
	$(".select2").select2();

	// datatable
	$('#reports-listing').DataTable({
		"bSort": false,
		"iDisplayLength": 100,
		"bSort": false,
		"bFilter": false,
		"paging": false,
		dom: 'Bfrtip',
        buttons: [
            'excel', 'pdf', 'print'
        ],
		"language": {
			"lengthMenu": "Показывать _MENU_ записей на странице",
			"zeroRecords": "Записей не найдено",
			"loadingRecords": "Загрузка... может занять несколько секунд...",
			"info": "Страница _PAGE_ из _PAGES_",
			"infoEmpty": "Показано с 0 по 0 из 0 записей",
			"search": "",
			"infoFiltered": "(Найдено записей: _TOTAL_)",
			"sInfo": "Показано с _START_ по _END_ из _TOTAL_ записей",
			"paginate":
			{
				"first":    "Первая",
				"last":     "Последняя",
				"next":     "Следующая",
				"previous": "Предыдущая"
			},
			"buttons":
			{
				"pageLength": {
					   _: "%d записей",
					'-1': "Показать все"
				},
				"print": "Печать",
				"excel": "Экспорт"
			}
		}
	});
</script>
@endpush

@section('content')

<div class="card">
	<div class="card-header">
		<a href="{{ route('stocktaking.perform') }}" class="btn btn-success">
			<i class="fa fa-plus"></i>Добавить</a>
	</div>
	<div class="card-body">
		<h4 class="card-title">Инвентаризации</h4>

		<div class="table-responsive">
			<table id="reports-listing" class="table">
				<thead>
					<tr>
						<th>ID</th>
						<th>Филиал</th>
						<th>Утеряно</th>
						<th>ОК</th>
						<th>Другое</th>
						<th></th>
					</tr>
				</thead>
				<tbody>

					@foreach($stocktakings as $stocktaking)
					<tr>
						<td>{{ $stocktaking->id }}</td>
						<td>{{ $stocktaking->location->address }}</td>
						<td>{{ $stocktaking->count_lost }}</td>
						<td>{{ $stocktaking->count_ok }}</td>
						<td>{{ $stocktaking->count_other }}</td>
						<td>
							<a href="{{ route('stocktaking.show', $stocktaking) }}" class="btn btn-sm btn-outline-info">
								Открыть
							</a>
						</td>

					</tr>
					@endforeach

				</tbody>
			</table>
		</div>
	</div>
</div>
@endsection