@extends('layouts.app')

@section('content')

@include('common.validation')
@include('common.notifications')

@include('common.tabs', ['page' => 'stocktaking'])
<div class="card">
	<div class="card-body">
		<h4 class="card-title">Инветаризация</h4>
		<div class="row">
			<div class="col-12">
				<table id="units-listing" class="table">
					<thead>
						<tr>
							<th>Артикул</th>
							<th>Категория</th>
							<th>Бренд</th>
							<th>Наименование</th>
							<th>Состояние</th>
							<th>Кол-во</th>
							<th>Статус</th>
						</tr>
					</thead>
					<tbody>

					@foreach ($units as $unit)
					<tr>
						<td>{{ $unit->code }}</td>
						<td>{{ $unit->product->category->name }}</td>
						<td>{{ $unit->product->brand->name }}</td>
						<td>{{ $unit->product->name }}</td>
						<td>{{ $unit->condition->name }}</td>
						<td>{{ $unit->count }}</td>
						<td>{!! $unit->badge !!}</td>
					</tr>
					@endforeach

					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>

<script>
(function($) {
	'use strict';
	$(function() {
		$('#units-listing').DataTable({
			"iDisplayLength": 100,
			"aaSorting": [[0, "asc"]],
			"language": {
				"lengthMenu": "Показывать _MENU_ записей на странице",
				"zeroRecords": "Записей не найдено",
				"loadingRecords": "Загрузка... может занять несколько секунд...",
				"info": "Страница _PAGE_ из _PAGES_",
				"infoEmpty": "Показано с 0 по 0 из 0 записей",
				"search": "",
				"infoFiltered": "(Найдено записей: _TOTAL_)",
				"sInfo": "Показано с _START_ по _END_ из _TOTAL_ записей",
				"paginate":
				{
					"first":    "Первая",
					"last":     "Последняя",
					"next":     "Следующая",
					"previous": "Предыдущая"
				}
			}
		});

		$('#units-listing').each(function() {
			var datatable = $(this);
			var search_input = datatable.closest('.dataTables_wrapper').find('div[id$=_filter] input');
			search_input.attr('placeholder', 'Поиск');
			search_input.removeClass('form-control-sm');
			var length_sel = datatable.closest('.dataTables_wrapper').find('div[id$=_length] select');
			length_sel.removeClass('form-control-sm');
		});
		$('#units-listing tbody').on('click', 'tr', function () {
			var id = $('td', this).eq(0).text();
			$(location).attr('href','/stock/'+parseInt(id.replace(/[^-0-9]/gim, ''))+'/edit');
		});
	});
})(jQuery);
</script>

@endsection
