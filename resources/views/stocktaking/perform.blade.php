@extends('layouts.app')

@section('content')

@include('common.validation')
@include('common.notifications')

@push('js')
<script>
(function($) {
	'use strict';
	$(".select2").select2();
})(jQuery);
</script>

<script type="text/javascript">
	$('#categories').on('select2:select', function () {
		if($(this).val()[0] == '')
		{
			$('#categories').val('').trigger('change');
		}
		getUnits();
	});

	$('#location_id').on('select2:select', function () {
		getUnits();
	});

	var unitsTable = $('#units-listing').DataTable({
		//"iDisplayLength": 100,
		"bSort": false,
		"bFilter": false,
		"paging": false,
		"language": {
			"lengthMenu": "Показывать _MENU_ записей на странице",
			"zeroRecords": "Записей не найдено",
			"loadingRecords": "Загрузка... может занять несколько секунд...",
			"info": "Страница _PAGE_ из _PAGES_",
			"infoEmpty": "Показано с 0 по 0 из 0 записей",
			"search": "",
			"infoFiltered": "(Найдено записей: _TOTAL_)",
			"sInfo": "Показано с _START_ по _END_ из _TOTAL_ записей",
			"bInfo": false,
			"paginate":
			{
				"first":    "Первая",
				"last":     "Последняя",
				"next":     "Следующая",
				"previous": "Предыдущая"
			}
		}
	});

	var codes = [];

    var countTotal = 0;
    var countInStock = 0;
    var countRemaining = 0;

    function updateCounts()
    {
        countRemaining = countTotal - countInStock;

        $('#c_total').html(countTotal);
        $('#c_remaining').html(countRemaining);
        $('#c_in_stock').html(countInStock);
    }

	function getUnits()
	{
		var file_data = $('#codeFile').prop('files')[0];
	    var form_data = new FormData();                  
	    form_data.append('file', file_data);
	    form_data.append('location_id', $('#location_id').val());
	    form_data.append('categories', $('#categories').val());

		$.ajax({
	        url: '{{ route('stocktaking.get_units') }}', 
	        data: form_data,                         
	        type: 'post',
	        contentType: false,
	        processData: false,
	        success: function(response){
	        	unitsTable.clear().draw();
	        	addRows(response.units);  

                countTotal = response.total;
                countInStock = response.in_stock;
                updateCounts();

                if(response.codes)
                {
    	        	for(var i = 0; i < response.codes.length; i++)
    				{
    					codes.push(response.codes[i]);
    					$('#codes').val(JSON.stringify(codes));
    				}
                }
	        }
	    });
	}

	$('#codeFile').change(function () {
		getUnits();
	});

	function addRows(units)
	{
		for(var i = 0; i < units.length; i++)
		{
			$('#codes').val(JSON.stringify(codes));
			var rowNode = unitsTable.row.add(units[i]).draw(false).node();
		}

		// scan();
	}

	function removeUnit(code)
	{
		codes.push(code);
		$('#codes').val(JSON.stringify(codes));

		var filteredData = unitsTable.rows()
		    .indexes()
		    .filter(function (value, index) {
		       return unitsTable.row(value).data()[0] == code; 
		    });

        if(unitsTable.rows(filteredData)[0].length > 0)
        {
    		unitsTable.rows(filteredData)
    			.remove()
    			.draw();

            countInStock++;
            updateCounts();
        }
	}

	// штрихсканнер
    var typingBarcode = '';
    var barcode = '';
    var prevKey;

	$(document).on('keyup', function (e) {
        clearInterval(timer);

        var code = e.originalEvent.code;

        // letter or digit
        if(code.indexOf('Key') != -1) {
            typingBarcode += code.substring(3);
        }

        else if(code.indexOf('Digit') != -1) {
            typingBarcode += code.substring(5);
        }

        // $('#client_popup_scan').html(typingBarcode);

        if(code == 'ArrowDown' && prevKey == 'Enter') {
        	finishBarcodeEnter();
            
            return true;
        }

        prevKey = code;
        timer = setInterval(ticker, 500);
    });

    var timer = setInterval(ticker, 500);

    var ticker = function () {
        resetValues();
        clearInterval(timer);
    }

    function resetValues() {
        typingBarcode = '';
        prevKey = '';
    }

    function finishBarcodeEnter() {
        barcode = typingBarcode;
        removeUnit(barcode);

        barcode = '';
        resetValues();
    }

    $(window).keydown(function(event){
        if(event.keyCode == 13) {
            if($('#codeText').val())
            {
                removeUnit($('#codeText').val());
                $('#codeText').val('');
            }
            else
            {
                event.preventDefault();
            }
            return false;
        }
    });

    // подтверждение
    $('#store').click(function (event) {
    	count = unitsTable.rows().count();
		 event.preventDefault();
		 swal({   
            title: "Подтвердите действие",   
            text: "Установить статус \"Утерян\" для " + count + " единиц?",   
            type: "warning",   
            showCancelButton: true,   
            confirmButtonColor: "#DD6B55",   
            confirmButtonText: "Да",   
            cancelButtonText: "Отмена",   
        }).then(function(result){   
        	if(result.value)
        	{
        		$('#stocktaking-form').submit();
        	}
        });
	});
</script>
@endpush

<div class="row">
	<div class="col-md-12">
		<div class="card">
			<div class="card-body">
				<h4 class="card-title">
					Инвентаризация
				</h4>
				<form id="stocktaking-form" method="post" action="{{ route('stocktaking.store') }}">
					@csrf

					<input type="hidden" name="codes" id="codes">

					<div class="row">
						<div class="form-group col-3">
							<label for="location_id">Текущий склад</label>
							<select class="form-control select2" name="location_id" id="location_id" @role('admin') disabled @endrole>
								@foreach ($locations as $location)
								<option value="{{ $location->id }}" @role('admin') {{ $location->id == auth()->user()->location_id ? 'selected' : '' }} @endrole>{{ $location->address }}</option>
								@endforeach
							</select>
						</div>

                        <div class="form-group col-3">
                            <label>Ввод вручную</label>
                            <input class="form-control" id="codeText" type="text">
                        </div>

						<div class="form-group col-6">
							<label for="condition">Категории</label>
							<select class="form-control select2" name="categories[]" id="categories" multiple>
								<option value="">Все</option>
								@foreach ($categories as $category)
								<option value="{{ $category->id }}">{{ $category->name }}</option>
								@endforeach
							</select>
						</div>
					</div>

					<div class="form-group">
						<input class="form-control" id="codeFile" type="file" onchange="" name="files[]" accept=".txt">
					</div>

					<hr />

                    <div>
                        <p class="mb-1">
                            <strong>Всего: </strong><span id="c_total">0</span>
                        </p>
                        <p class="mb-1">
                            <strong>В наличии: </strong><span id="c_in_stock">0</span>
                        </p>
                        <p class="mb-1">
                            <strong>Осталось: </strong><span id="c_remaining">0</span>
                        </p>
                    </div>

                    <hr />

					<table id="units-listing" class="table">
						<thead>
							<tr>
								<th>Артикул</th>
								<th>Категория</th>
								<th>Бренд</th>
								<th>Наименование</th>
								<th>Состояние</th>
								<th>Кол-во</th>
								<th>Статус</th>
							</tr>
						</thead>
						<tbody>

						</tbody>
					</table>

					<hr />

					<button type="button" id="store" class="btn btn-success">
						<i class="fa fa-save"></i>Далее
					</button>
				</form>
			</div>
		</div>

	</div>
</div>

@endsection
