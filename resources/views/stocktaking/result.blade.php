@extends('layouts.app')

@push('js')
<script type="text/javascript">
	$(".select2").select2();

	// datatable
	$('#reports-listing').DataTable({
		"bSort": false,
		"iDisplayLength": 100,
		"bSort": false,
		"bFilter": false,
		"paging": false,
		dom: 'Bfrtip',
        buttons: [
            'excel', 'pdf', 'print'
        ],
		"language": {
			"lengthMenu": "Показывать _MENU_ записей на странице",
			"zeroRecords": "Записей не найдено",
			"loadingRecords": "Загрузка... может занять несколько секунд...",
			"info": "Страница _PAGE_ из _PAGES_",
			"infoEmpty": "Показано с 0 по 0 из 0 записей",
			"search": "",
			"infoFiltered": "(Найдено записей: _TOTAL_)",
			"sInfo": "Показано с _START_ по _END_ из _TOTAL_ записей",
			"paginate":
			{
				"first":    "Первая",
				"last":     "Последняя",
				"next":     "Следующая",
				"previous": "Предыдущая"
			},
			"buttons":
			{
				"pageLength": {
					   _: "%d записей",
					'-1': "Показать все"
				},
				"print": "Печать",
				"excel": "Экспорт"
			}
		}
	});

	var unitsTable = $('#units-listing').DataTable({
		//"iDisplayLength": 100,
		"bSort": false,
		"bFilter": false,
		"paging": false,
		dom: 'Bfrtip',
        buttons: [
            'excel', 'pdf', 'print'
        ],
		"language": {
			"lengthMenu": "Показывать _MENU_ записей на странице",
			"zeroRecords": "Записей не найдено",
			"loadingRecords": "Загрузка... может занять несколько секунд...",
			"info": "Страница _PAGE_ из _PAGES_",
			"infoEmpty": "Показано с 0 по 0 из 0 записей",
			"search": "",
			"infoFiltered": "(Найдено записей: _TOTAL_)",
			"sInfo": "Показано с _START_ по _END_ из _TOTAL_ записей",
			"bInfo": false,
			"paginate":
			{
				"first":    "Первая",
				"last":     "Последняя",
				"next":     "Следующая",
				"previous": "Предыдущая"
			}
		}
	});
</script>
@endpush

@section('content')

<div class="card">
	<div class="card-body">
		<h4 class="card-title">Результат инвентаризации</h4>

		<div class="table-responsive">
			<table id="reports-listing" class="table">
				<thead>
					<tr>
						<th></th>
						<th>Утеряно</th>
						<th>ОК</th>
						<th>Прочее</th>
					</tr>
				</thead>
				<tbody>

				<tr>
					<td>Всего</td>
					<td>{{ $result['overall']['lost'] }}</td>
					<td>{{ $result['overall']['ok'] }}</td>
					<td>{{ $result['overall']['other'] }}</td>
				</tr>

				<tr>
					<td>Текущая проверка</td>
					<td>{{ $result['current']['lost'] }}</td>
					<td>{{ $result['current']['ok'] }}</td>
					<td>{{ $result['current']['other'] }}</td>
				</tr>

				</tbody>
			</table>
		</div>

		<hr />

		<h4>Утерянный инвентарь</h4>
		<table id="units-listing" class="table">
			<thead>
				<tr>
					<th>Артикул</th>
					<th>Категория</th>
					<th>Бренд</th>
					<th>Наименование</th>
					<th>Состояние</th>
					<th>Статус</th>
				</tr>
			</thead>
			<tbody>
				@foreach($lostUnitsCurrent as $unit)
				<tr>
					<td>{{ $unit->code }}</td>
					<td>{{ $unit->product->category->name }}</td>
					<td>{{ $unit->product->brand->name }}</td>
					<td>{{ $unit->product->name }}</td>
					<td>{{ $unit->condition->name }}</td>
					<td>{!! $unit->badge !!}</td>
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>
</div>
@endsection