@extends('layouts.app')

@push('js')
<script type="text/javascript">
	$(".select2").select2();

	// datatable
	$('#reports-listing').DataTable({
		"bSort": false,
		"iDisplayLength": 100,
		"bSort": false,
		"bFilter": false,
		"paging": false,
		dom: 'Bfrtip',
        buttons: [
            'excel', 'pdf', 'print'
        ],
		"language": {
			"lengthMenu": "Показывать _MENU_ записей на странице",
			"zeroRecords": "Записей не найдено",
			"loadingRecords": "Загрузка... может занять несколько секунд...",
			"info": "Страница _PAGE_ из _PAGES_",
			"infoEmpty": "Показано с 0 по 0 из 0 записей",
			"search": "",
			"infoFiltered": "(Найдено записей: _TOTAL_)",
			"sInfo": "Показано с _START_ по _END_ из _TOTAL_ записей",
			"paginate":
			{
				"first":    "Первая",
				"last":     "Последняя",
				"next":     "Следующая",
				"previous": "Предыдущая"
			},
			"buttons":
			{
				"pageLength": {
					   _: "%d записей",
					'-1': "Показать все"
				},
				"print": "Печать",
				"excel": "Экспорт"
			}
		}
	});

	var unitsTable = $('#units-listing').DataTable({
		//"iDisplayLength": 100,
		"bSort": false,
		"bFilter": false,
		"paging": false,
		dom: 'Bfrtip',
        buttons: [
            'excel', 'pdf', 'print'
        ],
		"language": {
			"lengthMenu": "Показывать _MENU_ записей на странице",
			"zeroRecords": "Записей не найдено",
			"loadingRecords": "Загрузка... может занять несколько секунд...",
			"info": "Страница _PAGE_ из _PAGES_",
			"infoEmpty": "Показано с 0 по 0 из 0 записей",
			"search": "",
			"infoFiltered": "(Найдено записей: _TOTAL_)",
			"sInfo": "Показано с _START_ по _END_ из _TOTAL_ записей",
			"bInfo": false,
			"paginate":
			{
				"first":    "Первая",
				"last":     "Последняя",
				"next":     "Следующая",
				"previous": "Предыдущая"
			}
		}
	});
</script>
@endpush

@section('content')

<div class="card">
	<div class="card-body">
		<h4 class="card-title">Инвентаризация от {{ $stocktaking->created_at->format('H:i d.m.Y') }}</h4>

		<div class="table-responsive">
			<table id="reports-listing" class="table">
				<thead>
					<tr>
						<th></th>
						<th>Утеряно</th>
						<th>ОК</th>
						<th>Прочее</th>
					</tr>
				</thead>
				<tbody>

				<tr>
					<td>Текущая проверка</td>
					<td>{{ $stocktaking->count_lost }}</td>
					<td>{{ $stocktaking->count_ok }}</td>
					<td>{{ $stocktaking->count_other }}</td>
				</tr>

				</tbody>
			</table>
		</div>

		<hr />

		<h4>Утерянный инвентарь</h4>
		<table id="units-listing" class="table">
			<thead>
				<tr>
					<th>Артикул</th>
					<th>Категория</th>
					<th>Бренд</th>
					<th>Наименование</th>
					<th>Состояние</th>
					<th>Статус</th>
				</tr>
			</thead>
			<tbody>
				@foreach($stocktaking->units as $unit)
				<tr>
					<td>{{ $unit->unit->code }}</td>
					<td>{{ $unit->unit->product->category->name }}</td>
					<td>{{ $unit->unit->product->brand->name }}</td>
					<td>{{ $unit->unit->product->name }}</td>
					<td>{{ $unit->condition->name }}</td>
					<td>{!! $unit->badge !!}</td>
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>
</div>
@endsection