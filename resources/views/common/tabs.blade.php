<ul class="nav nav-tabs tab-basic mb-0" role="tablist">
	@hasanyrole('admin|superadmin')
    <li class="nav-item">
        <a class="nav-link {{ $page == 'stock' ? 'active' : ''}}" href="{{ route('stock.index') }}">Инвентарь</a>
    </li>
    <li class="nav-item">
        <a class="nav-link {{ $page == 'massediting' ? 'active' : ''}}" href="{{ route('massediting.index') }}">Изменение признаков</a>
    </li>
    @endhasanyrole
    @role('superadmin')
    <li class="nav-item">
        <a class="nav-link {{ $page == 'products' ? 'active' : ''}}" href="{{ route('products.index') }}">Модели</a>
    </li>
    <li class="nav-item">
        <a class="nav-link {{ $page == 'brands' ? 'active' : ''}}" href="{{ route('brands.index') }}">Бренды</a>
    </li>
    <li class="nav-item">
        <a class="nav-link {{ $page == 'bindings' ? 'active' : ''}}" href="{{ route('bindings.index') }}">Крепления</a>
    </li>
    <li class="nav-item">
        <a class="nav-link {{ $page == 'categories' ? 'active' : ''}}" href="{{ route('categories.index') }}">Категории</a>
    </li>
    @endrole
</ul>