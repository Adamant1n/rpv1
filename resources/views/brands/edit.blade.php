@extends('layouts.app')

@section('content')

@include('common.validation')
@include('common.notifications')

@include('common.tabs', ['page' => 'brands'])
<div class="row">
	<div class="col-md-6">
		<div class="card">
			<div class="card-header">
				<a href="{{ route('brands.index') }}" class="btn btn-light pull-left"><i class="fa fa-angle-left"></i>Назад</a>
				<form method="post" action="{{ route('brands.destroy', $brand->id) }}">
					@csrf
					{{ method_field('DELETE') }}
					<button type="submit" class="btn btn-outline-danger pull-right"><i class="fa fa-trash-o"></i>Удалить</button>
				</form>
			</div>
			<div class="card-body">
				<h4 class="card-title">Изменить название бренда</h4>
				<form method="post" action="{{ route('brands.update', $brand->id) }}">
					{{ method_field('PATCH') }}
					@csrf
					<div class="form-group">
						<input name="name" type="text" class="form-control" placeholder="Название бренда" value="{{ $brand->name }}" required>
					</div>
					<button type="submit" class="btn btn-success"><i class="fa fa-save"></i>Сохранить</button>
				</form>
			</div>
		</div>

	</div>
</div>

@endsection
