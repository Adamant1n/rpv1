@extends('layouts.app')

@section('content')

@include('common.validation')
@include('common.notifications')

@include('common.tabs', ['page' => 'categories'])
<div class="row">
	<div class="col-md-8">
		<div class="card">
			<div class="card-header d-flex justify-content-between">
				<a href="{{ route('categories.index') }}" class="btn btn-light pull-left"><i class="fa fa-angle-left"></i>Назад</a>
				<form method="post" action="{{ route('categories.destroy', $category->id) }}">
					@csrf
					{{ method_field('DELETE') }}
					<button type="submit" class="btn btn-outline-danger pull-right"><i class="fa fa-trash-o"></i>Удалить</button>
				</form>
			</div>
			<div class="card-body">
				<h4 class="card-title">Изменить категорию</h4>
				<form method="post" action="{{ route('categories.update', $category->id) }}">
					{{ method_field('PATCH') }}
					@csrf
					<div class="form-group">
						<input name="name" type="text" class="form-control" placeholder="Название бренда" value="{{ $category->name }}" required>
					</div>

					<div class="row">
						<div class="form-group col-6">
							<label>Тип</label>
							<select name="type" class="form-control">
								<option value="sale" {{ $category->type == 'sale' ? 'selected' : '' }}>Реализация (товар)</option>
								<option value="rent" {{ $category->type == 'rent' ? 'selected' : '' }}>Прокат (услуга)</option>
							</select>
						</div>

						<div class="form-group col-6 pt-4 d-flex align-items-center">
							<label class="mb-0">
								<input name="group" type="checkbox" value="1" {{ $category->group ? 'checked' : '' }} >
								Групповой артикул
							</label>
						</div>
					</div>

					<div class="form-group">
						<label>Секция ФР</label>
						<input name="fr_section" type="number" class="form-control" placeholder="Секция ФР" value="{{ $category->fr_section }}" required>
					</div>

					<hr>

					<h5>Активные свойства</h5>
					@foreach($available_params as $param_id => $param_name)
					<div class="form-group">
						<label>
							<input type="checkbox" name="params[{{ $param_id }}]" value="1" {{ $category->params[$param_id] == 1 ? 'checked' : '' }}>
							{{ $param_name }}
						</label>
					</div>
					@endforeach

					<button type="submit" class="btn btn-success"><i class="fa fa-save"></i>Сохранить</button>
				</form>
			</div>
		</div>

	</div>
</div>

@endsection
