@extends('layouts.app')

@section('content')

@include('common.validation')
@include('common.notifications')

@include('common.tabs', ['page' => 'categories'])
<div class="row">
	<div class="col-md-6">
		<div class="card">
			<div class="card-header">
				<a href="{{ route('categories.index') }}" class="btn btn-light"><i class="fa fa-angle-left"></i>Назад</a>
			</div>
			<div class="card-body">
				<h4 class="card-title">Новая категория</h4>
				<form method="post" action="{{ route('categories.store') }}">
					@csrf
					<div class="form-group">
						<input name="name" type="text" class="form-control" placeholder="Название категории" required autofocus>
					</div>

					<div class="row">
						<div class="form-group col-6">
							<label>Тип</label>
							<select name="type" class="form-control">
								<option value="sale">Реализация (товар)</option>
								<option value="rent" selected>Прокат (услуга)</option>
							</select>
						</div>

						<div class="form-group col-6 pt-4 d-flex align-items-center">
							<label class="mb-0">
								<input name="group" type="checkbox" value="1">
								Групповой артикул
							</label>
						</div>
					</div>

					<div class="form-group">
						<label>Секция ФР</label>
						<input name="fr_section" type="number" class="form-control" placeholder="Секция ФР" value="0" required>
					</div>

					<hr>

					<h5>Активные свойства</h5>
					@foreach($available_params as $param_id => $param_name)
					<div class="form-group">
						<label>
							<input type="checkbox" name="params[{{ $param_id }}]" value="1">
							{{ $param_name }}
						</label>
					</div>
					@endforeach

					<button type="submit" class="btn btn-success"><i class="fa fa-save"></i>Сохранить</button>
				</form>
			</div>
		</div>

	</div>
</div>

@endsection
