@extends('layouts.app')

@section('content')

@card
	@slot('title')
		@empty($permission->id)
		Создать разрешение
		@else
		Изменить разрешение
		@endempty
	@endslot

	@form(['entity' => $permission])
		<div class="row">
			<div class="form-group col-6">
				<label>Название</label>
				<input name="name" type="text" class="form-control" value="{{ old('name', $permission->name) }}">
			</div>

			<div class="form-group col-6">
				<label>Заголовок</label>
				<input name="display" type="text" class="form-control" value="{{ old('display', $permission->display) }}">
			</div>

			<div class="form-group col-6">
				<label>Guard</label>
				<input name="guard_name" type="text" class="form-control" value="{{ old('guard_name', $permission->guard_name) ?? 'web' }}">
			</div>
		</div>

		<button class="btn btn-outline-primary">Сохранить</button>

		@isset($permission->id)
			@deleteButton(['entity' => $permission])
			@enddeleteButton
		@endisset
	@endform

@endcard

@endsection