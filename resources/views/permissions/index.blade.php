@extends('layouts.app')

@push('js')
{!! $dataTable->getJs() !!}
@endpush

@section('content')

@card
	@slot('title')
	Разрешения
	@endslot

	@slot('toolbar')
		<a href="{{ route(getActionName('create')) }}" class="btn btn-sm btn-success ml-2 float-right">
			<i class="mdi mdi-plus-circle"></i> Создать
		</a>
	@endslot

	{!! $dataTable->getHtml() !!}

@endcard

@endsection