@extends('layouts.app')

@section('content')

@include('common.validation')
@include('common.notifications')

@push('js')
<script type="text/javascript">
	$('#checkPassport').click(function () {
		$.ajax({
            'url': "{{ route('expired_passports.check') }}",
            'type': 'POST',
            'data': {
            	data: $('#passport').val()
            },
            'success': function(data) {
                if(data.success)
                {
                	swal({
				        title: 'Успешно',
				        text: 'Данные паспорта отсутствуют в базе просроченных паспортов',
				        type: 'success',
				        button: {
							text: "Продолжить",
							value: true,
							visible: true,
							className: "btn btn-primary"
				        }
				    });
                }
                else
                {
                	swal({
				        title: 'Внимание!',
				        text: 'Данные паспорта находятся в базе просроченных паспортов',
				        type: 'error',
				        button: {
							text: "Продолжить",
							value: true,
							visible: true,
							className: "btn btn-primary"
				        }
				    });
                }
            },
            'error': function(){
                console.error('Something wrong!');
            }
        });
	});
</script>
@endpush

<div class="card">

	<div class="card-header">
		<a href="{{ route('clients.index') }}" class="btn btn-light">
			<i class="fa fa-angle-left"></i>Назад</a>
	</div>

	<div class="card-body">
		<h4 class="card-title">Новый клиент</h4>
		<form method="post" action="{{ route('clients.store') }}" enctype="multipart/form-data">
			@csrf

		<div class="row">
			<div class="col-md-10">

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label>Фамилия</label>
					<input type="text" class="form-control" name="last_name" required autofocus>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label>Имя</label>
					<input type="text" class="form-control" name="first_name" required>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label>Отчество</label>
					<input type="text" class="form-control" name="mid_name">
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label>Номер телефона</label>
					<input type="text" class="form-control" name="phone">
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label>E-mail</label>
					<input type="email" class="form-control" name="email">
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label>Номер паспорта</label>

					<div class="input-group col-xs-12">
                        <input type="text" class="form-control passport" name="passport" id="passport">
                        <span class="input-group-append">
                         	<button class="file-upload-browse btn btn-info" type="button" id="checkPassport">Проверить</button>
                        </span>
                    </div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label>Дата рождения</label>
					<input type="text" class="form-control date" name="birthday">
				</div>
			</div>
		</div>

		<div class="form-group">
			<label>Заметка</label>
			<textarea class="form-control" name="description"></textarea>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label>Вес</label>
					<input type="text" class="form-control" name="weight">
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label>Рост</label>
					<input type="text" class="form-control" name="height">
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label>Размер обуви</label>
					<input type="text" class="form-control" name="shoe_size">
				</div>
			</div>
		</div>

		<div class="form-group">
			<label>
				<input type="checkbox" name="is_signed" value="1">
				Договор подписан
			</label>
		</div>

			</div>
			<div class="col-md-2">

				<div class="form-group">
					<label>Фото</label>
					<input type="file" class="dropify" name="file_photo">
				</div>

				<div class="form-group">
					<label>Скан паспорта</label>
					<input type="file" class="dropify" name="file_passport">
				</div>

			</div>
		</div>

		<button type="submit" class="btn btn-success">
			<i class="fa fa-save"></i>Сохранить</button>

		</form>
	</div>

</div>

<script>
(function($) {
	'use strict';
	$(".date").inputmask("dd.mm.yyyy", {"placeholder": "дд.мм.гггг"});
	$(".passport").inputmask("9999 999999");
	$('.dropify').dropify();
})(jQuery);
</script>

@endsection
