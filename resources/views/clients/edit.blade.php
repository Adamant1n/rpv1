@extends('layouts.app')

@section('content')

@include('modals.photo')

@include('common.validation')
@include('common.notifications')

<style>
	.input-danger {
		border: 3px solid #f90000 !important;
	}
	.input-success {
		border: 3px solid #09b76b !important;
	}
</style>

@push('js')
<script type="text/javascript">
	// модальное окно
	$('#photo').click(function () {
		$('#photoPopup').modal('show');
	});

	$('#checkPassport').click(function () {
		var client_id = {{ $client->id }};
		$.ajax({
            'url': "{{ route('expired_passports.check') }}",
            'type': 'POST',
            'data': {
            	data: $('#passport').val(),
            	client_id: client_id
            },
            'success': function(data) {
            	$('#passport').removeClass('input-success');
            	$('#passport').removeClass('input-danger');
            	$('#passport').removeClass('input-dark');
                if(data.success)
                {
                	$('#passport').addClass('input-success');
                	/*
                	swal({
				        title: 'Успешно',
				        text: 'Данные паспорта отсутствуют в базе просроченных паспортов',
				        type: 'success',
				        button: {
							text: "Продолжить",
							value: true,
							visible: true,
							className: "btn btn-primary"
				        }
				    });
				    */
                }
                else
                {
                	$('#passport').addClass('input-danger');
                	/*
                	swal({
				        title: 'Внимание!',
				        text: 'Данные паспорта находятся в базе просроченных паспортов',
				        type: 'error',
				        button: {
							text: "Продолжить",
							value: true,
							visible: true,
							className: "btn btn-primary"
				        }
				    });
				    */
                }
            },
            'error': function(){
                console.error('Something wrong!');
            }
        });
	});
</script>
@endpush

<div class="card">
	<div class="card-header">
		<a href="{{ route('clients.index') }}" class="btn btn-light">
			<i class="fa fa-angle-left"></i>Назад</a>

		<button onclick="printImg('{{ route('clients.barcode', $client) }}')" target="_blank" class="btn btn-outline-primary">
			Скачать карту
		</button>

		@if($client->currentOrder)
		<a href="#" class="btn btn-outline-primary" id="client_current_order" data-id="{{ $client->currentOrder->optional_id }}">
			{{ $client->currentOrder->optional_id }}
		</a>
		@else
		<a href="#" id="client_new_order" data-id="{{ $client->optional_id }}" class="btn btn-outline-success">
			Новый договор
		</a>
		@endif

		<a href="#" id="client_new_booking" data-id="{{ $client->id }}" class="btn btn-outline-primary">
			Бронирование
		</a>

		<a href="#" id="client_new_delivery" data-id="{{ $client->id }}" class="btn btn-outline-info">
			Доставка
		</a>

	</div>

	<div class="card-body">
		<h4 class="card-title">Карта клиента {{ $client->optional_id }}</h4>
		<form method="POST" action="{{ route('clients.update', $client) }}" enctype="multipart/form-data">
			@csrf
			{{ method_field('PATCH') }}

			<div class="row">
				<div class="col-md-10">

					<div class="row">
						<div class="col-md-4">
							<div class="form-group">
								<label>Фамилия</label>
								<input type="text" class="form-control" name="last_name"
									value="{{ $client->last_name }}" required autofocus>
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								<label>Имя</label>
								<input type="text" class="form-control" name="first_name"
									value="{{ $client->first_name }}" required>
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								<label>Отчество</label>
								<input type="text" class="form-control" name="mid_name"
									value="{{ $client->mid_name }}">
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>Номер телефона</label>
								<input type="text" class="form-control" name="phone"
									value="{{ $client->phone }}">
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label>E-mail</label>
								<input type="text" class="form-control" name="email"
									value="{{ $client->email }}">
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>Номер паспорта</label>

								<div class="input-group col-xs-12">
									@php
									if($client->passport_status === \App\Client::PASSPORT_STATUS_VALID)
										$passport_class = 'input-success';
									else if ($client->passport_status === \App\Client::PASSPORT_STATUS_INVALID)
										$passport_class = 'input-danger';
									else {
										$passport_class = 'input-dark'; // form-control-
									}
									@endphp

			                        <input type="text" class="form-control passport {{$passport_class}}" name="passport" id="passport" value="{{ $client->passport }}">

			                        @if($client->passport_status !== \App\Client::PASSPORT_STATUS_VALID)
			                        <span class="input-group-append">
			                         	<button class="file-upload-browse btn btn-info" type="button" id="checkPassport">Проверить</button>
			                        </span>
			                        @endif
			                    </div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label>Дата рождения</label>
								<input type="text" class="form-control date" name="birthday"
									value="{{ $client->birthday ? $client->birthday->format('d.m.Y') : '' }}">
							</div>
						</div>
					</div>

					<div class="form-group">
						<label>Заметка</label>
						<textarea class="form-control" name="description">{{ $client->description }}</textarea>
					</div>

					<div class="row">
						<div class="col-md-4">
							<div class="form-group">
								<label>Вес</label>
								<input type="number" class="form-control" name="weight"
									value="{{ $client->feature->weight }}" maxlength="3">
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								<label>Рост</label>
								<input type="number" class="form-control" name="height"
									value="{{ $client->feature->height }}" maxlength="3">
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								<label>Размер обуви</label>
								<input type="number" class="form-control" name="shoe_size"
									value="{{ $client->feature->shoe_size }}" maxlength="3">
							</div>
						</div>
					</div>

					<div class="form-group">
						<label>
							<input type="checkbox" name="is_signed" value="1" {{ $client->is_signed ? 'checked' : ''}}>
							Договор подписан
						</label>
					</div>

					<!--
					<div class="row">
						@if ($client->photo)
						<div class="col-md-6">
							<div class="form-group">
								<label>Фото</label>
								<input type="file" class="form-control" name="file_photo">
							</div>
						</div>
						@endif
						@if ($client->passport_scan)
						<div class="col-md-6">
							<div class="form-group">
								<label>Скан паспорта</label>
								<input type="file" class="form-control" name="file_passport">
							</div>
						</div>
						@endif
					</div>
				-->

				</div>
				<div class="col-md-2">

					<div class="form-group">
						<div>
							<label>Фото</label>
						</div>

						<button class="form-control btn mb-2" id="photo" type="button">Вебкамера</button>

					<!--
						<a href="{{ UploadImage::load('client').$client->photo }}"∂>
							<img alt="" src="{{ UploadImage::load('client', 150).$client->photo }}">
						</a>
					-->
						<input type="hidden" name="photo_webcam" id="photo_webcam">
						<input type="file" class="dropify" name="file_photo" data-default-file="{{ UploadImage::load('client', 150).$client->photo }}">
						<div class="text-center">
							<a href="{{ UploadImage::load('client').$client->photo }}" target="_blank">Скачать</a>
						</div>
					</div>

					<div class="form-group">
					<div>
						<label>Скан паспорта</label>
					</div>
						<input type="file" class="dropify" name="file_passport" data-default-file="{{ UploadImage::load('passport', 150).$client->passport_scan }}">
						<div class="text-center">
							<a href="{{ UploadImage::load('passport').$client->passport_scan }}" target="_blank">Скачать</a>
						</div>
					</div>

				</div>
			</div>

			<hr />
			<div class="row">
				<div class="form-group col-3">
					<label>Договор</label>
					<input type="text" class="form-control" disabled value="{{ $client->optional_id }}">
				</div>
				<!--
				<div class="form-group col-3">
					<label>Z-число</label>
					<input type="text" class="form-control" disabled value="{{ $client->z_num }}">
				</div>
				-->
				<div class="form-group col-3">
					<label>Дата создания</label>
					<input type="text" class="form-control" disabled value="{{ $client->created_at }}">
				</div>
				<div class="form-group col-3">
					<label>Дата редактирования</label>
					<input type="text" class="form-control" disabled value="{{ $client->updated_at }}">
				</div>
			</div>

			<button type="submit" class="btn btn-success">
				<i class="fa fa-save"></i>Сохранить</button>

		</form>
	</div>

</div>

<div class="card mt-3">
	<div class="card-body">
		<h4 class="card-title">История заказов</h4>

		<table id="orders-listing" class="table">
		<thead>
			<tr>
				<th>ID</th>
				<th>Длительность</th>
				<th>Стоимость</th>
				<th>Статус</th>
				<th>Дата создания</th>
				<th>Дата начала</th>
				<th>Дата окончания</th>
			</tr>
		</thead>
		<tbody>

		@php $d = ['m'=>'м.','h'=>'ч.','d'=>'д.'] @endphp

		@foreach ($client->orders as $order)
		<tr>
			<td>{{ $order->optional_id }}</td>
			<td>{{ $order->duration.' '.$d[$order->dimension] }}</td>
			<td>{{ $order->price }}</td>
			<td>
				{!! $order->status_badge !!}
			</td>
			<td>{{ $order->created_at->format('d.m.Y H:i') }}</td>
			<td>{{ $order->starts_at->format('d.m.Y H:i') }}</td>
			<td>{{ $order->ends_at->format('d.m.Y H:i') }}</td>
		</tr>
		@endforeach

		</tbody>
		</table>
	</div>
</div>

<div class="card mt-3">
	<div class="card-body">
		<h4 class="card-title">История скидок</h4>

		<table id="discounts-listing" class="table">
		<thead>
			<tr>
				<th>Заказ</th>
				<th>Агент</th>
				<th>Размер скидки, %</th>
				<th>Полная стоимость</th>
				<th>Скидка, руб.</th>
				<th>Итог, руб.</th>
				<th>Дата заказа</th>
			</tr>
		</thead>
		<tbody>

		@php $d = ['m'=>'м.','h'=>'ч.','d'=>'д.'] @endphp

		@foreach ($client->discounts as $order)
		<tr>
			<td>{{ $order->optional_id }}</td>
			<td>{{ $order->agent->name }}</td>
			<td>{{ $order->agent->discount }}%</td>
			<td>{{ $order->real_price }} руб.</td>
			<td>{{ $order->discount }} руб.</td>
			<td>{{ $order->price }}</td>
			<td>
				{!! $order->status_badge !!}
			</td>
			<td>{{ $order->created_at->format('d.m.Y H:i') }}</td>
		</tr>
		@endforeach

		</tbody>
		</table>
	</div>
</div>

<script>
$(document).ready(function () {
	
		$('#orders-listing').DataTable({
			"paging": false,
			"info": false,
			"searching": false,
			"aaSorting": [[0, "asc"]],
			"language": {
				"lengthMenu": "Показывать _MENU_ записей на странице",
				"zeroRecords": "Записей не найдено",
				"loadingRecords": "Загрузка... может занять несколько секунд...",
				"info": "Страница _PAGE_ из _PAGES_",
				"infoEmpty": "Показано с 0 по 0 из 0 записей",
				"search": "",
				"infoFiltered": "(Найдено записей: _TOTAL_)",
				"sInfo": "Показано с _START_ по _END_ из _TOTAL_ записей",
				"paginate":
				{
					"first":    "Первая",
					"last":     "Последняя",
					"next":     "Следующая",
					"previous": "Предыдущая"
				}
			}
		});
	
		$('#orders-listing').each(function() {
			var datatable = $(this);
			var search_input = datatable.closest('.dataTables_wrapper').find('div[id$=_filter] input');
			search_input.attr('placeholder', 'Поиск');
			search_input.removeClass('form-control-sm');
			var length_sel = datatable.closest('.dataTables_wrapper').find('div[id$=_length] select');
			length_sel.removeClass('form-control-sm');
		});

		//$(".date").inputmask("dd.mm.yyyy", {"placeholder": "дд.мм.гггг"});
		$(".passport").inputmask("9999 999999");
		$('.dropify').dropify();
		
});
</script>

@endsection
