@extends('layouts.app')

@section('content')

@include('common.validation')
@include('common.notifications')

<div class="row">
	<div class="col-md-6">
		<div class="card">
			<div class="card-header">
				Настройки системы
			</div>
			<div class="card-body">
				<form method="post" action="{{ route('settings.update') }}" enctype="multipart/form-data">
					@csrf
					<div class="form-group">
						<label>Шаблон договора</label>
						@if($template)
						<a href="{{ asset('storage/' . $template->value) }}" class="btn btn-outline-info btn-xs mb-2 btn-right">Скачать</a>
						@endif
						<input type="file" class="form-control" name="template">
					</div>

                    <div class="form-group">
                        <label>Шаблон залога</label>
                        @if($pledgeTemplate)
                        <a href="{{ asset('storage/' . $pledgeTemplate->value) }}" class="btn btn-outline-info btn-xs mb-2 btn-right">Скачать</a>
                        @endif
                        <input type="file" class="form-control" name="pledge_template">
                    </div>

					<button type="submit" class="btn btn-success"><i class="fa fa-save"></i>Сохранить</button>
				</form>
			</div>
		</div>

	</div>
</div>

@endsection
