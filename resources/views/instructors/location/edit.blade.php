@extends('instructors.layouts.template')

@push('js')
<script type="text/javascript">
    $(document).ready(function () {
        var timepicker_options = {
            use24hours: true,
            format: 'HH:mm',
            stepping: 10
        };

        $('#opens_at').datetimepicker(timepicker_options);
        $('#closes_at').datetimepicker(timepicker_options);
    });
</script>
<style>
.col-add-7 {
    position: relative;
    width: 100%;
    min-height: 1px;
    padding-right: 15px;
    padding-left: 15px;
    flex: 0 0 14.28571%;
    max-width: 14.28571%;
}
</style>
@endpush

@section('content')

<div class="row">
	<div class="col-md-6">

		@if ($errors->any())
			<div class="alert alert-danger" role="alert">
				<ul>
					@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif

		<div class="card">
			<div class="card-header">
				<a href="{{ route('instructors.location.index') }}" class="btn btn-light"><i class="fa fa-angle-left"></i>Назад</a>
				<a href="/instructors/location/{{ $location->id }}/delete" class="btn btn-outline-danger pull-right"><i class="fa fa-trash-o"></i>Удалить</a>
			</div>
			<div class="card-body">
				<h4 class="card-title">Настройка филиала</h4>
				<form method="post" action="{{ route('instructors.location.update', $location->id) }}">
					{{ method_field('PUT') }}
					@csrf
					<div class="form-group">
						<input name="address" type="text" class="form-control" placeholder="Адрес филиала" value="{{ $location->address }}" required>
					</div>				

                    <hr />

                    <div class="form-group">
                        <label class="">Тип филиала</label>
                        <select class="form-control" name="type">
                            <option value="both" {{ $location->type == 'both' ? 'selected' : '' }}>Прокат и инструкторы</option>
                            <option value="rent" {{ $location->type == 'rent' ? 'selected' : '' }}>Прокат</option>
                            <option value="instructors" {{ $location->type == 'instructors' ? 'selected' : '' }}>Инструкторы</option>
                        </select>
                    </div>

                    <hr />

                    <h5>
                        График работы
                    </h5>

                    <div class="form-group row" id="opens_closes">
                        <label class="col-1 col-form-label">С: </label>
                        <div class="col-4">
                            <div class="input-group date" id="opens_at" data-target-input="nearest">
                                <div class="input-group" data-target="#opens_at" data-toggle="datetimepicker">
                                    <input type="text" class="form-control datetimepicker-input" value="{{ $location->opens_at }}" name="opens_at" data-target="#opens_at"/>
                                    <div class="input-group-addon input-group-append"><i class="mdi mdi-clock input-group-text"></i></div>
                                </div>
                            </div>
                        </div>

                        <label class="col-3 text-right col-form-label">До</label>
                        <div class="col-4">
                            <div class="input-group date" id="closes_at" data-target-input="nearest">
                                <div class="input-group" data-target="#closes_at" data-toggle="datetimepicker">
                                    <input type="text" class="form-control datetimepicker-input" value="{{ $location->closes_at }}" name="closes_at" data-target="#closes_at"/>
                                    <div class="input-group-addon input-group-append"><i class="mdi mdi-clock input-group-text"></i></div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-12">Настройка выходных дней</label>
                        <label class="col-add-7">
                            <input type="checkbox" name="weekends[]" value=1 {{ in_array(1, $location->weekends) ? 'checked' : '' }}> Пн
                        </label>
                        <label class="col-add-7">
                            <input type="checkbox" name="weekends[]" value=2 {{ in_array(2, $location->weekends) ? 'checked' : '' }}> Вт
                        </label>
                        <label class="col-add-7">
                            <input type="checkbox" name="weekends[]" value=3 {{ in_array(3, $location->weekends) ? 'checked' : '' }}> Ср
                        </label>
                        <label class="col-add-7">
                            <input type="checkbox" name="weekends[]" value=4 {{ in_array(4, $location->weekends) ? 'checked' : '' }}> Чт
                        </label>
                        <label class="col-add-7">
                            <input type="checkbox" name="weekends[]" value=5 {{ in_array(5, $location->weekends) ? 'checked' : '' }}> Пт
                        </label>
                        <label class="col-add-7">
                            <input type="checkbox" name="weekends[]" value=6 {{ in_array(6, $location->weekends) ? 'checked' : '' }}> Сб
                        </label>
                        <label class="col-add-7">
                            <input type="checkbox" name="weekends[]" value=7 {{ in_array(7, $location->weekends) ? 'checked' : '' }}> Вс
                        </label>
                    </div>

                    <hr />

                    <button type="submit" class="btn btn-success"><i class="fa fa-save"></i>Сохранить</button>
                </form>

			</div>
		</div>

	</div>
</div>

@endsection
