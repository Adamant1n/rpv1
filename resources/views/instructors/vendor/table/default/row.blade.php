<tr>
    @if ($hasBatchActions)
        <td onclick="toggleInnerCheckbox();">
            <input type="checkbox" name="b[]" value="{{array_get($rowData, 'id')}}">
        </td>
    @endif
    @foreach($columns as $key => $column)
        <td @if($orderField == $key) class="ctable-ordered" @endif>
            {{array_get($rowData, $key)}}
        </td>
    @endforeach
    @if(count($actions) || count($filters) || count($exporters))
        <td>
            @foreach($actions as $route => $button)
                <a href="{{route($route, $rowData)}}" class="btn btn-sm btn-{{ $button['style'] }}">{{ $button['name'] }}</a>
            @endforeach
        </td>
    @endif
</tr>