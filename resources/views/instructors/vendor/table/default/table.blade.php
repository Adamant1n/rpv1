<div class="table-content">
    <table class="table">
        <thead>
        <tr>
            @if (!empty($batchActions))
                <th onclick="toggleInnerCheckbox();">
                    <input type="checkbox" onchange="toggleSelectAll()">
                </th>
            @endif
            @foreach($columns as $key => $column)
                <th>
                    @if(in_array($key, $sortables))
                        @if($orderField == $key)
                            <a href="?{{http_build_query(array_merge(request()->all(), [ 'orderField' => $key, 'orderDirection' => $orderDirection == 'asc' ? 'desc' : 'asc']))}}">
                                {{$column}}
                                @if($orderDirection == 'asc')
                                    <span class="table-arrow-up"></span>
                                    <span class="table-arrow-down" style="visibility: hidden;"></span>
                                @else
                                    <span class="table-arrow-down"></span>
                                    <span class="table-arrow-up" style="visibility: hidden;"></span>
                                @endif
                            </a>
                        @else
                            <a href="?{{http_build_query(array_merge(request()->all(), [ 'orderField' => $key, 'orderDirection' => 'asc']))}}">
                                {{ $column }}
                                <span class="table-arrow-up"></span><span class="table-arrow-down"></span>&nbsp;
                            </a>
                        @endif
                    @else
                        {{ $column }}
                    @endif
                </th>
            @endforeach
            @if(count($actions) || count($filters) || count($exporters))
                <th></th>
            @endif
        </tr>
        </thead>
        <tbody>
        <tr>
            @if($filters || $exporters)
            <form method="get" class="form-vertical">
            @foreach($columns as $key => $column)
            <td>
                @if(isset($filters[$key]))
                {!! $filters[$key]->render() !!}
                @endif
            </td>
            @endforeach

            <td>
                @if($filters)
                <input type="submit" class="btn btn-primary" value="{{trans('table::table.button.filter')}}">
                @endif
                @foreach($exporters as $key => $exporter)
                    <a class="btn btn-success" @if ($exporter->isTargetBlank()) target="_blank" @endif
                    href="?{{http_build_query(array_merge(request()->all(), ['export_to' => $key]))}}">
                        Экспорт .csv
                    </a>
                @endforeach
            </td>
            @if($filters)
            <input type="hidden" name="orderField" value="{{$orderField}}">
            <input type="hidden" name="orderDirection" value="{{ $orderDirection">
            @endif
            </form>
            @endif
        </tr>
        @foreach($rows as $rowData)
            @include($rowViewPath, [
                'data' => $rowData,
                'columns' => $columns,
                'actions' => $actions,
                'orderField' => $orderField,
                'hasBatchActions' => !empty($batchActions),
                'filters' => $filters,
                'exporters' => $exporters
            ])
        @endforeach
        </tbody>
    </table>
</div>
<nav>
    @include('table::default.pagination', ['paginator' => $pagination])
</nav>