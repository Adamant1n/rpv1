<div class="form-group">
    <input @foreach ($attributes as $k => $v) {{ $k }}="{{ $v }}" @endforeach class="form-control" type="text"
    value="{{$value}}" name="f_{{$name}}"/>
</div>