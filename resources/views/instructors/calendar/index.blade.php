@extends('instructors.layouts.template')

@section('content')

@if (Session::has('success'))
<div class="alert alert-success" role="alert">
	{{ Session::get('success') }}
</div>
@endif

<style>
.fc-resource-area {
	min-width: 350px !important;
	width: 350px !important;
}
</style>

<div class="card">
	@role('superadmin|admin|operator')
	<div class="card-header">
		<form method="get" action="/instructors/calendar">
			<div class="row">
				<div class="col-md-4">
					<select class="form-control select1" name="location" onchange="this.form.submit()">
						<option value="">Точка продаж...</option>
						@foreach ($locations as $location)
						<option value="{{ $location->id }}"
						{{ $location->id == app('request')->input('location') ? 'selected' : '' }}>{{ $location->address }}</option>
						@endforeach
					</select>
				</div>
				<div class="col-md-4">
					<select class="form-control select2" name="manager" onchange="this.form.submit()">
						<option value="">Менеджер...</option>
						@foreach ($managers as $manager)
						<option value="{{ $manager->id }}"
						{{ $manager->id == app('request')->input('manager') ? 'selected' : '' }}>{{ $manager->last_name }} {{ $manager->first_name }} {{ $manager->middle_name }}</option>
						@endforeach
					</select>
				</div>
				<div class="col-md-4">
					<select class="form-control select2" name="trainer" onchange="this.form.submit()">
						<option value="">Инструктор...</option>
						@foreach ($trainers as $trainer)
						<option value="{{ $trainer->id }}"
						{{ $trainer->id == app('request')->input('trainer') ? 'selected' : '' }}>{{ $trainer->last_name }} {{ $trainer->first_name }} {{ $trainer->middle_name }}</option>
						@endforeach
					</select>
				</div>
			</div>
		</form>
	</div>
	@endrole
	<div class="card-body">
		<div id="calendar"></div>
	</div>
</div>

<script>
(function($) {
	'use strict';
	$(function() {
		var category_id = 1;

		$('#calendar').fullCalendar({
			height: 'auto',
			customButtons: {
				tomorrow: {
					text: 'Завтра',
					click: function() {
						$('#calendar').fullCalendar('gotoDate', moment(new Date()).add(1, 'days'));
					}
				}
			},
			header: {
				left: 'prev,next today tomorrow',
				center: 'title',
				right: 'timelineDay,timelineWeek,month'
			},
			eventOverlap: false,
			defaultView: 'timelineDay',
			resourceAreaWidth: '25%',
			slotLabelInterval: '01:00',
			slotLabelFormat: 'HH:mm',
			minTime: '{{ $currentLocation ? $currentLocation->opens_at . ':00' : '23:59:00'}}',
			maxTime: '{{ $currentLocation ? $currentLocation->closes_at . ':00' : '23:59:00'}}',
			editable: false,
			droppable: false,
			selectable: true,
			titleFormat: 'D MMMM',
			timeFormat: 'HH:mm',
			resourceLabelText: 'Инструктор',
			resources: function (callback) {
				$.ajax({
				  	url: '/instructors/calendar/resources_json',
				  	data: {
						category_id: category_id
				  	}
				}).done(function(response) {
				  	callback(response); 
				});
			},
			events: '/instructors/calendar/events_json?location={{ app('request')->input('location') }}&manager={{ app('request')->input('manager') }}&trainer={{ app('request')->input('trainer') }}',
			dayClick: function(date, jsEvent, view, resource) {
				if (parseInt(resource.id)) {
					document.location.href = '/instructors/event/create?trainer_id='+resource.id+'&category_id='+resource.category_id+'&event_date='+date.format('YYYY-MM-DD')+'&start_time='+date.format('HH:mm');
				}
			},
			select: function(startDate, endDate, jsEvent, view, resource) {
				if (parseInt(resource.id)) {
					document.location.href = '/instructors/event/create?trainer_id='+resource.id+'&category_id='+resource.category_id+'&event_date='+startDate.format('YYYY-MM-DD')+'&start_time='+startDate.format('HH:mm')+'&end_time='+endDate.format('HH:mm');
				}
			}
		});

		$('.fc-cell-text').each(function () {
			var th = $(this).html();

			if(th == 'Инструктор') {
				var contents = '<ul class="nav tab-solid tab-solid-warning" role="tablist">';

				@foreach($categories as $category)
				contents += '<li class="nav-item">';
				contents += '<a class="nav-link {{ $loop->index == 0 ? 'active' : '' }} set_event_category" data-id="{{ $category->id }}" data-toggle="tab" aria-selected="{{ $loop->index == 0 ? 'true' : 'false' }}">';
				contents += '<img src="/instr/{{ $category->icon }}" width="30px" height="30px">';
				contents += '{{ $category->name }}';
				contents += '</a>';
				contents += '</li>';

				console.log(contents);
				@endforeach

				contents += '</ul>';
				
				$(this).parent().parent().parent().parent().parent().parent().parent().parent().parent().parent().parent().html(contents);
			}
		});


		$('body').on('click', '.set_event_category', function () {
			category_id = $(this).data('id');
			// console.log(category_id);
			$('#calendar').fullCalendar('refetchResources');
			$('#calendar').fullCalendar('refetchEvents')
		});
	});
	$(".select2").select2();
	$(".select1").select2({
		minimumResultsForSearch: -1
	});
})(jQuery);
</script>

@endsection
