<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<title>{{ config('app.name', 'Laravel') }}</title>
	<link rel="stylesheet" href="{{ asset('instr/vendors/iconfonts/mdi/css/materialdesignicons.min.css') }}">
	<link rel="stylesheet" href="{{ asset('instr/vendors/iconfonts/puse-icons-feather/feather.css') }}">
	<link rel="stylesheet" href="{{ asset('instr/vendors/css/vendor.bundle.base.css') }}">
	<link rel="stylesheet" href="{{ asset('instr/vendors/css/vendor.bundle.addons.css') }}">
	<link rel="stylesheet" href="{{ asset('instr/vendors/iconfonts/simple-line-icon/css/simple-line-icons.css') }}">
	<link rel="stylesheet" href="{{ asset('instr/vendors/lightgallery/css/lightgallery.css') }}">
	<link rel="stylesheet" href="{{ asset('instr/vendors/iconfonts/font-awesome/css/font-awesome.min.css') }}" />
	<link rel="stylesheet" href="{{ asset('instr/css/style.css') }}">
	<link rel="shortcut icon" href="{{ asset('instr/images/favicon.png') }}" />
	<style>.fc-cell-text {font-size:smaller;}</style>

	<script src="{{ asset('instr/vendors/js/vendor.bundle.base.js') }}"></script>
	<script src="{{ asset('instr/vendors/js/vendor.bundle.addons.js') }}"></script>

	@if (Request::is('report/*'))
	<script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
	<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
	<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
	<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>
	@endif

	@stack('style')
</head>
<body class="horizontal-menu">
	<div class="container-scroller">
		<!-- partial:partials/_navbar.html -->
		<nav class="navbar col-lg-12 col-12 p-0 fixed-top d-flex flex-row navbar-warning" style="background: #F7820C">
			<div class="text-center navbar-brand-wrapper d-flex align-items-center justify-content-center">
				<a class="navbar-brand brand-logo text-success" href="{{ url('/') }}">
                    <img src="{{ asset('/images/logo/lg_i.png') }}" style="object-fit:contain">
                </a>
                <a class="navbar-brand brand-logo-mini text-success" href="{{ url('/') }}">
                    <img src="{{ asset('/images/logo/sm_i.png') }}" style="object-fit:contain">            
                </a>
			</div>
			<div class="navbar-menu-wrapper d-flex align-items-stretch">
				<ul class="navbar-nav navbar-nav-right">
					<li class="nav-item dropdown">
						<a class="nav-link dropdown-toggle nav-profile" id="profileDropdown" href="#" data-toggle="dropdown" aria-expanded="false">
							<img src="{{ asset('instr/images/faces/face1.jpg') }}" alt="image">
							<span class="d-none d-lg-inline">
								<i class="fa fa-user mr-2"></i>
								{{ Auth::user()->full_name }}
							</span>
						</a>
						<div class="dropdown-menu navbar-dropdown w-100" aria-labelledby="profileDropdown">
							<!--<a class="dropdown-item" href="/user/{{ Auth::user()->id }}/edit">
								<i class="icon-user mr-2 text-success"></i>
								Профиль
							</a>-->
							<div class="dropdown-divider"></div>
							<a class="dropdown-item" href="{{ route('logout') }}">
								<i class="mdi mdi-logout mr-2 text-primary"></i>
								Выход
							</a>
						</div>
					</li>
				</ul>
			</div>
		</nav>
		<!-- partial -->
		<div class="container-fluid page-body-wrapper">
			<!-- partial -->
			<!-- partial:partials/_sidebar.html -->
			<nav class="sidebar sidebar-offcanvas" id="sidebar">
				<ul class="nav">
					<li class="nav-item">
						<a class="nav-link" href="/instructors/calendar">
							<i class="icon-calendar menu-icon"></i>
							<span class="menu-title">Расписание</span>
						</a>
					</li>
					@role('superadmin|admin|manager|operator')
					<li class="nav-item">
						<a class="nav-link" href="{{ route('instructors.event.index') }}">
							<i class="icon-trophy menu-icon"></i>
							<span class="menu-title">Занятия</span>
						</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="{{ route('instructors.clients.index') }}">
							<i class="icon-user-follow menu-icon"></i>
							<span class="menu-title">Клиенты</span>
						</a>
					</li>
					@endrole
					@role('superadmin|admin')
					<li class="nav-item">
						<a class="nav-link">
							<i class="icon-chart menu-icon"></i>
							<span class="menu-title">Отчеты</span>
							<i class="menu-arrow"></i>
						</a>
						<div class="collapse" id="reports">
							<ul class="nav flex-column sub-menu">
								<li class="nav-item">
									<a class="nav-link" href="/instructors/report/sales">Отчет о продажах</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" href="/instructors/report/trainers">Загрузка теренеров</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" href="/instructors/report/efficiency">Эффективность продаж</a>
								</li>
							</ul>
						</div>
					</li>
					<li class="nav-item">
						<a class="nav-link">
							<i class="icon-chart menu-icon"></i>
							<span class="menu-title">Настройки</span>
							<i class="menu-arrow"></i>
						</a>
						<div class="collapse" id="reports">
							<ul class="nav flex-column sub-menu">
								<li class="nav-item">
									<a class="nav-link" href="{{ route('instructors.service.index') }}">Услуги</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" href="{{ route('instructors.tariff.index') }}">Тарифы</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" href="{{ route('instructors.category.index') }}">Категории занятий</a>
								</li>
								@if(false)
								<li class="nav-item">
									<a class="nav-link" href="{{ route('instructors.location.index') }}">Точки продаж</a>
								</li>
								@endif
								<li class="nav-item">
									<a class="nav-link" href="{{ route('instructors.user.index') }}">Пользователи</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" href="{{ route('instructors.settings.edit') }}">Шаблоны</a>
								</li>
							</ul>
						</div>
					</li>
					@endrole
				</ul>
			</nav>
			<!-- partial -->
			<div class="main-panel">
				<div class="content-wrapper">
					@yield('content')
				</div>
				<!-- content-wrapper ends -->
				<!-- partial -->
			</div>
			<!-- main-panel ends -->
		</div>
		<!-- page-body-wrapper ends -->
	</div>
	<!-- container-scroller -->

	<script>
	(function($) {
	'use strict';
	$(function() {
		$('title').html($('h4.card-title').html());
	});
	})(jQuery);
	</script>
	<script src="{{ asset('instr/js/hoverable-collapse.js') }}"></script>
	@stack('js_bottom')
	@stack('js')

	@if(Auth::user() && Auth::user()->hasRole('operator'))
	@include('instructors.callback.phone')
	@endif
</body>
</html>
