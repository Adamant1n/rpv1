@extends('instructors.layouts.template')

@section('content')

<div class="row">
	<div class="col-md-6">

		@if ($errors->any())
			<div class="alert alert-danger" role="alert">
				<ul>
					@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif

		<div class="card">
			<div class="card-header">
				<a href="{{ route('instructors.user.index') }}" class="btn btn-light pull"><i class="fa fa-angle-left"></i>Назад</a>
				@if ($user->deleted_at)
				<a href="/instructors/user/{{ $user->id }}/restore" class="btn btn-outline-success pull-right"><i class="fa fa-trash-o"></i>Восстановить</a>
				@else
				<a href="/instructors/user/{{ $user->id }}/delete" class="btn btn-outline-danger pull-right"><i class="fa fa-trash-o"></i>Уволен</a>
				@endif
			</div>
			<div class="card-body">
				<h4 class="card-title">Профиль пользователя</h4>
				<form method="post" action="{{ route('instructors.user.update', $user->id) }}">
					@csrf
					{{ method_field('PUT') }}
					<div class="form-group text-capitalize">
						<label for="role">Роль</label>
						<select class="form-control select2" name="role" id="role" required>
							@foreach ($roles as $role)
							<option value="{{ $role->name }}"
							{{ $role->name == $user->getRoleNames()[0] ? 'selected' : '' }}>{{ $role->name }}</option>
							@endforeach
						</select>
					</div>
					<div id="category" style="{{ $user->getRoleNames()[0] != 'trainer' ? 'display:none;' : '' }}">
						<div class="form-group">
							<label for="category_id">Категория</label>
							<select class="form-control select2" name="category_id" id="category_id" style="width:100%;">
								<option value="">Сделайте выбор...</option>
								@foreach ($categories as $category)
								<option value="{{ $category->id }}"
								{{ $category->id == $user->category_id ? 'selected' : '' }}>{{ $category->name }}</option>
								@endforeach
							</select>
						</div>
						<div class="form-group">
							<label for="rate">Ставка</label>
							<input name="rate" id="rate" type="number" class="form-control" placeholder="Ставка" value="{{ $user->rate }}">
						</div>
					</div>
					<div class="form-group" id="location">
						<label for="location_id">Точка продаж</label>
						<select class="form-control select2" name="location_id" id="location_id" style="width:100%;" {{ optional($user->role)->name == \App\User::ROLE_TRAINER ? 'multiple' : '' }}>
							<option value="" disabled>Сделайте выбор...</option>
							@foreach ($locations as $location)
							<option value="{{ $location->id }}"
							{{ ($location->id == $user->location_id || in_array($location->id, $location_ids)) ? 'selected' : '' }}>{{ $location->address }}</option>
							@endforeach
						</select>
					</div>
					<div class="form-group">
						<label for="last_name">Фамилия</label>
						<input name="last_name" id="last_name" type="text" class="form-control" placeholder="Фамилия" value="{{ $user->last_name }}" maxlength="50" required>
					</div>
					<div class="form-group">
						<label for="first_name">Имя</label>
						<input name="first_name" id="first_name" type="text" class="form-control" placeholder="Имя" value="{{ $user->first_name }}" maxlength="50" required>
					</div>
					<div class="form-group">
						<label for="middle_name">Отчество</label>
						<input name="mid_name" id="mid_name" type="text" class="form-control" placeholder="Отчество" value="{{ $user->mid_name }}" maxlength="50" required>
					</div>
					<div class="form-group">
						<label for="email">E-mail</label>
						<input name="email" id="email" type="email" class="form-control" placeholder="E-mail" value="{{ $user->email }}" maxlength="100" required>
					</div>
					<div class="form-group">
						<label for="phone">Телефон</label>
						<input name="phone" id="phone" type="text" class="form-control" placeholder="Телефон" value="{{ $user->phone }}" maxlength="50" required>
					</div>
					<div class="form-group">
						<label for="password">Изменить пароль</label>
						<input name="password" id="password" type="password" class="form-control" placeholder="Пароль" value="">
					</div>
					<button type="submit" class="btn btn-success"><i class="fa fa-save"></i>Сохранить</button>
				</form>
			</div>
		</div>

	</div>
</div>

<script>
(function($) {
	'use strict';
	$(".select2").select2({
		minimumResultsForSearch: -1
	});

	$('#role').on('change', function() {
		if (this.value == '{{ \App\User::ROLE_TRAINER }}'){
			console.log('multiple');


			$('#location_id').attr('multiple', 'multiple');
			$('#location_id').attr('name', 'location_id[]');
			$('#location_id').select2();
			$('#category').show();
		}
		else {
			$('#location_id').attr('name', 'location_id');
			$('#location_id').removeAttr('multiple');
			$('#location_id').select2();
			$('#category').hide();
		}

		if (this.value == 'operator'){
			$('#location').hide();
		}
		else {
			$('#location').show();
		}
	});

	$('#role').trigger('change');

})(jQuery);
</script>

@endsection
