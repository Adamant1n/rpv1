@extends('instructors.layouts.template')

@section('content')

@if (Session::has('success'))
<div class="alert alert-success" role="alert">
	{{ Session::get('success') }}
</div>
@endif

<div class="card">
	<div class="card-header">
		<div class="row">
			<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
				<a href="{{ route('instructors.user.create') }}" class="btn btn-success"><i class="fa fa-plus"></i>Добавить пользователя</a>
			</div>
			<div class="col-xs-12 col-sm-6">
				<form>
				<label>
					<input type="checkbox" name="trashed" onchange="this.form.submit()" {{ $request->has('trashed') ? 'checked' : '' }}>
					Показать уволенных
				</label>
				</form>
			</div>
		</div>
	</div>
	<div class="card-body">
		<h4 class="card-title">Пользователи</h4>
		<div class="row">
			<div class="col-12">
				<table id="user-listing" class="table">
				<thead>
					<tr>
						<th>ID</th>
						<th>ФИО</th>
						<th>Роль</th>
						<th>Точка продаж</th>
						<th>Дата создания</th>
						<th>Дата изменения</th>
					</tr>
				</thead>
				<tbody>

				@foreach ($users as $user)
				<tr>
					<td>{{ $user->id }}</td>
					<td>{{ $user->last_name }} {{ $user->first_name }} {{ $user->middle_name }}</td>
					<td class="text-capitalize">{{ $user->getRoleNames()[0] }}</td>
					<td>{{ isset($user->location_id) ? $locations->find($user->location_id)->address : '' }}</td>
					<td>{{ $user->created_at->format('d.m.Y H:i') }}</td>
					<td>{{ $user->updated_at->format('d.m.Y H:i') }}</td>
				</tr>
				@endforeach

				</tbody>
				</table>
			</div>
		</div>
	</div>
</div>

<script>
(function($) {
	'use strict';
	$(function() {
		$('#user-listing').DataTable({
			"iDisplayLength": 10,
			"aaSorting": [[0, "asc"]],
			"language": {
				"lengthMenu": "Показывать _MENU_ записей на странице",
				"zeroRecords": "Записей не найдено",
				"loadingRecords": "Загрузка... может занять несколько секунд...",
				"info": "Страница _PAGE_ из _PAGES_",
				"infoEmpty": "Показано с 0 по 0 из 0 записей",
				"search": "",
				"infoFiltered": "(Найдено записей: _TOTAL_)",
				"sInfo": "Показано с _START_ по _END_ из _TOTAL_ записей",
				"paginate":
				{
					"first":    "Первая",
					"last":     "Последняя",
					"next":     "Следующая",
					"previous": "Предыдущая"
				}
			}
		});
		$('#user-listing').each(function() {
			var datatable = $(this);
			// SEARCH - Add the placeholder for Search and Turn this into in-line form control
			var search_input = datatable.closest('.dataTables_wrapper').find('div[id$=_filter] input');
			search_input.attr('placeholder', 'Поиск');
			search_input.removeClass('form-control-sm');
			// LENGTH - Inline-Form control
			var length_sel = datatable.closest('.dataTables_wrapper').find('div[id$=_length] select');
			length_sel.removeClass('form-control-sm');
		});
		$('#user-listing tbody').on('click', 'tr', function () {
			var id = $('td', this).eq(0).text();
			$(location).attr('href','/instructors/user/'+id+'/edit');
		});
	});
})(jQuery);
</script>

@endsection
