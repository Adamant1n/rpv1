<!-- barcode modal -->
@push('js')
<script>

</script>
@endpush

<div class="modal fade" id="eventPopup" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-md" role="document" style="max-width: 800px;">
        <div class="modal-content bg-white">
            <div class="modal-body">

            	<form action="{{ route('instructors.event.pay', $event) }}" method="post">
            		@csrf
	            	<div class="form-group">
		            	<table id="event_table_cashier" class="table table-bordered">
		                    <thead>
		                        <tr>
		                            <th>Тренер</th>
		                            <th>Дата</th>
		                            <th>Время начала</th>
		                            <th>Время окончания</th>
		                        </tr>
		                    </thead>
		                    <tbody>
		                    	<tr>
		                    		<td>
		                    			{{ $event->trainer->full_name }}
		                    		</td>
		                    		<td>
		                    			{{ $event->event_date->format('d.m.Y') }}
		                    		</td>
		                    		<td>
		                    			{{ $event->start_time }}
		                    		</td>
		                    		<td>
		                    			{{ $event->end_time }}
		                    		</td>
		                    	</tr>
		                    </tbody>
		                </table>
		            </div>

	                <div class="row mb-2">
	                	<div class="col-6 text-center">
	                		<h3 style="line-height: 5rem;">
	                			СУММА К ОПЛАТЕ
	                		</h3>
	                	</div>

	                	<div class="col-6">
		                	<div class="card border-primary border">
		                        <div class="card-body p-3 text-center">
		                            <h1 id="event_popup_price" class="mb-0">
		                            	{{ $event->cost }}
		                            </h1>
		                        </div>
		                    </div>
		                </div>
	                </div>

	                <div class="row mb-4">
	                	<div class="col-6">
	                		<button class="btn btn-primary btn-block" id="event_popup_paid_by_cash" type="button">Наличные</button>
	                	</div>
	                	<div class="col-6">
	                		<button class="btn btn-outline-success btn-block" id="event_popup_paid_by_card" type="button">Банковская карта</button>
	                	</div>
	                </div>

	                <input type="hidden" name="paid_by" value="cash" id="event_popup_paid_by">
	                <input type="hidden" id="event_popup_kkt"> 

	                <div class="row" id="amountContainer">
	                	<div class="form-group col-6">
	                		<label><h4>ВНЕСЕНО</h4></label>
	                		<input type="text" class="form-control" id="event_popup_amount">
	                	</div>
	                	<div class="form-group col-6">
	                		<label><h4>СДАЧА</h4></label>
	                		<input type="text" class="form-control" id="event_popup_change" disabled>
	                	</div>
	                </div>

	                <button id="event_popup_pay" class="btn btn-outline-primary">Оплатить</button>
	            </form>
            </div>
        </div>
    </div>
</div>
<!-- endbarcode modal -->