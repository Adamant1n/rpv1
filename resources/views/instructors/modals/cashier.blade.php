<!-- barcode modal -->
<div class="modal fade" id="cashierPopup" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-md" role="document" style="max-width: 800px;">
        <div class="modal-content bg-white">
            <div class="modal-body">
                    <div class="row">
                        <div class="col-12">
                            <table class="table table-bordered">
                                <tbody>
                                    <tr>
                                        <td>Имя</td>
                                        <td id="cashier_popup_full_name"></td>
                                    </tr>
                                    <tr>
                                        <td>Телефон</td>
                                        <td id="cashier_popup_phone"></td>
                                    </tr>
                                    <tr>
                                        <td>Договор</td>
                                        <td>
                                            <div class="badge badge-primary float-right ml-1 p-2" id="cashier_popup_group"></div>
                                            <div class="badge float-right p-2" id="cashier_popup_status"></div>
                                            <select class="form-control form-control-sm" id="cashier_popup_order_id" style="max-width: 140px;">
                                            </select> 
                                        </td>
                                    </tr>
                                </tbody>
                            </table>       
                        </div>
                    </div>

                    <hr />

                    <form name="cashier">
                        <div class="row">
                            <div class="form-group col-12">
                                <label>Способ оплаты</label>
                                <label class="float-right">
                                    <input type="checkbox" id="cashier_popup_kkt"> ККТ
                                </label>
                                <select class="form-control" id="cashier_popup_paid_by">
                                    <option value="cash">Наличные</option>
                                    <option value="card">Карта</option>
                                </select>
                            </div>
                        </div>
                    </form>

                    <div class="row">
                        <div class="col-12">
                            <div class="card border-primary border">
                                <div class="card-body p-3 text-center">
                                    К оплате:
                                    <div id="cashier_popup_price">0</div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <hr />

                    <button id="cashier_popup_pay" type="button" class="btn btn-outline-primary">Оплачено</button>
                    <button id="cashier_popup_agreement" type="button" class="btn btn-outline-info">Договор</button>
                    <button id="cashier_popup_end" type="button" class="btn btn-outline-success">Завершить</button>

                    <hr />

                    <h5>
                    Текущий заказ
	                </h5>
	                <table id="order_table_cashier" class="table table-bordered">
	                    <thead>
	                        <tr>
	                            <th>Артикул</th>
	                            <th>Категория</th>
	                            <th>Наименование</th>
	                            <th>Стоимость</th> 
	                            <th>Размер</th>
	                        </tr>
	                    </thead>
	                </table>

            </div>
        </div>
    </div>
</div>
<!-- endbarcode modal -->