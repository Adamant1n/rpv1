<script type="text/javascript">
    $(document).ready(function() {
        var inputStart;
        var inputEnd;
        var typingBarcode = '';
        var barcode = '';
        var prevKey;
        var clientData;
        var client_id;
        var order_id;
        var orderData;
        var orderTable = $('#order_table').DataTable({
            "bSort": false,
            "bPaginate": false,
            "bFilter": false,
            "bInfo": false,
        });

        var orderTableCashier = $('#order_table_cashier').DataTable({
            "bSort": false,
            "bPaginate": false,
            "bFilter": false,
            "bInfo": false,
        });

        $('#cashier_popup_unit').select2({
            dropdownAutoWidth: false
        });

        $('#cashier_popup_unit').change(function () {
            $('#cashier_popup_code').val($(this).val());
        });

        // загрузка списка клиентов
        var clientList = {};

        var htmlRub = ' <i class="fa fa-rub"></i>';

        var event_id = '{{ $event->id }}';
        var event_price = {{ $event->cost }};

        // hack for modal focus
        jQuery.fn.modal.Constructor.prototype.enforceFocus = function () { };
        jQuery('#clientPopup').on('shown.bs.modal', function() {
        	//initDatePickers();
            jQuery(document).off('focusin.modal');
        });

        jQuery('#eventPopup').on('shown.bs.modal', function() {
        	//initDatePickers();
            barcode = $('#client_barcode').val();
            console.log('get client');
            console.log(barcode);
            getClient();
            console.log('gotten');
            jQuery(document).off('focusin.modal');
        });

        // var userRole = "{{ auth()->user()->getRoleNames()[0] }}";
        var userRole = "cashier";
        //console.log(userRole);

        $(document).on('click', '.btn-remove', function () {
            var unit_id = $(this).data('unit');
            console.log(unit_id);
            console.log('remove');

            toggleUnitCount('remove', 1, unit_id, clientData.id, clientData.current_order.id);
        });

        var location = {!! json_encode(auth()->user()->location) !!};

        $(document).on('keyup', function (e) {
            clearInterval(timer);

            var code = e.originalEvent.code;

            if(code == 'F8')
            {
            	showReplaceUnitModal();
            }

            // letter or digit
            if(code.indexOf('Key') != -1) {
                typingBarcode += code.substring(3);
            }

            else if(code.indexOf('Digit') != -1) {
                typingBarcode += code.substring(5);
            }

            $('#client_popup_scan').html(typingBarcode);

            if(code == 'ArrowDown' && prevKey == 'Enter') {
            	if(!replaceMode)
            	{
            		finishBarcodeEnter();
            	}
                
                return true;
            }

            prevKey = code;
            timer = setInterval(ticker, 500);
        });

        var timer = setInterval(ticker, 500);

        var ticker = function () {
            resetValues();
            clearInterval(timer);
        }

        // сброс баркода
        function resetValues() {
            typingBarcode = '';
            prevKey = '';
            $('#client_popup_scan').html('–');
        }

        var replaceMode = false;

        // модалка на замену инвентаря
        function showReplaceUnitModal()
        {	
        	replaceMode = true;
        	swal({
			 	title: "Введите артикулы заменяемого и нового инвентаря",
				html:
			    	'<input id="swal-input1" class="swal2-input">' +
			    	'<input id="swal-input2" class="swal2-input">',
			  	preConfirm: function () {
			    	return new Promise(function (resolve) {
			      		resolve([
			        		$('#swal-input1').val(),
			        		$('#swal-input2').val()
			      		])
			    	})
			  	},
			  	onOpen: function () {
			    	$('#swal-input1').focus()
			  	}
			}).then(function (result) {
				codes = result.value;
				console.log(result.value);

				$.ajax({
	                url: "{{ '/scan/replace' }}",
	                type: "get", 
	                data: { user_id: {{ auth()->user()->id }},
	                    order_id: order_id,
	                    unit_1: codes[0],
	                    unit_2: codes[1]
	                },
	            })
	            .done(function(data) {
	            	//swal(data.message);
	            	if(data.success)
	            	{
	            		clientData.current_order = data.clientData;
	                	updateOrderData();
	            	}
	            });

				replaceMode = false;
				// swal(JSON.stringify(result))
			}).catch(swal.noop)
        }

        // окно выбора количества неуникальных товаров
        function chooseCount(code, client_id, order_id)
        {
            var mCode = code;
            var mClient_id = client_id;
            var mOrder_id = order_id;

            //var count = prompt('Выберите количество единиц инвентаря', 1);
            //toggleUnitCount(count, mCode, mClient_id, mOrder_id);

            var count = 1;

            swal({
                title: "Выберите количество единиц инвентаря",
                input: 'number',
                inputAttributes: {
                    value: 1,
                    min: 1,
                },
                showCancelButton: true,
                confirmButtonText: 'Добавить',
                cancelButtonText: 'Удалить',
                onOpen: function(swal) {
                    $(swal).find('.swal2-cancel').off().click(function(e) {
                        count = $('.swal2-input');
                    });
                }
            })
            .then((result) => {
                if(result.dismiss)
                {
                    toggleUnitCount('remove', $('.swal2-input').val(), mCode, mClient_id, mOrder_id);
                }
                else 
                {
                    toggleUnitCount('add', result.value, mCode, mClient_id, mOrder_id);
                }
            });
        }

        // toggle unit with count
        function toggleUnitCount(action, count, code, client_id, order_id)
        {
            $.ajax({
                url: "{{ '/scan/unit' }}",
                type: "get", 
                data: { user_id: {{ auth()->user()->id }},
                    barcode: code, 
                    client_id: client_id,
                    order_id: order_id,
                    count: count,
                    action: action
                },
            })
            .done(function(data) {
                clientData = data;
                updateOrderData();
            });
        }

        // окно подтверждения создания группы
        function confirmGroup()
        {
            // если групповой - проверка на наличие человека в группе
            if(clientData.current_order.group)
            {
                c_id = parseInt(barcode.replace(/[^-0-9]/gim, ''));
                for(var i = 0; i < clientData.current_order.group.orders.length; i++)
                {
                    if(clientData.current_order.group.orders[i].client_id == c_id)
                    {
                        getClient();

                        barcode = '';
                        resetValues();
                        return true;
                        break;
                    }
                }

            }

            swal({
                title: "Добавить клиента в группу договоров?",
                showCancelButton: true,
            })
            .then((result) => {

                if (!result.dismiss) {
                    addToGroup(barcode);
                } else {
                    // console.log('cancel group');
                    getClient();
                }

                barcode = '';
                resetValues();
            });
        }

        // добавление клиента в группу договоров
        function addToGroup(code)
        {
            $.ajax({
                url: "{{ '/scan/group' }}",
                type: "get", 
                data: { user_id: {{ auth()->user()->id }},
                    client_id: code,
                    order_id: order_id
                },
            })
            .done(function(data) {
                if(data != false) {
                    clientData = data;
                    client_id = data.id;
                    order_id = data.current_order.id;
                    updateClientData();
                    updateOrderData();
                }
            });
        }

        // окончание ввода кода
        function finishBarcodeEnter() {
            barcode = typingBarcode;
            console.log('barcode: ' + barcode);

            // открыть клиента
            if(barcode.substring(0, 1) === 'L')
            {
                // если окно закрыто, открыть клиента
                if(!$('#clientPopup').is(':visible')) {
                    getClient();
                }
                // если окно открыто, предложить добавить в договор
                else {
                    confirmGroup();
                    return true;
                }
            }

            // применить промокод
            else if(barcode.substring(0, 1) === 'P')
            {
                getPromocode(barcode);
            }

            // открыть заказ
            else if(barcode.substring(0, 1) === 'D') {
                getOrder(parseInt(barcode.replace(/[^-0-9]/gim, '')));
            }

            // открыть заказ по групповому ID ???
            else if(barcode.substring(0, 2) === 'GD') {
                getOrder(parseInt(barcode.replace(/[^-0-9]/gim, '')));
            }

            // аттач детач единицы товара
            else {
                // загрузка заказа по юниту если не открыто модальное окно
                if(!$('#clientPopup').is(':visible') && !$('#cashierPopup').is(':visible')) {
                    getOrderByUnit(barcode);
                }
                else {
                    toggleUnit();
                }
            }

            barcode = '';
            resetValues();
        }

        // открыть карту заказа по инвентарю
        function getOrderByUnit(code) {
            $.ajax({
                url: "{{ '/scan/client' }}",
                type: "get", 
                data: { user_id: {{ auth()->user()->id }},
                    unit_id: code
                },
            })
            .done(function(data) {
                if(data != false) {
                    clientData = data;
                    client_id = data.id;
                    order_id = data.current_order.id;
                    updateClientData();
                    updateOrderData();
                    
                    if(userRole == 'cashier' || userRole == 'delivery') $('#cashierPopup').modal('show');
                    else $('#clientPopup').modal('show');
                }
            });

            // getOrder(parseInt(id.replace(/[^-0-9]/gim, '')));
        }

        // открыть карту заказа
        function getOrder(id) {
            order_id = id;
            $.ajax({
                url: "{{ '/scan/client' }}",
                type: "get", 
                data: { user_id: {{ auth()->user()->id }},
                    order_id: order_id,
                    cashier: (userRole == 'cashier' || userRole == 'delivery') ? 1 : 0
                },
            })
            .done(function(data) {
                clientData = data;
                client_id = data.id;
                

                updateClientData();
                updateOrderData();
                
                if(userRole == 'cashier' || userRole == 'delivery') $('#cashierPopup').modal('show');
                else $('#clientPopup').modal('show');
            });
        }

        // открыть карту заказа
        if($('#order_table tbody').length) {
            $('#order_table tbody').on('click', 'tr', function () {
                var id = $('td', this).eq(0).text();
                if($(this).hasClass('table-success'))
                {
                	swal({
					 	title: "Выберите состояние инвентаря",
					 	input: 'select',
						inputOptions: {
						    
						},
						showCancelButton: true,
					}).then(function (result) {
						$.ajax({
			                url: "{{ '/scan/update_condition' }}",
			                type: "get", 
			                data: { user_id: {{ auth()->user()->id }},
			                    unit_id: id,
			                    condition_id: result.value
			                },
			            })
			            .done(function(data) {
			            	console.log(data);
			            });
					}).catch(swal.noop)
                }
            });
        }

        // изменить статус возвращенного инвентаря
        $('#orders-listing tbody').on('click', 'tr', function () {
			var id = $('td', this).eq(0).text();
			// $(location).attr('href','/orders/'+parseInt(id.replace(/[^-0-9]/gim, '')));
			getOrder(parseInt(id.replace(/[^-0-9]/gim, '')));
		});

        // карта заказа из меню клиента
        if($('#client_current_order').length) {
            $('#client_current_order').on('click', function () {
                var id = $(this).data('id');
                getOrder(parseInt(id.replace(/[^-0-9]/gim, '')));
            });
        }

        // новый заказ из меню клиента
        if($('#client_new_order').length) {
            $('#client_new_order').on('click', function () {
                barcode = $(this).data('id');
                getClient();
                barcode = '';
            });
        }

        // новая доставка
        if($('#client_new_delivery').length) {
	        $("#client_new_delivery").click(function () {
	        	client_id = $(this).data('id');
	            createDelivery();
	        });
	    }

        // новая бронь
        if($('#client_new_booking').length) {
	        $("#client_new_booking").click(function () {
	        	client_id = $(this).data('id');
	            createBooking();
	        });
	    }

	    // сохранение адреса
	    $('#client_popup_save_address').click(function () {
	    	$.ajax({
                url: "{{ '/scan/update' }}",
                type: "get", 
                data: { user_id: {{ auth()->user()->id }},
                    order_id: order_id,
                    address: $('#client_popup_address').val()
                },
            })
            .done(function(data) {
                clientData = data;

                updateOrderData();
            });
	    });

	    var timepicker = $('#timepicker-example');
	    var datepicker = $('#datepicker-popup');

	    initDatePickers();

	    var timeUpdatedManually = true;

	    function initDatePickers()
	    {
			timepicker.datetimepicker({
		    	use24hours: true,
		        format: 'HH:mm',
		        stepping: 10,
		    });

		    timepicker.on('change.datetimepicker', function(e) {
		    	console.log(e);
		    	if(timeUpdatedManually)
		    	{
		    		updateBookDate();
		    	}
		        
		        timeUpdatedManually = true;
		        console.log('d upd2');
		    });

		    datepicker.datepicker({
				enableOnReadonly: true,
				todayHighlight: false,
				format: 'dd.mm.yyyy'
			});

			datepicker.datepicker().on('changeDate', function (e) {
				updateBookDate();
				console.log('d upd1');
			});
	    }

		function updateBookDate()
		{
			$.ajax({
                url: "{{ '/scan/update' }}",
                type: "get", 
                data: { user_id: {{ auth()->user()->id }},
                    order_id: order_id,
                    book_at: $('#book_at_date').val() + ' ' + timepicker.datetimepicker('viewDate').format('HH:mm')
                },
            })
            .done(function(data) {
                clientData = data;

                updateOrderData();
            });
		}

        // создание нового договора брони
        function createBooking()
        {
            $.ajax({
                url: "{{ '/scan/create_order' }}",
                type: "get", 
                data: { user_id: {{ auth()->user()->id }},
                    client_id: client_id,
                    booking: true
                },
            })
            .done(function(data) {
                $('#clientPopup').modal('show');
                clientData = data;
                order_id = data.current_order.id;
                updateOrderData();      
                updateClientData();        
            });
        }

        // создание новой доставки
        function createDelivery()
        {
            $.ajax({
                url: "{{ '/scan/create_order' }}",
                type: "get", 
                data: { user_id: {{ auth()->user()->id }},
                    client_id: client_id,
                    delivery: true
                },
            })
            .done(function(data) {
                $('#clientPopup').modal('show');
                clientData = data;
                order_id = data.current_order.id;
                updateOrderData();      
                updateClientData();        
            });
        }

        // статус укомплектован
        $('#client_popup_staffed').click(function () {
        	$.ajax({
                url: "{{ '/scan/update' }}",
                type: "get", 
                data: { user_id: {{ auth()->user()->id }},
                    order_id: order_id,
                    status: 'staffed'
                },
            })
            .done(function(data) {
                clientData = data;

                updateOrderData();
            });
        });

        // открыть заказ из меню юнита ???
        if($('#unit_order').length) {
            $('#unit_order').on('click', function () {
                var id = $(this).data('order');
                getOrder(id);
            });
        }

        // ручной ввод артикула
        $('#codeInput').on('keypress', function (e) {
            if(e.which === 13){
                barcode = $('#codeInput').val();
                toggleUnit();
                $('#codeInput').val('');
            }
       });

        // получение промокода
        function getPromocode (promocode) {
            $.ajax({
                url: "{{ '/scan/promocode' }}",
                type: "get", 
                data: { user_id: {{ auth()->user()->id }},
                    order_id: order_id,
                    promocode: promocode, 
                },
            })
            .done(function(data) {
                clientData.current_order = data;
                updateOrderData();
            });
        }

        // получение клиента
        function getClient() {
            $.ajax({
                url: "{{ '/scan/client' }}",
                type: "get", 
                data: { user_id: {{ auth()->user()->id }},
                    barcode: barcode,
                    event_id: event_id,
                    cashier: (userRole == 'cashier' || userRole == 'delivery') ? 1 : 0
                },
            })
            .done(function(data) {
            	console.log(data);
                clientData = data;
                client_id = data.id;
                order_id = data.current_order.id;
                updateClientData();
                updateOrderData();
                
                if(userRole == 'cashier' || userRole == 'delivery') $('#cashierPopup').modal('show');
                else $('#clientPopup').modal('show');
            });
        }

        // update client data
        function updateClientData() {
            // $('#client_popup_full_name').html(clientData.full_name);
            $('#client_popup_phone').html(clientData.phone);
            $('#client_popup_description').html(clientData.description);
            $('#client_popup_feature_height').html(clientData.feature.height);
            $('#client_popup_feature_weight').html(clientData.feature.weight);
            $('#client_popup_feature_shoe_size').html(clientData.feature.shoe_size);

            $('#client_popup_order_id').find('option').remove();
            $('#cashier_popup_order_id').find('option').remove();

            // cashier popup
            $('#cashier_popup_full_name').html(clientData.full_name);
            $('#cashier_popup_phone').html(clientData.phone);
            $('#cashier_popup_description').html(clientData.description);

            for(var i = 0; i < clientData.orders.length; i++) {
                var selected = clientData.orders[i].id == order_id;
                $('#client_popup_order_id').append(new Option(clientData.orders[i].optional_id, clientData.orders[i].id, selected, selected));
                $('#cashier_popup_order_id').append(new Option(clientData.orders[i].optional_id, clientData.orders[i].id, selected, selected));
            }
        }

        // update order data
        function updateOrderData() {

        	// селектор договоров для группового
        	$('#client_popup_select_client').find('option').remove();
            // $('#client_popup_select_client_container').hide();
            $('#client_popup_full_name').hide();

            setPaymentMethod(clientData.current_order.paid_by);

            if(clientData.current_order.group_id != undefined)
            {
            	$('#client_popup_remove_client').show();
            	$('#client_popup_full_name').hide();
            	$('#client_popup_select_client_container').show();
            	for(var i = 0; i < clientData.current_order.group.orders.length; i++)
            	{
            		var selected = false;

            		if(clientData.current_order.group.orders[i].client.id == clientData.current_order.client.id){
            			selected = true;
            		}

            		$('#client_popup_select_client').append(new Option(
            			clientData.current_order.group.orders[i].client.full_name,
            			clientData.current_order.group.orders[i].id,
            			selected, selected));
            	}
            }
            else
            {
            	$('#client_popup_remove_client').hide();
            	var selected = false;
            	$('#client_popup_select_client').append(new Option(
            			clientData.full_name,
            			clientData.id,
            			selected, selected));
            }

        	// z num
        	$('#client_popup_z_num').html(clientData.current_order.z_num);
            // client popup
            $('#client_popup_type').val(clientData.current_order.type);
            // $('#client_popup_paid_by').val(clientData.current_order.paid_by);
            $('#client_popup_duration').val(clientData.current_order.duration + clientData.current_order.dimension);
            // $('#client_popup_pledge').val(clientData.current_order.pledge);
            $('#client_popup_starts_at').val(clientData.current_order.starts_at);
            $('#cashier_popup_ends_at').val(clientData.current_order.ends_at);
            $('#client_popup_price').html(clientData.current_order.price);

            // cashier popup
            $('#cashier_popup_paid_by').val(clientData.current_order.paid_by);
            $('#cashier_popup_pledge').val(clientData.current_order.pledge);
            $('#cashier_popup_starts_at').val(clientData.current_order.starts_at);
            $('#cashier_popup_ends_at').val(clientData.current_order.ends_at);

            let orderPrice = parseFloat(clientData.current_order.price);
            let fullPrice = parseFloat(event_price) + orderPrice;

            $('#cashier_popup_price').html(fullPrice + htmlRub);

            $('#client_popup_address').val(clientData.current_order.address);

            if(clientData.current_order.kkt){
                $("#cashier_popup_kkt").prop('checked', 1);
            }
            else {
                $("#cashier_popup_kkt").prop('checked', 0);
            }

            // hide pay button if price = 0

            // promocode
            if(clientData.current_order.agent != undefined) {
                $('#client_popup_promocode').val(clientData.current_order.agent.promocode);
                $('#client_popup_agent').val(
                    clientData.current_order.agent.name + ', ' +
                    clientData.current_order.discount + '%'
                );
            }
            else {
                $('#client_popup_promocode').val('');
                $('#client_popup_agent').val('');
            }

            orderTable.clear().draw();
            orderTableCashier.clear().draw();

            for(var i = 0; i < clientData.current_order.reservations.length; i++)
            {
                if(!clientData.current_order.reservations[i].unit.feature_1) clientData.current_order.reservations[i].unit.feature_1 = '';
                if(!clientData.current_order.reservations[i].unit.feature_4) clientData.current_order.reservations[i].unit.feature_4 = '';
                if(!clientData.current_order.reservations[i].unit.feature_3) clientData.current_order.reservations[i].unit.feature_3 = '';

                var binding = clientData.current_order.reservations[i].unit.binding ?  clientData.current_order.reservations[i].unit.binding.name : '';
                var size = clientData.current_order.reservations[i].unit.feature_5 ? clientData.current_order.reservations[i].unit.feature_5 : clientData.current_order.reservations[i].unit.feature_3;

                var rowNode = orderTable.row.add([
                //console.log([
                    clientData.current_order.reservations[i].unit.code,
                    clientData.current_order.reservations[i].unit.product.category.name,
                    clientData.current_order.reservations[i].unit.product.name,
                    //clientData.current_order.reservations[i].starts_at,
                    //clientData.current_order.reservations[i].ends_at,
                    clientData.current_order.reservations[i].price,
                    
                    //binding,
                    //clientData.current_order.reservations[i].unit.feature_1,
                    size,
                    //clientData.current_order.reservations[i].unit.feature_4,

                ]).draw(false).node();

                // returned
                if (clientData.current_order.reservations[i].status == 'returned') {
                    $(rowNode).addClass('table-success');
                }
                else if (clientData.current_order.reservations[i].status == 'cancelled') {
                    $(rowNode).addClass('table-danger');
                }
                else if(clientData.current_order.reservations[i].starts_at == undefined) {
                    $(rowNode).addClass('table-primary');
                }

                // cashier table
                var rowNode = orderTableCashier.row.add([
                //console.log([
                    clientData.current_order.reservations[i].unit.code,
                    clientData.current_order.reservations[i].unit.product.category.name,
                    clientData.current_order.reservations[i].unit.product.name,
                    //clientData.current_order.reservations[i].starts_at,
                    //clientData.current_order.reservations[i].ends_at,
                    clientData.current_order.reservations[i].price,
                    
                    //binding,
                    //clientData.current_order.reservations[i].unit.feature_1,
                    size,
                    //clientData.current_order.reservations[i].unit.feature_4,
                    '<button type="button" class="btn btn-sm btn-outline-danger btn-remove" data-unit="' + clientData.current_order.reservations[i].unit.code + '">Удалить</button>',

                ]).draw(false).node();

                // returned
                if (clientData.current_order.reservations[i].status == 'returned') {
                    $(rowNode).addClass('table-success');
                }
                else if (clientData.current_order.reservations[i].status == 'cancelled') {
                    $(rowNode).addClass('table-danger');
                }
                else if(clientData.current_order.reservations[i].starts_at == undefined) {
                    $(rowNode).addClass('table-primary');
                }
            }

            // обновление времени начала бронирования

            if(clientData.current_order.type == 'book' || clientData.current_order.type == 'delivery')
            {
            	var book_at = moment(clientData.current_order.book_at);
            	
            	if(clientData.current_order.type == 'book')
	            {
	            	$('#client_popup_book_at_container').show();
	            }

	            if(clientData.current_order.type == 'delivery')
	            {
	            	$('#client_popup_book_at_container').show();
	            }

	            var date = book_at.format('DD.MM.YYYY');
	            var time = book_at.format('HH:mm');

	            timeUpdatedManually = false;

            	datepicker.datepicker('update', date);
            	timepicker.datetimepicker('date', time);
            	
            }
            else{
            	$('#client_popup_book_at_container').hide();
            }

            updateStatus();
        }

        // добавление юнита
        function toggleUnit() {
            var code = barcode;
            $.ajax({
                url: "{{ '/scan/unit' }}",
                type: "get", 
                data: { user_id: {{ auth()->user()->id }},
                    barcode: barcode, 
                    client_id: client_id,
                    order_id: order_id
                },
            })
            .done(function(data) {
                if(data.is_unique == undefined) 
                {
                	if(data.success == false)
                	{
                		swal(data.message)
                	}
                	else
                	{
                		clientData = data;
                    	updateOrderData();
                	}
                }
                else 
                {
                    chooseCount(code, client_id, order_id);
                }
                
                // $('#clientPopup').modal('show');
            });
        }

        // изменение данных формы
        $('form[name=order]').on('change paste', 'input, select, textarea', function(){
        	console.log($(this).prop('id'));
        	if($(this).prop('id') == 'client_popup_select_client')
        	{
        		return false;
        	}

            $.ajax({
                url: "{{ '/scan/update' }}",
                type: "get", 
                data: { user_id: {{ auth()->user()->id }},
                    order_id: order_id,
                    duration: $('#client_popup_duration').find(':selected').data('duration'),
                    dimension: $('#client_popup_duration').find(':selected').data('dimension'),
                    // type: $('#client_popup_type').val()
                },
            })
            .done(function(data) {
                clientData = data;

                updateOrderData();
            });
        });

        // изменение данных формы оплаты
        $('form[name=cashier]').on('keyup change paste', 'input, select, textarea', function(){
            $.ajax({
                url: "{{ '/scan/update' }}",
                type: "get", 
                data: { user_id: {{ auth()->user()->id }},
                    order_id: order_id,
                    pledge: $('#cashier_popup_pledge').val(),
                    paid_by: $('#cashier_popup_paid_by').val(),
                    kkt: $("#cashier_popup_kkt").is(':checked') ? 1 : 0
                },
            })
            .done(function(data) {
                clientData = data;

                updateOrderData();
            });
        });

        // загрузка договора
        $('#client_popup_order_id').change(function () {
            $.ajax({
                url: "{{ '/scan/order' }}",
                type: "get", 
                data: { user_id: {{ auth()->user()->id }},
                    order_id: $(this).val() 
                },
            })
            .done(function(data) {
                clientData.current_order = data;
                order_id = data.id;
                updateOrderData();              
            });
        });

        // загрузка договора из группового
        $('#client_popup_select_client').change(function () {
            $.ajax({
                url: "{{ '/scan/order' }}",
                type: "get", 
                data: { user_id: {{ auth()->user()->id }},
                    order_id: $(this).val() 
                },
            })
            .done(function(data) {
            	clientData = data.client;
                clientData.current_order = data;

                order_id = data.id;
                updateOrderData();      
                updateClientData(); 
            });
        });

        // удалить клиента из группового договора
        $('#client_popup_remove_client').click(function () {
        	$.ajax({
                url: "{{ '/scan/ungroup' }}",
                type: "get", 
                data: { user_id: {{ auth()->user()->id }},
                    client_id: clientData.id,
                    order_id: clientData.current_order.id
                },
            })
            .done(function(data) {
                if(data != false) {
                    clientData = data;
                    client_id = data.id;
                    //order_id = data.current_order.id;
                    updateClientData();
                    updateOrderData();
                }
            });
        });

        // добавить клиента в групповой договор
        $('#client_popup_add_client').click(function () {
        	$.ajax({
	            url: "{{ '/scan/get_clients' }}",
	            data: {
	            	order_id: clientData.current_order.id
	            },
	            type: "get", 
	        })
	        .done(function(data) {
	        	clientList = data;

	        	html = '<select class="sw-select">';
	        	for(var i = 0; i < clientList.length; i++)
	        	{
	        		html += '<option value="' + clientList[i]['id'] + '">' + clientList[i]['name'] + '</option>';
	        	}

	        	html += '</select>';

	        	swal({
				 	title: "Выберите клиента",
				 	html: html,
				 	//input: 'select',
					//inputOptions: clientList,
					showCancelButton: true,
					onOpen: function () {
                        $('.sw-select').select2({
                            minimumResultsForSearch: 15,
                            width: '100%',
                        });
                    },
				}).then(function (result) {
					addToGroup($('.sw-select').val());
				}).catch(swal.noop)
	        });
        });

        // загрузка договора кассиром
        $('#cashier_popup_order_id').change(function () {
            $.ajax({
                url: "{{ '/scan/order' }}",
                type: "get", 
                data: { user_id: {{ auth()->user()->id }},
                    order_id: $(this).val(),
                    cashier: (userRole == 'cashier' || userRole == 'delivery') ? 1 : 0
                },
            })
            .done(function(data) {
                clientData.current_order = data;
                order_id = data.id;
                updateOrderData();              
            });
        });

        // создание нового договора
        function createOrder()
        {
            $.ajax({
                url: "{{ '/scan/create_order' }}",
                type: "get", 
                data: { user_id: {{ auth()->user()->id }},
                    client_id: client_id
                },
            })
            .done(function(data) {
                clientData = data;
                order_id = data.current_order.id;
                updateOrderData();      
                updateClientData();        
            });
        }


        // отмена заказа
        function cancelOrder()
        {
            $.ajax({
                url: "{{ '/scan/cancel_order' }}",
                type: "get", 
                data: { user_id: {{ auth()->user()->id }},
                    order_id: order_id
                },
            })
            .done(function(data) {
                clientData.current_order = data;

                updateOrderData();      
            });
        }

        function updateStatus()
        {
            // групповой договор
            $('#client_popup_group').hide();
            $('#cashier_popup_group').hide();

            if(clientData.current_order.group_id != undefined)
            {
                $('#client_popup_group').html(clientData.current_order.group.optional_id);
                $('#client_popup_group').show();

                $('#cashier_popup_group').html(clientData.current_order.group.optional_id);
                $('#cashier_popup_group').show();
            }

            $('#client_popup_staffed_container').hide();
            $('#client_popup_address_container').hide();

            if(clientData.current_order.type == 'delivery')
            {
	            $('#client_popup_address_container').show();
            }



            if(clientData.current_order.type == 'book' || clientData.current_order.type == 'delivery' && clientData.current_order.status == 'draft')
            {
            	$('#client_popup_staffed_container').show();
            }


            // сюда же закинуть обработку полей промокод, дисейбл полей и тд
            $('#client_popup_status').removeClass('badge-primary');
            $('#client_popup_status').removeClass('badge-outline-primary');
            $('#client_popup_status').removeClass('badge-danger');
            $('#client_popup_status').removeClass('badge-success');
            $('#client_popup_status').removeClass('badge-primary');

            // cashier status badge
            $('#cashier_popup_status').removeClass('badge-primary');
            $('#cashier_popup_status').removeClass('badge-outline-primary');
            $('#cashier_popup_status').removeClass('badge-danger');
            $('#cashier_popup_status').removeClass('badge-success');
            $('#cashier_popup_status').removeClass('badge-primary');

            $('#cashier_popup_agreement').hide();
            $('#client_popup_cancel_order').hide();

            $('#cashier_popup_pay').hide();
            $('#cashier_popup_end').hide();
            $('#cashier_popup_extra_card').hide();
            // $('#client_popup_paid_by').hide();

            $('#client_popup_paid_by').attr('disabled', 'disabled');
            $('#client_popup_pledge').attr('disabled', 'disabled');
            $('#client_popup_duration').attr('disabled', 'disabled');
            $('#client_popup_pledge').attr('disabled', 'disabled');
            $('#client_popup_type').attr('disabled', 'disabled');

            $('#client_popup_duration option').each(function () {
                $(this).show();
            });

            if(clientData.current_order.status == 'draft') {
                $('#client_popup_paid_by').removeAttr('disabled');
                $('#client_popup_pledge').removeAttr('disabled');
                $('#client_popup_duration').removeAttr('disabled');
                $('#client_popup_pledge').removeAttr('disabled');
                $('#client_popup_type').removeAttr('disabled');

                $('#client_popup_status').addClass('badge-outline-primary');
                $('#client_popup_status').html('Новый');

                $('#cashier_popup_status').addClass('badge-outline-primary');
                $('#cashier_popup_status').html('Новый');
                
                if(clientData.current_order.price == 0) {
                    $('#cashier_popup_pay').hide();
                    // $('#client_popup_paid_by').hide();
                }
                else {
                    $('#cashier_popup_pay').show();
                    // $('#client_popup_paid_by').show();
                }

                // убрать ненужные тарифы

                $('#client_popup_duration option').each(function () {
                    if($(this).data('dimension') == 'm' & location.tariff_step != '10m') {
                        $(this).hide();
                    }
                    if($(this).data('dimension') == 'h' & location.tariff_step == '1d') {
                        $(this).hide();
                    }
                });

                $('#client_popup_cancel_order').show();
            }
            if(clientData.current_order.status == 'staffed') {
                $('#client_popup_status').addClass('badge-info');
                $('#client_popup_status').html('Укомплектован');

                $('#cashier_popup_status').addClass('badge-info');
                $('#cashier_popup_status').html('Укомплектован');
                $('#client_popup_type').removeAttr('disabled');

                if(clientData.current_order.price == 0) {
                    $('#cashier_popup_pay').hide();
                    // $('#client_popup_paid_by').hide();
                }
                else {
                    $('#cashier_popup_pay').show();
                    // $('#client_popup_paid_by').show();
                }
            }
            if(clientData.current_order.status == 'paid') {
                $('#client_popup_status').addClass('badge-success');
                $('#client_popup_status').html('Аренда');
                $('#cashier_popup_agreement').show();

                $('#cashier_popup_status').addClass('badge-success');
                $('#cashier_popup_status').html('Аренда');

                // проверка на возвращенные товары
                var returned = true;
                for(var i = 0; i < clientData.current_order.reservations.length; i++)
                {  
                    if(clientData.current_order.reservations[i].status == 'rent') returned = false;
                }

                // проверка группы
                if(clientData.current_order.group != undefined)
                {
                    for(var i = 0; i < clientData.current_order.group.reservations.length; i++)
                    {  
                        if(clientData.current_order.group.reservations[i].status == 'rent') returned = false;
                    }
                }

                if(returned) $('#cashier_popup_end').show();

                if(clientData.current_order.price_extra > 0) {
                    $('#cashier_popup_extra_card').show();
                    $('#cashier_popup_extra').html(clientData.current_order.price_extra);
                }
            }
            if(clientData.current_order.status == 'cancelled') {
                $('#client_popup_status').addClass('badge-danger');
                $('#client_popup_status').html('Отменен');

                $('#cashier_popup_status').addClass('badge-danger');
                $('#cashier_popup_status').html('Отменен');
            }
            if(clientData.current_order.status == 'ended') {
                $('#client_popup_status').addClass('badge-primary');
                $('#client_popup_status').html('Завершен');

                $('#cashier_popup_status').addClass('badge-primary');
                $('#cashier_popup_status').html('Завершен');
            }
        }

        // создать заказ
        $("#client_popup_new_order").click(function () {
            createOrder();
        });

        // отменить заказ
        $('#client_popup_cancel_order').click(function () {
            cancelOrder();
        });

        // оплатить заказ
        function pay()
        {
            $.ajax({
                url: "{{ '/scan/pay' }}",
                type: "get", 
                data: { user_id: {{ auth()->user()->id }},
                    order_id: order_id
                },
            })
            .done(function(data) {
                clientData.current_order = data;

                console.log('submit');

                $('#eventForm').submit();

                updateOrderData();      
            });
        }

        $('#cashier_popup_pay').click(function () {
            pay();
        });

        // печать договора
        $('#cashier_popup_agreement').click(function () {
            var url = "/orders/" + order_id + "/agreement";

            window.open(url, '_blank');
        });

        // стикер
        $('#client_popup_sticker').click(function () {
        	var url = "/orders/" + order_id + "/sticker";

            window.open(url, '_blank');
        });

        // закрыть заказ
        $('#cashier_popup_end').click(function () {
            $.ajax({
                url: "{{ '/scan/end' }}",
                type: "get", 
                data: { user_id: {{ auth()->user()->id }},
                    order_id: order_id
                },
            })
            .done(function(data) {
                clientData.current_order = data;

                updateOrderData();      
            });
        });

        // применить промокод из формы
        $('#client_popup_apply_promocode').click(function () {
            getPromocode($('#client_popup_promocode').val());
        })

        function setPaymentMethod(type)
        {
            $('#event_popup_paid_by').val(type);

        	if(type == 'cash')
        	{
        		$('#cashier_popup_paid_by_cash').removeClass('btn-outline-primary');
	        	$('#cashier_popup_paid_by_cash').addClass('btn-primary');
	        	$('#cashier_popup_paid_by_card').removeClass('btn-success');
	        	$('#cashier_popup_paid_by_card').addClass('btn-outline-success');
	        	$('#cashier_popup_paid_by').val('cash');
        	}
        	else if(type == 'card')
        	{
        		$('#cashier_popup_paid_by_cash').addClass('btn-outline-primary');
	        	$('#cashier_popup_paid_by_cash').removeClass('btn-primary');
	        	$('#cashier_popup_paid_by_card').addClass('btn-success');
	        	$('#cashier_popup_paid_by_card').removeClass('btn-outline-success');
	        	$('#cashier_popup_paid_by').val('card');
        	}
        }

        function updatePaymentMethod()
        {
        	console.log($('#cashier_popup_paid_by').val());
        	$.ajax({
                url: "{{ '/scan/update' }}",
                type: "get", 
                data: { user_id: {{ auth()->user()->id }},
                    order_id: order_id,
                    paid_by: $('#cashier_popup_paid_by').val(),
                },
            })
            .done(function(data) {
                clientData = data;

                updateOrderData();
            });
        }

        $('#cashier_popup_paid_by_cash').click(function () {
        	setPaymentMethod('cash');
        	updatePaymentMethod();
        });

        $('#cashier_popup_paid_by_card').click(function () {
        	setPaymentMethod('card');
        	updatePaymentMethod();
        });

        $('#cashier_popup_add_unit').click(function () {
        	barcode = $('#cashier_popup_code').val();
            toggleUnit();
            $('#cashier_popup_code').val('');
        })

        $('#cashier_popup_amount').on('input change input paste', function () {
        	console.log('asd');
        	var amount = $(this).val();
        	var change = amount - clientData.current_order.price - event_price;
        	if(change < 0) change = 0;

        	$('#cashier_popup_change').val(change);
        })
    });
</script>