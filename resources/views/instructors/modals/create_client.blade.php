<div class="modal fade" id="createClientModal" tabindex="-1" role="dialog" aria-labelledby="createClientModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="createClientModalLabel">Новый клиент</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body pb-0 pt-0">
                <form method="post" action="{{ route('instructors.clients.store') }}" id="create_client_form" name="create_client">
                    @csrf
                    <input type="hidden" name="ajax" value="1">
                    <div class="row">
                        <div class="form-group col-6">
                            <label class="col-form-label">Фамилия</label>
                            <input type="text" class="form-control" name="last_name" required autofocus>
                        </div>
            
                        <div class="form-group col-6">
                            <label class="col-form-label">Имя</label>
                            <input type="text" class="form-control" name="first_name" required>
                        </div>
            
                        <div class="form-group col-6">
                            <label class="col-form-label">Отчество</label>
                            <input type="text" class="form-control" name="mid_name" required>
                        </div>
            
                        <div class="form-group col-6">
                            <label class="col-form-label">Номер телефона</label>
                            <input type="text" class="form-control" name="phone" required>
                        </div>
            
                        <div class="form-group col-6">
                            <label class="col-form-label">Email</label>
                            <input type="text" class="form-control" name="email">
                        </div>
            
                        <div class="form-group col-6">
                            <label class="col-form-label">Номер паспорта</label>
                            <input type="text" class="form-control" name="passport">
                        </div>
            
                        <div class="form-group col-12">
                            <label class="col-form-label">Заметка</label>
                            <textarea class="form-control" name="description"></textarea>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success" onclick="$('#create_client_form').submit();">Создать</button>
                <button type="button" class="btn btn-light" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>