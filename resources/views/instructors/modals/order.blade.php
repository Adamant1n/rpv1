<!-- barcode modal -->
<style>
    .select2-selection {
        /*position: relative !important;*/
    }

    .select2-container {
        /*display: inline !important;*/  
        width: 100%; 
    }
</style>

<div class="modal fade" id="cashierPopup" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-md" role="document" style="max-width: 800px;">
        <div class="modal-content bg-white">
            <div class="modal-body">

            	<div class="form-group">
	            	<table id="order_table_cashier" class="table table-bordered">
	                    <thead>
	                        <tr>
	                            <th>Артикул</th>
	                            <th>Категория</th>
	                            <th>Наименование</th>
	                            <th>Стоимость</th> 
	                            <th>Размер</th>
	                        </tr>
	                    </thead>
	                </table>
	            </div>

                <div class="row">
                    <div class="col-6">
                        <div class="form-group">
                            <select class="form-control" id="cashier_popup_unit">
                                @php
                                $availableUnits = \App\Unit::where(function ($query) {
                                    $query->where('count', '>', 1)
                                        ->orWhere(function ($query) {
                                            $query->whereDoesntHave('reservations');
                                        });
                                })->get();
                                @endphp

                                @foreach($availableUnits as $unit)
                                <option value="{{ $unit->code }}">{{ $unit->product->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                	<div class="col-3">
                		<div class="form-group">
	                		<input type="text" class="form-control" id="cashier_popup_code">
	                	</div>
                	</div>
                	<div class="col-3">
                		<div class="form-group">
	                		<button class="btn btn-success btn-block" id="cashier_popup_add_unit" type="button">
	                            <i class="fa fa-plus"></i>
	                            Добавить товар
	                        </button>
	                    </div>
                	</div>
                </div>

                <div class="row mb-2">
                	<div class="col-6 text-center">
                		<h3 style="line-height: 5rem;">
                			СУММА К ОПЛАТЕ
                		</h3>
                	</div>

                	<div class="col-6">
	                	<div class="card border-primary border">
	                        <div class="card-body p-3 text-center">
	                            <h1 id="cashier_popup_price" class="mb-0">0</h1>
	                        </div>
	                    </div>
	                </div>
                </div>

                <div class="row mb-4">
                	<div class="col-6">
                		<button class="btn btn-primary btn-block" id="cashier_popup_paid_by_cash">Наличные</button>
                	</div>
                	<div class="col-6">
                		<button class="btn btn-outline-success btn-block" id="cashier_popup_paid_by_card">Банковская карта</button>
                	</div>
                </div>

                <input type="hidden" name="cashier_popup_paid_by" value="cash" id="cashier_popup_paid_by">
                <input type="hidden" id="cashier_popup_kkt"> 

                <div class="row" id="amountContainer">
                	<div class="form-group col-6">
                		<label><h4>ВНЕСЕНО</h4></label>
                		<input type="text" class="form-control" id="cashier_popup_amount">
                	</div>
                	<div class="form-group col-6">
                		<label><h4>СДАЧА</h4></label>
                		<input type="text" class="form-control" id="cashier_popup_change" disabled>
                	</div>
                </div>

                <hr />

                <button id="cashier_popup_cancel" type="button" class="btn btn-outline-danger btn-right">Отменить</button>
                <button id="cashier_popup_pay" type="button" class="btn btn-outline-primary">Оплачено</button>
                <button id="cashier_popup_agreement" type="button" class="btn btn-outline-info">Договор</button>
                <button id="cashier_popup_end" type="button" class="btn btn-outline-success">Завершить</button>

            </div>
        </div>
    </div>
</div>
<!-- endbarcode modal -->