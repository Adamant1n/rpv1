@extends('instructors.layouts.template')

@push('js')
<script>
	function deleteEvent()
	{
		$('#deleteEvent').submit();
	}
</script>
@endpush

@section('content')

<div class="row">
	<div class="col-12">

		@if ($errors->any())
			<div class="alert alert-danger" role="alert">
				<ul>
					@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif

		<div class="card">
			<div class="card-header">
				<a href="{{ route('instructors.event.index') }}" class="btn btn-light"><i class="fa fa-angle-left"></i>Назад</a>
				<a href="{{ route('instructors.event.show', $event) }}" class="btn btn-outline-success pull-right"><i class="fa fa-cloud-download"></i>Скачать ваучер</a>
			</div>
			<div class="card-body">
				<h4 class="card-title">Редактирование занятия</h4>
				<form method="post" action="{{ route('instructors.event.update', $event->id) }}">
					@csrf
					{{ method_field('PUT') }}

					<div class="row">
						<div class="col-md-4">
							<div class="form-group">
								<label for="trainer">Инструктор</label>
								<select class="form-control select2" name="trainer_id" id="trainer">
									<option value="1">Сделайте выбор......</option>
									@foreach ($trainers as $trainer)
									<option value="{{ $trainer->id }}"
									{{ $trainer->id == $event->trainer_id ? 'selected' : '' }}>{{ $trainer->last_name }} {{ $trainer->first_name }} {{ $trainer->middle_name }}</option>
									@endforeach
								</select>
							</div>
						</div>
						@role('administrator|operator')
						<div class="col-md-4">
							<div class="form-group">
								<label for="manager">Менеджер</label>
								<select class="form-control select2" name="manager_id" id="manager">
									<option value="1">Сделайте выбор......</option>
									@foreach ($managers as $manager)
									<option value="{{ $manager->id }}"
									{{ $manager->id == $event->manager_id ? 'selected' : '' }}>{{ $manager->last_name }} {{ $manager->first_name }} {{ $manager->middle_name }}</option>
									@endforeach
								</select>
							</div>
						</div>
						@else
						<input type="hidden" name="manager_id" value="{{ $event->manager_id }}">
						@endrole
						<div class="col-md-4">
							<div class="form-group">
								<label for="client">Клиент</label>
								<select class="form-control select2" name="client_id" id="client">
									<option value="1">Сделайте выбор......</option>
									@foreach ($clients as $client)
									<option value="{{ $client->id }}"
									{{ $client->id == $event->client_id ? 'selected' : '' }}>{{ $client->last_name }} {{ $client->first_name }} {{ $client->mid_name }}</option>
									@endforeach
								</select>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label for="category">Категория</label>
								<select class="form-control select1" name="category_id" id="category">
								@foreach ($categories as $category)
									<option value="{{ $category->id }}"
									{{ $category->id == $event->category_id ? 'selected' : '' }}>{{ $category->name }}</option>
								@endforeach
								</select>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label for="service">Услуга</label>
								<select class="form-control select1" name="service_id" id="service">
								@foreach ($services as $service)
									<option value="{{ $service->id }}"
									{{ $service->id == $event->service_id ? 'selected' : '' }}>{{ $service->name }}</option>
								@endforeach
								</select>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-md-3">
							<div class="form-group">
								<label for="event_date">Дата</label>
								<input name="event_date" type="text" class="form-control date" id="event_date" placeholder="Дата" value="{{ $event->event_date->format('Y-m-d') }}" required>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label for="start_time">Время начала</label>
								<input name="start_time" type="text" class="form-control timepicker" id="start_time" placeholder="Время начала" value="{{ substr($event->start_time, 0, 5) }}" required>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label for="end_time">Время окончания</label>
								<input name="end_time" type="text" class="form-control timepicker" id="end_time" placeholder="Время окончания" value="{{ substr($event->end_time, 0, 5) }}" required>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label for="number_people">Кол-во человек</label>
								<input name="number_people" type="number" class="form-control" id="number_people" value="{{ $event->number_people }}" placeholder="Кол-во человек">
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-md-2">
							<div class="form-group">
								<label for="cost">Стоимость</label>
								<input name="cost" type="number" class="form-control" id="cost" value="{{ $event->cost }}" placeholder="Стоимость">
							</div>
						</div>

						<div class="col-md-2">
							<div class="form-group">
								<label for="cost">Скидка</label>
								<input disabled type="text" class="form-control" value="{{ $event->discount }}">
							</div>
						</div>

						<div class="col-md-2">
							<div class="form-group">
								<label>Контрагент</label>
								<select class="form-control select2" name="agent_id" id="agent_id" disabled>
									<option value="" data-discount=0>Нет</option>
									@foreach ($agents as $agent)
									<option value="{{ $agent->id }}" data-discount="{{ $agent->discount }}"
									{{ $agent->id == $event->agent_id ? 'selected' : '' }}>{{ $agent->name }}</option>
									@endforeach
								</select>
							</div>
						</div>

						<div class="col-md-2">
							<div class="form-group">
								<label for="sale_date">Дата продажи</label>
								<input name="sale_date" type="text" class="form-control date" id="sale_date" value="{{ $event->sale_date->format('Y-m-d') }}" placeholder="Дата">
							</div>
						</div>

						@if($event->is_paid)
						<div class="col-md-2">
							<div class="form-group">
								<label for="">Дата оплаты</label>
								<input type="text" disabled class="form-control" value="{{ $event->paid_at->format('H:i d.m.Y') }}">
							</div>
						</div>
						<div class="col-md-2">
							<div class="form-group">
								<label for="">Тип оплаты</label>
								<input disabled type="text" class="form-control" value="{{ $event->paid_by === 'card' ? 'Карта' : 'Наличные' }}">
							</div>
						</div>
						@endif

						<!--
						<div class="col-md-3">
							<div class="form-group">
								<label for="payment_status">Оплачено</label>
								<select class="form-control select1" name="payment_status" id="payment_status">
									<option value="0" {{ $event->payment_status == '0' ? 'selected' : '' }}>Нет</option>
									<option value="1" {{ $event->payment_status == '1' ? 'selected' : '' }}>Да</option>
								</select>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label for="online_payment">Способ оплаты</label>
								<select class="form-control select1" name="online_payment" id="online_payment">
									<option value="0" {{ $event->online_payment == '0' ? 'selected' : '' }}>Оффлайн</option>
									<option value="1" {{ $event->online_payment == '1' ? 'selected' : '' }}>Онлайн</option>
								</select>
							</div>
						</div>
						-->
					</div>

					<button type="submit" class="btn btn-success"><i class="fa fa-save"></i>Сохранить</button>
					<button type="button" class="btn btn-danger" onClick="deleteEvent()">Удалить</button>
					@role('admin|superadmin|cashier')
					@if(!$event->is_paid)
					<a href="#" id="payButton" class="btn btn-primary">Оплатить</a>
					@endif
					@endrole

				</form>

				<form id="deleteEvent" action="{{ route('instructors.event.destroy', $event) }}" method="post">
					@csrf
					@method('DELETE')
				</form>
			</div>
		</div>

	</div>
</div>

<script>
$(document).ready(function(){
	var event_id = {{ $event->id }};

	$('#payButton').click(function () {
		$('#eventPopup').modal('show');
	});

	$('#event_popup_amount').on('input change input paste', function () {
		console.log('smg')
    	var amount = $(this).val();
    	var change = amount - {{ $event->cost }};
    	if(change < 0) change = 0;

    	$('#event_popup_change').val(change);
    });

    function setPaymentMethod(type)
    {
    	if(type == 'cash')
    	{
    		$('#event_popup_paid_by_cash').removeClass('btn-outline-primary');
        	$('#event_popup_paid_by_cash').addClass('btn-primary');
        	$('#event_popup_paid_by_card').removeClass('btn-success');
        	$('#event_popup_paid_by_card').addClass('btn-outline-success');
        	$('#event_popup_paid_by').val('cash');
    	}
    	else if(type == 'card')
    	{
    		$('#event_popup_paid_by_cash').addClass('btn-outline-primary');
        	$('#event_popup_paid_by_cash').removeClass('btn-primary');
        	$('#event_popup_paid_by_card').addClass('btn-success');
        	$('#event_popup_paid_by_card').removeClass('btn-outline-success');
        	$('#event_popup_paid_by').val('card');
    	}
    }

    $('#event_popup_paid_by_cash').click(function () {
    	setPaymentMethod('cash');
    });

    $('#event_popup_paid_by_card').click(function () {
    	setPaymentMethod('card');
    });

	/*
	$('#payButton').on('click', function () {
		swal({
			title: 'Выберите метод оплаты',
			buttons: {
				cash: {
			    	text: "Наличные",
			    	value: "cash",
			    },
			    card: {
			    	text: "Карта",
			    	value: "card",
			    },
			}
		}).then((value) => {
			window.location.href = "{{ route('instructors.event.pay', $event) }}?paid_by=" + value;
		});
	});
	*/
	
	$(".select2").select2();
	$(".select1").select2({
		minimumResultsForSearch: -1
	});
	$('input.date').datepicker({
		'autoClose': true,
		'dateFormat': 'yyyy-mm-dd'
	});
	$('.timepicker').timepicker({
		timeFormat: 'HH:mm',
		interval: 30,
		minTime: '9',
		maxTime: '18',
		startTime: '9',
		dynamic: false,
		dropdown: true,
		scrollbar: true
	});
});
</script>
@include('instructors.modals.order-js')
@include('instructors.modals.event-order')

@endsection
