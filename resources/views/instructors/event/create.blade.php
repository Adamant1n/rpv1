@extends('instructors.layouts.template')

@section('content')

@include('instructors.modals.create_client')

@push('js_bottom')
<script type="text/javascript">
	// форма добавления клиента
	$('form[name=create_client]').submit(function(e){

		var form = $(this);

		$.ajax({
			url: form.attr('action'),
			type: form.attr('method'),
			data: form.serialize(),
			success: function (response) {
				console.log(response);
				$('#createClientModal').modal('hide');

				var option = new Option(response.name, response.id);
				option.selected = true;

				$("#client_id").append(option);
				$("#client_id").trigger("change");
			},
		});

		e.preventDefault();
		return false;
	});


</script>
@endpush

<div class="row">
	<div class="col-md-12 col-lg-10">

	@if ($errors->any())
		<div class="alert alert-danger" role="alert">
			<ul>
				@foreach ($errors->all() as $error)
					<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div>
	@endif

	<div class="card">
		<div class="card-header">
			<a href="{{ route('instructors.event.index') }}" class="btn btn-light"><i class="fa fa-angle-left"></i>Назад</a>
		</div>
		<div class="card-body">
			<h4 class="card-title">Новое занятие</h4>
			<form method="post" action="{{ route('instructors.event.store') }}">
				@csrf
				@role('superadmin|admin|operator')
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label for="location_id">Точка продаж</label>
							<select class="form-control select1" name="location_id" id="location_id" required>
								<option value="">Сделайте выбор...</option>
								@foreach ($locations as $location)
								<option value="{{ $location->id }}"
								{{ $location->id == $request->location_id ? 'selected' : '' }}>{{ $location->address }}</option>
								@endforeach
							</select>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label for="manager_id">Менеджер</label>
							<select class="form-control select2" name="manager_id" id="manager_id" required>
								<option value="">Сделайте выбор...</option>
								@foreach ($managers as $manager)
								<option value="{{ $manager->id }}"
								{{ $manager->id == $request->manager_id ? 'selected' : '' }}>{{ $manager->last_name }} {{ $manager->first_name }} {{ $manager->middle_name }}</option>
								@endforeach
							</select>
						</div>
					</div>
				</div>
				@else
				<input type="hidden" name="manager_id" value="{{ auth()->user()->id }}">
				<input type="hidden" name="location_id" value="{{ auth()->user()->location_id }}">
				@endrole

				<div class="row">
					<div class="col-md-4">
						<div class="form-group">
							<label for="category_id">Категория</label>
							<select class="form-control select1" name="category_id" id="category_id" required>
								<option value="">Сделайте выбор...</option>
									@foreach ($categories as $category)
									<option value="{{ $category->id }}"
									{{ $category->id == $request->category_id ? 'selected' : '' }}>{{ $category->name }}</option>
									@endforeach
							</select>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label for="service_id">Услуга</label>
							<select class="form-control select1" name="service_id" id="service_id" required>
								<option value="">Сделайте выбор...</option>
								@foreach ($services as $service)
								<option value="{{ $service->id }}"
								{{ $service->id == $request->service_id ? 'selected' : '' }}>{{ $service->name }}</option>
								@endforeach
							</select>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label for="tariff_id">Тариф</label>
							<select class="form-control select1" name="tariff_id" id="tariff_id" required>
								<option value="">Сделайте выбор...</option>
							</select>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-md-4">
						<div class="form-group">
							<label for="trainer_id">Инструктор</label>
							<select class="form-control select2" name="trainer_id" id="trainer_id" required>
								<option value="">Сделайте выбор...</option>
								@foreach ($trainers as $trainer)
								<option value="{{ $trainer->id }}"
								{{ $trainer->id == $request->trainer_id ? 'selected' : '' }}>{{ $trainer->last_name }} {{ $trainer->first_name }} {{ $trainer->middle_name }}</option>
								@endforeach
							</select>
						</div>
					</div>

					<div class="col-md-3">
						<div class="form-group">
							<label>Контрагент</label>
							<select class="form-control select2" name="agent_id" id="agent_id">
								<option value="" data-discount=0>Сделайте выбор...</option>
								@foreach ($agents as $agent)
								<option value="{{ $agent->id }}" data-discount="{{ $agent->discount }}"
								{{ $agent->id == $request->agent_id ? 'selected' : '' }}>{{ $agent->name }}</option>
								@endforeach
							</select>
						</div>
					</div>

					<div class="col-md-3">
						<div class="form-group">
							<label for="client">Клиент</label>
							<select class="form-control select2" name="client_id" id="client_id" required>
								<option value="">Сделайте выбор...</option>
								@foreach ($clients as $client)
								<option value="{{ $client->id }}">{{ $client->last_name }} {{ $client->first_name }} {{ $client->mid_name }}</option>
								@endforeach
							</select>
						</div>
					</div>
					<div class="col-md-2">
						<div class="form-group">
							<label>&nbsp;</label>
							<button type="button" class="btn btn-info" style="display: block" data-toggle="modal" data-target="#createClientModal">Создать</button>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-md-3">
						<div class="form-group">
							<label for="event_date">Дата</label>
							<input name="event_date" type="text" class="form-control date" id="event_date" placeholder="Дата" value="{{ $request->event_date }}" required>
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<label for="start_time">Время начала</label>
							<input name="start_time" type="text" class="form-control timepicker" id="start_time" placeholder="Время начала" value="{{ $request->start_time }}" required>
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<label for="end_time">Время окончания</label>
							<input name="end_time" type="text" class="form-control timepicker" id="end_time" placeholder="Время окончания" value="{{ $request->end_time }}" required>
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<label for="number_people">Кол-во человек</label>
							<input name="number_people" type="number" class="form-control" id="number_people" min=1 value=1 placeholder="Кол-во человек" required>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-md-3">
						<div class="form-group">
							<label for="cost">Стоимость</label>
							<input name="cost" type="number" class="form-control" id="cost" placeholder="Стоимость" required>
						</div>
					</div>

					<input type="hidden" name="payment_status" id="payment_status" value=0>
					<input type="hidden" name="online_payment" id="online_payment" value=0>
					<input type="hidden" name="discount" id="discount" value=0>
					<input type="hidden" name="full_cost" id="full_cost" value=0>
				</div>

				<button type="submit" class="btn btn-success"><i class="fa fa-save"></i>Сохранить</button>

			</form>
		</div>
	</div>

	</div>
</div>

<script>
(function($) {
	'use strict';
	$(".select2").select2();
	$(".select1").select2({
		minimumResultsForSearch: -1
	});

    var disabledDays = @if(isset($managerLocation)) @json($managerLocation->weekends) @else [5, 6]; @endif

	$('input.date').datepicker({
		'autoClose': true,
		'dateFormat': 'yyyy-mm-dd',
        onRenderCell: function (date, cellType) {
            if (cellType == 'day') {
                var day = date.getDay();
                // isDisabled = disabledDays.indexOf(day) != -1;

                var isDisabled = false;
                for(var i = 0; i < disabledDays.length; i ++)
                {
                    if(disabledDays[i] == day || day == 0 && disabledDays[i] == 7)
                    {
                        isDisabled = true;
                    }
                }

                return {
                    disabled: isDisabled
                }
            }
        }
	});

	$('.timepicker').timepicker({
		timeFormat: 'HH:mm',
		interval: 30,
		minTime: '12:00',
		maxTime: '18:00',
		startTime: '9',
		dynamic: false,
		dropdown: true,
		scrollbar: true
	});

    $('#start_time')
        .timepicker('option', 'change', function(time) {
            $('#end_time').timepicker('option', 'minTime', time);
        });

	var tariffs = {!! json_encode($tariffs) !!};
	var trainers = {!! json_encode($trainers) !!};
	var managers = {!! json_encode($managers) !!};
	var locations = {!! json_encode($locations) !!};

	// выбор услуги
	$('#service_id').on('change', function () {
		// количество участников
		if($(this).val() == 1) {
			$('#number_people').attr('readonly', true);
			$('#number_people').val(1);
		}
		else {
			$('#number_people').removeAttr('readonly');
			$('#number_people').val(1);
		}

		// изменение списка тарифов
		$('#tariff_id').children().remove();
		$('#tariff_id').append(
			$("<option></option>").attr("value", "").text("Сделайте выбор...")
		);

		for(var i = 0; i < tariffs.length; i++)
		{
			if(tariffs[i].service_id == $(this).val())
			{
				$('#tariff_id').append(
					$("<option></option>").attr("value", tariffs[i].id).text(tariffs[i].name)
				);
			}
		}
	});

	// обновление стоимости
	function updatePrice()
	{
		var number_people = $('#number_people').val();
		var tariff_cost = $('#tariff_id option:selected').data('cost');
		var discount = $('#agent_id option:selected').data('discount');
		var price = number_people * tariff_cost * (100 - discount) / 100;
		var full_cost = number_people * tariff_cost;

		$('#discount').val(discount);
		$('#full_cost').val(full_cost);

		$('#cost').attr("value", price);
	}

	// изменение кол-ва человек
	$('#number_people').on('change', function () {
		updatePrice();
	});

	var location_id = {!! auth()->user()->location_id !!};
	// var location_id = null;
    setSchedule();

	$('#location_id').on('change', function () {
		location_id = $(this).val();
		setTrainers();
		setManagers();
        setSchedule();

	});

    function setSchedule() {
        let selectedLocation;
        $.each(locations, function (index, location) {
            if(location.id == location_id)
            {
                selectedLocation = location;
            }
        });

        $('#start_time').timepicker('option', 'minTime', selectedLocation.opens_at);
        $('#start_time').timepicker('option', 'startTime', selectedLocation.opens_at);
        $('#start_time').timepicker('option', 'maxTime', selectedLocation.closes_at);

        $('#end_time').timepicker('option', 'minTime', selectedLocation.opens_at);
        $('#end_time').timepicker('option', 'startTime', selectedLocation.opens_at);
        $('#end_time').timepicker('option', 'maxTime', selectedLocation.closes_at);

        disabledDays = selectedLocation.weekends;

        console.log(disabledDays);
    }

	var category_id = null;

	$('#category_id').on('change', function () {
		category_id = $(this).val();
		setTrainers();
	});

	function setTrainers()
	{
		$('#trainer_id').children().remove();

		$('#trainer_id').append(
			$("<option></option>").attr("value", "").attr('disabled', '').text("Сделайте выбор...")
		);

		$.each(locations, function (index, location) {
			if(location.id == location_id)
			{
				$.each(location.trainers, function (index, trainer) {
					if(category_id == null || (category_id != null && trainer.category_id == category_id))
					{
						$('#trainer_id').append(
							$("<option></option>").attr("value", trainer.id).text(trainer.full_name)
						);
					}
				});
			}
		});

		/*
		for(var i = 0; i < trainers.length; i++)
		{
			if((parseInt(location_id) && parseInt(category_id) && trainers[i].location_id == location_id && trainers[i].category_id == category_id) || (parseInt(location_id) && !parseInt(category_id) && trainers[i].location_id == location_id) || (!parseInt(location_id) && parseInt(category_id) && trainers[i].category_id == category_id))
			{
				$('#trainer_id').append(
					$("<option></option>").attr("value", trainers[i].id).text(trainers[i].last_name + ' ' + trainers[i].first_name + ' ' + trainers[i].middle_name)
				);
			}
		}
		*/
	}

	function setManagers()
	{
		$('#manager_id').children().remove();

		$('#manager_id').append(
			$("<option></option>").attr("value", "").text("Сделайте выбор...")
		);
		for(var i = 0; i < managers.length; i++)
		{
			if(managers[i].location_id == location_id)
			{
				$('#manager_id').append(
					$("<option></option>").attr("value", managers[i].id).text(managers[i].last_name + ' ' + managers[i].first_name + ' ' + managers[i].middle_name)
				);
			}
		}
	}

	$('#service_id').on('change', function () {

		$('#tariff_id').children().remove();

		$('#tariff_id').append(
			$("<option></option>").attr({
				"value": "",
				"data-cost": ""
			}).text("Сделайте выбор...")
		);
		for(var i = 0; i < tariffs.length; i++)
		{
			if(tariffs[i].service_id == $(this).val())
			{
				$('#tariff_id').append(
					$("<option></option>").attr({
						"value": tariffs[i].id,
						"data-cost": tariffs[i].cost
					}).text(tariffs[i].name)
				);
			}
		}
	});

	$('#agent_id').on('change', function () {
		updatePrice();
	});

	$('#tariff_id').on('change', function () {
		//$('#cost').attr("value", $('#tariff_id option:selected').data('cost'));
		updatePrice();
	});

})(jQuery);
</script>

@endsection
