@extends('instructors.layouts.template')

@section('content')

<div class="row">
	<div class="col-md-12 col-lg-10">

		@if ($errors->any())
			<div class="alert alert-danger" role="alert">
				<ul>
					@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif

		<div class="card">
			<div class="card-header">
				<a href="{{ route('instructors.event.index') }}" class="btn btn-light"><i class="fa fa-angle-left"></i>Назад</a>
			</div>
			<div class="card-body">

				<p><a href="/instructors/download/voucher_{{ $event->id }}.docx" class="btn btn-success"><i class="fa fa-cloud-download"></i>Скачать ваучер</a></p>

				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label>Точка продаж</label>
							<input class="form-control" value="{{ App\Location::find($event->location_id)->address }}" disabled>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label>Менеджер</label>
							@php $manager = auth()->user()->find($event->manager_id) @endphp
							<input class="form-control" value="{{ $manager->last_name }} {{ $manager->first_name }} {{ $manager->middle_name }}" disabled>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-md-4">
						<div class="form-group">
							<label>Категория</label>
							<input class="form-control" value="{{ App\Category::find($event->category_id)->name }}" disabled>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label>Услуга</label>
							<input class="form-control" value="{{ App\Service::find($event->service_id)->name }}" disabled>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label>Тариф</label>
							<input class="form-control" value="{{ App\Tariff::find($event->tariff_id)->name }}" disabled>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label>Инструктор</label>
							@php $trainer = auth()->user()->find($event->trainer_id) @endphp
							<input class="form-control" value="{{ $trainer->last_name }} {{ $trainer->first_name }} {{ $trainer->middle_name }}" disabled>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label>Клиент</label>
							@php $client = App\Client::find($event->client_id) @endphp
							<input class="form-control" value="{{ $client->last_name }} {{ $client->first_name }} {{ $client->mid_name }}" disabled>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-md-3">
						<div class="form-group">
							<label>Дата</label>
							<input class="form-control" value="{{ $event->event_date->format('d.m.Y') }}" disabled>
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<label>Время начала</label>
							<input class="form-control" value="{{ substr($event->start_time, 0, 5) }}" disabled>
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<label>Время окончания</label>
							<input class="form-control" value="{{ substr($event->end_time, 0, 5) }}" disabled>
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<label>Кол-во человек</label>
							<input class="form-control" value="{{ $event->number_people }}" disabled>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-md-3">
						<div class="form-group">
							<label>Стоимость</label>
							<input class="form-control" value="{{ $event->cost }}" disabled>
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<label>Дата продажи</label>
							<input class="form-control" value="{{ $event->sale_date->format('d.m.Y') }}" disabled>
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<label>Оплачено</label>
							<input class="form-control" value="{{ $event->payment_status ? 'Да' : 'Нет' }}" disabled>
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<label>Способ оплаты</label>
							<input class="form-control" value="{{ $event->online_payment ? 'Онлайн' : 'Оффлайн' }}" disabled>
						</div>
					</div>
				</div>

			</div>
		</div>

	</div>
</div>

@endsection
