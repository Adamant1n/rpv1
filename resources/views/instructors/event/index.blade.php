@extends('instructors.layouts.template')

@section('content')

@if (Session::has('success'))
<div class="alert alert-success" role="alert">
	{{ Session::get('success') }}
</div>
@endif

<div class="card">

	<div class="card-header">
		<div class="row">
			<div class="col-12">
				<div class="form-group">
					<a href="{{ route('instructors.event.create') }}" class="btn btn-success"><i class="fa fa-plus"></i>Добавить занятие</a>
				</div>
			</div>
		</div>
		@role('administrator|operator')
		<form method="get" action="/event">
			<div class="form-group">
				<div class="row">
					<div class="col-md-4">
						<select class="form-control select1" name="location" onchange="this.form.submit()">
							<option value="">Точка продаж...</option>
							@foreach ($locations as $location)
							<option value="{{ $location->id }}"
							{{ $location->id == app('request')->input('location') ? 'selected' : '' }}>{{ $location->address }}</option>
							@endforeach
						</select>
					</div>
					<div class="col-md-4">
						<select class="form-control select2" name="manager" onchange="this.form.submit()">
							<option value="">Менеджер...</option>
							@foreach ($managers as $manager)
							<option value="{{ $manager->id }}"
							{{ $manager->id == app('request')->input('manager') ? 'selected' : '' }}>{{ $manager->last_name }} {{ $manager->first_name }} {{ $manager->middle_name }}</option>
							@endforeach
						</select>
					</div>
					<div class="col-md-4">
						<select class="form-control select2" name="trainer" onchange="this.form.submit()">
							<option value="">Инструктор...</option>
							@foreach ($trainers as $trainer)
							<option value="{{ $trainer->id }}"
							{{ $trainer->id == app('request')->input('trainer') ? 'selected' : '' }}>{{ $trainer->last_name }} {{ $trainer->first_name }} {{ $trainer->middle_name }}</option>
							@endforeach
						</select>
					</div>
				</div>
			</div>
			<div class="form-group">
				<div class="row">
					<div class="col-md-4">
						<select class="form-control select2" name="client" onchange="this.form.submit()">
							<option value="">Клиент...</option>
							@foreach ($clients as $client)
							<option value="{{ $client->id }}"
							{{ $client->id == app('request')->input('client') ? 'selected' : '' }}>{{ $client->last_name }} {{ $client->first_name }} {{ $client->mid_name }}</option>
							@endforeach
						</select>
					</div>
					<div class="col-md-3">
						<select class="form-control select1" name="payment_status" onchange="this.form.submit()">
							<option value="">Статус оплаты...</option>
							<option value="1" {{ app('request')->input('payment_status') == '1' ? 'selected' : '' }}>Оплачено</option>
							<option value="0" {{ app('request')->input('payment_status') == '0' ? 'selected' : '' }}>Не оплачено</option>
						</select>
					</div>
					<div class="col-md-3">
						<select class="form-control select1" name="online_payment" onchange="this.form.submit()">
							<option value="">Тип оплаты...</option>
							<option value="1" {{ app('request')->input('online_payment') == '1' ? 'selected' : '' }}>Онлайн</option>
							<option value="0" {{ app('request')->input('online_payment') == '0' ? 'selected' : '' }}>Оффлайн</option>
						</select>
					</div>
				</div>
			</div>
		</form>
		@endrole
	</div>

	<div class="card-body">
		<h4 class="card-title">Занятия</h4>

		<div class="row">
		<div class="col-12">
			<table id="event-listing" class="table">
			<thead>
				<tr>
					<th>ID</th>
					<th>Менеджер</th>
					<th>Инструктор</th>
					<th>Клиент</th>
					<th>Услуга</th>
					<th>Дата</th>
					<th>Время</th>
					<th>Длительность</th>
					<th>Стоимость</th>
					<th>Оплачено</th>
					<th>Тип оплаты</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
			</table>
		</div>
		</div>
	</div>

</div>

<script>
(function($) {
'use strict';
$(function() {
	$('#event-listing').DataTable({
		"processing": true,
		"serverSide": false,
		"ajax": '/instructors/event/events_json?location={{ app('request')->input('location') }}&manager={{ app('request')->input('manager') }}&trainer={{ app('request')->input('trainer') }}&payment_status={{ app('request')->input('payment_status') }}&online_payment={{ app('request')->input('online_payment') }}',
		"iDisplayLength": 10,
		"aaSorting": [[0, "desc"]],
		"language": {
			"lengthMenu": "Показывать _MENU_ записей на странице",
			"zeroRecords": "Записей не найдено",
			"loadingRecords": "Загрузка... может занять несколько секунд...",
			"info": "Страница _PAGE_ из _PAGES_",
			"infoEmpty": "Показано с 0 по 0 из 0 записей",
			"search": "",
			"infoFiltered": "(Найдено записей: _TOTAL_)",
			"sInfo": "Показано с _START_ по _END_ из _TOTAL_ записей",
			"paginate":
			{
				"first":    "Первая",
				"last":     "Последняя",
				"next":     "Следующая",
				"previous": "Предыдущая"
			}
		}
	});
	$('#event-listing').each(function() {
		var datatable = $(this);
		// SEARCH - Add the placeholder for Search and Turn this into in-line form control
		var search_input = datatable.closest('.dataTables_wrapper').find('div[id$=_filter] input');
		search_input.attr('placeholder', 'Поиск');
		search_input.removeClass('form-control-sm');
		// LENGTH - Inline-Form control
		var length_sel = datatable.closest('.dataTables_wrapper').find('div[id$=_length] select');
		length_sel.removeClass('form-control-sm');
	});
	$('#event-listing tbody').on('click', 'tr', function () {
		var id = $('td', this).eq(0).text();
		$(location).attr('href','/instructors/event/'+id+'/edit');
	});
	$(".select2").select2();
	$(".select1").select2({
		minimumResultsForSearch: -1
	});
});
})(jQuery);
</script>

@endsection
