@extends('instructors.layouts.template')

@section('content')

<div class="row">
	<div class="col-md-6">

		@if ($errors->any())
			<div class="alert alert-danger" role="alert">
				<ul>
					@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif

		<div class="card">
			<div class="card-header">
				<a href="{{ route('instructors.service.index') }}" class="btn btn-light"><i class="fa fa-angle-left"></i>Назад</a>
				<a href="/instructors/service/{{ $service->id }}/delete" class="btn btn-outline-danger pull-right"><i class="fa fa-trash-o"></i>Удалить</a>
			</div>
			<div class="card-body">
				<h4 class="card-title">Переименовать услугу</h4>
				<form method="post" action="{{ route('instructors.service.update', $service->id) }}">
					{{ method_field('PUT') }}
					@csrf
					<div class="form-group">
						<input name="name" type="text" class="form-control" placeholder="Название услуги" value="{{ $service->name }}" required>
					</div>
					<button type="submit" class="btn btn-success"><i class="fa fa-save"></i>Сохранить</button>
				</form>
			</div>
		</div>

	</div>
</div>

@endsection
