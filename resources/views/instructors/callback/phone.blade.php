<div class="modal fade" id="callbackModal" tabindex="-1" role="dialog" aria-labelledby="ModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="ModalLabel">Входящий звонок</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body pb-0 pt-0">
                <table class="table">
                    <tbody>
                    <tr>
                        <td>Фамилия</td> 
                        <td id="callbackModalLastName"></td>
                    </tr>
                    <tr>
                        <td>Имя</td> 
                        <td id="callbackModalFirstName"></td>
                    </tr>
                    <tr>
                        <td>Отчество</td> 
                        <td id="callbackModalMidName"></td>
                    </tr>
                    <tr>
                        <td>Номер телефона</td> 
                        <td id="callbackModalPhone"></td>
                    </tr>
                    <tr>
                        <td>Email</td> 
                        <td id="callbackModalEmail"></td>
                    </tr>
                    </tbody>
                </table>
                <a class="btn-success btn ml-1" target="_blank" id="callbackModalId" href="#">Карточка клиента</a>
                <a class="btn-success btn ml-1" target="_blank" id="callbackModalEventId" href="#">Карточка занятия</a>
            </div>
            <div class="modal-footer"></div>
        </div>
    </div>
</div>

<script src="https://api.yandex.mightycall.ru/api/v2/sdk/mightycall.webphone.sdk.js">
</script>
<script type="text/javascript">
    var mcConfig = {login: "3da9aaa1-8abb-4ae8-8237-0d1b57e7825d", password: "0e4a31337327"};
    MightyCallWebPhone.ApplyConfig(mcConfig);
    MightyCallWebPhone.Phone.Init();

    var status = MightyCallWebPhone.Phone.Status();
    console.log(status);
    
    function webPhoneOnCallIncoming(callInfo) {
        console.log(callInfo);

        $.ajax({
            type: "get", 
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: "{{ route('instructors.callback.get_client') }}",
            data: "phone=" + callInfo.From,
            success: function(data) {
                console.log(data);

                if(data.success == true) {

                    $('#callbackModalLastName').html(data.client.last_name);
                    $('#callbackModalFirstName').html(data.client.first_name);
                    $('#callbackModalMidName').html(data.client.mid_name);
                    $('#callbackModalPhone').html(data.client.phone);
                    $('#callbackModalEmail').html(data.client.email);
                    $('#callbackModalId').attr('href', '/instructors/clients/' + data.client.id + '/edit');

                    if(data.client.events.length != 0) {
                        $('#callbackModalEventId').attr('href', '/instructors/event/' + data.client.events[0].id);
                    }

                    $('#callbackModal').modal('show');
                }
                else {

                }
            },
            error: function() {

            }
        });
    }

    /*
    webPhoneOnCallIncoming({
        From: "+79992198110"
    });
    */

    function webPhoneOnCallCompleted(callInfo) {
        console.log('Звонок окончен');
        $('#callbackModal').modal('hide');
    }

    MightyCallWebPhone.Phone.OnCallCompleted.subscribe(webPhoneOnCallCompleted);
    MightyCallWebPhone.Phone.OnHangUp.subscribe(webPhoneOnCallCompleted);
    MightyCallWebPhone.Phone.OnReject.subscribe(webPhoneOnCallCompleted);
    MightyCallWebPhone.Phone.OnCallIncoming.subscribe(webPhoneOnCallIncoming);
</script>