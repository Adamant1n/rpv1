@extends('instructors.layouts.template')

@section('content')

<div class="row">
	<div class="col-md-6">

		@if ($errors->any())
			<div class="alert alert-danger" role="alert">
				<ul>
					@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif

		<div class="card">
			<div class="card-header">
				<a href="{{ route('instructors.tariff.index') }}" class="btn btn-light"><i class="fa fa-angle-left"></i>Назад</a>
			</div>
			<div class="card-body">
				<h4 class="card-title">Новый тариф</h4>
				<form method="post" action="{{ route('instructors.tariff.store') }}">
					@csrf
					<div class="form-group">
						<label for="service">Выберите услугу</label>
						<select class="select2" name="service_id" id="service" style="width: 100%;">
						@foreach ($services as $service)
							<option value="{{ $service->id }}">{{ $service->name }}</option>
						@endforeach
						</select>
					</div>
					<div class="form-group">
						<label for="name">Название тарифа</label>
						<input name="name" type="text" class="form-control" id="name" placeholder="Название тарифа" required autofocus>
					</div>
					<div class="form-group">
						<label for="cost">Стоимость</label>
						<input name="cost" type="number" class="form-control" id="cost" placeholder="Стоимость" required>
					</div>
					<button type="submit" class="btn btn-success"><i class="fa fa-save"></i>Сохранить</button>
				</form>
			</div>
		</div>

	</div>
</div>

<script>
(function($) {
	'use strict';
	$(".select2").select2();
})(jQuery);
</script>

@endsection
