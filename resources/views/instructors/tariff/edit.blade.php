@extends('instructors.layouts.template')

@section('content')

<div class="row">
	<div class="col-md-6">

		@if ($errors->any())
			<div class="alert alert-danger" role="alert">
				<ul>
					@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif

		<div class="card">
			<div class="card-header">
				<a href="{{ route('instructors.tariff.index') }}" class="btn btn-light"><i class="fa fa-angle-left"></i>Назад</a>
				<a href="/instructors/tariff/{{ $tariff->id }}/delete" class="btn btn-outline-danger pull-right"><i class="fa fa-trash-o"></i>Удалить</a>
			</div>
			<div class="card-body">
				<h4 class="card-title">Изменить тариф</h4>
				<form method="post" action="{{ route('instructors.tariff.update', $tariff->id) }}">
					{{ method_field('PUT') }}
					@csrf
					<div class="form-group">
						<label for="service">Услуга</label>
						<input type="text" class="form-control" id="service" value="{{ $service }}" disabled>
					</div>
					<div class="form-group">
						<label for="name">Название тарифа</label>
						<input name="name" type="text" class="form-control" id="name" placeholder="Название тарифа" value="{{ $tariff->name }}" required>
					</div>
					<div class="form-group">
						<label for="cost">Стоимость</label>
						<input name="cost" type="number" class="form-control" id="cost" placeholder="Стоимость" value="{{ $tariff->cost }}" required>
					</div>
					<button type="submit" class="btn btn-success"><i class="fa fa-save"></i>Сохранить</button>
				</form>
			</div>
		</div>

	</div>
</div>

@endsection
