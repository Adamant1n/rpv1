@extends('instructors.layouts.template')

@section('content')

<div class="row">
	<div class="col-md-6">
		<div class="card">
			<div class="card-header">
				Настройки шаблонов
			</div>
			<div class="card-body">
				<form method="post" action="{{ route('instructors.settings.update') }}" enctype="multipart/form-data">
					@csrf
					<div class="form-group">
						<label>Шаблон договора</label>
						@if($template)
						<a href="{{ asset('storage/' . $template->value) }}" class="btn btn-outline-info btn-xs mb-2 btn-right">Скачать</a>
						@endif
						<input type="file" class="form-control" name="template">
					</div>

					<hr />

					<div class="form-group">
						<label>Шаблон SMS</label>
						<textarea class="form-control" name="sms">{{ $sms->value }}</textarea>
						<code>
							Доступные переменные:<br>
							{name} – Имя клиента<br>
							{date} – Дата занятия<br>
							{time_start} – Время начала занятия<br>
							{time_end} – Время окончания занятия
						</code>
					</div>

					<button type="submit" class="btn btn-success"><i class="fa fa-save"></i>Сохранить</button>
				</form>
			</div>
		</div>

	</div>
</div>

@endsection
