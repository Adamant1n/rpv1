@extends('instructors.layouts.template')

@section('content')

<div class="card">

	<div class="card-header">
		<form method="get" action="/instructors/report/trainers">
			<div class="row">
				<div class="col-md-2">
					<input name="date_start" type="text" class="form-control date" placeholder="Дата начала" value="{{ $request->date_start }}">
				</div>
				<div class="col-md-2">
					<input name="date_end" type="text" class="form-control date" placeholder="Дата конец" value="{{ $request->date_end }}">
				</div>
				<div class="col-md-3">
					<button type="submit" class="btn btn-light"><i class="fa fa-filter"></i>Применить</button>
				</div>
			</div>
		</form>
	</div>

	<div class="card-body">
		<h4 class="card-title">Загрузка инструкторов</h4>

		<div class="row">
		<div class="col-12">
			<table id="report-listing" class="table">
			<thead>
				<tr>
					<th>Инструктор</th>
					<th>Телефон инструктора</th>
					<th>Лыжник</th>
					<th>Сноубордист</th>
					<th>Дата приема</th>
					<th>Часов занятий</th>
					<th>Средняя загрузка</th>
					<th>Часов отдыха</th>
					<th>Всего ЗП начислено</th>
				</tr>
			</thead>
			<tbody>

			@foreach ($trainers as $trainer)

			@php
			$where = [['trainer_id', '=', $trainer->id]];
			if ($request->date_start !== null)
				$where[] = ['event_date', '>=', $request->date_start];
			if ($request->date_end !== null)
				$where[] = ['event_date', '<=', $request->date_end];
			$events = App\Instructors\Event::where($where)->get();
			$hours = 0;
			foreach ($events as $event)
				$hours += ((strtotime($event->event_date->format('Y-m-d').' '.$event->end_time) - strtotime($event->event_date->format('Y-m-d').' '.$event->start_time)) / 60) / 60;
			@endphp

			<tr>
				<td>
					{{ $trainer->last_name }}
					{{ $trainer->first_name }}
					{{ $trainer->middle_name }}
				</td>
				<td>{{ $trainer->phone }}</td>
				<td>-</td>
				<td>-</td>
				<td>{{ $trainer->created_at->format('d.m.Y') }}</td>
				<td>{{ $hours }}</td>
				<td>-</td>
				<td>-</td>
				<td>{{ round($hours * $trainer->rate) }}</td>
			</tr>
			@endforeach

			</tbody>
			</table>
		</div>
		</div>
	</div>

</div>

<script>
(function($) {
'use strict';
$(function() {
	$('#report-listing').DataTable({
		"dom": 'Bfrtip',
		"buttons": [
			'excel', 'pdf', 'print'
		],
		"searching": false,
		"iDisplayLength": 1000,
		"aaSorting": [[0, "asc"]],
		"language": {
			"lengthMenu": "Показывать _MENU_ записей на странице",
			"zeroRecords": "Записей не найдено",
			"loadingRecords": "Загрузка... может занять несколько секунд...",
			"info": "Страница _PAGE_ из _PAGES_",
			"infoEmpty": "Показано с 0 по 0 из 0 записей",
			"infoFiltered": "(Найдено записей: _TOTAL_)",
			"sInfo": "Показано с _START_ по _END_ из _TOTAL_ записей",
			"paginate":
			{
				"first":    "Первая",
				"last":     "Последняя",
				"next":     "Следующая",
				"previous": "Предыдущая"
			}
		}
	});
	$('input.date').datepicker({
		'autoClose': true,
		'dateFormat': 'yyyy-mm-dd'
	});
});
})(jQuery);
</script>

@endsection
