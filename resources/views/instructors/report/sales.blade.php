@extends('instructors.layouts.template')

@section('content')

<div class="card">

	<div class="card-header">
		<form method="get" action="/instructors/report/sales">
			<div class="form-group">
				<div class="row">
					<div class="col-md-3">
						<select class="form-control select2" name="location" onchange="this.form.submit()">
							<option value="">Точка продаж...</option>
							@foreach ($locations as $location)
							<option value="{{ $location->id }}"
							{{ $location->id == app('request')->input('location') ? 'selected' : '' }}>{{ $location->address }}</option>
							@endforeach
						</select>
					</div>
					<div class="col-md-3">
						<select class="form-control select2" name="service">
							<option value="">Тип занятия...</option>
							@foreach ($services as $service)
							<option value="{{ $service->id }}"
							{{ $service->id == app('request')->input('service') ? 'selected' : '' }}>{{ $service->name }}</option>
							@endforeach
						</select>
					</div>
					<div class="col-md-3">
						<select class="form-control select2" name="trainer">
							<option value="">Инструктор...</option>
							@foreach ($trainers as $trainer)
							<option value="{{ $trainer->id }}"
							{{ $trainer->id == app('request')->input('trainer') ? 'selected' : '' }}>{{ $trainer->last_name }} {{ $trainer->first_name }} {{ $trainer->middle_name }}</option>
							@endforeach
						</select>
					</div>
					<div class="col-md-3">
						<select class="form-control select2" name="manager">
							<option value="">Менеджер...</option>
							@foreach ($managers as $manager)
							<option value="{{ $manager->id }}"
							{{ $manager->id == app('request')->input('manager') ? 'selected' : '' }}>{{ $manager->last_name }} {{ $manager->first_name }} {{ $manager->middle_name }}</option>
							@endforeach
						</select>
					</div>
				</div>
			</div>
			<div class="form-group">
				<div class="row">
					<div class="col-md-2">
						<input name="date_start" type="text" class="form-control date" placeholder="Дата начала">
					</div>
					<div class="col-md-2">
						<input name="date_end" type="text" class="form-control date" placeholder="Дата конец">
					</div>
					<div class="col-md-3">
						<button type="submit" class="btn btn-light"><i class="fa fa-filter"></i>Применить</button>
					</div>
				</div>
			</div>
		</form>
	</div>

	<div class="card-body">
		<h4 class="card-title">Отчет о продажах</h4>

		<div class="row">
		<div class="col-12">
			<table id="report-listing" class="table">
			<thead>
				<tr>
					<th>Клиент</th>
					<th>Телефон клиента</th>
					<th>Дата продажи</th>
					<th>Дата занятия</th>
					<th>Длитель- ность, мин</th>
					<th>Тип занятия</th>
					<th>Тариф</th>
					<th>Кол-во человек</th>
					<th>Снаряд</th>
					<th>Точка продаж</th>
					<th>Сумма</th>
					<th>Менеджер</th>
					<th>Инструктор</th>
					<th>ЗП инструктора</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
			</table>
		</div>
		</div>
	</div>

</div>

<script>
(function($) {
'use strict';
$(function() {
	$('#report-listing').DataTable({
		"processing": true,
		"serverSide": true,
		"ajax": '/instructors/report/sales_json?location={{ app('request')->input('location') }}&service={{ app('request')->input('service') }}&trainer={{ app('request')->input('trainer') }}&manager={{ app('request')->input('manager') }}&date_start={{ app('request')->input('date_start') }}&date_end={{ app('request')->input('date_end') }}',
		"dom": 'Bfrtip',
		"buttons": [
			'excel', 'pdf', 'print'
		],
		"columns": [
			null, //Клиент
			{"orderable": false }, //Телефон клиента
			null, //Дата продажи
			null, //Дата занятия
			{"orderable": false }, //Длительность
			null, //Тип занятия
			null, //Тариф
			null, //Кол-во человек
			null, //Снаряд
			null, //Точка продаж
			{"orderable": false }, //Сумма
			null, //Менеджер
			null, //Инструктор
			{"orderable": false }, //ЗП инструктора
		],
		"searching": false,
		"iDisplayLength": 1000,
		"aaSorting": [[0, "asc"]],
		"language": {
			"lengthMenu": "Показывать _MENU_ записей на странице",
			"zeroRecords": "Записей не найдено",
			"loadingRecords": "Загрузка... может занять несколько секунд...",
			"info": "Страница _PAGE_ из _PAGES_",
			"infoEmpty": "Показано с 0 по 0 из 0 записей",
			"infoFiltered": "(Найдено записей: _TOTAL_)",
			"sInfo": "Показано с _START_ по _END_ из _TOTAL_ записей",
			"paginate":
			{
				"first":    "Первая",
				"last":     "Последняя",
				"next":     "Следующая",
				"previous": "Предыдущая"
			}
		}
	});
	$(".select2").select2();
	$('input.date').datepicker({
		'autoClose': true,
		'dateFormat': 'yyyy-mm-dd'
	});
});
})(jQuery);
</script>

@endsection
