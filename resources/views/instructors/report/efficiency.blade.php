@extends('instructors.layouts.template')

@section('content')

<div class="card">

	<div class="card-header">
		<form method="get" action="/instructors/report/efficiency">
			<div class="form-group">
				<div class="row">
					<div class="col-md-3">
						<select class="form-control select2" name="location" onchange="this.form.submit()">
							<option value="">Точка продаж...</option>
							@foreach ($locations as $location)
							<option value="{{ $location->id }}"
							{{ $location->id == app('request')->input('location') ? 'selected' : '' }}>{{ $location->address }}</option>
							@endforeach
						</select>
					</div>
					<div class="col-md-3">
						<select class="form-control select2" name="manager">
							<option value="">Менеджер...</option>
							@foreach ($managers as $manager)
							<option value="{{ $manager->id }}"
							{{ $manager->id == app('request')->input('manager') ? 'selected' : '' }}>{{ $manager->last_name }} {{ $manager->first_name }} {{ $manager->middle_name }}</option>
							@endforeach
						</select>
					</div>
				</div>
			</div>
			<div class="form-group">
				<div class="row">
					<div class="col-md-2">
						<input name="date_start" type="text" class="form-control date" placeholder="Дата начала" value="{{ $request->date_start }}">
					</div>
					<div class="col-md-2">
						<input name="date_end" type="text" class="form-control date" placeholder="Дата конец" value="{{ $request->date_end }}">
					</div>
					<div class="col-md-3">
						<button type="submit" class="btn btn-light"><i class="fa fa-filter"></i>Применить</button>
					</div>
				</div>
			</div>
		</form>
	</div>

	<div class="card-body">
		<h4 class="card-title">Эффективность продаж</h4>

		<div class="row">
		<div class="col-12">
			<table id="report-listing" class="table">
			<thead>
				<tr>
					<th>Менеджер</th>
					<th>Точка продаж</th>
					<th>Выручка всего</th>
					<th>Продано часов занятий</th>
					<th>Создано клиентов</th>
				</tr>
			</thead>
			<tbody>

			@foreach ($managers as $manager)

			@php
			$where = [['manager_id', '=', $manager->id]];
			$where_cli = [['user_id', '=', $manager->id]];
			if ($request->date_start !== null)
			{
				$where[] = ['event_date', '>=', $request->date_start];
				$where_cli[] = ['created_at', '>=', $request->date_start.' 00:00:00'];
			}
			if ($request->date_end !== null)
			{
				$where[] = ['event_date', '<=', $request->date_end];
				$where_cli[] = ['created_at', '<=', $request->date_end.' 00:00:00'];
			}
			$events = App\Instructors\Event::where($where)->get();
			$hours = 0;
			$proceeds = 0;
			foreach ($events as $event)
			{
				$hour = ((strtotime($event->event_date->format('Y-m-d').' '.$event->end_time) - strtotime($event->event_date->format('Y-m-d').' '.$event->start_time)) / 60) / 60;
				$hours += $hour;
				$proceeds += $hour * (App\Instructors\Tariff::find($event->tariff_id)->cost);
			}
			@endphp

			<tr>
				<td>
					{{ $manager->last_name }}
					{{ $manager->first_name }}
					{{ $manager->middle_name }}
				</td>
				<td>{{ $locations->find($manager->location_id)->address }}</td>
				<td>{{ round($proceeds) }}</td>
				<td>{{ round($hours) }}</td>
				<td>{{ App\Instructors\Client::where($where_cli)->count() }}</td>
			</tr>
			@endforeach

			</tbody>
			</table>
		</div>
		</div>
	</div>

</div>

<script>
(function($) {
'use strict';
$(function() {
	$('#report-listing').DataTable({
		"dom": 'Bfrtip',
		"buttons": [
			'excel', 'pdf', 'print'
		],
		"searching": false,
		"iDisplayLength": 1000,
		"aaSorting": [[0, "asc"]],
		"language": {
			"lengthMenu": "Показывать _MENU_ записей на странице",
			"zeroRecords": "Записей не найдено",
			"loadingRecords": "Загрузка... может занять несколько секунд...",
			"info": "Страница _PAGE_ из _PAGES_",
			"infoEmpty": "Показано с 0 по 0 из 0 записей",
			"infoFiltered": "(Найдено записей: _TOTAL_)",
			"sInfo": "Показано с _START_ по _END_ из _TOTAL_ записей",
			"paginate":
			{
				"first":    "Первая",
				"last":     "Последняя",
				"next":     "Следующая",
				"previous": "Предыдущая"
			}
		}
	});
	$(".select2").select2();
	$(".select1").select2({
		minimumResultsForSearch: -1
	});
	$('input.date').datepicker({
		'autoClose': true,
		'dateFormat': 'yyyy-mm-dd'
	});
});
})(jQuery);
</script>

@endsection
