@extends('instructors.layouts.template')

@section('content')

@if (Session::has('success'))
<div class="alert alert-success" role="alert">
	{{ Session::get('success') }}
</div>
@endif

<div class="card">
	<div class="card-header">
		<a href="{{ route('instructors.category.create') }}" class="btn btn-success"><i class="fa fa-plus"></i>Добавить категорию</a>
	</div>
	<div class="card-body">
		<h4 class="card-title">Категории занятий</h4>
		<div class="row">
		<div class="col-12">
			<table id="category-listing" class="table">
			<thead>
				<tr>
					<th>ID</th>
					<th>Название категории</th>
					<th>Дата создания</th>
					<th>Дата изменения</th>
				</tr>
			</thead>
			<tbody>

			@foreach ($categories as $category)
			<tr>
				<td>{{ $category->id }}</td>
				<td>{{ $category->name }}</td>
				<td>{{ optional($category->created_at)->format('d.m.Y H:i') }}</td>
				<td>{{ optional($category->updated_at)->format('d.m.Y H:i') }}</td>
			</tr>
			@endforeach

			</tbody>
			</table>
		</div>
		</div>
	</div>
</div>

<script>
(function($) {
	'use strict';
	$(function() {
		$('#category-listing').DataTable({
			"iDisplayLength": 10,
			"aaSorting": [[0, "asc"]],
			"language": {
				"lengthMenu": "Показывать _MENU_ записей на странице",
				"zeroRecords": "Записей не найдено",
				"loadingRecords": "Загрузка... может занять несколько секунд...",
				"info": "Страница _PAGE_ из _PAGES_",
				"infoEmpty": "Показано с 0 по 0 из 0 записей",
				"search": "",
				"infoFiltered": "(Найдено записей: _TOTAL_)",
				"sInfo": "Показано с _START_ по _END_ из _TOTAL_ записей",
				"paginate":
				{
					"first":    "Первая",
					"last":     "Последняя",
					"next":     "Следующая",
					"previous": "Предыдущая"
				}
			}
		});
		$('#category-listing').each(function() {
			var datatable = $(this);
			// SEARCH - Add the placeholder for Search and Turn this into in-line form control
			var search_input = datatable.closest('.dataTables_wrapper').find('div[id$=_filter] input');
			search_input.attr('placeholder', 'Поиск');
			search_input.removeClass('form-control-sm');
			// LENGTH - Inline-Form control
			var length_sel = datatable.closest('.dataTables_wrapper').find('div[id$=_length] select');
			length_sel.removeClass('form-control-sm');
		});
		$('#category-listing tbody').on('click', 'tr', function () {
			var id = $('td', this).eq(0).text();
			$(location).attr('href','/instructors/category/'+id+'/edit');
		});
	});
})(jQuery);
</script>

@endsection
