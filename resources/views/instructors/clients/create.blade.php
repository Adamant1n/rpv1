@extends('instructors.layouts.template')

@section('content')

<div class="card">
	<div class="card-header">
		<a href="{{ route('instructors.clients.index') }}" class="btn btn-light"><i class="fa fa-angle-left"></i>Назад</a>
	</div>
	<div class="card-body">
		<h4 class="card-title">Новый клиент</h4>
		<form method="POST" action="{{ route('instructors.clients.store') }}">
			@csrf
			<div class="form-group row">
				<label class="col-sm-2 col-form-label text-md-right">Фамилия</label>
				<div class="col-md-6">
					<input type="text" class="form-control" name="last_name" required autofocus>
				</div>
			</div>

			<div class="form-group row">
				<label class="col-sm-2 col-form-label text-md-right">Имя</label>
				<div class="col-md-6">
					<input type="text" class="form-control" name="first_name" required>
				</div>
			</div>

			<div class="form-group row">
				<label class="col-sm-2 col-form-label text-md-right">Отчество</label>
				<div class="col-md-6">
					<input type="text" class="form-control" name="mid_name" required>
				</div>
			</div>

			<div class="form-group row">
				<label class="col-sm-2 col-form-label text-md-right">Номер телефона</label>
				<div class="col-md-6">
					<input type="text" class="form-control" name="phone" required>
				</div>
			</div>

			<div class="form-group row">
				<label class="col-sm-2 col-form-label text-md-right">Email</label>
				<div class="col-md-6">
					<input type="text" class="form-control" name="email">
				</div>
			</div>

			<div class="form-group row">
				<label class="col-sm-2 col-form-label text-md-right">Номер паспорта</label>
				<div class="col-md-6">
					<input type="text" class="form-control" name="passport">
				</div>
			</div>

			<div class="form-group row">
				<label class="col-sm-2 col-form-label text-md-right">Заметка</label>
				<div class="col-md-6">
					<textarea class="form-control" name="description"></textarea>
				</div>
			</div>

			<div class="form-group row">
				<label class="col-sm-2 col-form-label text-md-right"></label>
				<div class="col-md-6">
					<button type="submit" class="btn btn-success"><i class="fa fa-save"></i>Сохранить</button>
				</div>
			</div>
		</form>
	</div>
</div>

@endsection
