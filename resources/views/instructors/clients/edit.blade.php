@extends('instructors.layouts.template')

@section('content')

<div class="card">
	<div class="card-header">
		<a href="{{ route('instructors.clients.index') }}" class="btn btn-light"><i class="fa fa-angle-left"></i>Назад</a>

        @if($client->currentOrder)
        <a href="#" class="btn btn-outline-primary" id="client_current_order" data-id="{{ $client->currentOrder->optional_id }}">
            Продажа товаров
        </a>
        @else
        <a href="#" id="client_new_order" data-id="{{ $client->optional_id }}" class="btn btn-outline-success">
            Продажа товаров
        </a>
        @endif

	</div>
	<div class="card-body">
		<h4 class="card-title">Изменить клиента</h4>
		<form method="POST" action="{{ route('instructors.clients.update', $client) }}">
			@csrf
			<input type="hidden" name="_method" value="PUT">
			<div class="form-group row">
				<label class="col-sm-4 col-form-label text-md-right">Фамилия</label>
				<div class="col-md-6">
					<input type="text" value="{{ $client->last_name }}" class="form-control" name="last_name" required autofocus>
				</div>
			</div>

			<div class="form-group row">
				<label class="col-sm-4 col-form-label text-md-right">Имя</label>
				<div class="col-md-6">
					<input type="text" value="{{ $client->first_name }}" class="form-control" name="first_name" required>
				</div>
			</div>

			<div class="form-group row">
				<label class="col-sm-4 col-form-label text-md-right">Отчество</label>
				<div class="col-md-6">
					<input type="text" value="{{ $client->mid_name }}" class="form-control" name="mid_name" required>
				</div>
			</div>

			<div class="form-group row">
				<label class="col-sm-4 col-form-label text-md-right">Номер телефона</label>
				<div class="col-md-6">
					<input type="text" value="{{ $client->phone }}" class="form-control" name="phone" required>
				</div>
			</div>

			<div class="form-group row">
				<label class="col-sm-4 col-form-label text-md-right">Email</label>
				<div class="col-md-6">
					<input type="text" value="{{ $client->email }}" class="form-control" name="email">
				</div>
			</div>

			<div class="form-group row">
				<label class="col-sm-4 col-form-label text-md-right">Номер паспорта</label>
				<div class="col-md-6">
					<input type="text" value="{{ $client->passport }}" class="form-control" name="passport">
				</div>
			</div>

			<div class="form-group row">
				<label class="col-sm-4 col-form-label text-md-right">Заметка</label>
				<div class="col-md-6">
					<textarea class="form-control" name="description">{{ $client->description }}</textarea>
				</div>
			</div>

			<div class="form-group row mb-0">
				<div class="col-md-8 offset-md-4">
					<button type="submit" class="btn btn-primary">
						Сохранить
					</button>
				</div>
			</div>
		</form>
	</div>
</div>

@endsection
