@extends('instructors.layouts.template')

@section('content')

<div class="card">
	<div class="card-header">
		<a href="{{ route('instructors.clients.index') }}" class="btn btn-light"><i class="fa fa-angle-left"></i>Назад</a>
	</div>
	<div class="card-body">
		<h4 class="card-title">Карточка клиента</h4>
		<table class="table">
			<tbody>
				<tr>
					<td>
						Имя
					</td>
					<td>
						{{ $client->full_name }}
					</td>
				</tr>
				<tr>
					<td>
						Номер телефона
					</td>
					<td>
						{{ $client->phone }}
					</td>
				</tr>
				<tr>
					<td>
						Email
					</td>
					<td>
						{{ $client->email }}
					</td>
				</tr>
				<tr>
					<td>
						Номер паспорта
					</td>
					<td>
						{{ $client->passport }}
					</td>
				</tr>
				<tr>
					<td>
						Заметка
					</td>
					<td>
						{{ $client->description }}
					</td>
				</tr>
			</tbody>
		</table>
	</div>
</div>

@endsection
