@extends('layouts.app')

@section('content')

@if (Session::has('success'))
<div class="alert alert-success" role="alert">
	{{ Session::get('success') }}
</div>
@endif

<div class="card">
	<div class="card-header">
		<a href="{{ route('tariffs.create') }}" class="btn btn-success pull-left mr-4 mb-2"><i class="fa fa-plus"></i>Добавить тариф</a>
		@role('superadmin')
		<form>
			<select class="form-control select2 col-md-6 col-lg-3" name="location" onchange="this.form.submit()">
				<option value="">Все филиалы</option>
				@foreach ($locations as $location)
				<option value="{{ $location->id }}"
				{{ $location->id == $request->location ? 'selected' : '' }}>{{ $location->address }}</option>
				@endforeach
			</select>
		</form>
		@endrole
	</div>
	<div class="card-body">
		<h4 class="card-title">Тарифы</h4>
		<div class="row">
			<div class="col-12">
				<table id="tariffs-listing" class="table">
				<thead>
					<tr>
						<th>ID</th>
						<th>Категория товара</th>
						<th>Прейскурант</th>
						<th>Длительность</th>
						<th>Единица</th>
						<th>Стоимость</th>
						<th>Дата создания</th>
						<th>Дата изменения</th>
					</tr>
				</thead>
				<tbody>

				@php $d = ['h'=>'час','d'=>'день'] @endphp

				@foreach ($tariffs as $tariff)
				<tr>
					<td>{{ $tariff->id }}</td>
					<td>{{ $categories->find($tariff->category_id)->name }}</td>
					<td>{{ $price_lists->find($tariff->price_list_id)->name }}</td>
					<td>{{ $tariff->duration }}</td>
					<td>{{ $d[$tariff->dimension] }}</td>
					<td>{{ $tariff->price }}</td>
					<td>{{ $tariff->created_at->format('d.m.Y H:i') }}</td>
					<td>{{ $tariff->updated_at->format('d.m.Y H:i') }}</td>
				</tr>
				@endforeach

				</tbody>
				</table>
			</div>
		</div>
	</div>
</div>

<script>
(function($) {
	'use strict';
	$(function() {
		$('#tariffs-listing').DataTable({
			"iDisplayLength": 10,
			"aaSorting": [[0, "asc"]],
			"language": {
				"lengthMenu": "Показывать _MENU_ записей на странице",
				"zeroRecords": "Записей не найдено",
				"loadingRecords": "Загрузка... может занять несколько секунд...",
				"info": "Страница _PAGE_ из _PAGES_",
				"infoEmpty": "Показано с 0 по 0 из 0 записей",
				"search": "",
				"infoFiltered": "(Найдено записей: _TOTAL_)",
				"sInfo": "Показано с _START_ по _END_ из _TOTAL_ записей",
				"paginate":
				{
					"first":    "Первая",
					"last":     "Последняя",
					"next":     "Следующая",
					"previous": "Предыдущая"
				}
			}
		});
		$('#tariffs-listing').each(function() {
			var datatable = $(this);
			var search_input = datatable.closest('.dataTables_wrapper').find('div[id$=_filter] input');
			search_input.attr('placeholder', 'Поиск');
			search_input.removeClass('form-control-sm');
			var length_sel = datatable.closest('.dataTables_wrapper').find('div[id$=_length] select');
			length_sel.removeClass('form-control-sm');
		});
		$('#tariffs-listing tbody').on('click', 'tr', function () {
			var id = $('td', this).eq(0).text();
			$(location).attr('href','/tariffs/'+id+'/edit');
		});
	});
	$(".select2").select2();
})(jQuery);
</script>

@endsection
