@extends('layouts.app')

@section('content')

<div class="row">
	<div class="col-md-6">

		@if ($errors->any())
			<div class="alert alert-danger" role="alert">
				<ul>
					@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif

		<div class="card">
			<div class="card-header">
				<a href="{{ route('tariffs.index') }}" class="btn btn-light"><i class="fa fa-angle-left"></i>Назад</a>
			</div>
			<div class="card-body">
				<h4 class="card-title">Новый тариф</h4>
				<form method="post" action="{{ route('tariffs.store', 'price_list='.$request->price_list) }}">
					@csrf
					@role('superadmin')
					<div class="form-group">
						<label for="location_id">Филиал</label>
						<select class="form-control select2" name="location_id" id="location_id" required>
							<option value="">Сделайте выбор...</option>
							@foreach ($locations as $location)
							<option value="{{ $location->id }}">{{ $location->address }}</option>
							@endforeach
						</select>
					</div>
					@endrole
					<div class="form-group">
						<label for="price_list_id">Прейскурант</label>
						<select class="form-control select2" name="price_list_id" id="price_list_id" required>
							<option value="">Сделайте выбор...</option>
							@foreach ($price_lists as $price_list)
							<option value="{{ $price_list->id }}"
								{{ $request->price_list == $price_list->id ? 'selected' : '' }}>
								{{ $price_list->name }}</option>
							@endforeach
						</select>
					</div>
					<div class="form-group">
						<label for="category_id">Категория</label>
						<select class="form-control select2" name="category_id" id="category_id" required>
							<option value="">Сделайте выбор...</option>
							@foreach ($categories as $category)
							<option value="{{ $category->id }}">{{ $category->name }}</option>
							@endforeach
						</select>
					</div>
					<div class="form-group">
						<label for="price">Стоимость</label>
						<input name="price" id="price" type="number" class="form-control" placeholder="Стоимость" required autofocus>
					</div>
					<div class="form-group">
						<label for="duration">Длительность</label>
						<input name="duration" id="duration" type="number" class="form-control" placeholder="Длительность" required>
					</div>
					<div class="form-group">
						<label for="dimension">Единица</label>
						<select class="form-control select2" name="dimension" id="dimension" required>
							<option value="h">час</option>
							<option value="d">день</option>
						</select>
					</div>
					<button type="submit" class="btn btn-success">
						<i class="fa fa-save"></i>Сохранить</button>
				</form>
			</div>
		</div>

	</div>
</div>

<script>
(function($) {
	'use strict';
	$(".select2").select2();

	@role('superadmin')
	var priceLists = {!! json_encode($price_lists) !!};
	$('#location_id').on('change', function () {
		$('#price_list_id').children().remove();
		$('#price_list_id').append(
			$("<option></option>").attr("value", "").text("Сделайте выбор...")
		);
		for(var i = 0; i < priceLists.length; i++)
		{
			if(priceLists[i].location_id == $(this).val())
			{
				$('#price_list_id').append(
					$("<option></option>").attr("value", priceLists[i].id).text(priceLists[i].name)
				);
			}
		}
	});
	@endrole

})(jQuery);
</script>

@endsection
