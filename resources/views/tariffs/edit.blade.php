@extends('layouts.app')

@section('content')

<div class="row">
	<div class="col-md-6">

		@if ($errors->any())
			<div class="alert alert-danger" role="alert">
				<ul>
					@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif

		<div class="card">
			<div class="card-header">
				<a href="{{ route('tariffs.index') }}" class="btn btn-light pull-left"><i class="fa fa-angle-left"></i>Назад</a>
				<form method="post" action="{{ route('tariffs.destroy', $tariff->id) }}">
					@csrf
					{{ method_field('DELETE') }}
					<button type="submit" class="btn btn-outline-danger pull-right"><i class="fa fa-trash-o"></i>Удалить</button>
				</form>
			</div>
			<div class="card-body">
				<h4 class="card-title">Изменить тариф</h4>
				<form method="post" action="{{ route('tariffs.update', $tariff->id) }}">
					{{ method_field('PATCH') }}
					@csrf
					<div class="form-group">
						<label for="category_id">Категория</label>
						<select class="form-control select2" name="category_id" id="category_id" required>
							@foreach ($categories as $category)
							<option value="{{ $category->id }}"
							{{ $category->id == $tariff->category_id ? 'selected' : '' }}>{{ $category->name }}</option>
							@endforeach
						</select>
					</div>
					<div class="form-group">
						<label for="price_list_id">Прейскурант</label>
						<select class="form-control select2" name="price_list_id" id="price_list_id" required>
							@foreach ($price_lists as $price_list)
							<option value="{{ $price_list->id }}"
							{{ $price_list->id == $tariff->price_list_id ? 'selected' : '' }}>{{ $price_list->name }}</option>
							@endforeach
						</select>
					</div>
					<div class="form-group">
						<label for="price">Стоимость</label>
						<input name="price" id="price" type="number" class="form-control" placeholder="Стоимость" value="{{ $tariff->price }}" required autofocus>
					</div>
					<div class="form-group">
						<label for="duration">Длительность</label>
						<input name="duration" id="duration" type="number" class="form-control" placeholder="Длительность" value="{{ $tariff->duration }}" required>
					</div>
					<div class="form-group">
						<label for="dimension">Единица</label>
						<select class="form-control select2" name="dimension" id="dimension" required>
							<option value="h" {{ $tariff->dimension == 'h' ? 'selected' : '' }}>час</option>
							<option value="d" {{ $tariff->dimension == 'd' ? 'selected' : '' }}>день</option>
						</select>
					</div>
					<button type="submit" class="btn btn-success"><i class="fa fa-save"></i>Сохранить</button>
				</form>
			</div>
		</div>

	</div>
</div>

<script>
(function($) {
	'use strict';
	$(".select2").select2();
})(jQuery);
</script>

@endsection
