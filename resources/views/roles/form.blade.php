@extends('layouts.app')

@push('js')
<script type="text/javascript">
	$('.select2').select2();
</script>
@endpush

@section('content')

@card
	@slot('title')
		@empty($role->id)
		Создать роль
		@else
		Изменить роль
		@endempty
	@endslot

	@form(['entity' => $role])
		<div class="row">
			<div class="form-group col-4">
				<label>Название</label>
				<input name="name" type="text" class="form-control" value="{{ old('name', $role->name) }}">
			</div>

			<div class="form-group col-4">
				<label>Заголовок</label>
				<input name="display" type="text" class="form-control" value="{{ old('display', $role->display) }}">
			</div>

			<div class="form-group col-4">
				<label>Цвет</label>
				<select class="form-control" name="color">
					@foreach($colors as $color => $display)
					<option value="{{ $color }}" {{ $role->color === $color ? 'selected' : '' }}>{{ $display }}</option>
					@endforeach
				</select>
			</div>
		</div>

		<div class="form-group">
			<label>Права</label>
			<select class="form-control select2" name="permissions[]" multiple>
				@foreach($permissions as $permission)
				<option value="{{ $permission->name }}" {{ in_array($permission->name, $role->permissions->pluck('name')->toArray()) ? 'selected' : '' }}>{{ $permission->display }}</option>
				@endforeach
			</select>
		</div>

		<button class="btn btn-outline-primary">Сохранить</button>

		@isset($role->id)
			@deleteButton(['entity' => $role])
			@enddeleteButton
		@endisset
	@endform

@endcard

@endsection