@extends('guest.layout')

@push('js')
<script src="https://unpkg.com/imask"></script>

<script type="text/javascript">
    $(document).ready(function () {
        $('#datepicker-popup').datepicker({
          enableOnReadonly: true,
          todayHighlight: false,
          format: 'dd.mm.yyyy'
        });
    });

	var element = document.getElementById('phone');
	var maskOptions = {
	  mask: '+{7}(000)000-00-00'
	};
	var mask = new IMask(element, maskOptions);
</script>
@endpush

@section('content')

@include('common.validation')
@include('common.notifications')

<h2>Создать профиль</h2>
<form class="pt-5" method="post" action="{{ route('guest.store') }}">
    @csrf

    <div class="form-group">
        <label>Номер телефона / Phone number</label>
        <input name="phone" type="text" id="phone" class="form-control" value="{{ old('phone', Input::get('phone')) }}" required>
    </div>

    <div class="row">
	    <div class="form-group col-6">
	        <label>Фамилия / Last name</label>
	        <input name="last_name" type="text" class="form-control" value="{{ old('last_name') }}" required autofocus>
	    </div>
	    <div class="form-group col-6">
	        <label>Имя / First name</label>
	        <input name="first_name" type="text" class="form-control" value="{{ old('first_name') }}" required autofocus>
	    </div>
	</div>

	<div class="row">
        {{--
	    <div class="form-group col-6">
	        <label>Отчество</label>
	        <input name="mid_name" type="text" class="form-control" value="{{ old('mid_name') }}">
	    </div>
        
        
        <div class="form-group col-6">
            <label>Дата рождения</label>
            <div id="datepicker-popup" class="input-group date datepicker">
                <input type="text" class="form-control" required name="birthday" value="{{ old('birthday') }}">
                <span class="input-group-addon input-group-append border-left">
                    <span class="mdi mdi-calendar input-group-text"></span>
                </span>
            </div>
        </div>
        --}}

        <div class="form-group col-6">
	        <label>Возраст / Age</label>
	        <input name="age" type="number" class="form-control" min="5" max="99" value="{{ old('age') ?? 18 }}">
	    </div>

        <div class="form-group col-6">
	        <label>Навык / skill</label>
	        <select class="form-control" name="skill">
                <option value="beginner">Новичок / Beginner</option>
                <option value="mid">Средний / Middle</option>
                <option value="pro">Профессионал / Pro</option>
            </select>
	    </div>
    </div>
    <hr />
    <div class="row">
        <div class="form-group col-4">
            <label>Рост / Height</label>
            <input name="profile[height]" type="number" min="100" max="250" class="form-control" value="{{ old('profile.height') }}" required>
        </div>
        <div class="form-group col-4">
            <label>Вес / Weight</label>
            <input name="profile[weight]" type="number" min="20" max="200" class="form-control" value="{{ old('profile.weight') }}" required>
        </div>
        <div class="form-group col-4">
            <label>Размер обуви / Shoe size</label>
            <input name="profile[shoe_size]" type="number" min="16" max="50" class="form-control" value="{{ old('profile.shoe_size') }}" required>
        </div>
    </div>
    <div class="mt-5">
        <button type="submit" class="btn btn-block btn-outline-success btn-lg font-weight-medium">Создать</button>
    </div>
</form>

@endsection