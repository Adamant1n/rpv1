@extends('guest.layout')

@push('js')
<script src="https://unpkg.com/imask"></script>
<script>
	var element = document.getElementById('phone');
	var maskOptions = {
	  mask: '+{7}(000)000-00-00'
	};
	var mask = new IMask(element, maskOptions);

	function print () {
		printImg('{{ route('clients.barcode', $client) }}');
		window.location.href = '/guest';
	}
</script>
@endpush

@section('content')

@include('common.validation')
@include('common.notifications')

<h2>Карточка клиента</h2>
@csrf
<div class="form-group">
    <label>Фамилия / Last name</label>
    <input type="text" class="form-control" value="{{ $client->last_name }}" disabled>
</div>
<div class="form-group">
    <label>Имя / First name</label>
    <input type="text" class="form-control" value="{{ $client->first_name }}" disabled>
</div>
{{--
<div class="form-group">
    <label>Отчество</label>
    <input type="text" class="form-control" value="{{ $client->mid_name }}" disabled>
</div>
--}}
<div class="row">
    <div class="form-group col-6">
        <label>Номер телефона / Phone number</label>
        <input type="text" class="form-control" id="phone" value="{{ $client->phone }}" disabled>
    </div>
    <div class="form-group col-6">
        <label>Возраст / Age</label>
        <input type="number" class="form-control" id="age" value="{{ $client->age }}" disabled>
    </div>

    <div class="form-group col-6">
        <label>Навык / skill</label>
        <select class="form-control" disabled>
            <option value="beginner" {{ $client->skill=='beginner' ? 'selected' : '' }}>Новичок / Beginner</option>
            <option value="mid" {{ $client->skill=='mid' ? 'selected' : '' }}>Средний / Middle</option>
            <option value="pro" {{ $client->skill=='pro' ? 'selected' : '' }}>Профессионал / Pro</option>
        </select>
    </div>
    {{--
    <div class="form-group col-6">
        <label>Дата рождения</label>
        <input type="text" class="form-control" value="{{ \Carbon\Carbon::parse($client->birthday)->format('d.m.Y') }}" disabled>
    </div>
    --}}
</div>
<hr />
<div class="row">
    <div class="form-group col-4">
        <label>Рост / Height</label>
        <input type="number" min="100" max="250" class="form-control" value="{{ $client->feature->height }}" disabled>
    </div>
    <div class="form-group col-4">
        <label>Вес / Weight</label>
        <input type="number" min="20" max="200" class="form-control" value="{{ $client->feature->weight }}" disabled>
    </div>
    <div class="form-group col-4">
        <label>Размер обуви / Shoe size</label>
        <input type="number" min="16" max="50" class="form-control" value="{{ $client->feature->shoe_size }}" disabled>
    </div>
</div>
<div class="mt-5">
    <a class="btn btn-block btn-outline-success btn-lg font-weight-medium" href="#" onclick="print()">Печать</a>
    <a class="btn btn-block btn-outline-primary btn-lg font-weight-medium" href="{{ route('guest.index') }}">Назад</a>
</div>

@endsection