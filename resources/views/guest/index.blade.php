@extends('guest.layout')

@push('js')
<script src="https://unpkg.com/imask"></script>
<script>
	var element = document.getElementById('phone');
	var maskOptions = {
	  mask: '+{7}(000)000-00-00'
	};
	var mask = new IMask(element, maskOptions);
</script>
@endpush

@section('content')

@include('common.validation')
@include('common.notifications')

<h2></h2>
<form action="{{ route('guest.find') }}" method="post">
	@csrf
	<div class="form-group">
	    <label>Введите номер телефона</label>
	    <input type="text" class="form-control" id="phone" value="+7" name="phone">
	</div>
	<div class="mt-5">
	    <button class="btn btn-block btn-outline-success btn-lg font-weight-medium">Далее</button>
	</div>
</form>

@endsection