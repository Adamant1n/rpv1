@extends('layouts.app')

@push('js')
<script>
    $('#updateDashboard').click(function () {
        $.get('/dashboard', {
            ajax: 1
        }, function (response) {
            $('#dashboardMain').html(response);
        })
    });
</script>
@endpush

@section('content')

<button class="btn btn-primary mb-3" id="updateDashboard">Обновить</button>

<div class="row" id="dashboardMain">
    @include('dashboard.main')
</div>

@endsection