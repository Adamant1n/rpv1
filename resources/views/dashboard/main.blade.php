<div class="col-md-3 grid-margin">
    <div class="card">
        <div class="card-body">
            <a href="/orders?type=rent" class="nav-link px-0 py-0">
                <h4 class="card-title mb-0">Активные договоры</h4>
                <div class="d-flex justify-content-between align-items-center">
                    <div class="d-inline-block pt-3">
                        <div class="d-flex">
                            <h2 class="mb-0">{{ $rentOrdersCount }}</h2>
                        </div>
                        <small class="text-gray">К возврату: {{ $reservedInventoryCount }}</small><br>
                        <small class="text-gray">&nbsp;</small>
                    </div>
                    <div class="d-inline-block">
                        <div class="bg-success px-4 py-2 rounded">
                            <i class="mdi mdi-wallet text-white icon-lg"></i>
                        </div>
                    </div>
                </div>
            </a>
        </div>
    </div>
</div>

<div class="col-md-3 grid-margin">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title mb-0">К возврату сегодня</h4>
            <div class="d-flex justify-content-between align-items-center">
                <div class="d-inline-block pt-3">
                    <div class="d-flex">
                        <h2 class="mb-0">{{ $reservedInventoryUntilToday }}</h2>
                    </div>
                    <small class="text-gray">&nbsp;</small><br/>
                    <small class="text-gray">&nbsp;</small>
                </div>
                <div class="d-inline-block">
                    <div class="bg-warning px-4 py-2 rounded">
                        <i class="mdi mdi-wallet text-white icon-lg"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="col-md-3 grid-margin">
    <div class="card">
        <div class="card-body">
            <a href="/orders?type=book" class="nav-link px-0 py-0">
                <h4 class="card-title mb-0">Бронирования</h4>
                <div class="d-flex justify-content-between align-items-center">
                    <div class="d-inline-block pt-3">
                        <div class="d-flex">
                            <h2 class="mb-0">{{ $bookedCount }}</h2>
                        </div>
                        <small class="text-gray">Укомплектовано: {{ $bookedStaffedCount }}</small><br />
                        <small class="text-gray">Не укомплектовано: {{ $bookedNotStaffedCount }}</small>
                    </div>
                    <div class="d-inline-block">
                        <div class="bg-info px-4 py-2 rounded">
                            <i class="mdi mdi-wallet text-white icon-lg"></i>
                        </div>
                    </div>
                </div>
            </a>
        </div>
    </div>
</div>

<div class="col-md-3 grid-margin">
    <div class="card">
        <div class="card-body">
            <a href="/orders?type=delivery" class="nav-link px-0 py-0">
                <h4 class="card-title mb-0">Доставки</h4>
                <div class="d-flex justify-content-between align-items-center">
                    <div class="d-inline-block pt-3">
                        <div class="d-flex">
                            <h2 class="mb-0">{{ $deliverTodayCount }}</h2>
                        </div>
                        <small class="text-gray">Укомплектовано: {{ $deliverTodayStaffedCount }}</small><br />
                        <small class="text-gray">Не укомплектовано: {{ $deliverTodayNotStaffedCount }}</small>
                    </div>
                    <div class="d-inline-block">
                        <div class="bg-primary px-4 py-2 rounded">
                            <i class="mdi mdi-wallet text-white icon-lg"></i>
                        </div>
                    </div>
                </div>
            </a>
        </div>
    </div>
</div>

<div class="col-md-3 grid-margin">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title mb-0">Текущий тариф</h4>
            <div class="d-flex justify-content-between align-items-center">
                <div class="d-inline-block pt-3">
                    <div class="d-flex">
                        <h2 class="mb-0">{{ $priceList->name }}</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="col-md-3 grid-margin">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title mb-0">Текущая задолженность</h4>
            <div class="d-flex justify-content-between align-items-center">
                <div class="d-inline-block pt-3">
                    <div class="d-flex">
                        <h2 class="mb-0">{{ $debt }} <i class="fas fa-ruble-sign"></i></h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>