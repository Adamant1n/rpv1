<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- plugins:css -->
    <link rel="stylesheet" href="vendors/iconfonts/mdi/css/materialdesignicons.min.css">
    <link rel="stylesheet" href="vendors/iconfonts/puse-icons-feather/feather.css">
    <link rel="stylesheet" href="vendors/css/vendor.bundle.base.css">
    <link rel="stylesheet" href="vendors/css/vendor.bundle.addons.css">
    <!-- endinject -->
    <!-- plugin css for this page -->
    <!-- End plugin css for this page -->
    <!-- inject:css -->
    <link rel="stylesheet" href="css/style.css">
    <!-- endinject -->
    <link rel="shortcut icon" href="images/favicon.png" />
</head>

<body>
    <div class="container-scroller">
        <div class="container-fluid page-body-wrapper full-page-wrapper">
            <div class="content-wrapper d-flex align-items-center auth login-full-bg">
                <div class="row w-100">
                    <div class="col-lg-4 mx-auto">
                        <div class="auth-form-dark text-left p-5">
                            <h2>Вход</h2>
                            <form class="pt-5" method="post" action="{{ route('login') }}" aria-label="{{ __('Login') }}">
                                @if (request()->session == 1)
                                <div class="alert alert-danger">Невозможно авторизоваться. Данынй аккаунт уже авторизован на другом компьютере.</div>
                                @endif

                                @csrf
                                <div class="form-group">
                                    <label for="exampleInputEmail1">E-mail</label>
                                    <input name="email" type="email" class="form-control" id="exampleInputEmail1" value="{{ old('email') }}" required autofocus>
                                    <i class="mdi mdi-account"></i>
                                    @if ($errors->has('email'))
                                        <span class="text-warning">{{ $errors->first('email') }}</span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Пароль</label>
                                    <input name="password" type="password" class="form-control" id="exampleInputPassword1" required>
                                    <i class="mdi mdi-eye"></i>
                                    @if ($errors->has('password'))
                                        <span class="text-warning">{{ $errors->first('password') }}</span>
                                    @endif
                                </div>
                                <div class="mt-3">
                                    <div class="form-check">
                                        <label class="form-check-label" for="remember">
                                            <input type="checkbox" class="form-check-input" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                                            Запомнить меня
                                        </label>
                                    </div>
                                </div>
                                <div class="mt-5">
                                    <button type="submit" class="btn btn-block btn-warning btn-lg font-weight-medium">Войти</button>
                                </div>
                                <div class="mt-3 text-center">
                                    <a href="{{ route('password.request') }}" class="auth-link text-white">Забыли пароль?</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- content-wrapper ends -->
        </div>
        <!-- page-body-wrapper ends -->
    </div>
    <!-- container-scroller -->
    <!-- plugins:js -->
    <script src="vendors/js/vendor.bundle.base.js"></script>
    <script src="vendors/js/vendor.bundle.addons.js"></script>
    <!-- endinject -->
    <!-- inject:js -->
    <script src="js/off-canvas.js"></script>
    <script src="js/hoverable-collapse.js"></script>
    <script src="js/misc.js"></script>
    <script src="js/settings.js"></script>
    <script src="js/todolist.js"></script>
    <!-- endinject -->
    <script>
    	// штрихсканнер
	    var typingBarcode = '';
	    var barcode = '';
	    var prevKey;

	    $.ajaxSetup({
		    headers: {
		        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		    }
		});

		$(document).on('keyup', function (e) {
	        clearInterval(timer);

	        var code = e.originalEvent.code;

	        // letter or digit
	        if(code.indexOf('Key') != -1) {
	            typingBarcode += code.substring(3);
	        }

	        else if(code.indexOf('Digit') != -1) {
	            typingBarcode += code.substring(5);
	        }

	        // $('#client_popup_scan').html(typingBarcode);

	        if(code == 'ArrowDown' && prevKey == 'Enter') {
	        	finishBarcodeEnter();
	            
	            return true;
	        }

	        prevKey = code;
	        timer = setInterval(ticker, 500);
	    });

	    var timer = setInterval(ticker, 500);

	    var ticker = function () {
	        resetValues();
	        clearInterval(timer);
	    }

	    function resetValues() {
	        typingBarcode = '';
	        prevKey = '';
	    }

	    function finishBarcodeEnter() {
	        barcode = typingBarcode;
	        login(barcode);

	        barcode = '';
	        resetValues();
	    }

	    function login(barcode)
	    {
	    	$.ajax({
                url: "{{ route('login.barcode') }}",
                type: "post", 
                data: { 
                    code: barcode
                },
            })
            .done(function(data) {
            	if(data.success == true)
            	{
            		$.toast({
			            heading: 'Успешная авторизация',
			            text: '',
			            position: 'top-right',
			            loaderBg:'#ff6849',
			            icon: 'success',
			            bgColor : 'success',
			            hideAfter: 2000, 
			            stack: 6
			        });

			        setTimeout(function() {
					  window.location.href = "/";
					}, 1000);
            	}
            	else
            	{
            		$.toast({
			            heading: 'Доступ запрещен',
			            text: '',
			            position: 'top-right',
			            loaderBg:'#ff6849',
			            bgColor : 'danger',    
			            hideAfter: 2000, 
			            stack: 6
			        });
            	}
            })
            .fail(function(data) {
            	$.toast({
		            heading: 'Доступ запрещен',
		            text: '',
		            position: 'top-right',
		            loaderBg:'#ff6849',
		            bgColor : 'danger',    
		            hideAfter: 2000, 
		            stack: 6
		        });
            });
	    }
    </script>
</body>

</html>
