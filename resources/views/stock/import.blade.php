@extends('layouts.app')

@section('content')

<div class="row">
	<div class="col-12">
		<div class="card">
			<div class="card-header">
				<a href="{{ route('stock.index') }}" class="btn btn-light">
					<i class="fa fa-angle-left"></i>Назад</a>
			</div>
			<div class="card-body">
				<h4 class="card-title">Импорт</h4>

				@if (empty($excel))

					<form method="post" action="{{ route('stock.import') }}" enctype="multipart/form-data">
						@csrf
						<div class="form-group">
							<input type="file" name="excel_file" accept="application/msexcel,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" required>
						</div>
						<button type="submit" class="btn btn-primary">
							<i class="fa fa-cloud-download"></i>Загрузить</button>
					</form>

				@else

					<form method="post" action="{{ route('stock.import') }}">
					@csrf

					@role('superadmin')
					<div class="row">
						<div class="col-sm-8 col-md-6 col-lg-4">
							<div class="form-group">

							<label for="location_id">Филиал</label>
							<select class="form-control select2" name="location_id" id="location_id" required>
								<option value="">Сделайте выбор...</option>
								@foreach ($locations as $location)
								<option value="{{ $location->id }}">{{ $location->address }}</option>
								@endforeach
							</select>

							</div>
						</div>
					</div>
					@endrole

					<div class="mb-4" style="overflow-x: scroll;">
						<table class="table">
						<thead>
							<tr>

							@for ($i = 0; $i < count($excel[0]); $i++)
							<th>
							<select name="col[]">
								<option value=""></option>
								@foreach ($cols as $key => $col)
									<option value="{{ $col['id'] }}"
									{{ trim($excel[0][$i]) == $col['name'] ? 'selected' : '' }}>
										{{ $col['name'] }}
									</option>
								@endforeach
							</select>
							</th>
							@endfor

							</tr>
						</thead>
						<tbody>

						@php $i = 0 @endphp

						@foreach ($excel as $row)
						@php $i++;
						if ($i == 1) continue;
						if ($i == 11) break;
						@endphp
						<tr>
							@foreach ($row as $cell)
								<td>{{ $cell }}</td>
							@endforeach
						</tr>
						@endforeach

						</tbody>
						</table>
					</div>

					<input type="hidden" name="excel" value="{{ json_encode($excel) }}">

					<button type="submit" class="btn btn-primary">
						<i class="fa fa-save"></i>Начать импорт
					</button>

					</form>

				@endif

			</div>
		</div>

	</div>
</div>

<script>
(function($) {
	'use strict';
	$(".select2").select2();
})(jQuery);
</script>

@endsection
