@extends('layouts.app')

@section('content')

@include('common.validation')
@include('common.notifications')

@include('common.tabs', ['page' => 'stock'])

<div class="card">
	<div class="card-header">
		<a href="{{ route('stock.create') }}" class="btn btn-success pull-left mr-2">
			<i class="fa fa-plus"></i>Добавить единицу товара</a>
		<a href="{{ route('stock.import') }}" class="btn btn-primary pull-left mr-3">
			<i class="fa fa-cloud-download"></i>Импорт</a>
	</div>
	<div class="card-body">
		<h4 class="card-title">Склад</h4>

		<form>
		<div class="row">

			@role('superadmin')
			<div class="col-md-3 mb-4">
				<select class="form-control select2" name="location"
					onchange="this.form.submit()">
					<option value="">Филиал</option>
					@foreach ($locations as $location)
					<option value="{{ $location->id }}"
					{{ $location->id == $request->location ? 'selected' : '' }}>
					{{ $location->address }}</option>
					@endforeach
				</select>
			</div>
			@endrole
			<div class="col-md-3 mb-4">
				<select class="form-control select2" name="brand"
					onchange="this.form.submit()">
					<option value="">Бренд</option>
					@foreach ($brands as $brand)
						<option value="{{ $brand->id }}"
						{{ $brand->id == $request->brand ? 'selected' : '' }}>
						{{ $brand->name }}</option>
					@endforeach
				</select>
			</div>
			<div class="col-md-3 mb-4">
				<select class="form-control select2" name="category"
					onchange="this.form.submit()">
					<option value="">Категория</option>
					@foreach ($categories as $category)
						<option value="{{ $category->id }}"
						{{ $category->id == $request->category ? 'selected' : '' }}>
						{{ $category->name }}</option>
					@endforeach
				</select>
			</div>
			<div class="col-md-3 mb-4">
				<select class="form-control select2" name="condition"
					onchange="this.form.submit()">
					<option value="">Статус</option>
					@foreach ($conditions as $condition)
						<option value="{{ $condition->id }}"
						{{ $condition->id == $request->condition ? 'selected' : '' }}>
						{{ $condition->name }}</option>
					@endforeach
				</select>
			</div>
		</div>
		</form>

		<div class="row">
			<div class="col-12">
			<table id="stock-listing" class="table">
				<thead>
					<tr>
						<th name="code">Артикул</th>
						<th name="category_id">Категория</th>
						<th name="brand_id">Бренд</th>
						<th>Наименование</th>
						<th>Состояние</th>
						<th>Крепление</th>
						<th>Длина</th>
						<th>Размер</th>
						<th>Колодка</th>
						<th>Кол-во</th>
						<th>Статус</th>
					</tr>
				</thead>
				<tbody>
				</tbody>
			</table>
			</div>
		</div>
	</div>
</div>

<script>
(function($) {
	'use strict';
	$(function() {
		var table = $('#stock-listing').DataTable({
			"columnDefs": [
				{
					"targets": 'no-sort',
					"orderable": false,
				}
			],
			"processing": true,
			"serverSide": true,
			"ajax": "{{ route('stock.json', $request->all()) }}",
			"dom": 'Bfrtip',
			"lengthMenu": [
				[10, 25, 50, -1],
				['10 записей','25 записей','50 записей','Показать все']
			],
			"buttons": [
				'pageLength','excel','print'
			],
			"iDisplayLength": 25,
			"aaSorting": [[0, "asc"]],
			"language": {
				"lengthMenu": "Показывать _MENU_ записей на странице",
				"zeroRecords": "Записей не найдено",
				"loadingRecords": "Загрузка... может занять несколько секунд...",
				"info": "Страница _PAGE_ из _PAGES_",
				"infoEmpty": "Показано с 0 по 0 из 0 записей",
				"search": "",
				"infoFiltered": "(Найдено записей: _TOTAL_)",
				"sInfo": "Показано с _START_ по _END_ из _TOTAL_ записей",
				"paginate":
				{
					"first":    "Первая",
					"last":     "Последняя",
					"next":     "Следующая",
					"previous": "Предыдущая"
				},
				"buttons":
				{
					"pageLength": {
						   _: "%d записей",
						'-1': "Показать все"
					},
					"print": "Печать",
					"excel": "Экспорт"
				}
			},
			"createdRow": function( row, data, dataIndex ) {
				console.log(data);

            	if ( data[9] != "" ) {        
         			$(row).addClass('table-primary');
       			}
       		},
			"pageLength": {{ Request::get('length') ? Request::get('length') : 10 }},
			initComplete: function () {
      			setTimeout(function () {
        				table.page({{ Request::get('page') ? Request::get('page') - 1 : 0 }})
							.draw(false);
      			}, 10);
    		},
			"drawCallback": function( settings ) {
				var page = Math.ceil(settings._iDisplayStart / settings._iDisplayLength) + 1;

				updateUrlParameter('page', page);
				updateUrlParameter('length', settings._iDisplayLength);
			}
		});

		$('#stock-listing').each(function() {
			var datatable = $(this);
			var search_input = datatable.closest('.dataTables_wrapper').find('div[id$=_filter] input');
			search_input.attr('placeholder', 'Поиск по артикулу');
			search_input.removeClass('form-control-sm');
			var length_sel = datatable.closest('.dataTables_wrapper').find('div[id$=_length] select');
			length_sel.removeClass('form-control-sm');
		});

		$('#stock-listing tbody').on('click', 'tr', function () {
			var id = $('td', this).eq(0).text();
			$(location).attr('href','/stock/'+id+'/edit');
		});
	});
	$(".select2").select2();
})(jQuery);
</script>

@endsection
