@extends('layouts.app')

@push('js')
<script type="text/javascript">
	$(document).ready(function () {
	    // обработка категорий товаров
	    var products = {!! json_encode($products) !!};
	    var product_id = {!! $unit->product_id !!};

	    // обработка категорий товаров
	    var bindings_group = 'feature_{{ \App\Category::PARAM_BINDING }}_group';

	    $('#' + bindings_group).hide();	// скрыть поле крепления
	    var categories = {!! json_encode($categories) !!};
	    var category_id;

	    // обработка полей, соответствующих категории
	    var params;	// поля категории

	    $('#category_id').on('change', function () {
	    	category_id = $(this).val();

	    	handleCategoryChange();
	    });

	    function handleCategoryChange()
	    {
	    	var current_category;

	    	// определение параметров категории
	    	categories.forEach(function (category, index) {
	    		if(category.id == category_id) {
	    			params = category.params;
	    			current_category = category;
	    		}
	    	});

	    	// отображение и скрытие нужных полей
	    	for(var i in params)
	    	{
	    		if(params[i] == 0) {
	    			$('#feature_' + i + '_group').hide();
	    			$('#feature_' + i).removeAttr('required');
	    		}
	    		else {
	    			$('#feature_' + i + '_group').show();
	    			$('#feature_' + i).attr('required', 'required');
	    		}
	    	}

	    	// добавление и удаление моделей товаров, соответствующих категории
		    $('#product_id').html('');
		    
		    current_category.products.forEach(function (product) {
		    	if(product.id == product_id) {
		    		$('#product_id')
		    			.append($("<option></option>")
		    			.attr("value", product.id)
		    			.attr('selected', '1')
		    			.text(product.name));
		    	}
		    	else {
		    		$('#product_id')
		    			.append($("<option></option>")
		    			.attr("value", product.id)
		    			.text(product.name));
		    	}
		    });
	    }

	    category_id = {!! $unit->product->category_id !!}; //categories[0].id;
	    $('#category_id').val(category_id).trigger('change');
	});
</script>
@endpush

@section('content')

@include('common.validation')
@include('common.notifications')

@include('common.tabs', ['page' => 'stock'])
<div class="row">
	<div class="col-8 grid-margin stretch-card">
		<div class="card">
			<div class="card-header">

				<a href="{{ route('stock.index') }}" class="btn btn-light">
					<i class="fa fa-angle-left"></i>Назад</a>

				@if(!$clone)
				<form method="post" action="{{ route('stock.destroy', $unit->id) }}" style="display: inline;">
					@csrf
					{{ method_field('DELETE') }}
					<button type="submit" class="btn btn-outline-danger pull-right">
						<i class="fa fa-trash-o"></i>Удалить</button>
				</form>
				@endif

			</div>
			<div class="card-body">
				<h4 class="card-title">Единица товара</h4>

				<!-- клонирование -->
				@if($clone)
				<form method="post" action="{{ route('stock.store') }}">
				@else
				<!-- редактирование -->
				<form method="post" action="{{ route('stock.update', $unit->id) }}">
					@method('PATCH')
				@endif
				
					@csrf

					<div class="row">
						<div class="form-group col-4">
							<label for="code">Артикул</label>
							<input name="code" id="code" type="text" class="form-control" placeholder="Артикул" value="{{ $unit->code }}" required autofocus>
						</div>

						@if($unit->product->category->group)
						<div class="form-group col-4" id="count">
							<label>Количество позиций</label>
							<input class="form-control" type="number" name="count" value="{{ $unit->count }}" min="0" max="100000">
						</div>
						@endif

						<div class="form-group col-2">
							@if($unit->is_unique && !$clone)
								@if($unit->is_reserved)
								<a id="unit_order" data-order="{{ $unit->current_order->id }}" href="#" class="btn mt-4 btn-sm pull-right btn-outline-primary">{{ $unit->current_order->optional_id }}</a>
								@elseif($unit->count == 0)
								<a href="#" class="btn mt-4 btn-sm pull-right btn-danger">Продано</a>
								@else
								<a href="#" class="btn mt-4 btn-sm pull-right btn-success">В наличии</a>
								@endif
							@endif
						</div>
						@if(!$clone)
						<div class="form-group col-2">
							<a href="{{ route('stock.create') . '?clone=' . $unit->id }}" class="btn btn-outline-success pull-right btn-sm mt-4">Копировать</a>
						</div>
						@endif
					</div>

					<div class="row">
						<div class="form-group col-6">
							<label for="location_id">Текущий склад</label>
							<select class="form-control select2" name="location_id" id="location_id" required>
								@foreach ($locations as $location)
								<option value="{{ $location->id }}"
								{{ $location->id == $unit->location_id ? 'selected' : '' }}>{{ $location->address }}</option>
								@endforeach
							</select>
						</div>

						<div class="form-group col-6">
							<label for="main_location">Основной склад</label>
							<select class="form-control select2" name="main_location" id="main_location" required>
								@foreach ($locations as $location)
								<option value="{{ $location->id }}"
								{{ $location->id == $unit->main_location ? 'selected' : '' }}>{{ $location->address }}</option>
								@endforeach
							</select>
						</div>
					</div>

					<div class="row">
						<div class="form-group col-4">
							<label for="condition_id">Состояние</label>
							<select class="form-control select2" name="condition_id" id="condition_id" required>
								@foreach ($conditions as $condition)
								<option value="{{ $condition->id }}"
								{{ $condition->id == $unit->condition_id ? 'selected' : '' }}>{{ $condition->name }}</option>
								@endforeach
							</select>
						</div>

						<div class="form-group col-4">
							<label for="product_id">Категория</label>
							<select class="form-control select2" name="category_id" id="category_id" required>
								<option value="">Сделайте выбор...</option>
								@foreach ($categories as $category)
								<option value="{{ $category->id }}" {{ $unit->product->category_id === $category->id ? 'selected' : '' }}>{{ $category->name }}</option>
								@endforeach
							</select>
						</div>

						<div class="form-group col-4">
							<label for="product_id">Модель</label>
							<select class="form-control select2" name="product_id" id="product_id" required>
								@foreach ($unit->product->category->products as $product)
								<option value="{{ $product->id }}"
								{{ $product->id == $unit->product_id ? 'selected' : '' }}>{{ $product->name }}</option>
								@endforeach
							</select>
						</div>
					</div>

					<div class="row">
						<!-- крепления -->
						<div class="form-group col-3" id="feature_{{ \App\Category::PARAM_BINDING }}_group">
							<label for="product_id">Крепление</label>
							<select class="form-control select2" name="binding_id" id="binding_id">
								<option value="">Сделайте выбор...</option>
								@foreach ($bindings as $binding)
								<option value="{{ $binding->id }}" {{ $unit->binding_id === $binding->id ? 'selected' : '' }}>{{ $binding->name }}</option>
								@endforeach
							</select>
						</div>

						@foreach($features as $feature_id => $feature_name)

						<!-- не отображать крепление -->
						@if($feature_id === \App\Category::PARAM_BINDING)
						@continue
						@endif

						<div class="form-group col-3" id="feature_{{ $feature_id }}_group">
							<label>{{ $feature_name }}</label>
							@if($feature_id === \App\Category::PARAM_SIZE_SML)
							<select class="form-control" name="feature_{{ $feature_id }}" id="feature_{{ $feature_id }}" required>
								<option value="">Сделайте выбор...</option>
								@foreach(\App\Unit::getSMLSizes() as $size)
								<option value="{{ $size }}" {{ $unit->{'feature_' . $feature_id} === $size ? 'selected' : '' }}>{{ $size }}</option>
								@endforeach
							</select>
							@else
							<input name="feature_{{ $feature_id }}" id="feature_{{ $feature_id }}" type="text" class="form-control" placeholder="{{ 
							$feature_name }}" value="{{ $unit->{'feature_' . $feature_id} }}" required>
							@endif
						</div>
						@endforeach
					</div>

					<button type="submit" class="btn btn-success">
						<i class="fa fa-save"></i>Сохранить</button>
				</form>

				@if(!$clone)
				<hr />

				<div class="row">
					@if($unit->product->photo)
					<div class="form-group col-2 text-center">
						<img src="{{ UploadImage::load('product', 150).$unit->product->photo }}" class="img img-responsive">
					</div>
					<hr>
					@endif
					<div class="form-group col-3">
						<label>Бренд</label>
						<input type="text" class="form-control" disabled value="{{ $unit->product->brand->name }}">
					</div>
					<div class="form-group col-3">
						<label>Дата создания</label>
						<input type="text" class="form-control" disabled value="{{ $unit->created_at->format('d.m.Y') }}">
					</div>
					<div class="form-group col-3">
						<label>Количество прокатов</label>
						<input type="text" class="form-control" disabled value="{{ $unit->rents }}">
					</div>
					<div class="form-group col-3">
						<label>Возраст (мес.)</label>
						<input type="text" class="form-control" disabled value="{{ $unit->age }} мес.">
					</div>
					<div class="form-group col-3">
						<label>Сумма выручки</label>
						<input type="text" class="form-control" disabled value="{{ $unit->revenue }}">
					</div>

					@if($unit->product->for_sale)
					<div class="form-group col-3">
						<label>Цена закупки</label>
						<input type="text" class="form-control" disabled value="{{ $unit->product->price_buy }}">
					</div>
					<div class="form-group col-3">
						<label>Стоимость продажи</label>
						<input type="text" class="form-control" disabled value="{{ $unit->product->price_sell }}">
					</div>
					@endif

					<div class="form-group col-12">
						<input type="text" class="form-control" disabled value="{{ $unit->long_string }}">
					</div>
				</div>
				@endif
			</div>
		</div>

	</div>
</div>

<script>
(function($) {
	'use strict';
	$(".select2").select2();
})(jQuery);
</script>

@endsection
