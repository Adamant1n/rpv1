@extends('layouts.app')

@push('js')
<script type="text/javascript">
	$(document).ready(function () {
		// массовое добавление товаров
		$('#multiple').on('change', function () {
	        if($(this).is(':checked')) {
	        	$('#count').hide();
	            $('#code').hide();
	            $('#code').val('');
	            $('#codes').show();
	        }
	        else {
	        	$('#count').show();
	            $('#code').show();
	            $('#codes').val('');
	            $('#codes').hide();
	        }
	    });

	    // обработка категорий товаров
	    var bindings_group = 'feature_{{ \App\Category::PARAM_BINDING }}_group';

	    $('#' + bindings_group).hide();	// скрыть поле крепления
	    var categories = {!! json_encode($categories) !!};
	    var category_id;

	    // обработка полей, соответствующих категории
	    var params;	// поля категории

	    $('#category_id').on('change', function () {
	    	category_id = $(this).val();

	    	if($('#category_id option:selected').data('group') == 0)
	    	{
	    		$('#count').hide();
	    		$('#countValue').val(1);
	    	}
	    	else
	    	{
	    		$('#count').show();
	    	}

	    	handleCategoryChange();
	    });

	    function handleCategoryChange()
	    {
	    	var current_category;

	    	// определение параметров категории
	    	categories.forEach(function (category, index) {
	    		if(category.id == category_id) {
	    			params = category.params;
	    			current_category = category;
	    		}
	    	});

	    	// отображение и скрытие нужных полей
	    	for(var i in params)
	    	{
	    		if(params[i] == 0) {
	    			$('#feature_' + i + '_group').hide();
	    			$('#feature_' + i).removeAttr('required');
	    			console.log('#feature_' + i + ' unreq');
	    		}
	    		else {
	    			$('#feature_' + i + '_group').show();
	    			$('#feature_' + i).attr('required', 'required');
	    		}
	    	}

	    	// добавление и удаление моделей товаров, соответствующих категории
		    $('#product_id').html('');
		    
		    current_category.products.forEach(function (product, product_id) {
		    	$('#product_id').append($("<option></option>").attr("value", product.id).text(product.name));
		    });
	    }

	    category_id = categories[0].id;
	    $('#category_id').val(category_id).trigger('change');

	    // проверка артикула на уникальность перед добавлением
	    $('#formSubmit').click(function() {
	    	$.ajax({
                url: "{{ route('stock.is_unique') }}",
                type: "get", 
                data: { 
                    code: $('#code').val(),
                    location_id: $('#location_id').val()
                },
            })
            .done(function(data) {
                if(data.result == true) {
                	$('#addUnitForm').submit();
                }
                else {
                	alert('Инвентарь с таким артикулом уже существует в базе выбранного филиала');
                }
            });
		})
	});
</script>
@endpush

@section('content')

@include('common.validation')
@include('common.notifications')

@include('common.tabs', ['page' => 'stock'])
<div class="row">
	<div class="col-8 grid-margin stretch-card">
		<div class="card">
			<div class="card-header">
				<a href="{{ route('stock.index') }}" class="btn btn-light"><i class="fa fa-angle-left"></i>Назад</a>
			</div>
			<div class="card-body">
				<h4 class="card-title">Новая единица товара</h4>
				<form method="post" id="addUnitForm" action="{{ route('stock.store') }}">
					@csrf

					<div class="row">
						<div class="form-group col-9">
							<label for="code">Артикул</label>
							<label class="float-right">
	                            <input type="checkbox" id="multiple" /> Несколько
	                        </label>
							<input name="code" id="code" type="text" class="form-control" placeholder="Артикул" autofocus>
							<textarea name="codes" id="codes" class="form-control" style="display: none;"></textarea>
						</div>

						<div class="form-group col-3" id="count">
							<label>Количество позиций</label>
							<input class="form-control" type="number" id="countValue" name="count" value="1" min="1" max="100000">
						</div>
					</div>

					<div class="row">
						<div class="form-group col-4">
							<label for="condition_id">Состояние</label>
							<select class="form-control select2" name="condition_id" id="condition_id" required>
								<option value="">Сделайте выбор...</option>
								@foreach ($conditions as $condition)
								<option value="{{ $condition->id }}" {{ $condition->id === 1 ? 'selected' : '' }}>{{ $condition->name }}</option>
								@endforeach
							</select>
						</div>

						<div class="form-group col-4">
							<label for="product_id">Категория</label>
							<select class="form-control select2" name="category_id" id="category_id" required>
								<option value="">Сделайте выбор...</option>
								@foreach ($categories as $category)
								<option value="{{ $category->id }}" data-group="{{ $category->group ? 1 : 0 }}">{{ $category->name }}</option>
								@endforeach
							</select>
						</div>

						<div class="form-group col-4">
							<label for="product_id">Модель</label>
							<select class="form-control select2" name="product_id" id="product_id" required>
								<option value="">Сделайте выбор...</option>
								@foreach ($products as $product)
								<option value="{{ $product->id }}">{{ $product->name }}</option>
								@endforeach
							</select>
						</div>
					</div>

					<div class="row">
						<div class="form-group col-3" id="feature_{{ \App\Category::PARAM_BINDING }}_group">
							<label for="product_id">Крепление</label>
							<select class="form-control select2" name="binding_id" id="binding_id">
								<option value="">Сделайте выбор...</option>
								@foreach ($bindings as $binding)
								<option value="{{ $binding->id }}">{{ $binding->name }}</option>
								@endforeach
							</select>
						</div>

						@foreach($features as $feature_id => $feature_name)
						<!-- не отображать крепление -->
						@if($feature_id === \App\Category::PARAM_BINDING)
						@continue
						@endif

						<div class="form-group col-3" id="feature_{{ $feature_id }}_group">
							<label>{{ $feature_name }}</label>
							@if($feature_id === \App\Category::PARAM_SIZE_SML)
							<select class="form-control" name="feature_{{ $feature_id }}" id="feature_{{ $feature_id }}" required>
								<option value="">Сделайте выбор...</option>
								@foreach(\App\Unit::getSMLSizes() as $size)
								<option value="{{ $size }}">{{ $size }}</option>
								@endforeach
							</select>
							@else
							<input name="feature_{{ $feature_id }}" id="feature_{{ $feature_id }}" type="text" class="form-control" placeholder="{{ 
							$feature_name }}" required>
							@endif
						</div>
						@endforeach
					</div>

					<div class="row">
						<div class="form-group col-6">
							<label for="location_id">Текущий склад</label>
							<select class="form-control select2" name="location_id" id="location_id" required>
								@foreach ($locations as $location)
								<option value="{{ $location->id }}">{{ $location->address }}</option>
								@endforeach
							</select>
						</div>

						<div class="form-group col-6">
							<label for="main_location">Основной склад</label>
							<select class="form-control select2" name="main_location" id="main_location" required>
								@foreach ($locations as $location)
								<option value="{{ $location->id }}" {{ $location->id === \App\Location::MAIN ? 'selected' : '' }}>{{ $location->address }}</option>
								@endforeach
							</select>
						</div>
					</div>

					<button type="button" id="formSubmit" class="btn btn-success">
						<i class="fa fa-save"></i>Сохранить</button>
				</form>
			</div>
		</div>

	</div>
</div>

<script>
(function($) {
	'use strict';
	$(".select2").select2();
})(jQuery);
</script>

@endsection
