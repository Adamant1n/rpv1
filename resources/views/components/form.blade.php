@if(is_null($entity->id))
<form action="{{ $action ?? route(($route ?? getRouteBase()) . '.store') }}" method="post" {!! (isset($enctype) && $enctype) ? 'enctype="multipart/form-data"' : '' !!}>
@else
<form action="{{ $action ?? route(($route ?? getRouteBase()) . '.update', $entity->id) }}" method="post" {!! (isset($enctype) && $enctype) ? 'enctype="multipart/form-data"' : '' !!}>
	@method('put')
@endif
	@csrf

	{{ $slot }}
</form>