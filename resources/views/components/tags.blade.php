@push('js')
<script type="text/javascript">
	$('#tags-{{ $name }}').tagsInput({
		'width': '100%',
		'height': '75%',
		'interactive': true,
		'defaultText': '{{ isset($placeholder) ? $placeholder : '' }}',
		'removeWithBackspace': true,
		'minChars': 0,
		'maxChars': 20, 
		'placeholderColor': '#666666'
    });
</script>
@endpush

<input name="{{ $name }}"" id="tags-{{ $name }}"" value="{{ isset($value) ? $value : $slot }}" />