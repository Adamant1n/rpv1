@push('js')
<form id="destroy-form{{ isset($id) ? '-' . $id : '' }}" action="{{ $route ?? route(getActionName('destroy'), $entity) }}" method="POST" style="display: none;">
    @csrf
    @method('DELETE')
</form>

<script type="text/javascript">
	$('#{{ $id ?? 'deleteButton' }}').click(function (event) {
		 event.preventDefault();
		 swal({   
		 	buttons: {
		 		cancel: {
				    text: "Отмена",
				    value: null,
				    visible: true,
				    className: "danger",
				    closeModal: true,
				},
				confirm: {
				    text: "OK",
				    value: true,
				    visible: true,
				    className: "success",
				    closeModal: true
				}
		 	},
            title: "Подтвердить действие?",   
            text: "После удаления невозможно восстановить данные",   
            icon: "warning",   
            dangerMode: true,
        }).then((result) => {
  			if (result) {
  				$('#destroy-form{{ isset($id) ? '-' . $id : '' }}').submit();
    		}
  		});
	});

</script>
@endpush

<a class="btn btn-outline btn-danger pull-right" href="#" id="{{ $id ?? 'deleteButton' }}">Delete</a>