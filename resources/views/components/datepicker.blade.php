@push('js')
<script type="text/javascript">
	$('#{{ $id ?? 'datepicker-' . $name }}').datepicker({
    	enableOnReadonly: true,
    	todayHighlight: false,
    	format: '{{ $format ?? 'yyyy-mm-dd' }}'
    });
</script>
@endpush

@if(isset($icon) && $icon)
<div id="{{ $id ?? 'datepicker-' . $name }}" class="input-group date datepicker">
	<input type="text" class="form-control" name="{{ $name }}" value="{{ $value ?? $slot }}" placeholder="{{ $placeholder ?? '' }}" {{ (isset($required) && $required) ? 'required' : '' }}>
    <span class="input-group-addon input-group-append border-left">
    	<span class="mdi mdi-calendar input-group-text"></span>
    </span>
</div>
@else
<input type="text" class="form-control" name="{{ $name }}" id="{{ $id ?? 'datepicker-' . $name }}" value="{{ $value ?? $slot }}" placeholder="{{ isset($placeholder) ? $placeholder : '' }}">
@endif