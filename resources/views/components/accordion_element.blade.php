<div class="card">
    <div class="card-header" role="tab" id="heading{{ $name }}">
        <h6 class="mb-0">
        	<a {{ (!isset($active) || !$active) ? 'class="collapsed"' : '' }} data-toggle="collapse" href="#collapse{{ $name }}" aria-expanded="true" aria-controls="collapse{{ $name }}">
        		@if(isset($icon))
            	<i class="card-icon {{ $icon }}"></i>
            	@endif
            	{{ $title }}
            </a>
        </h6>
    </div>
    <div id="collapse{{ $name }}" class="collapse {{ (isset($active) && $active) ? 'show' : '' }}" role="tabpanel" aria-labelledby="heading{{ $name }}" data-parent="#{{ $parent }}">
        <div class="card-body">
            {{ $slot }}
        </div>
    </div>
</div>