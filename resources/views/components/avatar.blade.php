@if($user->image)
<img class="img-sm rounded-circle" src="{{ $user->image }}" alt="{{ $user->full_name }}">
@else
<div class="img-sm rounded-circle bg-{{ $color ?? 'danger' }} d-flex align-items-center justify-content-center text-white">{{ $user->short_name }}</div>
@endif