<script>
	showToast = function(style = 'success', text = '', heading = '', icon = '') {
		var color = '#f96868';

		if(style == 'success')
		{
			color = '#f96868';
		}
		else if(style == 'warning')
		{
			color = '#57c7d4';
		}
		else if(style == 'danger')	
		{
			color = '#f2a654';
		}
		else if(style == 'info')
		{
			color = '#46c35f';
		}

		if(icon == '')
		{
			if(style == 'danger') 
				icon = 'error';
			else
				icon = style;
		}

		$.toast({
			heading: heading,
			text: text,
			showHideTransition: 'slide',
			icon: icon,
			loaderBg: color,
			position: 'top-right'
		})
	};

	@if(Session::has('notification'))
	$(document).ready(function () {
		showToast('{{ Session::get('notification')[0] }}', '{{ Session::get('notification')[1] }}');
	});
	@endif
</script>