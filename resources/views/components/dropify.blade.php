@push('js')
<script type="text/javascript">
	$('#{{ $id ?? $name }}').dropify();
</script>
@endpush

<input type="file" class="" id="{{ $id ?? $name }}" name="{{ $name }}" @if(!empty($value))data-default-file="{{ $value ?? '' }}"@endif />