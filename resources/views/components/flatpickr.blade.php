@prepend('js')
<script type="text/javascript">
	var {{ $id ?? 'datepicker' . $name }} = $('#{{ $id ?? 'datepicker-' . $name }}').flatpickr({
    	enableTime: {{ ((isset($dateTime) && $dateTime) || (isset($timeOnly) && $timeOnly )) ? 'true' : 'false' }},
    	noCalendar: {{ (isset($timeOnly) && $timeOnly) ? 'true' : 'false' }},
    	dateFormat: '{{ isset($format) ? $format : 'Y-m-d H:i' }}',
    	time_24hr: {{ (isset($time24hr) && !$time24hr) ? 'false' : 'true' }},
    	minTime: '{{ isset($minTime) ? $minTime : '' }}',
    	maxTime: '{{ isset($maxTime) ? $maxTime : '' }}',
    	wrap: '{{ (isset($icon) && $icon) ? 'true' : 'false' }}',
    	mode: '{{ isset($mode) ? $mode : 'single' }}',
    	minuteIncrement: {{ isset($step) ? $step : 1 }},
    	allowInput: true,
    });
</script>
@endprepend

@if(isset($icon) && $icon)
<div class="input-group" id="{{ $id ?? 'datepicker-' . $name }}">
	<input data-input type="text" class="form-control" name="{{ $name }}" value="{{ $value ?? $slot }}" placeholder="{{ $placeholder ?? '' }}" {{ (isset($required) && $required) ? 'required' : '' }}>
	<span class="input-group-addon input-group-append border-left">
	    <a class="mdi mdi-calendar input-group-text" data-toggle></a>
	</span>
</div>
@else
<input type="text" class="form-control" name="{{ $name }}" id="{{ $id ?? 'datepicker-' . $name }}" value="{{ $value ?? $slot }}" placeholder="{{ $placeholder ?? '' }}" {{ (isset($required) && $required) ? 'required' : '' }}>
@endif