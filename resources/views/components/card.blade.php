<div class="card {{ $class ?? '' }}">
    @if(isset($title))
    <div class="card-header">
        <span class="utils__title">
            {{ $title }}
        </span>

        @if(isset($toolbar))
            {{ $toolbar }}
        @endif
    </div>
    @endif

    <div class="card-body {{ $body_class ?? '' }}">

        @if(isset($description))
        <p class="card-description">{{ $description }}</p>
        @endif

        {{ $slot }}
    </div>
</div>