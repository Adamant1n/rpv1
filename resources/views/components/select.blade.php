<select name="{{ $name }}" id="{{ $id ?? $name }}" class="form-control {{ $class ?? '' }}" {{ isset($required) && $required ? 'required' : '' }} {{ isset($multiple) && $multiple ? 'multiple' : '' }}>
	@if(isset($default))
	<option value="{{ $defaultValue ?? '' }}">{{ $default }}</option>
	@endif

	@if(isset($use_key) && $use_key)
		@foreach($options as $key => $option)
		<option value="{{ $key }}" {{ ($current ?? old($name, $entity->{$name})) === $key ? 'selected' : '' }}>{{ is_array($option) ? $option[$display ?? 'display'] : $option->{$display ?? 'display'} }}</option>
		@endforeach
	@else
		@foreach($options as $option)
		<option value="{{ is_array($option) ? $option[$value ?? 'id'] : $option->{$value ?? 'id'} }}" {{ ($current ?? old($name, $entity->{$name})) === (is_array($option) ? $option[$value ?? 'id'] : $option->{$value ?? 'id'}) ? 'selected' : '' }}>{{ is_array($option) ? $option[$display ?? 'display'] : $option->{$display ?? 'display'} }}</option>
		@endforeach
	@endif
</select>