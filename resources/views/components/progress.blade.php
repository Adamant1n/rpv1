@if(isset($label))
<div class="d-flex justify-content-between">
	<small>{{ $label }}</small>
</div>
@endif
<div class="progress progress-{{ isset($size) ? $size : '' }} {{ isset($class) ? $class : (isset($label) ? 'mt-2' : '') }}">
	<div class="progress-bar bg-{{ isset($color) ? $color : 'success' }}" role="progressbar" style="width: {{ $value }}%" aria-valuenow="{{ $value }}" aria-valuemin="0" aria-valuemax="100">{{ $slot }}</div>
</div>