<div class="modal fade" id="{{ $name }}Modal" role="dialog" aria-labelledby="{{ $name }}ModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-{{ isset($size) ? $size : 'md' }}" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="{{ $name }}ModalLabel">
					{{ $title }}
				</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				{{ $slot }}
			</div>

			@if(isset($footer))
			<div class="modal-footer">
				{{ $footer }}
			</div>
			@endif
		</div>
	</div>
</div>