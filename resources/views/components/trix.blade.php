@push('css')
<style>
trix-editor {
    min-height: 150px;
    overflow-y: auto;
}

.trix-button {
	border-bottom: none !important;
	border-color: rgb(204, 204, 204) !important;
}

.trix-button-group {
	border-radius: 5px !important;
	border-bottom-color: #ccc !important;
}
</style>
@endpush

@push('js')
<script type="text/javascript">
	(function() {
		'use strict'

		var route = '{{ route('attachments.trix_upload', $directory) }}';

		$('trix-editor').on('trix-attachment-add', function (event) {
			if (event.originalEvent.attachment.file) {
				var attachment = event.originalEvent.attachment;
				uploadFile(attachment.file, setProgress, setAttributes);

				function setProgress(progress) {
			    	attachment.setUploadProgress(progress)
			    }

			    function setAttributes(attributes) {
			    	attachment.setAttributes(attributes)
			    }
			}
		});

		$('trix-editor').on('trix-file-accept', function (event) {
			var file = event.originalEvent.file;
			var extension = file.name.split('.').slice(-1)[0];

		    if (['png', 'jpg'].indexOf(extension.toLowerCase()) === -1) {
		    	event.preventDefault()
		    }
		});

		function uploadFile(file, progressCallback, successCallback) {
			var formData = new FormData()
			formData.append("file", file)

			$.ajax({
		        url: route,
		        type: 'post',
		        data: formData,
		        contentType: false,
				processData: false
		    }).done(function(data) {
		    	var attributes = {
          			url: data.link,
          			href: data.link
        		}

		    	successCallback(attributes);
		  	});
		}
	})();
</script>
@endpush

<input id="{{ $id ?? 'trix-' . $name }}" type="hidden" name="{{ $name }}" value="{{ $value ?? $slot }}">
<trix-editor input="{{ $id ?? 'trix-' . $name }}"></trix-editor>