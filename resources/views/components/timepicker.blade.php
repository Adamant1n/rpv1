@push('js')
<script type="text/javascript">
	$('#{{ $id ?? 'timepicker-' . $name }}').datetimepicker({
    	format: '{{ $format ?? 'LT' }}'
    });
</script>
@endpush

<div class="input-group date" id="{{ $id ?? 'timepicker-' . $name }}" data-target-input="nearest">
	<div class="input-group" data-target="{{ $id ?? 'timepicker-' . $name }}" data-toggle="datetimepicker">
		<input type="text" class="form-control datetimepicker-input" data-target="{{ $id ?? 'timepicker-' . $name }}" name="{{ $name }}" value="{{ $value ?? $slot }}" placeholder="{{ $placeholder ?? '' }}" {{ (isset($required) && $required) ? 'required' : '' }} />
		<div class="input-group-addon input-group-append">
			<i class="mdi mdi-clock input-group-text"></i>
        </div>
    </div>
</div>