<div class="row mb-4">
	<div class="col-12 d-flex align-items-center justify-content-between">
		<h4 class="page-title">{{ $slot }}</h4>
		@if(isset($toolbar))
		<div class="d-flex align-items-center">
			{{ $toolbar }}
		</div>
		@endif
	</div>
</div>