@push('js')
<script type="text/javascript">
	var attachments = '';
	Dropzone.autoDiscover = false;

	$('#dropzone-{{ $name }}').dropzone({
		url: "{{ route('attachments.upload', $directory) }}",
		addRemoveLinks: true,
		sending: function(file, xhr, formData) {
		    formData.append("_token", "{{ csrf_token() }}");
		},
		removedfile: function(file) {
			
		    var id = file.id;        

		    $.ajax({
		        type: 'DELETE',
		        url: '/attachments/' + id,
		    });

			var _ref;
			return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;        
			
        },
		init: function () {
    		this.on("success", function (file, response) {
      			file.id = response.attachment_id;
      			attachments += response.attachment_id + ',';
      			$('#{{ $id ?? $name }}').val(attachments);
    		});
  		}
	});
</script>
@endpush

<div id="dropzone-{{ $name }}" class="dropzone">
	<div class="fallback">
    	<input name="dropzone-file-{{ $name }}" type="file" multiple />
	</div>
	<input type="hidden" name="{{ $name }}" id="{{ $id ?? $name }}">
</div>