import './bootstrap';

import Vue from 'vue'
window.Vue = Vue;

import App from './components/App'

import VueToast from 'vue-toast-notification';
import 'vue-toast-notification/dist/index.css';
 
Vue.use(VueToast, {
    position: 'top'
});

// Vue.component('example-component', require('./components/ExampleComponent.vue'));

const app = new Vue({
    components: {
        App
    },
    // router,
    // store,
    render:  h  =>  h(App)
}).$mount('#app');