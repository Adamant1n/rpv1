<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Synchronization extends Model
{
    /** attributes **/

    protected $guarded = ['id'];

    protected $dates = [
        'started_at',
        'ended_at'
    ];

    protected $casts = [
        'result'    => 'array'
    ];

    /** accessors **/

    /*
    public function getEndedAtAttribute()
    {
    	return $this->ended_at ? \Carbon\Carbon::parse($this->ended_at) : null;
    }
    */
}
