<?php

namespace App\Services;

use GuzzleHttp\Client;

class CheckLicenseService
{
    /**
     * @var string
     */
    protected $pinCode = '';

    /**
     * @var string
     */
    protected $license = '';

    /**
     * @var Client
     */
    protected $client;

    /**
     * CheckLicenseService constructor.
     */
    public function __construct()
    {
        $this->pinCode = env('app.pin');
        $this->license = env('app.license');

        // @ToDo Доделать
        $this->client = new Client([
            'base_uri' => 'https://www.google.com/',
        ]);
    }

    /**
     * @return bool
     */
    public function execute(): bool
    {
        $response = $this->client->post('api/check-license', [
            'pin' => $this->pinCode,
            'license' => $this->license
        ]);

        return $response->getStatusCode() === 200;
    }
}
