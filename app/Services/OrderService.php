<?php

namespace App\Services;

use App\Order;
use App\Unit;
use App\Location;
use App\Reservation;
use App\PriceList;
use App\Client;
use App\Agent;
use App\Category;
use Webpatser\Uuid\Uuid;

use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class OrderService
{
    protected $order;

    public function __construct(Order $order)
    {
        $this->order = $order;
        $this->order->loadMissing('agent.products');
    }

    public function printReceipt()
    {
        $request = $this->generateAtolReceipt();

        $this->order->update([
            'uuid' => $request['uuid'],
            'payment_accepted_by' => Auth::id(),
        ]);

        $client = new \GuzzleHttp\Client();
        $url = config('services.atol.url') . 'requests';
        try {
            info('Order service – try');
            $response = $client->request('POST', $url, ['json' => $request]);
            $body = (string) $response->getBody();
            info('Order service – response');
            info($body);
            return response()->json(['success' => true, 'message' => $body]);
        } catch (\Exception $e) {
            info('Order service – error');
            info($e->getMessage());
            return response()->json(['success' => false, 'message' => $e->getMessage()]);
        }
    }

    public function printRefundReceipt()
    {
        $request = $this->generateAtolRefundReceipt();

        $this->order->update([
            'uuid' => $request['uuid'],
            'payment_accepted_by' => Auth::id(),
        ]);

        $client = new \GuzzleHttp\Client();
        $url = config('services.atol.url') . 'requests';
        try {
            info('Order service, refund – try');
            $response = $client->request('POST', $url, ['json' => $request]);
            $body = (string) $response->getBody();
            info('Order service, refund – response');
            info($body);
            return response()->json(['success' => true, 'message' => $body]);
        } catch (\Exception $e) {
            info('Order service, refund – error');
            info($e->getMessage());
            return response()->json(['success' => false, 'message' => $e->getMessage()]);
        }
    }

    public function generateAtolRefundReceipt()
    {
        $uuid = Uuid::generate()->string;
        $user = auth()->user();

        // услуга
        if ($this->order->reservations->where('duration', '>', 0)->first()->unit->product->category->type == Category::TYPE_RENT) {
            $taxationType = $this->order->location->rent_taxation_type ?? Location::TAXATION_TYPE_ENVD;
        }
        // продажа
        else {
            $taxationType = $this->order->location->sale_taxation_type ?? Location::TAXATION_TYPE_USNINCOME;
        }

        $receipt = [
            'uuid'      => $uuid,
            'request'   => [
                'type'          => 'sellReturn',
                'taxationType'  => $taxationType,
                'ignoreNonFiscalPrintErrors'    => false,
                'operator'  => [
                    'name'  => $user->last_name,
                    'vatin' => $user->atol_id,
                ],
                'items'     => [],
                'payments'  => [
                    [
                        'type'  => $this->order->paid_by == 'cash' ? 'cash' : 'electronically',
                        'sum'   => 0
                    ]
                ],
                'total'     => 0
            ]
        ];

        info('ATOL RECEIPT 1', $receipt);

        $this->order->loadMissing('reservations.unit.product');

        foreach ($this->order->reservations->where('refund', '>', 0) as $reservation) {

            $name = '(Возврат) ' . $reservation->unit->product->name;
            $price = (float) number_format($reservation->refund, 2, '.', '');

            $tax = $this->order->location->rent_tax ?? Location::TAX_NONE;
            $paymentObject = 'service';

            $receipt['request']['items'][] = [
                'type'              => 'position',
                'name'              => $name,
                'price'             => $price,
                'quantity'          => 1,
                'amount'            => $price,
                'department'        => 1,
                'measurementUnit'   => 'шт.',
                'paymentMethod'     => 'fullPrepayment',
                'paymentObject'     => $paymentObject,
                'tax'               => [
                    'type'  => $tax
                ]
            ];

            $receipt['request']['total'] += $price;
            $receipt['request']['payments'][0]['sum'] += $price;

            $receipt['request']['items'][] = [
                'type'      => 'text',
                'text'      => '--------------------------------',
                'alignment' => 'left',
                'font'      => 0,
                'doubleWidth'   => false,
                'doubleHeight'  => false
            ];
        }

        info('ATOL RECEIPT 2', $receipt);

        return $receipt;
    }

    public function printExtraReceipt()
    {
        $request = $this->generateAtolExtraReceipt();

        $this->order->update([
            'uuid' => $request['uuid'],
        ]);

        $client = new \GuzzleHttp\Client();
        $url = config('services.atol.url') . 'requests';
        try {
            info('Order service, extra – try');
            $response = $client->request('POST', $url, ['json' => $request]);
            $body = (string) $response->getBody();
            info('Order service, extra – response');
            info($body);
            return response()->json(['success' => true, 'message' => $body]);
        } catch (\Exception $e) {
            info('Order service – error');
            info($e->getMessage());
            return response()->json(['success' => false, 'message' => $e->getMessage()]);
        }
    }

    public function generateAtolExtraReceipt()
    {
        $uuid = Uuid::generate()->string;
        $user = auth()->user();

        $taxationType = $this->order->location->rent_taxation_type ?? Location::TAXATION_TYPE_ENVD;

        $receipt = [
            'uuid'      => $uuid,
            'request'   => [
                'type'          => 'sell',
                'taxationType'  => $taxationType,
                'ignoreNonFiscalPrintErrors'    => false,
                'operator'  => [
                    'name'  => $user->last_name,
                    'vatin' => $user->atol_id,
                ],
                'items'     => [],
                'payments'  => [
                    [
                        'type'  => $this->order->paid_by == 'cash' ? 'cash' : 'electronically',
                        'sum'   => 0
                    ]
                ],
                'total'     => 0
            ]
        ];

        info('ATOL RECEIPT 1', $receipt);

        $this->order->loadMissing('reservations.unit.product');

        foreach ($this->order->reservations->where('price_extra', '>', 0) as $reservation) {

            $name = '(Задолженность) ' . $reservation->unit->product->name;
            $price = (float) number_format($reservation->price_extra, 2, '.', '');

            $tax = $this->order->location->rent_tax ?? Location::TAX_NONE;
            $paymentObject = 'service';

            $receipt['request']['items'][] = [
                'type'              => 'position',
                'name'              => $name,
                'price'             => $price,
                'quantity'          => 1,
                'amount'            => $price,
                'department'        => 1,
                'measurementUnit'   => 'шт.',
                'paymentMethod'     => 'fullPrepayment',
                'paymentObject'     => $paymentObject,
                'tax'               => [
                    'type'  => $tax
                ]
            ];

            $receipt['request']['total'] += $price;
            $receipt['request']['payments'][0]['sum'] += $price;

            $receipt['request']['items'][] = [
                'type'      => 'text',
                'text'      => '--------------------------------',
                'alignment' => 'left',
                'font'      => 0,
                'doubleWidth'   => false,
                'doubleHeight'  => false
            ];
        }

        info('ATOL RECEIPT 2', $receipt);

        return $receipt;
    }

    public function generateAtolReceipt()
    {
        $uuid = Uuid::generate()->string;
        $user = auth()->user();

        // if($this->order->location->)
        // $this->order->reservations->where('duration', '>', 0)->first()->unit->product->category->fr_section == 0 ? 'envd' : 'usnIncome',

        // услуга
        if ($this->order->reservations->where('duration', '>', 0)->first()->unit->product->category->type == Category::TYPE_RENT) {
            $taxationType = $this->order->location->rent_taxation_type ?? Location::TAXATION_TYPE_ENVD;
        }
        // продажа
        else {
            $taxationType = $this->order->location->sale_taxation_type ?? Location::TAXATION_TYPE_USNINCOME;
        }

        $receipt = [
            'uuid'      => $uuid,
            'request'   => [
                'type'          => 'sell',
                'taxationType'  => $taxationType,
                'ignoreNonFiscalPrintErrors'    => false,
                'operator'  => [
                    'name'  => $user->last_name,
                    'vatin' => $user->atol_id,
                ],
                'items'     => [],
                'payments'  => [
                    [
                        'type'  => $this->order->paid_by == 'cash' ? 'cash' : 'electronically',
                        'sum'   => 0
                    ]
                ],
                'total'     => 0
            ]
        ];

        info('ATOL RECEIPT 1', $receipt);

        $this->order->loadMissing('reservations.unit.product');

        foreach ($this->order->reservations->where('duration', '>', 0) as $reservation) {

            $isDebt = ($reservation->price_extra > 0);
            if ($isDebt) {
                $name = '(Задолженность) ' . $reservation->unit->product->name;
                $price = (float) number_format($reservation->price_extra, 2, '.', '');
            } else {
                $name = $reservation->unit->product->name;
                $price = (float) number_format($reservation->price, 2, '.', '');
            }

            // услуга
            if ($reservation->unit->product->category->type == Category::TYPE_RENT) {
                $tax = $this->order->location->rent_tax ?? Location::TAX_NONE;
                $paymentObject = 'service';
            }
            // продажа
            else {
                $tax = $this->order->location->sale_tax ?? Location::TAX_NONE;
                $paymentObject = 'commodity';
            }

            $receipt['request']['items'][] = [
                'type'              => 'position',
                'name'              => $name,
                'price'             => $price,
                'quantity'          => 1,
                'amount'            => $price,
                'department'        => 1,
                'measurementUnit'   => 'шт.',
                'paymentMethod'     => 'fullPrepayment',
                'paymentObject'     => $paymentObject,
                'tax'               => [
                    'type'  => $tax
                ]
            ];

            $receipt['request']['total'] += $price;
            $receipt['request']['payments'][0]['sum'] += $price;

            $receipt['request']['items'][] = [
                'type'      => 'text',
                'text'      => '--------------------------------',
                'alignment' => 'left',
                'font'      => 0,
                'doubleWidth'   => false,
                'doubleHeight'  => false
            ];
        }

        info('ATOL RECEIPT 2', $receipt);

        return $receipt;
    }

    public function createOrder(Client $client): Order
    {
        $location = auth()->user()->location;
        if ($location->tariff_step == Location::STEP_MINUTE) {
            $dimension = 'm';
            $duration = 10;
        } else if ($location->tariff_step == Location::STEP_HOUR) {
            $dimension = 'h';
            $duration = 1;
        } else {
            $dimension = 'd';
            $duration = 1;
        }

        $order  = $client->orders()->create([
            'user_id'       => auth()->user()->id,
            'location_id'   => auth()->user()->location_id,
            'dimension'     => $dimension,
            'duration'      => $duration
        ]);

        $order = $order->fresh();
        $this->order = $order;
        $this->calculateOrderTime();

        return $order;
    }

    public function attachUnit(Unit $unit): Reservation
    {
        $user = auth()->user();

        $reservation = $this->order->reservations()->create([
            'unit_id'     => $unit->id,
            'given_by'    => $user->id,
            'status'    => Reservation::STATUS_RENT,
            'duration'    => $this->order->duration,
            'dimension'    => $this->order->dimension
        ]);

        info('attached unit');

        $unit->update(['reserved' => true]);

        $this->calculateReservationTime($reservation);
        $this->calculateReservationPrice($reservation);

        return $reservation;
    }

    public function calculateReservationRefund($reservation)
    {
        info('calculateReservationRefund');

        if (is_null($reservation->starts_at)) {
            info('full price');
            // return $reservation->full_price;
            return ['value' => $reservation->full_price, 'duration' => 1];
        }

        $diffReservation = $reservation->replicate();

        if ($reservation->dimension == 'd') {
            $diff = now()->diffInDays($reservation->ends_at, false);
        } else if ($reservation->dimension == 'h') {
            $diff = now()->diffInHours($reservation->ends_at, false);
        }

        $realDuration = now()->diffInDays($reservation->starts_at);
        $diffReservation->duration = $realDuration;

        info('real duration: ' . $realDuration);

        if ($diff < 0) {
            info('diff less than 0');
            return ['value' => 0, 'duration' => $realDuration];
        }

        /** calculate ends at **/

        // филиал
        $location = $this->order->location;

        // время работы филиал
        $closes_at      = $location->closes_at;      // закрытие
        $closed_at      = Carbon::parse(now()->format('Y-m-d ' . $closes_at . ':00'));   // закрытие сегодня

        // тариф в часах
        if ($reservation->dimension === 'h')  $ends_at = $reservation->starts_at->copy()->addHours($diffReservation->duration);
        // тариф в днях
        else if ($reservation->dimension === 'd')  $ends_at = $reservation->starts_at->copy()->addDays($diffReservation->duration);
        // тариф в минутах
        else if ($reservation->dimension === 'm')  $ends_at = $reservation->starts_at->copy()->addMinutes($diffReservation->duration);

        if ($ends_at->format('d') === $closed_at->format('d'))
            $ends_at->addDays(1);
        $ends_at = Carbon::parse($ends_at->format('Y-m-d ' . $location->return_by . ':00'));

        $diffReservation->ends_at = $ends_at;

        /** calculate reservation price **/

        $diffReservation = $this->calculateReservationPrice($diffReservation, false);

        $diffPrice = $reservation->full_price - $diffReservation->full_price;
        info('diff price: ' . $diffPrice);

        if ($diffPrice > 0) {
            return ['value' => $diffPrice, 'duration' => $realDuration];
        } else {
            return ['value' => 0, 'duration' => $realDuration];
        }
    }

    // забрать юнит
    public function takeUnit(Reservation $reservation, $replaced = false)
    {
        if (is_null($reservation->starts_at)) {
            //$reservation->delete();
            //return true;
        }

        $refundValue = 0;

        // расчет возврата
        if (!$replaced) {
            info('takeUnit not replaced');
            $refund = $this->calculateReservationRefund($reservation);
            $refundValue = $refund['value']; // $refund['duration'] > 0 ? $refund['value'] : 0;
            info('refund: ' . $refundValue);
        }

        info('updating reservation');

        $reservation->update([
            'status'        => Reservation::STATUS_RETURNED,
            'taken_by'      => auth()->user()->id,
            'returned_at'   => now(),
            'replaced'        => $replaced
        ]);

        info('0 – ' . $reservation->status);

        $reservation->unit->update(['location_id' => auth()->user()->location_id]);

        // пересчет экстра прайса
        //if($this->order->status == Order::STATUS_PAID) {
        if ($refundValue == 0) {
            info('calculateReservationExtraPrice');
            info('1 – ' . $reservation->status);
            $this->calculateReservationExtraPrice($reservation);
        } else if (isset($refundValue) && $refundValue > 0) {
            $reservation->update([
                'refund'    => $refundValue,
                // 'duration'  => $refund['duration'],
            ]);
        }

        if ($this->order->status == Order::STATUS_PAID) {
            info('status paid');
            info('2 – ' . $reservation->status);
            $this->order->price_extra = 0;
            foreach ($this->order->reservations->where('duration', '>', 0) as $_reservation) {
                info('plus price extra: ' . $_reservation->price_extra);
                $this->order->price_extra += $_reservation->price_extra;
            }
            $this->order->save();
        } else if ($this->order->status == Order::STATUS_DRAFT) {
            info('3 – ' . $reservation->status);
            $reservation->update([
                'duration'  => 0,
                'price_extra' => 0,
                'price' => 0,
                'full_price' => 0,
            ]);
            /*
        	$reservation->price = $reservation->price_extra;
        	$reservation->full_price = $reservation->price_extra;
        	$reservation->price_extra = 0;
        	$reservation->update();
        	*/
        }

        info('4 – ' . $reservation->status);
        //}
    }

    // отдать юнит
    public function untakeUnit(Reservation $reservation)
    {
        $reservation->update([
            'status'        => Reservation::STATUS_RENT,
            'returned_at'   => null
        ]);
    }

    // пересчет задолженности
    public function calculateReservationExtraPrice(Reservation $reservation, $extra = false, $update = true)
    {
        $now = $reservation->returned_at;

        if (is_null($reservation->starts_at)) {
            return 0;
        }

        if ($reservation->status == Reservation::STATUS_RETURNED) {
            // $ends_at = $reservation->returned_at->subMinutes($this->order->location->free_time);
            $ends_at = $reservation->ends_at->addMinutes($this->order->location->free_time);
        } else {
            $ends_at = $reservation->ends_at;
        }

        if ($extra) {
            $now = now();
        }

        // если просрочена выдача
        if ((!is_null($now) && $now->gt($ends_at)) || $extra) {
            info('выдача просрочена');
            $tariff_step = $this->order->location->tariff_step;
            $productCategory = $reservation->unit->product->category->id;
            $location = $this->order->location;

            // разница времени между текущим временем и return_by
            $diffInMinutes = $now->diffInMinutes($ends_at);
            $diffInHours = $now->diffInHours($ends_at);
            // $diffInDays = $now->diffInDays($ends_at);

            $diffInDays = Carbon::parse($now->format('Y-m-d 00:00:00'))->diffInDays(
                Carbon::parse($ends_at->format('Y-m-d 00:00:00'))
            );

            info('ends_at: ' . $ends_at->format('d.m.Y H:i:s'));
            info('now: ' . $now->format('d.m.Y H:i:s'));
            info('ddays ' . $diffInDays);
            info('dhours ' . $diffInHours);

            $reservation->price_extra = 0;

            // если разница меньше часа
            if ($diffInMinutes < 60) {
                info('разница меньше часа');
                // если тарификация 10 мин
                if ($tariff_step === Location::STEP_MINUTE) {
                    info('тарификация 10 мин');
                    // HARDCODE MODE
                    if ($diffInMinutes <= 10) {
                        $duration = 10;
                        $dimension = 'm';
                    } else if ($diffInMinutes <= 30) {
                        $duration = 30;
                        $dimension = 'm';
                    } else {
                        $duration = 1;
                        $dimension = 'h';
                    }
                }
                // тарификация - 1 час
                else if ($tariff_step === Location::STEP_HOUR) {
                    info('тарификация час');
                    $duration = 1;
                    $dimension = 'h';
                }
                // тарификация - сутки
                else {
                    info('тарифиация день');
                    $duration = 1;
                    $dimension = 'd';
                }
            }
            // если разница меньше суток
            else if ($diffInHours <= $location->hours_max) {
                info('разница меньше суток');
                $duration = $diffInHours; // - $reservation->duration;
                $dimension = 'h';
            }
            // если разница сутки и больше
            else {
                info('разница сутки и больше');
                $duration = $diffInDays; // - $reservation->duration; // если возвращено < 24 часов назад, будет 0
                info('разница реальная – ' . $diffInDays);
                $dimension = 'd';
            }

            // если разница - в минутах или часах, стоимость считается по тарифу на сегодня
            if ($dimension != 'd') {
                $dayOfWeek = $now->format('N');
                // выходной
                if (in_array($dayOfWeek, $this->order->location->weekends)) {
                    $priceList = $this->order->location->priceWeekends;
                }
                // будний
                else {
                    $priceList = $this->order->location->priceWeekdays;
                }

                info($productCategory . ', ' . $dimension . ', ' . $duration);
                $reservation->price_extra = round($priceList->tariffs[$productCategory][$dimension][$duration], 2);
                info('стоимость считается по тарифу на сегодня ' . $reservation->price_extra);
            }
            // если разница - в днях, стоимость считается за каждый просроченный день с дня возврата
            else {
                $day = $ends_at->copy();

                $returnBy = $reservation->returned_at ?? now();
                // $returnBy = $reservation->ends_at;

                info('стоимость считается за каждый просроченный день');

                info('Дата возврата: ' . $returnBy->format('H:i d.m.Y'));

                $shouldReturn = Carbon::parse($returnBy->format('Y-m-d ' . $this->order->location->return_by . ':00'));
                info('Возврат до след дня должен быть до ' . $shouldReturn->format('H:i d.m.Y'));

                info('Duration: ' . $duration);

                if ($returnBy->gt($shouldReturn)) {
                    $duration += 1;
                    info('Возвращено после беслпатного времени, длительность ' . $duration . ' дней');
                }

                for ($i = 0; $i < $duration; $i++) {
                    $dayOfWeek = $day->format('N');

                    // выходной
                    if (in_array($dayOfWeek, $this->order->location->weekends)) {
                        $priceList = $this->order->location->priceWeekends;
                    }
                    // будний
                    else {
                        $priceList = $this->order->location->priceWeekdays;
                    }

                    //update reservations set taken_by=null, status='rent', duration=2, price_extra=0, refund=0, returned_at=null where id=10001
                    $reservation->price_extra += round($priceList->tariffs[$productCategory][$dimension][1], 2);
                    info('стоимость считается за каждый просроченный день – плюс ' . $reservation->price_extra);

                    $day->addDays(1);
                }
            }

            /**
             * скидка
             */
            info('расчет скидки');
            if ($this->order->agent) {
                info('агент задан');
                $discountProduct = $this->order->agent->products->where('id', $reservation->unit->product_id)->first();
                if ($discountProduct) {
                    $discount = $discountProduct->pivot->discount;
                }
            }

            if (!isset($discount)) {
                $discount = $this->order->discount;
                info('скидка ' . $discount);
            }

            info('экстра прайс до скидки ' . $reservation->price_extra);
            $reservation->price_extra = round($reservation->price_extra * (1 - $discount / 100), 2);
            info('экстра прайс после скидки ' . $reservation->price_extra);

            if ($update) {
                $reservation->save();
            }
        } else {
            info('пересчет не нужен');
            // пересчет не нужен

        }

        return $reservation->price_extra;
    }

    public function calculateReservationPrice(Reservation $reservation, $save = true): Reservation
    {
        // не считать возвращенные
        /*
        if($reservation->status != Reservation::STATUS_RENT) {
            if($this->order->status != 'paid') {
                $reservation->update([
                    'price'         => 0,
                    'full_price'    => 0
                ]);
            }

            return $reservation;
        }
        */

        if ($reservation->status == Reservation::STATUS_RETURNED && $save) {
            if ($this->order->status != 'paid') {
                $reservation->update([
                    'price'         => 0,
                    'full_price'    => 0
                ]);
            } else {
                $reservation->price = $this->calculateReservationExtraPrice($reservation);
                $reservation->price_extra = 0;
                $reservation->update();
            }

            return $reservation;
        }

        if ($this->order->agent) {
            $discountProduct = $this->order->agent->products->where('id', $reservation->unit->product_id)->first();
            if ($discountProduct) {
                $discount = $discountProduct->pivot->discount;
            }
        }

        if (!isset($discount)) {
            $discount = $this->order->discount;
        }

        if ($reservation->unit->product->for_sale) {
            $discount = 0;
            if ($this->order->agent) {
                $discount = $this->order->agent->sell_discount;
            }

            $price = round($reservation->unit->product->price_sell * (1 - $discount / 100), 2);

            $reservation->update([
                'price' => $price,
                'full_price' => $reservation->unit->product->price_sell,
            ]);

            return $reservation;
        }

        $productCategory = $reservation->unit->product->category->id;
        if ($save) {
            $dimension    = $this->order->dimension;
            $duration    = $this->order->duration;
        } else {
            $dimension = $reservation->dimension;
            $duration = $reservation->duration;
        }

        // часы и минуты считаются по дню начала
        if ($dimension <> 'd') {
            $dayOfWeek = $reservation->starts_at->format('N');

            // выходной
            if (in_array($dayOfWeek, $this->order->location->weekends)) {
                $priceList = $this->order->location->priceWeekends;
            }
            // будний
            else {
                $priceList = $this->order->location->priceWeekdays;
            }

            $reservation->full_price = round($priceList->tariffs[$productCategory][$dimension][$duration], 2);
        } else {
            $reservation->full_price = 0;
            $day = $reservation->starts_at;

            // расчет фикс цены
            $dayOfWeek = $day->format('N');

            // выходной
            if (in_array($dayOfWeek, $this->order->location->weekends)) {
                $priceList = $this->order->location->priceWeekends;
            }
            // будний
            else {
                $priceList = $this->order->location->priceWeekdays;
            }

            if (isset($priceList->tariffs[$productCategory][$dimension][$duration])) {
                $reservation->full_price = round($priceList->tariffs[$productCategory][$dimension][$duration], 2);
            }

            // расчет посуточно
            else {
                for ($i = 0; $i < $duration; $i++) {
                    $dayOfWeek = $day->format('N');

                    // выходной
                    if (in_array($dayOfWeek, $this->order->location->weekends)) {
                        $priceList = $this->order->location->priceWeekends;
                    }
                    // будний
                    else {
                        $priceList = $this->order->location->priceWeekdays;
                    }
                    $reservation->full_price += round($priceList->tariffs[$productCategory][$dimension][1], 2);

                    $day->addDays(1);
                }
            }
        }

        $reservation->price = round($reservation->full_price * (1 - $discount / 100), 2);

        // сохранять только для реальной брони. при расчете рефанда не нужно
        if ($save) {
            $reservation->save();
        }

        return $reservation;
    }

    public function calculateOrderPrice($save = true): Order
    {
        foreach ($this->order->reservations->where('duration', '>', 0) as $reservation) {
            $this->calculateReservationPrice($reservation, $save);
        }

        $this->order->full_price = $this->order->reservations->where('duration', '>', 0)->sum('full_price');
        $this->order->price = round($this->order->reservations->where('duration', '>', 0)->sum('price'), 2);
        // $this->order->price = $this->order->full_price * (1 - $this->order->discount / 100);

        if ($save) {
            $this->order->save();
        }

        return $this->order;
    }

    public function calculateReservationTime(Reservation $reservation, $save = true): Reservation
    {
        if ($reservation->unit->product->for_sale) {
            $reservation->update([
                'starts_at' => null,
                'ends_at' => null,
            ]);

            return $reservation;
        }

        // филиал
        $location = $this->order->location;

        // текущее время с округлением до 5 минут в большую сторону
        // $now = $this->roundUpToMinuteInterval(now(), 5);
        // $now = Carbon::parse('2018-11-09 18:00:00');
        $now = $reservation->created_at;

        // время работы филиала
        $opens_at       = $location->opens_at;       // открытие
        $opened_at      = Carbon::parse($now->format('Y-m-d ' . $opens_at . ':00'));    // открытие сегодня
        $closes_at      = $location->closes_at;      // закрытие
        $closed_at      = Carbon::parse($now->format('Y-m-d ' . $closes_at . ':00'));   // закрытие сегодня

        // если время закрытия после 0, перенос на следующий день
        if ($opened_at->gte($closed_at)) $closed_at->addDays(1);

        info('closed_at ' . $closed_at->format('H:i d.m.Y'));

        // настройки филиала
        $return_by      = $location->return_by;      // бесплатный возврат до
        $next_day_at    = $location->next_day_at;    // начало отсчета со следующего дня если в прокат взято после времени
        $all_day        = $location->all_day;        // круглосуточный режим работы

        info('ret next all ' . $return_by . ', ' . $next_day_at . ', ' . $all_day);

        // выбраный тариф
        $duration       = $this->order->duration;
        $dimension      = $this->order->dimension;

        // поправка на hours_max
        if ($dimension === 'h' && $duration > $location->hours_max) {
            $dimension  = 'd';
            $duration   = 1;
        }

        // параметр next_day для сегодняшнего дня
        $next_day_by    = Carbon::parse($now->format('Y-m-d ' . $next_day_at . ':00'));
        // начало отсчета со следующего дня ?
        $is_next_day    = $now->gte($next_day_by);
        $is_today       = $next_day_by->gt($now);

        // начало аренды. если начало с сегодня (или круглосуточный) – текущее время, если нет – завтра с открытия филиала
        $starts_at      = ($is_today || $all_day) ? $now : $opened_at->copy()->addDays(1);

        // тариф в часах
        if ($dimension === 'h')  $ends_at = $starts_at->copy()->addHours($duration);
        // тариф в днях
        else if ($dimension === 'd')  $ends_at = $starts_at->copy()->addDays($duration);
        // тариф в минутах
        else if ($dimension === 'm')  $ends_at = $starts_at->copy()->addMinutes($duration);

        // круглосуточный филиал
        if ($all_day) {
            info('all day');
            // $ends_at = $ends_at
        }
        // часы работы
        else {
            // если оканчивается до часов работы СЕГОДНЯ – возврат в текущее время
            if ($closed_at->gt($ends_at)) {
                info('closed at > ends_at');
                // $ends_at = $ends_at
            }

            // если оканчивается после часов работы – возврат через сутки до бесплатного возврата
            else {
                info('оканчивается после часов работы сегодня');
                info($ends_at->format('H:i d') . ', ' . $closed_at->format('H:i d'));
                if ($ends_at->format('d') === $closed_at->format('d')) {
                    $ends_at->addDays(1);
                    $ends_at = Carbon::parse($ends_at->format('Y-m-d ' . $return_by . ':00'));
                    info('d = d 1 ' . $ends_at->format('H:i d.m.Y'));
                } else {
                    $ends_at = Carbon::parse($ends_at->format('Y-m-d ' . $return_by . ':00'));
                    info('d = d 2 ' . $ends_at->format('H:i d.m.Y'));
                    // непонятна логика
                }
            }
        }

        if ($save) {
            $reservation->update([
                'starts_at' => $starts_at,
                'ends_at'    => $ends_at
            ]);
        } else {
            $reservation->starts_at = $starts_at;
            $reservation->ends_at = $ends_at;
        }

        info($starts_at->format('H:i d.m.Y') . ', ' . $ends_at->format('H:i d.m.Y'));

        if ($reservation->ends_at->gt($this->order->ends_at)) {
            if ($save) {
                $this->order->update([
                    'ends_at'    => $reservation->ends_at
                ]);
            } else {
                $reservation->ends_at = $ends_at;
            }
        }

        return $reservation;
    }

    public function calculateOrderTime(): Order
    {
        // филиал
        $location = $this->order->location;

        // текущее время с округлением до 5 минут в большую сторону
        // $now = $this->roundUpToMinuteInterval(now(), 5);
        // $now = Carbon::parse('2018-11-09 18:00:00');

        if ($this->order->reservations->where('duration', '>', 0)->count() === 0) {
            $now = $this->order->created_at;
        } else {
            foreach ($this->order->reservations->where('duration', '>', 0) as $reservation) {
                $this->calculateReservationTime($reservation);
            }

            $starts_at = optional($this->order->reservations()->where('duration', '>', 0)->orderBy('starts_at', 'asc')->whereNotNull('starts_at')->first())->starts_at ?? now();
            $ends_at = optional($this->order->reservations()->where('duration', '>', 0)->orderBy('ends_at', 'desc')->whereNotNull('starts_at')->first())->ends_at ?? now();

            $this->order->update([
                'starts_at' => $starts_at,
                'ends_at'   => $ends_at
            ]);

            return $this->order;
        }

        // время работы филиала
        $opens_at       = $location->opens_at;       // открытие
        $opened_at      = Carbon::parse($now->format('Y-m-d ' . $opens_at . ':00'));    // открытие сегодня
        $closes_at      = $location->closes_at;      // закрытие
        $closed_at      = Carbon::parse($now->format('Y-m-d ' . $closes_at . ':00'));   // закрытие сегодня

        // если время закрытия после 0, перенос на следующий день
        if ($opened_at->gte($closed_at)) $closed_at->addDays(1);

        // настройки филиала
        $return_by      = $location->return_by;      // бесплатный возврат до
        $next_day_at    = $location->next_day_at;    // начало отсчета со следующего дня если в прокат взято после времени
        $all_day        = $location->all_day;        // круглосуточный режим работы

        // выбраный тариф
        $duration       = $this->order->duration;
        $dimension      = $this->order->dimension;

        // поправка на hours_max
        if ($dimension === 'h' && $duration > $location->hours_max) {
            $dimension  = 'd';
            $duration   = 1;
        }

        // параметр next_day для сегодняшнего дня
        $next_day_by    = Carbon::parse($now->format('Y-m-d ' . $next_day_at . ':00'));
        // начало отсчета со следующего дня ?
        $is_next_day    = $now->gte($next_day_by);
        $is_today       = $next_day_by->gt($now);

        // начало аренды. если начало с сегодня (или круглосуточный) – текущее время, если нет – завтра с открытия филиала
        $starts_at      = ($is_today || $all_day) ? $now : $opened_at->copy()->addDays(1);

        // тариф в часах
        if ($dimension === 'h')  $ends_at = $starts_at->copy()->addHours($duration);
        // тариф в днях
        else if ($dimension === 'd')  $ends_at = $starts_at->copy()->addDays($duration);
        // тариф в минутах
        else if ($dimension === 'm')  $ends_at = $starts_at->copy()->addMinutes($duration);

        else {
            die($dimension);
        }

        // круглосуточный филиал
        if ($all_day) {
            // $ends_at = $ends_at
        }
        // часы работы
        else {
            // если оканчивается до часов работы СЕГОДНЯ – возврат в текущее время
            if ($closed_at->gt($ends_at)) {
                // $ends_at = $ends_at
            }

            // если оканчивается после часов работы – возврат через сутки до бесплатного возврата
            else {
                if ($ends_at->format('d') === $closed_at->format('d')) {
                    $ends_at->addDays(1);
                }

                $ends_at = Carbon::parse($ends_at->format('Y-m-d ' . $return_by . ':00'));
            }
        }

        $this->order->update([
            'starts_at' => $starts_at,
            'ends_at'    => $ends_at
        ]);

        foreach ($this->order->reservations->where('duration', '>', 0) as $reservation) {
            $this->calculateReservationTime($reservation);
        }

        return $this->order;
    }
}
