<?php
namespace App\Services;

use App\OnlineOrder;
use App\Order;

class OnlineOrderService
{
    protected $order;

    public function __construct(OnlineOrder $order)
    {
        $this->order = $order;
    }

    public function process($method)
    {
        return $this->setPaid();
    }

    public function setPaid()
    {
        $amount = $this->getAmount();

        $this->order->update([
            'paid_amount'   => $amount,
            'paid_at'       => now(),
        ]);

        $this->makeOrder();

        return $this->redirect();
    }

    public function makeOrder()
    {
        $order = Order::create([
            'location_id'   => $this->order->location_id,
            'agent_id'      => $this->order->agent_id,
            'client_id'     => $this->order->client_id,
            'status'        => Order::STATUS_DRAFT,
            'type'          => ($this->order->delivery_type == 'self') ? Order::TYPE_BOOK : Order::TYPE_DELIVERY,
            'pledge'        => 'cash',
            'price'         => $this->order->total,
            'dimension'     => $this->order->dimension,
            'duration'      => $this->order->duration,
            'full_price'    => $this->order->price,
            'starts_at'     => $this->order->starts_at,
            'ends_at'       => $this->order->ends_at,
            'book_at'       => $this->order->date . ' ' . $this->order->time,
        ]);

        $this->order->order_id = $order->id;
        $this->order->save();

        return $order;
    }

    public function getAmount()
    {
        $amount = ($this->order->payment_type == OnlineOrder::PAYMENT_TYPE_FULL) ? $this->order->total : ($this->order->total * 15 / 100);

        return $amount;
    }

    public function redirect()
    {
        return redirect()->route('public.success', $this->order);
    }

}