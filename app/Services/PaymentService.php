<?php

namespace App\Services;

use Omnipay\Omnipay;
use App\Models\Payment;
use YandexCheckout\Client;
use App\Models\PaymentMethod;
use App\Models\Withdrawal;

class PaymentService
{
	private $user;
	private $payment;

	public function __construct($payment = null)
	{
		$this->user = auth()->user();
		$this->payment = $payment;
	}

	public function create($product, $price = null)
	{
		$this->payment = $this->user->payments()->create([
			'amount'		=> is_null($price) ? $product->price : $price,
			'model_type'	=> get_class($product),
			'model_id'		=> isset($product->id) ? $product->id : null,
			'status'		=> Payment::STATUS_PENDING
		]);

		return redirect()->route('payments.show', $this->payment)->with('notification', ['success', 'Создан заказ']);
	}

    public function proceed($method)
    {
    	$this->payment->update([
    		'method' => $method
    	]);

    	switch($method)
    	{
    		case Payment::METHOD_YANDEX:
    			return $this->proceedYandex();
    			break;

            case Payment::METHOD_BALANCE:
                return $this->proceedBalance();
                break;

    		default:
    			abort(403);
    	}
    }

    public function proceedBalance()
    {
        $balance = $this->user->balance;
        $canUseBalance = ($balance > $this->payment->amount);

        if(!$canUseBalance)
        {
            return redirect()->route('payments.show', $this->payment)->with('notification', ['error', 'На счету недостаточно средств']);
        }

        $this->user->withdrawals()->create([
            //'payment_method'    => PaymentMethod::TYPE_BALANCE,
            'model_type'        => $this->payment->model_type,
            'model_id'          => $this->payment->model_id,
            'amount'            => $this->payment->amount,
            'status'            => Withdrawal::STATUS_COMPLETED,
        ]);

        $this->setPaid();

        return redirect()->route('news.index')->with('notification', ['success', 'Заказ успешно оплачен']);
    }

    public function proceedYandex()
    {
        $client = new Client();
        $client->setAuth(config('services.yandex.shopId'), config('services.yandex.secret'));

        $idempotenceKey = uniqid($this->payment->id, true);
        $amount = $this->payment->amount;

        $balance = $this->user->balance;
        $canUseBalance = ($balance > 0);

        if($canUseBalance)
        {
            $amount = $this->payment->amount - $balance;
            $this->payment->update([
                'paid_amount' => $amount,
            ]);
        }

        $response = $client->createPayment([
            'amount' => [
                'value'     => (float) $amount,
                'currency'  => 'RUB',
            ],
            'payment_method_data' => [
                'type' => 'bank_card',
            ],
            'confirmation' => [
                'type'          => 'redirect',
                'return_url'    => route('payments.result', $this->payment->id),
            ],
            'description'   => 'Внутренний платеж №' . $this->payment->id,
            'receipt'       => [
                'customer'  => $this->payment->user->full_name,
                'email'     => $this->payment->user->email,
                'items'     => [
                    [
                        'description'   => optional($this->payment->model)->name ?? ($this->payment->model_type == \App\Models\Shares\Share::class ? 'Акция' : 'Неизвестно'),
                        'quantity'      => 1,
                        'amount'        => [
                            'value'     => (float) $amount,
                            'currency'  => 'RUB',
                        ],
                        'vat_code'      => config('app.vat_code'),
                    ]
                ],
            ],
        ], $idempotenceKey);

        $paymentId = $response->getId();

        $this->payment->update([
            'external_id'   => $paymentId,
        ]);

        $redirectUrl = $response->confirmation->getConfirmationUrl();

        /*
        $idempotenceKey = uniqid($this->payment->id, true);

        $response = $client->capturePayment([
            'amount' => [
                'value'     => (float) $this->payment->amount,
                'currency'  => 'RUB',
            ],
        ], $paymentId, $idempotenceKey);
        */

        return redirect($redirectUrl);
    }

    public function checkResult()
    {
        $client = new Client();
        $client->setAuth(config('services.yandex.shopId'), config('services.yandex.secret'));

        $idempotenceKey = uniqid($this->payment->id, true);

        $payment = $client->getPaymentInfo($this->payment->external_id);
        $status = $payment->getStatus();

        if($status == 'waiting_for_capture')
        {
            $paymentId = $this->payment->external_id;
            $idempotenceKey = uniqid($this->payment->id, true);

            $response = $client->capturePayment([
                'amount' => [
                    'value'     => (float) $this->payment->amount,
                    'currency'  => 'RUB',
                ],
            ], $paymentId, $idempotenceKey);

            $status = $response->getStatus();
        }

        if($status == 'succeeded')
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public function result()
    {

    }

    public function setPaid()
    {
        if($this->payment->paid_amount < $this->payment->model->price && $this->payment->method != Payment::METHOD_BALANCE)
        {
            if(!$this->user)
            {
                $this->user = \App\Models\User\User::find(1);
            }
            $this->user->withdrawals()->create([
                //'payment_method'    => PaymentMethod::TYPE_BALANCE,
                'model_type'        => $this->payment->model_type,
                'model_id'          => $this->payment->model_id,
                'amount'            => $this->payment->model->price - $this->payment->paid_amount,
                'status'            => Withdrawal::STATUS_COMPLETED,
            ]);
        }

    	$this->payment->update([
    		'status'	=> Payment::STATUS_PAID
    	]);

    	$this->payment->attachProduct();
    	$this->payment->distributeEarnings();
    	$this->payment->assignShares();
    }

}
