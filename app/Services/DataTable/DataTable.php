<?php

namespace App\Services\DataTable;
use Illuminate\Http\Request;

class DataTable
{
	public $columns;
	public $collection;
	public $baseCollection;
	public $filteredCollection;
	public $items = [];
	public $request;
	public $tableName;
	public $ajaxRoute;
	public $routeName;
	public $customFilters = [];
	public $joins = [];
	public $routeParams = [];

	public $data = [
		'data' 				=> [],
		'draw'				=> null,
		'recordsTotal'		=> null,
		'recordsFiltered'	=> null,
	];

	public function __construct(Request $request, $routeParams = [])
	{
		if(count($routeParams)) $this->routeParams = $routeParams;
		$this->ajaxRoute = route($this->routeName, $this->routeParams);
		//$this->columns = $table->columns;
		$this->request = $request;
		$this->loadDynamicFilters();
	}

	public function loadDynamicFilters()
	{
		// 
	}

	public function setCustomFilters()
	{
		foreach($this->customFilters as $filter)
		{
			foreach($this->columns as $i => $column)
			{
				if(isset($this->request->columns[$i]['search']['value']))
    			{
    				$value = $this->request->columns[$i]['search']['value'];

    				if($column['data'] == $filter['column'])
	    			{

	    				if(isset($filter['joins']))
    					{
    						foreach($filter['joins'] as $table => $fields)
    						{
    							$this->filteredCollection->join($table, $fields[0], '=', $fields[1]);
    						}
    					}

    					if(isset($filter['select']))
	    				{
	    					$this->filteredCollection->select($filter['select'] . '.*');
	    				}

	    				$this->filteredCollection->where(function ($query) use ($filter, $value) {
	    					foreach($filter['queries'] as $queryType => $field)
		    				{
		    					if(isset($filter['select']) && strpos($field, '.') == 0)
		    						$field = $filter['select'] . '.' . $field;

		    					if($queryType == 'where')
		    						$query->where($field, $value);
		    					else if($queryType == 'orWhere')
		    						$query->orWhere($field, $value);
		    					else if($queryType == 'whereLike')
		    						$query->where($field, 'like', '%' . $value . '%');
		    					else if($queryType == 'orWhereLike')
		    						$query->orWhere($field, 'like', '%' . $value . '%');
		    					else
		    						$query->{$queryType}($value);
		    				}
	    				});
	    			}
    			}
			}
		}
	}

	public function applyFilters()
	{
		$columns = $this->columns;
		$request = $this->request;

		$this->filteredCollection = $this->baseCollection->where(function ($query) use ($request, $columns) {
    		foreach($columns as $i => $column)
    		{
    			if(isset($column['filter']) && !$column['filter']['custom'] && isset($request->columns[$i]['search']['value']))
    			{
    				if($column['filter']['type'] == 'select')
    				{
    					$query->where($column['data'], $request->columns[$i]['search']['value']);
    				}

    				else if($column['filter']['type'] == 'input')
    				{
    					$query->where($column['data'], 'like', '%' . $request->columns[$i]['search']['value'] . '%');
    				}
    			}
    		}
    	});

	}

	public function getCollection()
	{
		$orderField = $this->columns[$this->request->order[0]['column']]['data'];
		$orderDirection	= $this->request->order[0]['dir'];

		$this->collection = (clone $this->filteredCollection)
			->orderBy($orderField, $orderDirection)
			->skip($this->request->start)
			->take($this->request->length)
			->get();

		return $this->collection;
	}

	public function mapItem($item)
	{
		return [
			//
		];
	}

	public function mapItems()
	{
		$this->items = $this->collection->map(function ($item) {
			return $item = $this->mapItem($item);
		});
	}

	public function setBaseCollection()
	{
		//
	}

	public function getData()
	{
		$this->setBaseCollection();
		$this->applyFilters();
		$this->setCustomFilters();
		$this->getCollection();
		$this->mapItems();

		$data = [
			'data' 				=> $this->items,
			'draw'				=> $this->request->draw,
			'recordsTotal'		=> $this->collection->count(),
			'recordsFiltered'	=> $this->filteredCollection->count(),
		];

		return $data;
	}

	public function getJs()
	{
		$columns = $this->columns;
		$tableName = $this->tableName;
		$ajaxRoute = $this->ajaxRoute;
		return view('datatables.js', compact('columns', 'tableName', 'ajaxRoute'));
	}

	public function getHtml()
	{
		$columns = $this->columns;
		$tableName = $this->tableName;
		$ajaxRoute = $this->ajaxRoute;
		return view('datatables.html', compact('columns', 'tableName', 'ajaxRoute'));
	}

	/*
	$collectionWithFilters = $baseCollection
		->when($request->search['value'], function ($query) use ($request) {
			$query->where('first_name', 'like', '%' . $request->search['value'] . '%');
			$query->orWhere('last_name', 'like', '%' . $request->search['value'] . '%');
		});
    */
}
