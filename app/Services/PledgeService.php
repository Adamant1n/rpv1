<?php

namespace App\Services;

use PhpOffice\PhpWord\IOFactory;
use PhpOffice\PhpWord\Settings;
use App\Order;

class PledgeService
{
    private $order;

    public function __construct(Order $order)
    {
        $this->order = $order;
        $this->order->loadMissing('client', 'location');
    }

    public function generateDocument()
    {
        // $templateName = Setting::where('name', 'pledge_template')->first()->value;

        $templateName = 'pledge.docx';
        // $template = base_path() . '/storage/app/public/' . $templateName;
        // $template = resource_path('agreements/pledge.docx');
        // $template = base_path() . '/resources/agreements/pledge.docx';

        $template = \App\Setting::where('name', 'pledge_template')->first()->value;
        $templateProcessor = new \PhpOffice\PhpWord\TemplateProcessor(base_path() . '/storage/app/public/' . $template);

        $fields = [
            'order_id'      => $this->order->optional_id,
            'today'         => now()->format('d.m.Y'),
            'legal_name'    => $this->order->location->legal_name,
            'client_age'    => $this->order->client->birthday ? $this->order->client->age : '–',
            'client_name'   => $this->order->client->full_name,
            'client_phone'  => $this->order->client->phone,
            'client_weight' => $this->order->client->feature->weight,
            'client_height' => $this->order->client->feature->height,
            'client_shoe'   => $this->order->client->feature->shoe_soze,
            'date_start'    => optional($this->order->starts_at)->format('H:i d.m.Y'),
            'date_end'      => optional($this->order->ends_at)->format('H:i d.m.Y'),
            'price'         => $this->order->price,
            'pledge_amount' => $this->order->pledge_amount,
        ];

        // $templateProcessor = new \PhpOffice\PhpWord\TemplateProcessor($template);

        foreach($fields as $field => $value)
        {
            $templateProcessor->setValue($field, $value);
        }

        $temp_file = tempnam(sys_get_temp_dir(), 'PHPWord');
        $templateProcessor->saveAs($temp_file);

        $phpWord = \PhpOffice\PhpWord\IOFactory::load($temp_file);

        // генерация штрихкода
        $generator = new \Picqer\Barcode\BarcodeGeneratorJPG();
        $file = $generator->getBarcode($this->order->optional_id, $generator::TYPE_CODE_128_A);
        $barcode_file = base_path() . '/storage/app/public/' . $this->order->optional_id .  '.jpg';
        file_put_contents($barcode_file, $file);

        // добавить штрихкод
        $sections = $phpWord->getSections();
        $header = $sections[0]->addHeader();
        $header->addImage($barcode_file, ['align' => 'center']);
        // $phpWord->save($temp_file);

        header("Content-Disposition: attachment; filename=\"" . $this->order->optional_id . "-залог.docx\"");
        $phpWord->save('php://output');
        // readfile($temp_file);
        // unlink($temp_file);
    }

}