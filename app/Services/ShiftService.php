<?php

namespace App\Services;

use App\User;
use App\Shift;
use Webpatser\Uuid\Uuid;

class ShiftService
{
    private $user;

    public function __construct(User $user = null)
    {
        if(!$user)
        {
            $user = auth()->user();
        }

        $this->user = $user;
    }

    public function openShiftIfNeeded()
    {
        if($this->user->shifts()->opened()->count() > 0)
        {
            return false;
        }

        $uuid = Uuid::generate()->string;

        $request = [
            'uuid'      => $uuid,
            'request'   => [
                'type'      => 'openShift',
                'operator'  => [
                    'name'  => $this->user->last_name,
                    'vatin' => $this->user->atol_id
                ]
            ]
        ];

        $client = new \GuzzleHttp\Client();
        $url = config('services.atol.url') . 'requests';
        try
        {
            info('Shift service – try');
            $response = $client->request('POST', $url, ['json' => $request]);
            info('Shift service – response');
            info((string) $response->getBody());
        }
        catch(\Exception $e)
        {
            info('Shift service – error');
            info($e->getMessage());
        }

        $this->user->shifts()->create([
            'uuid'      => $uuid,
            'opened_at' => now()
        ]);
    }

    public function closeShift($shift)
    {
        $uuid = Uuid::generate()->string;

        $request = [
            'uuid'      => $uuid,
            'request'   => [
                'type'      => 'closeShift',
                'operator'  => [
                    'name'  => $shift->user->last_name,
                    'vatin' => $shift->user->atol_id
                ]
            ]
        ];

        $client = new \GuzzleHttp\Client();
        $url = config('services.atol.url') . 'requests';
        try
        {
            $response = $client->request('POST', $url, ['json' => $request]);
        }
        catch(\Exception $e)
        {
            
        }

        $shift->update([
            'closed_at' => now()
        ]);
    }

    public function closePendingShift()
    {
        $shift = $this->user
            ->shifts()
            ->opened()
            ->latest()
            ->first();

        if($shift->is_closed || !$shift || !now()->subHours(20)->gte($shift->opened_at))
        {
            return true;
        }

        $this->closeShift($shift);
    }
}