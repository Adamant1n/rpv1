<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Binding extends Model
{
	use \App\Traits\LocationIdentificator;

	public $incrementing = false;
	protected $primaryKey = 'id';

	// protected $guarded = ['id'];
	protected $fillable = [
		'id',
		'name',
		'created_at',
		'updated_at',
	];

	protected $dates = [
		'created_at',
		'updated_at'
	];

	public function units()
	{
		return $this->hasMany(Unit::class);
	}

	public function products()
	{
		return $this->hasManyThrough(Unit::class, Product::class);
	}
}
