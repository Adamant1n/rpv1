<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Condition extends Model
{
	// protected $guarded = ['id'];

	protected $fillable = [
		'id',
		'name',
		'created_at',
		'updated_at',
	];

	protected $dates = [
		'created_at',
		'updated_at'
	];
}
