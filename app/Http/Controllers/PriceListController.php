<?php

namespace App\Http\Controllers;

use App\Tariff;
use App\Category;
use App\PriceList;
use App\Location;
use Illuminate\Http\Request;

class PriceListController extends Controller
{
	public function __construct()
	{
		$this->middleware(function ($request, $next) {

			$this->user = auth()->user();

			return $next($request);
		});

		$this->authorizeResource(PriceList::class);
	}

	public function index(Request $request)
	{
		if ($this->user->hasRole('superadmin'))
		{
			if (empty($request->location))
				$price_lists = PriceList::all();
			else
				$price_lists = PriceList::where('location_id', $request->location)->get();

			$locations = Location::all();
		}
		else
		{
			$price_lists = PriceList::where('location_id', $this->user->location_id)->get();

			$locations = Location::find($this->user->location_id);
		}

		return view('price_lists.index', compact('price_lists','locations','request'));
	}

	public function create()
	{
		$locations = Location::all();

		return view('price_lists.create', compact('locations'));
	}

	public function store(Request $request)
	{
		request()->validate([
			'name' => 'required|string|max:100'
		]);

		$data = $request->all();

		if (!$this->user->hasRole('superadmin'))
			$data['location_id'] = $this->user->location_id;

		PriceList::create($data);

		return redirect()->route('price_lists.index')->with('success', 'Прейскурант успешно создан');
	}

	public function show($id)
	{
		//
	}

	public function timeline(Request $request, PriceList $price_list)
	{
		$price_list->update($request->all());
		return back()->with('success', 'Тарифная сетка успешно обновлена');
	}

	public function edit(PriceList $price_list)
	{
		$locations = Location::all();
		$categories = Category::all();
		// $price_list->load('tariffs');

		$casts = ['m' => 'мин.', 'h' => 'ч.', 'd' => 'д.'];

		$timeline = [
			'm' => [
				10 => 0,
				20 => 0,
				30 => 0,
			],
			'h' => [
				1 => 0,
				2 => 0,
				3 => 0,
				4 => 0,
				5 => 0,
				6 => 0,
				7 => 0
			],
			'd' => [
				1 => 0,
				2 => 0,
				3 => 0,
				4 => 0,
				5 => 0,
				6 => 0,
				7 => 0
			]
		];

		$table = [];

		// на случай, если добавилась категория, заполняем нулями
		foreach($categories as $category) {
			$table[$category->id] = [
				'name' => $category->name,
				'timeline' => $timeline
			];
		}

		if($price_list->tariffs) {
			foreach($price_list->tariffs as $category_id => $tariff) {
				foreach($tariff as $dimension => $durations) {
					foreach($durations as $duration => $value) {
						$table[$category_id]['timeline'][$dimension][$duration] = $value;
					}
				}
			}
		}

		/*
		foreach($price_list->tariffs as $tariff) {
			foreach($table[$tariff->category_id]['timeline'] as $i => $time) {
				if($time['duration'] == $tariff->duration && $time['dimension'] == $tariff->dimension) {
					$table[$tariff->category_id]['timeline'][$i]['value'] = $tariff->price;
				}
			}
		}
		*/

		return view('price_lists.edit', compact('price_list', 'locations', 'categories', 'table', 'timeline', 'casts'));
	}

	public function update(Request $request, PriceList $price_list)
	{
		request()->validate([
			'name' => 'required|string|max:100'
		]);

		$price_list->update($request->all());

		return redirect()->route('price_lists.index')->with('success', 'Прейскурант успешно сохранен');
	}

	public function destroy(PriceList $price_list)
	{
		$price_list->delete();

		return redirect()->route('price_lists.index')->with('success', 'Прейскурант удален');
	}
}
