<?php

namespace App\Http\Controllers\Instructors;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Instructors\Setting;

class SettingsController extends Controller
{
	public function __construct()
	{
		$this->middleware(['role:superadmin|admin']);
	}

    public function edit()
    {
        /// $this->authorize('manageSettings', Setting::class);

    	$template = Setting::where('name', 'voucher')->first();
    	$sms = Setting::firstOrCreate(
    		['name'	=> 'sms'],
    		[
    			'value' => '{name}, ваше занятие {date} с {time_start} до {time_end} подтверждено.'
    		]
    	);

    	return view('instructors.settings.edit', compact('template', 'sms'));
    }

    public function update(Request $request)
    {
        // $this->authorize('manageSettings', Setting::class);
        
        if($request->has('template'))
        {
	    	$template = $request->template->store('templates', 'public');

	    	Setting::updateOrCreate(
	    		['name' => 'voucher'],
	    		['value' => $template]
	    	);
	    }

    	Setting::updateOrCreate(
    		['name' => 'sms'],
    		['value' => $request->sms]
    	);

    	return redirect()->back()->with('success', 'Настройки обновлены');
    }
}
