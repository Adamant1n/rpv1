<?php

namespace App\Http\Controllers\Instructors;

use App\Http\Controllers\Controller;
use App\Instructors\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Schema;

class ClientController extends Controller
{
	public function __construct()
	{
		$this->middleware(['role:superadmin|admin|manager|operator']);
	}

	public function index()
	{
		return view('instructors.clients.index');
	}

	public function clients_json(Request $request)
	{
		$columns = Schema::getColumnListing('clients');

		$clients = Client::where([])
						->orderBy(
							$columns[$request->order[0]['column']],
							$request->order[0]['dir']
						);


		if ($request->search['value'] !== null)
		{
			foreach ($columns as $column)
				$clients->orWhere($column, 'like', '%'.$request->search['value'].'%');
		}

        $filteredCount = $clients->count();
        
		$clients = $clients->offset($request->start)
            ->limit($request->length)
            ->get();

		$output = [];

		foreach ($clients as $client)
		{
			$output[] = [
				$client->id,
				$client->last_name.' '.$client->first_name.' '.$client->mid_name,
				$client->phone,
				$client->email,
				$client->created_at->format('d.m.Y H:i'),
				$client->updated_at->format('d.m.Y H:i')
			];
		}

		return json_encode([
			'draw' => $request->draw,
			'recordsTotal' => Client::count(),
			'recordsFiltered' => $filteredCount,
			'data' => $output
		]);
	}

	public function create()
	{
		return view('instructors.clients.create');
	}

	public function store(Request $request)
	{
		request()->validate([
			'last_name'   => 'required|string|max:50',
			'first_name'  => 'required|string|max:50',
			'mid_name'    => 'required|string|max:50'
		]);

		$client = Client::create($request->except('ajax'));

		if($request->ajax)
		{
			return response()->json([
				'success' => true,
				'name' => $client->full_name,
				'id' => $client->id
			]);
		}
		return redirect()->route('instructors.clients.index', $client)
			->with('success', 'Клиент успешно добавлен');
	}

	public function show()
	{
		//
	}

	public function edit(Client $client)
	{
		return view('instructors.clients.edit', compact('client'));
	}

	public function update(Request $request, Client $client)
	{
		request()->validate([
			'last_name'   => 'required|string|max:50',
			'first_name'  => 'required|string|max:50',
			'mid_name'    => 'required|string|max:50'
		]);

		$client->update($request->all());

		return redirect()->route('instructors.clients.index', $client)
			->with('success', 'Данные успешно сохранены');
	}

	public function destroy(Client $client)
	{
		$client->delete();

		return redirect()->route('instructors.clients.index')->with('success', 'Клиент удален');
	}
}
