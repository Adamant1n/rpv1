<?php

namespace App\Http\Controllers\Instructors;

use App\Http\Controllers\Controller;
use App\Instructors\Service;
use App\Instructors\Tariff;
use Illuminate\Http\Request;

class ServiceController extends Controller
{
	public function __construct()
	{
		$this->middleware(['role:superadmin|admin']);
	}

	public function index()
	{
		$services = Service::all();

		return view('instructors.service.index', compact('services'));
	}

	public function create()
	{
		return view('instructors.service.create');
	}

	public function store(Request $request, Service $service)
	{
		request()->validate([
			'name' => 'required|string|max:100'
		]);

		$service->create($request->all());

		return redirect()->route('instructors.service.index')->with('success', 'Услуга успешно создана');
	}

	public function show($id)
	{
		//
	}

	public function edit(Service $service)
	{
		return view('instructors.service.edit', compact('service'));
	}

	public function update(Request $request, Service $service)
	{
		request()->validate([
			'name' => 'required|string|max:100'
		]);

		$service->update($request->all());

		return redirect()->route('instructors.service.index')->with('success', 'Услуга успешно обновлена');
	}

	public function destroy($id)
	{
		$service = Service::find($id);

		$service->tariffs()->delete();

		$service->delete();

		return redirect()->route('instructors.service.index')->with('success', 'Услуга успешно удалена');
	}
}
