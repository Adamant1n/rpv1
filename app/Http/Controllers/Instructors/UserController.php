<?php

namespace App\Http\Controllers\Instructors;

use App\Http\Controllers\Controller;
use App\User;
use App\Instructors\Category;
use App\Instructors\Location;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;

class UserController extends Controller
{
	public function index(Request $request)
	{
		if ($request->has('trashed'))
			$users = User::onlyTrashed()->get();
		else
			$users = User::all();

		$locations = Location::withoutGlobalScopes()->get();

		return view('instructors.user.index', compact('users','locations','request'));
	}

	public function create()
	{
		$categories = Category::all();
		$roles = Role::all();
		$locations = Location::all();

		return view('instructors.user.create', compact('categories','roles','locations'));
	}

	public function store(Request $request)
	{
		request()->validate([
			'last_name'   => 'required|string|max:50',
			'first_name'  => 'required|string|max:50',
			//'middle_name' => 'required|string|max:50',
			'phone'       => 'required|string|max:50',
			'email'       => 'required|email|unique:users',
			'password'    => 'required|min:6',
		]);

		$data = $request->except('location_id', 'password', 'role');

		if(!empty($request->password))
		{
			$data['password'] = bcrypt($request->password);
		}

		if(!is_array($request->location_id))
		{
			$data['location_id'] = $request->location_id;
		}
		else
		{
			// $user->locations()->sync($request->location_id);
		}

		$user = User::create($data);
		$user->assignRole($request->role);

		if(is_array($request->location_id))
		{
			$user->locations()->sync($request->location_id);
		}

		return redirect()->route('instructors.user.index')->with('success', 'Пользователь успешно создан');
	}

	public function show($id)
	{
		//
	}

	public function edit($id)
	{
		$categories = Category::all();
		$roles = Role::all();
		$locations = Location::all();
		$user = User::withTrashed()->find($id);

		$location_ids = $user->locations()->pluck('id')->toArray();

		return view('instructors.user.edit', compact('user','categories','roles','locations','location_ids'));
	}

	public function update(Request $request, User $user)
	{
		request()->validate([
			'last_name'   => 'required|string|max:50',
			'first_name'  => 'required|string|max:50',
			'phone'       => 'required|string|max:50',
			'email'       => 'required|email',
		]);

		$user->syncRoles($request->role);

		$data = $request->except('location_id', 'password', 'role');

		if(!empty($request->password))
		{
			$data['password'] = bcrypt($request->password);
		}

		if(!is_array($request->location_id))
		{
			$data['location_id'] = $request->location_id;
		}
		else
		{
			$user->locations()->sync($request->location_id);
		}

		$user->update($data);

		/*
		$user->update([
			'first_name' => $request->first_name,
			'last_name' => $request->last_name,
			'middle_name' => $request->middle_name,
			'email' => $request->email,
			'phone' => $request->phone,
			'password' => !empty($request->password) ? Hash::make($request->password) : $user->password,
			'location_id' => $request->location_id,
			'category_id' => $request->category_id,
			'rate' => $request->rate,
		]);
		*/

		return redirect()->route('instructors.user.index')->with('success', 'Данные успешно обновлены');
	}

	public function destroy($id)
	{
		User::destroy($id);

		return redirect()->route('instructors.user.index')->with('success', 'Пользователь заблокирован');
	}

	public function restore($id)
	{
		User::withTrashed()->find($id)->restore();

		return redirect()->route('instructors.user.index')->with('success', 'Пользователь разблокирован');
	}
}
