<?php

namespace App\Http\Controllers\Instructors;

use App\Http\Controllers\Controller;

use App\Instructors\Location;
use Illuminate\Http\Request;

class LocationController extends Controller
{
	public function index()
	{
		$locations = Location::all();

		return view('instructors.location.index', compact('locations'));
	}

	public function create()
	{
		return view('instructors.location.create');
	}

	public function store(Request $request, Location $location)
	{
		request()->validate([
			'address' => 'required|string|max:100'
		]);

		$location->create($request->all());

		return redirect()->route('instructors.location.index')->with('success', 'Точка продаж успешно создана');
	}

	public function show($id)
	{
		//
	}

	public function edit(Location $location)
	{
		return view('instructors.location.edit', compact('location'));
	}

	public function update(Request $request, Location $location)
	{
		request()->validate([
			'address' => 'required|string|max:100'
		]);

		$location->update($request->all());

		return redirect()->route('instructors.location.index')->with('success', 'Точка продаж успешно сохранена');
	}

	public function destroy($id)
	{
		Location::destroy($id);

		return redirect()->route('instructors.location.index')->with('success', 'Точка продаж удалена');
	}
}
