<?php

namespace App\Http\Controllers\Instructors;

use App\Http\Controllers\Controller;
use App\Instructors\Event;
use App\Instructors\Service;
use App\Instructors\Category;
use App\Instructors\Client;
use App\Instructors\Location;
use App\Instructors\Tariff;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Schema;

class ReportController extends Controller
{
	public function __construct()
	{
		$this->middleware(['role:superadmin|admin']);
	}

	public function sales(Request $request)
	{
		$locations = Location::all();

		$services = Service::all();

		if ($request->has('location'))
		{
			$managers = auth()->user()->role('manager')
				->where('location_id', $request->location)->get();
			$trainers = auth()->user()->role('trainer')
				->where('location_id', $request->location)->get();
		}
		else
		{
			$managers = auth()->user()->role('manager')->get();
			$trainers = auth()->user()->role('trainer')->get();
		}

		return view('instructors.report.sales', compact('services','locations','managers','trainers'));
	}

	public function sales_json(Request $request)
	{
		$where = [['payment_status', true]];

		if ($request->location !== null)
			$where[] = ['location_id', $request->location];

		if ($request->service !== null)
			$where[] = ['service_id', $request->service];

		if ($request->manager !== null)
			$where[] = ['manager_id', $request->manager];

		if ($request->trainer !== null)
			$where[] = ['trainer_id', $request->trainer];

		if ($request->date_start !== null)
			$where[] = ['event_date', '>=', $request->date_start];

		if ($request->date_end !== null)
			$where[] = ['event_date', '<=', $request->date_end];

		$columns = [
			'client_id',
			null,
			'sale_date',
			'event_date',
			null,
			'service_id',
			'tariff_id',
			'number_people',
			'category_id',
			'location_id',
			null,
			'manager_id',
			'trainer_id',
			null
		];

		$events = Event::where($where)
						->offset($request->start)
						->limit($request->length)
						->orderBy(
							$columns[$request->order[0]['column']],
							$request->order[0]['dir']
						)->get();

		$output = [];

		foreach ($events as $event)
		{
			$client = Client::find($event->client_id);

			$tariff = Tariff::find($event->tariff_id);

			$manager = auth()->user()->withTrashed()->find($event->manager_id);

			$trainer = auth()->user()->withTrashed()->find($event->trainer_id);

			$output[] = [
				$client->last_name.' '.$client->first_name.' '.$client->mid_name,
				$client->phone,
				$event->sale_date->format('d.m.Y'),
				$event_date = $event->event_date->format('d.m.Y'),
				$min = (strtotime($event_date.' '.$event->end_time) - strtotime($event_date.' '.$event->start_time)) / 60,
				Service::find($event->service_id)->name,
				$tariff->name,
				$event->number_people,
				Category::find($event->category_id)->name,
				Location::find($event->location_id)->address,
				round($tariff->cost * ($min / 60)),
				$manager->last_name.' '.$manager->first_name.' '.$manager->middle_name,
				$trainer->last_name.' '.$trainer->first_name.' '.$trainer->middle_name,
				round($trainer->rate * ($min / 60))
			];
		}

		return json_encode([
			'draw' => $request->draw,
			'recordsTotal' => Event::count(),
			'recordsFiltered' => count($output),
			'data' => $output
		]);
	}

	public function trainers(Request $request)
	{
		$trainers = auth()->user()->role('trainer')->get();

		return view('instructors.report.trainers', compact('trainers','request'));
	}

	public function efficiency(Request $request)
	{
		$locations = Location::all();

		if ($request->has('location'))
			$managers = auth()->user()->role('manager')
					->where('location_id', $request->location)->get();
		else
			$managers = auth()->user()->role('manager')->get();

		return view('instructors.report.efficiency', compact('locations','managers','request'));
	}

}
