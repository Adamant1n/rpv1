<?php

namespace App\Http\Controllers\Instructors;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class HomeController extends Controller
{
	public function __construct()
	{
		$this->middleware(['role:superadmin|admin|manager|operator']);
	}

	public function download($file)
	{
		return response()->download(storage_path($file));
	}
}
