<?php

namespace App\Http\Controllers\Instructors;

use App\Http\Controllers\Controller;
use App\Instructors\Location;
use App\Instructors\Client;
use Illuminate\Http\Request;

class YandexCallController extends Controller
{
	public function callback(Request $request)
	{
		$headers = apache_request_headers();
		if(isset($headers['Echo'])) {
			$echo = $headers["Echo"];
			header("Echo: " . $echo);
		}

		if(isset($request->Body)) {
			// file_put_contents('debug.txt', json_encode($request->all()) . "\r\n", FILE_APPEND);
		}

		/*
		$post = json_decode(file_get_contents('php://input'), true);
		$str .= "From : ".$post["Body"]["From"].PHP_EOL;
		$str .= "To: ".$post["Body"]["To"].PHP_EOL;
		$str .= "Call ID: ".$post["Body"]["Id"].PHP_EOL;
		
		file_put_contents('call.txt', $str . PHP_EOL, FILE_APPEND);
		*/

		echo 'OK';
	}

	public function getClient(Request $request)
	{
		$phone = $request->phone;
		$phone = str_replace('+7', '', $phone);
		$phone = str_replace(' ', '', $phone);
		$phone = str_replace('-', '', $phone);

		$client = Client::where('phone', 'like', '%' . $phone . '%')
			->with('events.manager', 'events.trainer', 'events.category', 'events.tariff', 'events.location')
			->first();

		if(!is_null($client)) {
			return response()->json([
				'success' => true,
				'client' => $client
			]);
		}
		else {
			return response()->json([
				'success' => false
			]);
		}
	}
}
