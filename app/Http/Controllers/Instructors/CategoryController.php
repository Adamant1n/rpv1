<?php

namespace App\Http\Controllers\Instructors;

use App\Http\Controllers\Controller;
use App\Instructors\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
	public function __construct()
	{
		$this->middleware(['role:superadmin|admin']);
	}

	public function index()
	{
		$categories = Category::all();

		return view('instructors.category.index', compact('categories'));
	}

	public function create()
	{
		return view('instructors.category.create');
	}

	public function store(Request $request, Category $category)
	{
		request()->validate([
			'name' => 'required|string|max:100'
		]);

		$category->create($request->all());

		return redirect()->route('instructors.category.index')->with('success', 'Категория успешно создана');
	}

	public function show($id)
	{
		//
	}

	public function edit(Category $category)
	{
		return view('instructors.category.edit', compact('category'));
	}

	public function update(Request $request, Category $category)
	{
		request()->validate([
			'name' => 'required|string|max:100'
		]);

		$category->update($request->all());

		return redirect()->route('instructors.category.index')->with('success', 'Категория успешно сохранена');
	}

	public function destroy($id)
	{
		Category::destroy($id);

		return redirect()->route('instructors.category.index')->with('success', 'Категория удалена');
	}
}
