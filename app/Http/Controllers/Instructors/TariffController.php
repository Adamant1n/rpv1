<?php

namespace App\Http\Controllers\Instructors;

use App\Http\Controllers\Controller;
use App\Instructors\Tariff;
use App\Instructors\Service;
use Illuminate\Http\Request;

class TariffController extends Controller
{
	public function __construct()
	{
		$this->middleware(['role:superadmin|admin']);
	}

	public function index()
	{
		$tariffs = Tariff::all();

		$services = Service::all();

		return view('instructors.tariff.index', compact('tariffs','services'));
	}

	public function create()
	{
		$services = Service::all();

		return view('instructors.tariff.create', compact('services'));
	}

	public function store(Request $request, Tariff $tariff)
	{
		request()->validate([
			'name'       => 'required|string|max:100',
			'cost'       => 'required|integer',
			'service_id' => 'required|integer'
		]);

		$tariff->create($request->all());

		return redirect()->route('instructors.tariff.index')->with('success', 'Тариф успешно создан');
	}

	public function show($id)
	{
		//
	}

	public function edit(Tariff $tariff)
	{
		$service = Service::find($tariff->service_id)->name;

		return view('instructors.tariff.edit', compact('tariff','service'));
	}

	public function update(Request $request, Tariff $tariff)
	{
		request()->validate([
			'name' => 'required|string|max:100',
			'cost' => 'required|integer'
		]);

		$tariff->update($request->all());

		return redirect()->route('instructors.tariff.index')->with('success', 'Тариф успешно обновлен');
	}

	public function destroy($id)
	{
		Tariff::destroy($id);

		return redirect()->route('instructors.tariff.index')->with('success', 'Тариф успешно удален');
	}
}
