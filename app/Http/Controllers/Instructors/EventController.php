<?php

namespace App\Http\Controllers\Instructors;

use App\Http\Controllers\Controller;
use App\Instructors\Event;
use App\Instructors\Service;
use App\Instructors\Category;
use App\Instructors\Client;
use App\Instructors\Location;
use App\Instructors\Tariff;
use App\Instructors\Agent;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Schema;
use App\Instructors\Mailers\Devino;
use Exception;
use App\Instructors\Setting;
use App\Services\ShiftService;

class EventController extends Controller
{
	public function __construct()
	{
		$this->middleware(['role:superadmin|admin|manager|operator']);
	}

	public function pay(Request $request, Event $event)
	{
		$event->update([
			'payment_status'	=> 1,
			'online_payment'	=> 1,
			'paid_by'			=> $request->paid_by,
			'paid_at'			=> now()
		]);

        $shiftService = new ShiftService(auth()->user());
        $shiftService->openShiftIfNeeded();

		return redirect()->back()->with('success', 'Заказ оплачен');
	}

	public function index(Request $request)
	{
		$services = Service::all();
		$locations = Location::all();
		$clients = Client::all();

		if ($request->has('location'))
		{
			$managers = auth()->user()->role('manager')
				->where('location_id', $request->location)->get();
			$trainers = auth()->user()->role('trainer')
				->where('location_id', $request->location)->get();
		}
		else
		{
			$managers = auth()->user()->role('manager')->get();
			$trainers = auth()->user()->role('trainer')->get();
		}

		return view('instructors.event.index', compact(
			'services',
			'locations',
			'managers',
			'trainers',
			'clients'
		));
	}

	public function events_json(Request $request)
	{
		$services = Service::all();

		$where = [];

		$user = auth()->user();

		if ($user->hasRole('trainer'))
		{
			$where[] = ['trainer_id', $user->id];
		}
		elseif ($user->hasRole('manager'))
		{
			$where[] = ['location_id', $user->location_id];
		}
		else
		{
			if ($request->location !== null)
				$where[] = ['location_id', $request->location];

			if ($request->service !== null)
				$where[] = ['servicet_id', $request->service];

			if ($request->manager !== null)
				$where[] = ['manager_id', $request->manager];

			if ($request->trainer !== null)
				$where[] = ['trainer_id', $request->trainer];

			if ($request->payment_status !== null)
				$where[] = ['payment_status', $request->payment_status];

			if ($request->online_payment !== null)
				$where[] = ['online_payment', $request->online_payment];
		}

		$events = Event::where($where)->get();

		$output = [];

		foreach ($events as $event)
		{
			$manager = auth()->user()->withTrashed()->find($event->manager_id);

			$trainer = auth()->user()->withTrashed()->find($event->trainer_id);

			$client = Client::find($event->client_id);

			$output[] = [
				$event->id,
				$manager->last_name.' '.$manager->first_name.' '.$manager->middle_name,
				$trainer->last_name.' '.$trainer->first_name.' '.$trainer->middle_name,
				$client->last_name.' '.$client->first_name.' '.$client->mid_name,
				$services->find($event->service_id)->name,
				$event_date = $event->event_date->format('d.m.Y'),
				substr($event->start_time, 0, 5),
				((strtotime($event_date.' '.$event->end_time) - strtotime($event_date.' '.$event->start_time)) / 60).' мин',
				$event->cost,
				$event->payment_status == 1 ? 'Да' : 'Нет',
				$event->online_payment == 1 ? 'Онлайн' : 'Оффлайн',
			];
		}

		return json_encode([
			'data' => $output
		]);
	}

	public function events_json_ajax(Request $request)
	{
		$where = [];

		$user = auth()->user();

		if ($user->hasRole('trainer'))
			$where[] = ['trainer_id', $user->id];
		elseif ($user->hasRole('manager'))
			$where[] = ['location_id', $user->location_id];
		else
		{
			if ($request->location !== null)
				$where[] = ['location_id', $request->location];

			if ($request->service !== null)
				$where[] = ['servicet_id', $request->service];

			if ($request->manager !== null)
				$where[] = ['manager_id', $request->manager];

			if ($request->trainer !== null)
				$where[] = ['trainer_id', $request->trainer];

			if ($request->payment_status !== null)
				$where[] = ['payment_status', $request->payment_status];

			if ($request->online_payment !== null)
				$where[] = ['online_payment', $request->online_payment];
		}

		$columns = Schema::getColumnListing('events');

		$events = Event::where($where)
						->offset($request->start)
						->limit($request->length)
						->orderBy(
							$columns[$request->order[0]['column']],
							$request->order[0]['dir']
						);

		if ($request->search['value'] !== null)
		{
			foreach ($columns as $column)
				$events->orWhere($column, 'like', '%'.$request->search['value'].'%');
		}

		$events = $events->get();

		$output = [];

		foreach ($events as $event)
		{
			$manager = auth()->user()->find($event->manager_id);
			$trainer = auth()->user()->find($event->trainer_id);
			$client = Client::find($event->client_id);

			$output[] = [
				$event->id,
				$manager->last_name.' '.$manager->first_name.' '.$manager->middle_name,
				$trainer->last_name.' '.$trainer->first_name.' '.$trainer->middle_name,
				$client->last_name.' '.$client->first_name.' '.$client->mid_name,
				Service::find($event->service_id)->name,
				$event_date = $event->event_date->format('d.m.Y'),
				substr($event->start_time, 0, 5),
				((strtotime($event_date.' '.$event->end_time) - strtotime($event_date.' '.$event->start_time)) / 60).' мин',
				$event->cost,
				$event->payment_status == 1 ? 'Да' : 'Нет',
				$event->online_payment == 1 ? 'Онлайн' : 'Оффлайн',
			];
		}

		return json_encode([
			'draw' => $request->draw,
			'recordsTotal' => Event::count(),
			'recordsFiltered' => count($output),
			'data' => $output
		]);
	}

	public function create(Request $request)
	{
		$locations = Location::with('trainers')->get();
		$agents = Agent::all();
		$services = Service::all();
		$categories = Category::all();
		$clients = Client::all();
		$tariffs = Tariff::all();

		if ($request->has('location_id'))
			$managers = auth()->user()->role('manager')->where('location_id', $request->location_id)->get();
		else
			$managers = auth()->user()->role('manager')->get();

		// $trainers = auth()->user()->role('trainer')->get();

		$user = auth()->user();

		if(!empty($user->location_id))
		{
			$trainers = $user->location->trainers()->get();
		}
		else
		{
			$trainers = User::role('trainer')->get();
		}

        $managerLocation = auth()->user()->location;

		return view('instructors.event.create', compact(
			'services',
			'categories',
			'managers',
			'trainers',
			'clients',
			'tariffs',
			'locations',
			'request',
			'agents',
            'managerLocation'
		));
	}

	public function store(Request $request)
	{
		request()->validate([
			'manager_id'     => 'required|integer',
			'trainer_id'     => 'required|integer',
			'client_id'      => 'required|integer',
			'service_id'     => 'required|integer',
			'tariff_id'      => 'required|integer',
			'category_id'    => 'required|integer',
			'location_id'       => 'required|integer',
			'number_people'  => 'required|integer',
			'cost'           => 'required|integer',
			//'payment_status' => 'required|boolean',
			'online_payment' => 'required|boolean',
			'event_date'     => 'required|date',
			//'sale_date'      => 'required|date',
			'start_time'     => 'required',
			'end_time'       => 'required'
		]);

		$event = Event::create([
			'manager_id'     => $request->manager_id,
			'trainer_id'     => $request->trainer_id,
			'client_id'      => $request->client_id,
			'service_id'     => $request->service_id,
			'category_id'    => $request->category_id,
			'tariff_id'      => $request->tariff_id,
			'location_id'       => ($request->manager_id) ?
				auth()->user($request->manager_id)->location_id : null,
			'number_people'  => $request->number_people,
			'cost'           => $request->cost,
			'agent_id'		 => $request->agent_id,
			'discount'		 => $request->discount,
			'full_cost'		 => $request->full_cost,
			'payment_status' => $request->payment_status,
			'online_payment' => $request->online_payment,
			'event_date'     => $request->event_date,
			'sale_date'      => \Carbon\Carbon::now(),//$request->sale_date,
			'start_time'     => $request->start_time,
			'end_time'       => $request->end_time
		]);

		//$trainer = auth()->user()->find($event->trainer_id);

		//$service = Service::find($event->service_id)->name;

		//$text_sms = $service.', '.$event->event_date->format('d.m.Y').' с '.$event->start_time.' по '.$event->end_time;

		//$config = config('mailer.devino');
		//$sender = new SmsClient($config['login'], $config['password']);
		//$sender->getSessionID();
		//$sender->send($config['sourceAddress'], $trainer->phone, $text_sms);

		return redirect()->route('instructors.event.edit', $event->id);
	}

	public function show(Event $event)
	{
		$event_date = $event->event_date->format('d.m.Y');
		$start_time = substr($event->start_time, 0, 5);
		$end_time = substr($event->end_time, 0, 5);
		$duration = ((strtotime($event_date.' '.$end_time) - strtotime($event_date.' '.$start_time)) / 60);
		$manager = auth()->user()->find($event->manager_id);
		$trainer = auth()->user()->find($event->trainer_id);
		$client = Client::find($event->client_id);
		$category = Category::find($event->category_id)->name;
		$service = Service::find($event->service_id)->name;
		$tariff = Tariff::find($event->tariff_id)->name;

		//$templateProcessor = new \PhpOffice\PhpWord\TemplateProcessor(base_path() . '/resources/agreements/voucher.docx');
		//$templateProcessor = new \PhpOffice\PhpWord\TemplateProcessor(base_path() . '/resources/agreements/voucher.docx');

		$template = Setting::where('name', 'voucher')->first()->value;
		$templateProcessor = new \PhpOffice\PhpWord\TemplateProcessor(base_path() . '/storage/app/public/' . $template);

        $templateProcessor->setValue('date', $event_date);
        $templateProcessor->setValue('start', $start_time);
        $templateProcessor->setValue('end', $end_time);
        $templateProcessor->setValue('duration', $duration);
        $templateProcessor->setValue('admin', $manager->full_name);
        $templateProcessor->setValue('instructor', $trainer->full_name);
        $templateProcessor->setValue('tariff', $tariff);
        $templateProcessor->setValue('service', $service);
        $templateProcessor->setValue('category', $category);
        $templateProcessor->setValue('name', $client->full_name);
        $templateProcessor->setValue('created_at', $event->created_at->format('d.m.Y'));
        $templateProcessor->setValue('phone', $client->phone);
        $templateProcessor->setValue('people', $event->number_people);
        $templateProcessor->setValue('instructor_phone', $trainer->phone);
        $templateProcessor->setValue('price', $event->cost);

        $temp_file = tempnam(sys_get_temp_dir(), 'PHPWord');
        $templateProcessor->saveAs($temp_file);

        header("Content-Disposition: attachment; filename=\"voucher.docx\"");
        readfile($temp_file);
        unlink($temp_file);
	}

	public function show_old(Event $event)
	{
		$event_date = $event->event_date->format('d.m.Y');
		$start_time = substr($event->start_time, 0, 5);
		$end_time = substr($event->end_time, 0, 5);
		$duration = ((strtotime($event_date.' '.$end_time) - strtotime($event_date.' '.$start_time)) / 60).' мин';
		$manager = auth()->user()->find($event->manager_id);
		$trainer = auth()->user()->find($event->trainer_id);
		$client = Client::find($event->client_id);
		$category = Category::find($event->category_id)->name;
		$service = Service::find($event->service_id)->name;
		$tariff = Tariff::find($event->tariff_id)->name;
		$phpWord = new \PhpOffice\PhpWord\PhpWord();
		$section = $phpWord->addSection();
		$tableStyle = [
			'cellMarginBottom' => 100,
			'borderBottomSize' => 6,
			'borderBottomColor'=> '000000',
			'width' => 100
		];
		$cellStyle = [
			'borderSize' => 6,
			'borderColor'=> '000000'
		];
		$alignRightStyle = [
			'alignment' => 'right'
		];
		$smallTextStyle = [
			'size' => 8,
			'color' => '555555'
		];
		$cellSpacing = [
			'cellSpacing' => 50
		];
		$table = $section->addTable($tableStyle);
		$table->addRow();
		$cell = $table->addCell(4600);
		$cell->addText('Training CRM',[ //логотип
			'bold' => true,
			'color' => '09B76B',
			'size' => 16
		]);
		$cell = $table->addCell(4600);
		$cell->addText('',[],$alignRightStyle); //контакты
		$section->addText('экземпляр клиента',$smallTextStyle,$alignRightStyle);
		$table = $section->addTable($tableStyle);
		$table->addRow();
		$cell = $table->addCell(4000);
		$cellTable = $cell->addTable($cellSpacing);
		$cellTable->addRow();
		$cellTableCell = $cellTable->addCell(1600);
		$cellTableCell->addText('Дата');
		$cellTableCell = $cellTable->addCell(1600,$cellStyle);
		$cellTableCell->addText($event_date);
		$cellTable = $cell->addTable($cellSpacing);
		$cellTable->addRow();
		$cellTableCell = $cellTable->addCell(1600);
		$cellTableCell->addText('Начало');
		$cellTableCell = $cellTable->addCell(1600,$cellStyle);
		$cellTableCell->addText($start_time);
		$cellTable = $cell->addTable($cellSpacing);
		$cellTable->addRow();
		$cellTableCell = $cellTable->addCell(1600);
		$cellTableCell->addText('Окончание');
		$cellTableCell = $cellTable->addCell(1600,$cellStyle);
		$cellTableCell->addText($end_time);
		$cellTable = $cell->addTable($cellSpacing);
		$cellTable->addRow();
		$cellTableCell = $cellTable->addCell(1600);
		$cellTableCell->addText('Продолжит-ть');
		$cellTableCell = $cellTable->addCell(1600,$cellStyle);
		$cellTableCell->addText($duration);
		$cellTable = $cell->addTable($cellSpacing);
		$cellTable->addRow();
		$cellTableCell = $cellTable->addCell(1600);
		$cellTableCell->addText('Сумма');
		$cellTableCell = $cellTable->addCell(1600,$cellStyle);
		$cellTableCell->addText($event->cost);
		$cellTable = $cell->addTable($cellSpacing);
		$cellTable->addRow();
		$cellTableCell = $cellTable->addCell(1600);
		$cellTableCell->addText('Дата продажи');
		$cellTableCell = $cellTable->addCell(1600,$cellStyle);
		$cellTableCell->addText($event->sale_date->format('d.m.Y'));
		$cellTable = $cell->addTable($cellSpacing);
		$cellTable->addRow();
		$cellTableCell = $cellTable->addCell(1600);
		$cellTableCell->addText('Администратор');
		$cellTableCell = $cellTable->addCell(1600,$cellStyle);
		$cellTableCell->addText($manager->last_name.' '.$manager->first_name.' '.$manager->middle_name);
		$cell = $table->addCell(5200);
		$cellTable = $cell->addTable($cellSpacing);
		$cellTable->addRow();
		$cellTableCell = $cellTable->addCell(1600);
		$cellTableCell->addText('ФИО');
		$cellTableCell = $cellTable->addCell(3000,$cellStyle);
		$cellTableCell->addText($client->last_name.' '.$client->first_name.' '.$client->mid_name);
		$cellTable = $cell->addTable($cellSpacing);
		$cellTable->addRow();
		$cellTableCell = $cellTable->addCell(1600);
		$cellTableCell->addText('Телефон');
		$cellTableCell = $cellTable->addCell(3000,$cellStyle);
		$cellTableCell->addText($client->phone);
		$cellTable = $cell->addTable($cellSpacing);
		$cellTable->addRow();
		$cellTableCell = $cellTable->addCell(1600);
		$cellTableCell->addText('Категория');
		$cellTableCell = $cellTable->addCell(3000,$cellStyle);
		$cellTableCell->addText($category);
		$cellTable = $cell->addTable($cellSpacing);
		$cellTable->addRow();
		$cellTableCell = $cellTable->addCell(1600);
		$cellTableCell->addText('Услуга');
		$cellTableCell = $cellTable->addCell(3000,$cellStyle);
		$cellTableCell->addText($service);
		$cellTable = $cell->addTable($cellSpacing);
		$cellTable->addRow();
		$cellTableCell = $cellTable->addCell(1600);
		$cellTableCell->addText('Тариф');
		$cellTableCell = $cellTable->addCell(3000,$cellStyle);
		$cellTableCell->addText($tariff);
		$cellTable = $cell->addTable($cellSpacing);
		$cellTable->addRow();
		$cellTableCell = $cellTable->addCell(1600);
		$cellTableCell->addText('Кол-во человек');
		$cellTableCell = $cellTable->addCell(3000,$cellStyle);
		$cellTableCell->addText($event->number_people);
		$cellTable = $cell->addTable($cellSpacing);
		$cellTable->addRow();
		$cellTableCell = $cellTable->addCell(1600);
		$cellTableCell->addText('Инструктор');
		$cellTableCell = $cellTable->addCell(3000,$cellStyle);
		$cellTableCell->addText($trainer->last_name.' '.$trainer->first_name.' '.$trainer->middle_name);
		$cellTable = $cell->addTable($cellSpacing);
		$cellTable->addRow();
		$cellTableCell = $cellTable->addCell(1600);
		$cellTableCell->addText('Телефон инструктора');
		$cellTableCell = $cellTable->addCell(3000,$cellStyle);
		$cellTableCell->addText($trainer->phone);
		$section->addText('экземпляр инструктора',$smallTextStyle,$alignRightStyle);
		$table = $section->addTable($tableStyle);
		$table->addRow();
		$cell = $table->addCell(4000);
		$cellTable = $cell->addTable($cellSpacing);
		$cellTable->addRow();
		$cellTableCell = $cellTable->addCell(1600);
		$cellTableCell->addText('Дата');
		$cellTableCell = $cellTable->addCell(1600,$cellStyle);
		$cellTableCell->addText($event_date);
		$cellTable = $cell->addTable($cellSpacing);
		$cellTable->addRow();
		$cellTableCell = $cellTable->addCell(1600);
		$cellTableCell->addText('Начало');
		$cellTableCell = $cellTable->addCell(1600,$cellStyle);
		$cellTableCell->addText($start_time);
		$cellTable = $cell->addTable($cellSpacing);
		$cellTable->addRow();
		$cellTableCell = $cellTable->addCell(1600);
		$cellTableCell->addText('Окончание');
		$cellTableCell = $cellTable->addCell(1600,$cellStyle);
		$cellTableCell->addText($end_time);
		$cellTable = $cell->addTable($cellSpacing);
		$cellTable->addRow();
		$cellTableCell = $cellTable->addCell(1600);
		$cellTableCell->addText('Продолжит-ть');
		$cellTableCell = $cellTable->addCell(1600,$cellStyle);
		$cellTableCell->addText($duration);
		$cellTable = $cell->addTable($cellSpacing);
		$cellTable->addRow();
		$cellTableCell = $cellTable->addCell(1600);
		$cellTableCell->addText('Сумма');
		$cellTableCell = $cellTable->addCell(1600,$cellStyle);
		$cellTableCell->addText($event->cost);
		$cellTable = $cell->addTable($cellSpacing);
		$cellTable->addRow();
		$cellTableCell = $cellTable->addCell(1600);
		$cellTableCell->addText('Дата продажи');
		$cellTableCell = $cellTable->addCell(1600,$cellStyle);
		$cellTableCell->addText($event->sale_date->format('d.m.Y'));
		$cellTable = $cell->addTable($cellSpacing);
		$cellTable->addRow();
		$cellTableCell = $cellTable->addCell(1600);
		$cellTableCell->addText('Администратор');
		$cellTableCell = $cellTable->addCell(1600,$cellStyle);
		$cellTableCell->addText($manager->last_name.' '.$manager->first_name.' '.$manager->middle_name);
		$cell = $table->addCell(5200);
		$cellTable = $cell->addTable($cellSpacing);
		$cellTable->addRow();
		$cellTableCell = $cellTable->addCell(1600);
		$cellTableCell->addText('ФИО');
		$cellTableCell = $cellTable->addCell(3000,$cellStyle);
		$cellTableCell->addText($client->last_name.' '.$client->first_name.' '.$client->mid_name);
		$cellTable = $cell->addTable($cellSpacing);
		$cellTable->addRow();
		$cellTableCell = $cellTable->addCell(1600);
		$cellTableCell->addText('Телефон');
		$cellTableCell = $cellTable->addCell(3000,$cellStyle);
		$cellTableCell->addText($client->phone);
		$cellTable = $cell->addTable($cellSpacing);
		$cellTable->addRow();
		$cellTableCell = $cellTable->addCell(1600);
		$cellTableCell->addText('Категория');
		$cellTableCell = $cellTable->addCell(3000,$cellStyle);
		$cellTableCell->addText($category);
		$cellTable = $cell->addTable($cellSpacing);
		$cellTable->addRow();
		$cellTableCell = $cellTable->addCell(1600);
		$cellTableCell->addText('Услуга');
		$cellTableCell = $cellTable->addCell(3000,$cellStyle);
		$cellTableCell->addText($service);
		$cellTable = $cell->addTable($cellSpacing);
		$cellTable->addRow();
		$cellTableCell = $cellTable->addCell(1600);
		$cellTableCell->addText('Тариф');
		$cellTableCell = $cellTable->addCell(3000,$cellStyle);
		$cellTableCell->addText($tariff);
		$cellTable = $cell->addTable($cellSpacing);
		$cellTable->addRow();
		$cellTableCell = $cellTable->addCell(1600);
		$cellTableCell->addText('Кол-во человек');
		$cellTableCell = $cellTable->addCell(3000,$cellStyle);
		$cellTableCell->addText($event->number_people);
		$cellTable = $cell->addTable($cellSpacing);
		$cellTable->addRow();
		$cellTableCell = $cellTable->addCell(1600);
		$cellTableCell->addText('Инструктор');
		$cellTableCell = $cellTable->addCell(3000,$cellStyle);
		$cellTableCell->addText($trainer->last_name.' '.$trainer->first_name.' '.$trainer->middle_name);
		$cellTable = $cell->addTable($cellSpacing);
		$cellTable->addRow();
		$cellTableCell = $cellTable->addCell(1600);
		$cellTableCell->addText('Телефон инструктора');
		$cellTableCell = $cellTable->addCell(3000,$cellStyle);
		$cellTableCell->addText($trainer->phone);
		$section->addText('экземпляр администратора',$smallTextStyle,$alignRightStyle);
		$table = $section->addTable([
			'cellMarginBottom' => 100,
			'cellMarginTop' => 100,
			'width' => 100
		]);
		$table->addRow();
		$cell = $table->addCell(4000);
		$cellTable = $cell->addTable($cellSpacing);
		$cellTable->addRow();
		$cellTableCell = $cellTable->addCell(1600);
		$cellTableCell->addText('Дата');
		$cellTableCell = $cellTable->addCell(1600,$cellStyle);
		$cellTableCell->addText($event_date);
		$cellTable = $cell->addTable($cellSpacing);
		$cellTable->addRow();
		$cellTableCell = $cellTable->addCell(1600);
		$cellTableCell->addText('Начало');
		$cellTableCell = $cellTable->addCell(1600,$cellStyle);
		$cellTableCell->addText($start_time);
		$cellTable = $cell->addTable($cellSpacing);
		$cellTable->addRow();
		$cellTableCell = $cellTable->addCell(1600);
		$cellTableCell->addText('Окончание');
		$cellTableCell = $cellTable->addCell(1600,$cellStyle);
		$cellTableCell->addText($end_time);
		$cellTable = $cell->addTable($cellSpacing);
		$cellTable->addRow();
		$cellTableCell = $cellTable->addCell(1600);
		$cellTableCell->addText('Продолжит-ть');
		$cellTableCell = $cellTable->addCell(1600,$cellStyle);
		$cellTableCell->addText($duration);
		$cellTable = $cell->addTable($cellSpacing);
		$cellTable->addRow();
		$cellTableCell = $cellTable->addCell(1600);
		$cellTableCell->addText('Сумма');
		$cellTableCell = $cellTable->addCell(1600,$cellStyle);
		$cellTableCell->addText($event->cost);
		$cellTable = $cell->addTable($cellSpacing);
		$cellTable->addRow();
		$cellTableCell = $cellTable->addCell(1600);
		$cellTableCell->addText('Дата продажи');
		$cellTableCell = $cellTable->addCell(1600,$cellStyle);
		$cellTableCell->addText($event->sale_date->format('d.m.Y'));
		$cellTable = $cell->addTable($cellSpacing);
		$cellTable->addRow();
		$cellTableCell = $cellTable->addCell(1600);
		$cellTableCell->addText('Администратор');
		$cellTableCell = $cellTable->addCell(1600,$cellStyle);
		$cellTableCell->addText($manager->last_name.' '.$manager->first_name.' '.$manager->middle_name);
		$cell = $table->addCell(5200);
		$cellTable = $cell->addTable($cellSpacing);
		$cellTable->addRow();
		$cellTableCell = $cellTable->addCell(1600);
		$cellTableCell->addText('ФИО');
		$cellTableCell = $cellTable->addCell(3000,$cellStyle);
		$cellTableCell->addText($client->last_name.' '.$client->first_name.' '.$client->mid_name);
		$cellTable = $cell->addTable($cellSpacing);
		$cellTable->addRow();
		$cellTableCell = $cellTable->addCell(1600);
		$cellTableCell->addText('Телефон');
		$cellTableCell = $cellTable->addCell(3000,$cellStyle);
		$cellTableCell->addText($client->phone);
		$cellTable = $cell->addTable($cellSpacing);
		$cellTable->addRow();
		$cellTableCell = $cellTable->addCell(1600);
		$cellTableCell->addText('Категория');
		$cellTableCell = $cellTable->addCell(3000,$cellStyle);
		$cellTableCell->addText($category);
		$cellTable = $cell->addTable($cellSpacing);
		$cellTable->addRow();
		$cellTableCell = $cellTable->addCell(1600);
		$cellTableCell->addText('Услуга');
		$cellTableCell = $cellTable->addCell(3000,$cellStyle);
		$cellTableCell->addText($service);
		$cellTable = $cell->addTable($cellSpacing);
		$cellTable->addRow();
		$cellTableCell = $cellTable->addCell(1600);
		$cellTableCell->addText('Тариф');
		$cellTableCell = $cellTable->addCell(3000,$cellStyle);
		$cellTableCell->addText($tariff);
		$cellTable = $cell->addTable($cellSpacing);
		$cellTable->addRow();
		$cellTableCell = $cellTable->addCell(1600);
		$cellTableCell->addText('Кол-во человек');
		$cellTableCell = $cellTable->addCell(3000,$cellStyle);
		$cellTableCell->addText($event->number_people);
		$cellTable = $cell->addTable($cellSpacing);
		$cellTable->addRow();
		$cellTableCell = $cellTable->addCell(1600);
		$cellTableCell->addText('Инструктор');
		$cellTableCell = $cellTable->addCell(3000,$cellStyle);
		$cellTableCell->addText($trainer->last_name.' '.$trainer->first_name.' '.$trainer->middle_name);
		$cellTable = $cell->addTable($cellSpacing);
		$cellTable->addRow();
		$cellTableCell = $cellTable->addCell(1600);
		$cellTableCell->addText('Телефон инструктора');
		$cellTableCell = $cellTable->addCell(3000,$cellStyle);
		$cellTableCell->addText($trainer->phone);
		$section->addText('Оплачивая услуги инструкторов и пользуясь непосредственно услугами инструкторов,',$smallTextStyle);
		$section->addText('Я подтверждаю, что не имею медицинских противопоказаний для занятий горнолыжным спортом и/или сноубордом и/или медицинских противопоказаний у детей, и участвую в занятиях с инструктором на свой риск осознанно и добровольно.',$smallTextStyle);
		$section->addText('Я понимаю, что катание на горных лыжах, сноуборде связано с повышенным риском получения травмы и повреждения здоровья, в случае получения травмы претензий к организаторам не имею. Самостоятельно оцениваю возможности своего организма (организма членов моей семьи) и его/их соответствие условиям физической нагрузки, имеющимся у меня/членов моей семьи навыкам катания, погодным условиям и условиям катания на склоне в целом.',$smallTextStyle);
		$section->addText('Я безусловно подтверждаю, что полностью ознакомлен(на), согласен(на) с Правилами пользования услугами инструкторов (ИП Сычев П.А.) и Правилами поведения на территории Всесезонного курорта "Горки Город"/СТК "Горная Карусель" (НАО "Красная Поляна") в соответствии со статьей 428 ГК РФ и обязуюсь их выполнять.',$smallTextStyle);
		$section->addTextRun();
		$section->addText('__________________________ / _____________',[],$alignRightStyle);
		$writer = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
		$writer->save(storage_path('voucher_'.$event->id.'.docx'));

		return view('instructors.event.show', compact('event'));
	}

	public function edit(Event $event)
	{
		$services = Service::all();
		$agents = Agent::all();

		$categories = Category::all();

		$clients = Client::all();
        $event->load('client');

		$managers = auth()->user()->role('manager')->get();

		$trainers = auth()->user()->role('trainer')->get();

		return view('instructors.event.edit', compact(
			'event',
			'services',
			'categories',
			'managers',
			'trainers',
			'clients',
			'agents'
		));
	}

	public function update(Request $request)
	{
		request()->validate([
			'manager_id'     => 'required|integer',
			'trainer_id'     => 'required|integer',
			'client_id'      => 'required|integer',
			'service_id'     => 'required|integer',
			'tariff_id'      => 'required|integer',
			'category_id'    => 'required|integer',
			'location_id'       => 'required|integer',
			'number_people'  => 'required|integer',
			'cost'           => 'required|integer',
			'payment_status' => 'required|boolean',
			'online_payment' => 'required|boolean',
			'event_date'     => 'required|date',
			'sale_date'      => 'required|date',
			'start_time'     => 'required',
			'end_time'       => 'required'
		]);

		Event::update([
			'manager_id'     => $request->manager_id,
			'trainer_id'     => $request->trainer_id,
			'client_id'      => $request->client_id,
			'service_id'     => $request->service_id,
			'category_id'    => $request->category_id,
			'tariff_id'      => $request->tariff_id,
			'location_id'       => ($request->manager_id) ?
				auth()->user($request->manager_id)->location_id : null,
			'number_people'  => $request->number_people,
			'cost'           => $request->cost,
			'payment_status' => $request->payment_status,
			'online_payment' => $request->online_payment,
			'event_date'     => $request->event_date,
			'sale_date'      => $request->sale_date,
			'start_time'     => $request->start_time,
			'end_time'       => $request->end_time
		]);

		return redirect()->route('instructors.event.index')->with('success', 'Занятие успешно сохранено');
	}

	public function destroy($id)
	{
		Event::destroy($id);

		return redirect()->route('instructors.event.index')->with('success', 'Занятие удалено');
	}
}
