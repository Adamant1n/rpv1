<?php

namespace App\Http\Controllers\Instructors;

use App\Http\Controllers\Controller;
use App\Instructors\Event;
use App\Instructors\Service;
use App\Instructors\Category;
use App\Instructors\Location;
use App\User;
use Illuminate\Http\Request;

class CalendarController extends Controller
{
	public function __construct()
	{
		$this->middleware(['role:superadmin|admin|manager|operator|trainer']);
	}

	public function index(Request $request)
	{
		// manager@crm.promokod.fun

		$categories = Category::all();

		$categories->map(function ($category) {
			if($category->id === Category::SKI)
			{
				$category->icon = '/images/ski.png';
			}
			else if($category->id === Category::SNOWBOARD)
			{
				$category->icon = '/images/board.png';
			}
		});

		if (auth()->user()->hasRole('trainer|manager'))
			return view('instructors.calendar.index', compact('categories'));

		$services = Service::all();
		$locations = Location::all();
		
		$currentLocation = null;

		if ($request->has('location'))
		{
			$managers = auth()->user()->role('manager')
				->where('location_id', $request->location)->get();
			$trainers = auth()->user()->role('trainer')
				->where('location_id', $request->location)->get();
			$currentLocation = Location::find($request->location);
		}
		else
		{
			$managers = auth()->user()->role('manager')->get();
			$trainers = auth()->user()->role('trainer')->get();
		}

		return view('instructors.calendar.index', compact(
			'services',
			'categories',
			'locations',
			'managers',
			'trainers',
			'currentLocation'
		));
	}

	public function event($id)
	{
		$event = Event::find($id);
		return view('instructors.calendar.event', compact('event'));
	}

	public function resources_json(Request $request)
	{
		$category_id = isset($request->category_id) ? $request->category_id : 1;
		$user = auth()->user();

		$output = [];

		if($user->hasRole('trainer')) {
			$output[] = [
				'id' => $user->id,
				'title' => $user->full_name,
				'category_id' => $user->category_id,
				'location_id' => $user->location_id,
			];
		}
		else {
			$trainers = User::role('trainer')->where('category_id', $category_id);

			if($user->hasRole('manager')) {
				$trainers = $trainers = $user->location->trainers()->role('trainer')->where('category_id', $category_id);
			}
			
			$trainers = $trainers->get();
			foreach($trainers as $trainer) {
				$output[] = [
					'id' => $trainer->id,
					'title' => $trainer->full_name,
					'category_id' => $trainer->category_id,
					'location_id' => $trainer->location_id 
				];
			}
		}

		return response()->json($output);
	}

	public function events_json(Request $request)
	{
		$services = Service::all();

		$where = [
			//['event_date', '>=', explode('T', $request->start)[0]],
			//['event_date', '<=', explode('T', $request->end)[0]]
		];

		$user = auth()->user();

		if ($user->hasRole('trainer'))
		{
			$where[] = ['trainer_id', $user->id];
		}
		/*
		elseif ($user->hasRole('manager'))
		{
			$where[] = ['location_id', $user->location_id];
		}
		*/
		else
		{
			if ($request->location !== null)
				$where[] = ['location_id', $request->location];

			if ($request->manager !== null)
				$where[] = ['manager_id', $request->manager];

			if ($request->trainer !== null)
				$where[] = ['trainer_id', $request->trainer];
		}

		$events = Event::where($where)->get();

		$output = [];

		foreach ($events as $event)
		{
			$output[] = [
				'id' => $event->id,
				'resourceId' => $event->trainer_id,
				'title' => $services->find($event->service_id)->name,
				'start' => $event->event_date->format('Y-m-d').'T'.$event->start_time,
				'end' => $event->event_date->format('Y-m-d').'T'.$event->end_time,
				'url' => '/instructors/calendar/'.$event->id.'/event'
			];
		}

		return json_encode($output);
	}
}
