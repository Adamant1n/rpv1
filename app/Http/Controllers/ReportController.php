<?php

namespace App\Http\Controllers;

use App\Location;
use App\Order;
use App\Agent;
use App\Unit;
use App\Reservation;
use App\Category;
use App\Condition;
use App\User;
use \Carbon\Carbon;
use App\Services\OrderService;

use Illuminate\Http\Request;

class ReportController extends Controller
{
	private $user;

	public function __construct()
	{
		$this->middleware(function ($request, $next) {
			$this->user = auth()->user();
			return $next($request);
		});
	}

	// задолженность по договорам
	public function debt()
	{
		$this->authorize('index', \App\Report::class);

		$location_id = $this->user->hasRole('admin') ? $this->user->location_id : request()->location_id;

		$orders = Order::with('reservations.unit.product', 'location', 'client')
			->when($location_id, function ($query) use ($location_id) {
				$query->where('location_id', $location_id);
			})
			->paid()
			->expired()
			->latest()
			->get();

		$data = [];

		foreach ($orders as $order) {
			$orderService = new OrderService($order);

			$order->price_extra = 0;
			foreach ($order->reservations as $reservation) {
				$order->price_extra += $orderService->calculateReservationExtraPrice($reservation);
			}

			$data[] = [
				'agreement' => $order->optional_id,
				'last_name'	=> $order->client->last_name,
				'phone'		=> $order->client->phone,
				'date'		=> $order->starts_at,
				'debt'		=> $order->price_extra,
				'location'	=> $order->location->address
			];
		}

		$header = [
			'Договор',
			'Фамилия Клиента',
			'Телефон',
			'Дата',
			'Задолженность',
			'Пункт проката'
		];

		$table = [
			'head' => $header,
			'body' => $data
		];

		$filters = [
			'dates' => 'ignore',
			'location_id'	=> [
				'label' => 'Точка проката',
				'options'	=> Location::select(['id', 'address as title'])->get()->toArray()
			],
		];

		return view('reports.base', compact('table', 'filters'));
	}

	// эффективность сотрудников
	public function effectivity()
	{
		$this->authorize('index', \App\Report::class);

		$date_start_input	= request()->date_start ? request()->date_start : Carbon::now()->startOfMonth()->format('Y-m-d');
		$date_end_input		= request()->date_end ? request()->date_end : Carbon::now()->endOfMonth()->format('Y-m-d');

		$date_start 	= $date_start_input . ' 00:00:00';
		$date_end 		= $date_end_input . ' 23:59:59';

		$location_id = $this->user->hasRole('admin') ? $this->user->location_id : request()->location_id;

		$users = User::with([
			'location',
			'taken' => function ($query) use ($date_start, $date_end) {
				$query->where('starts_at', '>=', $date_start);
				$query->where('ends_at', '>=', $date_start);
			},
			'given' => function ($query) use ($date_start, $date_end) {
				$query->where('starts_at', '>=', $date_start);
				$query->where('ends_at', '>=', $date_start);
			}
		])
			->when($location_id, function ($query) use ($location_id) {
				$query->where('location_id', $location_id);
			})
			->latest()
			->get();

		$data = [];

		foreach ($users as $user) {
			$data[] = [
				'location'		=> optional($user->location)->address,
				'login'			=> $user->email,
				'role'			=> $user->role_name,
				'fio'			=> $user->full_name,
				'sold'			=> 0,
				'given'			=> $user->given->count(),
				'taken'			=> $user->taken->count(),
				'revenue'		=> 0,
				'revenue_2'		=> 0
			];
		}

		$header = [
			'Пункт проката',
			'Логин',
			'Роль',
			'ФИО сотрудника',
			'Выдано в прокат',
			'Принято инвентаря',
			'Продано товаров',
			'Сумма товаров',
			'Принято оплат',
		];

		$table = [
			'head' => $header,
			'body' => $data
		];

		$filters = [
			'location_id'	=> [
				'label' => 'Точка проката',
				'options'	=> Location::select(['id', 'address as title'])->get()->toArray()
			],
		];

		return view('reports.base', compact('table', 'filters', 'date_start_input', 'date_end_input'));
	}

	// бронирования
	public function bookings()
	{
		$this->authorize('index', \App\Report::class);

		$date_start_input	= request()->date_start ? request()->date_start : Carbon::now()->startOfMonth()->format('Y-m-d');
		$date_end_input		= request()->date_end ? request()->date_end : Carbon::now()->endOfMonth()->format('Y-m-d');

		$date_start 	= $date_start_input . ' 00:00:00';
		$date_end 		= $date_end_input . ' 23:59:59';

		$location_id = $this->user->hasRole('admin') ? $this->user->location_id : request()->location_id;

		$orders = Order::where('type', Order::TYPE_BOOK)
			->when($location_id, function ($query) use ($location_id) {
				$query->where('location_id', $location_id);
			})
			->with('reservations.unit.binding', 'location', 'client')
			->latest()
			->where('book_at', '>=', $date_start)
			->where('book_at', '<=', $date_end)
			->get();

		$data = [];

		foreach ($orders as $order) {
			$data[] = [
				'header'		=> true,
				'order_id'		=> $order->optional_id,
				'last_name'		=> $order->client->last_name,
				'phone'			=> $order->client->phone,
				'revenue'		=> $order->full_price,
				'paid'			=> 0,
				'location'		=> $order->location->address,
				'code'			=> '',
				'name'			=> '',
				'category'		=> '',
				'brand'			=> '',
				'binding'		=> '',
				'length'		=> '',
				'size'			=> '',
				'feet'			=> '',
			];

			foreach ($order->reservations as $reservation) {

				$reservation->unit->size = $reservation->unit->feature_5 ? $reservation->unit->feature_5 : $reservation->unit->feature_3;

				$data[] = [
					'order_id'		=> $order->optional_id,
					'last_name'		=> $order->client->last_name,
					'phone'			=> $order->client->phone,
					'revenue'		=> $reservation->full_price,
					'paid'			=> 0,
					'location'		=> $order->location->address,
					'code'			=> $reservation->unit->code,
					'name'			=> $reservation->unit->product->name,
					'category'		=> $reservation->unit->product->category->name,
					'brand'			=> $reservation->unit->product->brand->name,
					'binding'		=> optional($reservation->unit->binding)->name,
					'length'		=> $reservation->unit->feature_1,
					'size'			=> $reservation->unit->size,
					'feet'			=> $reservation->unit->feature_4,
				];
			}
		}

		$header = [
			'№ брони',
			'Фамилия',
			'Телефон',
			'Сумма брони',
			'Оплачено',
			'Пункт проката',
			'Артикул',
			'Название',
			'Категория',
			'Марка',
			'Крепление',
			'Длина',
			'Размер',
			'Колодка',
		];

		$table = [
			'head' => $header,
			'body' => $data
		];

		$filters = [
			'location_id'	=> [
				'label' => 'Точка проката',
				'options'	=> Location::select(['id', 'address as title'])->get()->toArray()
			],
		];

		return view('reports.base', compact('table', 'filters', 'date_start_input', 'date_end_input'));
	}

	// запас товаров
	public function inventory_products()
	{
		$this->authorize('index', \App\Report::class);

		$date_start_input	= request()->date_start ? request()->date_start : Carbon::now()->startOfMonth()->format('Y-m-d');
		$date_end_input		= request()->date_end ? request()->date_end : Carbon::now()->endOfMonth()->format('Y-m-d');

		$date_start 	= $date_start_input . ' 00:00:00';
		$date_end 		= $date_end_input . ' 23:59:59';

		$location_id 	= request()->location_id;

		$units = Unit::with('product.category', 'product.brand', 'location')
			->when($location_id, function ($query) use ($location_id) {
				$query->where('units.location_id', $location_id);
			})
			->join('products', 'products.id', 'units.product_id')
			->where('products.for_sale', 1)
			->select('units.*')
			->latest()
			->get();

		$data = [];

		foreach ($units as $unit) {
			$unit->size = $unit->feature_5 ? $unit->feature_5 : $unit->feature_3;

			$data[] = [
				'id'			=> $unit->optional_id,
				'category'		=> $unit->product->category->name,
				'name'			=> $unit->product->name,
				'brand'			=> $unit->product->brand->name,
				'size'			=> $unit->size,
				'count'			=> $unit->count,
				'location'		=> $unit->location->address,
				'price_buy'		=> $unit->product->price_buy,
				'price_sell'	=> $unit->product->price_sell,
				'price'			=> $unit->count * $unit->product->price_sell
			];
		}

		$header = [
			'ID товара',
			'Группа',
			'Наименование',
			'Марка',
			'Размер',
			'Остаток',
			'Склад',
			'Закуп. цена',
			'Цена',
			'Цена всех товаров',
		];

		$table = [
			'head' => $header,
			'body' => $data
		];

		$filters = [
			'dates' => 'ignore',
			'location_id'	=> [
				'label' => 'Точка проката',
				'options'	=> Location::select(['id', 'address as title'])->get()->toArray()
			],
		];

		return view('reports.base', compact('table', 'filters', 'date_start_input', 'date_end_input'));
	}

	// запас инвентаря
	public function inventory_history()
	{
		$this->authorize('index', \App\Report::class);

		$date_start_input	= request()->date_start ? request()->date_start : Carbon::now()->startOfMonth()->format('Y-m-d');
		$date_end_input		= request()->date_end ? request()->date_end : Carbon::now()->endOfMonth()->format('Y-m-d');

		$date_start 	= $date_start_input . ' 00:00:00';
		$date_end 		= $date_end_input . ' 23:59:59';

		$code		= request()->code;
		$unit = Unit::where('code', $code)->firstOrFail();

		$unit->load('product.category', 'product.brand');

		$reservations = $unit->reservations()
			->with('order.client', 'taken', 'given')
			->latest()
			->get();

		$data = [];

		foreach ($reservations as $reservation) {
			$data[] = [
				'category'		=> $unit->product->category->name,
				'name'			=> $unit->product->name,
				'brand'			=> $unit->product->brand->name,
				'starts_at'		=> $reservation->starts_at,
				'duration'		=> $reservation->order->duration,
				'ends_at'		=> $reservation->ends_at,
				'agreement'		=> $reservation->order->optional_id,
				'last_name'		=> $reservation->order->client->last_name,
				'phone'			=> $reservation->order->client->phone,
				'price'			=> $reservation->full_price,
				'price_other'	=> '',
				'given'			=> $reservation->given->last_name,
				'taken'			=> optional($reservation->taken)->last_name
			];
		}

		$header = [
			'Группа',
			'Наименование',
			'Марка',
			'Дата и время начала',
			'Срок аренды',
			'Дата и время завершения',
			'Договор',
			'Фамилия',
			'Телефон',
			'Сумма',
			'Остаточная стоимость',
			'Выдал',
			'Принял',
		];

		$table = [
			'head' => $header,
			'body' => $data
		];

		$filters = [
			'dates' => 'ignore',
		];

		return view('reports.base', compact('table', 'filters', 'date_start_input', 'date_end_input'));
	}

	// продажа инвентаря
	public function inventory_sale()
	{
		$this->authorize('index', \App\Report::class);

		$date_start_input	= request()->date_start ? request()->date_start : Carbon::now()->startOfMonth()->format('Y-m-d');
		$date_end_input		= request()->date_end ? request()->date_end : Carbon::now()->endOfMonth()->format('Y-m-d');

		$date_start 	= $date_start_input . ' 00:00:00';
		$date_end 		= $date_end_input . ' 23:59:59';

		$location_id 	= request()->location_id;

		$reservations = Reservation::with('unit.product.category', 'unit.product.brand', 'order.client', 'order.location')
			->when($location_id, function ($query) use ($location_id) {
				$query->where('location_id', $location_id);
			})
			->isSold()
			->where('starts_at', '>=', $date_start)
			->where('ends_at', '<=', $date_end)
			->latest()
			->get();

		$orders = $reservations->groupBy('order_id');

		$data = [];

		foreach ($orders as $order => $reservations) {
			$rows = [];
			foreach ($reservations as $reservation) {
				$rows[] = [
					'order_id'			=> $reservation->order_id,
					'last_name'			=> $reservation->order->client->last_name,
					'phone'				=> $reservation->order->client->phone,
					'revenue'			=> $reservation->full_price,
					'location'			=> $reservation->order->location->address,
					'code'				=> $reservation->unit->code,
					'category'			=> $reservation->unit->product->category->name,
					'name'				=> $reservation->unit->product->name,
					'brand'				=> $reservation->unit->product->brand->name
				];
			}

			$rows_collection = collect($rows);

			$data[] = [
				'header'			=> true,
				'order_id'			=> $order,
				'last_name'			=> $rows_collection->first()['last_name'],
				'phone'				=> $rows_collection->first()['phone'],
				'revenue'			=> $rows_collection->sum('revenue'),
				'location'			=> $rows_collection->first()['location'],
				'code'				=> '',
				'category'			=> '',
				'name'				=> '',
				'brand'				=> '',
			];

			foreach ($rows as $row) {
				$data[] = $row;
			}
		}

		$header = [
			'Договор',
			'Фамилия клиента',
			'Телефон',
			'Выручка',
			'Пункт проката',
			'Артикул',
			'Наименование',
			'Марка',
		];

		$table = [
			'head' => $header,
			'body' => $data
		];

		$filters = [
			'location_id'	=> [
				'label' => 'Точка проката',
				'options'	=> Location::select(['id', 'address as title'])->get()->toArray()
			],
		];

		return view('reports.base', compact('table', 'filters', 'date_start_input', 'date_end_input'));
	}

	// запас инвентаря
	public function inventory_stock_detailed()
	{
		$this->authorize('index', \App\Report::class);

		$date_start_input	= request()->date_start ? request()->date_start : Carbon::now()->startOfMonth()->format('Y-m-d');
		$date_end_input		= request()->date_end ? request()->date_end : Carbon::now()->endOfMonth()->format('Y-m-d');

		$date_start 	= $date_start_input . ' 00:00:00';
		$date_end 		= $date_end_input . ' 23:59:59';

		$location_id 	= request()->location_id;
		$category_id	= request()->category_id;
		$condition_id	= request()->condition_id;

		$units = Unit::with('location', 'mainLocation', 'product.category', 'product.brand', 'binding', 'reservations')
			->when($location_id, function ($query) use ($location_id) {
				$query->where('location_id', $location_id);
			})
			->when($category_id, function ($query) use ($category_id) {
				$query->join('products', 'products.id', 'units.product_id');
				$query->select('units.*');
				$query->where('products.category_id', $category_id);
			})
			->when($condition_id, function ($query) use ($condition_id) {
				$query->where('condition_id', $condition_id);
			})
			->whereHas('product', function ($query) {
				$query->where('products.for_sale', 0);
			})
			->latest()
			->get();

		$data = [];

		foreach ($units as $unit) {
			$unit->size = $unit->feature_5 ? $unit->feature_5 : $unit->feature_3;

			$data[] = [
				'id'			=> $unit->code,
				'category'		=> $unit->product->category->name,
				'name'			=> $unit->product->name,
				'brand'			=> $unit->product->brand->name,
				'binding'		=> optional($unit->binding)->name,
				'length'		=> $unit->feature_1,
				'size'			=> $unit->size,
				'feet'			=> $unit->feature_4,
				'status'		=> optional($unit->condition)->name ?? '–',
				'main_location'	=> optional($unit->mainLocation)->address,
				'location'		=> $unit->location->adress,
				'created_at'	=> $unit->created_at,
				'price_buy'		=> $unit->product->price_buy,
				'price_sell'	=> $unit->product->price_sell,
				'effectivity'	=> $unit->revenue,
				'age'			=> $unit->age
			];
		}

		$header = [
			'ID',
			'Группа',
			'Наименование',
			'Марка',
			'Крепление',
			'Длина',
			'Размер',
			'Колодка',
			'Состояние',
			'Основной склад',
			'Текущий склад',
			'Дата записи',
			'Закуп. цена',
			'Остаточная стоимость',
			'Эффективность в прокате',
			'Возраст',
		];

		$table = [
			'head' => $header,
			'body' => $data
		];

		$filters = [
			'dates' => 'ignore',
			'location_id'	=> [
				'label' => 'Точка проката',
				'options'	=> Location::select(['id', 'address as title'])->get()->toArray()
			],
			'category_id'	=> [
				'label' => 'Категория',
				'options'	=> Category::select(['id', 'name as title'])->get()->toArray()
			],
			'condition_id'	=> [
				'label' => 'Состояние',
				'options'	=> Condition::select(['id', 'name as title'])->get()->toArray()
			],
		];

		return view('reports.base', compact('table', 'filters', 'date_start_input', 'date_end_input'));
	}

	// запас инвентаря
	public function inventory_stock()
	{
		$this->authorize('index', \App\Report::class);

		$category_id 	= request()->category_id;
		$location_id 	= request()->location_id;
		$condition_id	= request()->condition_id;

		$units = Unit::with('product.category', 'mainLocation', 'condition')
			->when($location_id, function ($query) use ($location_id) {
				$query->where('main_location', $location_id);
			})
			->when($category_id, function ($query) use ($category_id) {
				$query->join('products', 'products.id', 'units.product_id');
				$query->where('products.category_id', $category_id);
				$query->select('units.*');
			})
			->when($condition_id, function ($query) use ($condition_id) {
				$query->where('condition_id', $condition_id);
			})
			->whereHas('product', function ($query) {
				/*
				$query->whereHas('category', function ($q) {
					$q->where('group', 0);
				});
				*/
				$query->where('products.for_sale', 0);
			})
			->get();

		$units = $units->map(function ($unit) {
			for ($i = 1; $i < 6; $i++) {
				if ($unit->{'feature_' . $i}) {
					$unit->size_group = $unit->{'feature_' . $i};
					break;
				}
			}
			$unit->size = $unit->feature_5 ? $unit->feature_5 : $unit->feature_3;

			return $unit;
		});

		$data = [];

		$unitsByLocation = $units->groupBy('main_location');
		$unitsByLocation = $unitsByLocation->map(function ($location) {
			$unitsByCategory = $location->groupBy('product.category_id');
			$unitsByCategory = $unitsByCategory->map(function ($category) {
				$unitsBySize = $category->groupBy('size_group');
				$unitsBySize = $unitsBySize->map(function ($size) {
					$unitsByCondition = $size->groupBy('condition_id');
					return $unitsByCondition;
				});
				return $unitsBySize;
			});
			return $unitsByCategory;
		});

		foreach ($unitsByLocation as $location => $categories) {
			foreach ($categories as $category => $sizes) {
				foreach ($sizes as $size => $conditions) {
					foreach ($conditions as $condition => $units) {
						$data[] = [
							'category'		=> $units->first()->product->category->name,
							'length'		=> $units->first()->feature_1,
							'size'			=> $units->first()->size,
							'condition'		=> optional($units->first()->condition)->name ?? '–',
							'count'			=> $units->count(),
							'main_location'	=> optional($units->first()->mainLocation)->address,
							'price_buy'		=> $units->sum('product.price_buy'),
							'effectivity'	=> $units->sum('revenue')
						];
					}
				}
			}
		}

		$header = [
			'Категория',
			'Длина',
			'Размер',
			'Состояние',
			'Кол-во',
			'Основной склад',
			'Цена закупки',
			'Эффективность'
		];

		$table = [
			'head' => $header,
			'body' => $data
		];

		$filters = [
			'dates' => 'ignore',
			'main_location'	=> [
				'label' => 'Точка проката',
				'options'	=> Location::select(['id', 'address as title'])->get()->toArray()
			],
			'category_id'	=> [
				'label' => 'Категория',
				'options'	=> Category::select(['id', 'name as title'])->get()->toArray()
			],
			'condition_id'	=> [
				'label' => 'Состояние',
				'options'	=> Condition::select(['id', 'name as title'])->get()->toArray()
			],
		];

		return view('reports.base', compact('table', 'filters'));
	}

	// инвентарь в аренде
	public function inventory_rent()
	{
		$this->authorize('index', \App\Report::class);

		$date_start_input	= request()->date_start ? request()->date_start : Carbon::now()->startOfMonth()->format('Y-m-d');
		$date_end_input		= request()->date_end ? request()->date_end : Carbon::now()->endOfMonth()->format('Y-m-d');

		$date_start 	= $date_start_input . ' 00:00:00';
		$date_end 		= $date_end_input . ' 23:59:59';

		$location_id 	= request()->location_id;
		$agent_id 		= request()->agent_id;

		$reservations = Reservation::with('unit.product.category', 'unit.product.brand', 'order.client', 'unit.location', 'unit.mainLocation')
			->join('orders', 'reservations.order_id', 'orders.id')
			->select('reservations.*')
			->when($location_id, function ($query) use ($location_id) {
				$query->where('orders.location_id', $location_id);
			})
			->where('orders.status', 'paid')
			->whereNull('reservations.returned_at')
			->where('reservations.starts_at', '>=', $date_start)
			->where('reservations.ends_at', '<=', $date_end)
			->get();

		$data = [];

		foreach ($reservations as $reservation) {
			$data[] = [
				'id'			=> $reservation->unit->code,
				'category'		=> $reservation->unit->product->category->name,
				'name'			=> $reservation->unit->product->name,
				'brand'			=> $reservation->unit->product->brand->name,
				'starts_at'		=> $reservation->starts_at,
				'duration'		=> $reservation->order->duration,
				'ends_at'		=> $reservation->ends_at,
				'main_location'	=> optional($reservation->unit->mainLocation)->address,
				'agreement'		=> $reservation->order->optional_id,
				'last_name'		=> $reservation->order->client->last_name,
				'phone'			=> $reservation->order->client->phone,
				'expired'		=> $reservation->expired
			];
		}

		$header = [
			'ID',
			'Группа',
			'Наименование',
			'Марка',
			'Дата и время начала',
			'Срок аренды',
			'Возврат до',
			'Основной склад',
			'Договор',
			'Фамилия',
			'Телефон',
			'Просрочено дней'
		];

		$table = [
			'head' => $header,
			'body' => $data
		];

		$filters = [
			'location_id'	=> [
				'label' => 'Точка проката',
				'options'	=> Location::select(['id', 'address as title'])->get()->toArray()
			],
		];

		return view('reports.base', compact('table', 'filters', 'date_start_input', 'date_end_input'));
	}

	// продажа агентами детально
	// договоры
	public function agent_sales_detailed()
	{
		$this->authorize('index', \App\Report::class);

		// полную сумму считать из бд

		$date_start_input	= request()->date_start ? request()->date_start : Carbon::now()->startOfMonth()->format('Y-m-d');
		$date_end_input		= request()->date_end ? request()->date_end : Carbon::now()->endOfMonth()->format('Y-m-d');

		$date_start 	= $date_start_input . ' 00:00:00';
		$date_end 		= $date_end_input . ' 23:59:59';

		$location_id 	= request()->location_id;
		$agent_id		= request()->agent_id;

		$orders = Order::with('client', 'agent', 'location')
			->when($location_id, function ($query) use ($location_id) {
				$query->where('location_id', $location_id);
			})
			->when($agent_id, function ($query) use ($agent_id) {
				$query->where('agent_id', $agent_id);
			})
			->where('created_at', '>=', $date_start)
			->where('created_at', '<=', $date_end)
			->whereNotNull('agent_id')
			->real()
			->oldest()
			->get();

		$dates = $orders->groupBy(function ($order) {
			return $order->created_at->format('Y-m-d');
		});

		$data = [];

		foreach ($dates as $date => $orders) {
			$rows = [];
			foreach ($orders as $order) {
				$rows[] = [
					'agent' 		=> $order->agent->name,
					'commission'	=> $order->agent->commission . '%',
					'created_at'	=> $order->created_at->format('d.m.Y'),
					'location'		=> $order->location->address,
					'order_id'		=> $order->optional_id,
					'client_name'	=> $order->client->last_name,
					'revenue'		=> $order->price,
					'income'		=> $order->full_price * $order->agent->commission / 100,
				];
			}

			$rows_collection = collect($rows);

			$data[] = [
				'name' 			=> Carbon::parse($date)->format('d.m.Y'),
				'header'		=> true,
				'commission' 	=> '',
				'created_at'	=> '',
				'location'		=> '',
				'order_id'		=> '',
				'client_name'	=> '',
				'revenue'		=> $rows_collection->sum('revenue'),
				'income'		=> $rows_collection->sum('income'),
			];

			foreach ($rows as $row) {
				$data[] = $row;
			}
		}

		$header = [
			'Агент',
			'Агентский процент',
			'Дата создания',
			'Пункт проката',
			'Договор',
			'Фамилия клиента',
			'Сумма договора',
			'Сумма вознаграждения'
		];

		$table = [
			'head' => $header,
			'body' => $data
		];

		$filters = [
			'location_id'	=> [
				'label' => 'Точка проката',
				'options'	=> Location::select(['id', 'address as title'])->get()->toArray()
			],
			'agent_id'		=> [
				'label' => 'Агент',
				'options' 	=> Agent::select(['id', 'name as title'])->get()->toArray(),
			],
		];

		return view('reports.base', compact('table', 'filters', 'date_start_input', 'date_end_input'));
	}

	// продажа агентами общая
	public function agent_sales_common()
	{
		$this->authorize('index', \App\Report::class);

		// полную сумму считать из бд

		$date_start_input	= request()->date_start ? request()->date_start : Carbon::now()->startOfMonth()->format('Y-m-d');
		$date_end_input		= request()->date_end ? request()->date_end : Carbon::now()->endOfMonth()->format('Y-m-d');

		$date_start 	= $date_start_input . ' 00:00:00';
		$date_end 		= $date_end_input . ' 23:59:59';

		$location_id 	= request()->location_id;
		$agent_id 		= request()->agent_id;

		$orders = Order::with('agent', 'location')
			->when($location_id, function ($query) use ($location_id) {
				$query->where('location_id', $location_id);
			})
			->when($agent_id, function ($query) use ($agent_id) {
				$query->where('agent_id', $agent_id);
			})
			->where('created_at', '>=', $date_start)
			->where('created_at', '<=', $date_end)
			->whereNotNull('agent_id')
			->real()
			->oldest()
			->get();

		$orders_by_locations = $orders->groupBy('location_id');
		$data = [];

		foreach ($orders_by_locations as $orders_location) {
			$orders_by_agents = $orders_location->groupBy('agent_id');

			// dd($orders_by_agents);

			foreach ($orders_by_agents as $orders_agent) {
				$data[] = [
					'agent'			=> $orders_agent->first()->agent->name,
					'commission'	=> $orders_agent->first()->agent->commission,
					'revenue'		=> $orders_agent->sum('price'),
					'income'		=> $orders_agent->sum(function ($order) {
						return $order->full_price * $order->agent->commission / 100;
					}),
					'location'		=> $orders_agent->first()->location->address
				];
			}
		}

		$header = [
			'Агент',
			'Агентский процент',
			'Сумма продаж',
			'Сумма агенту',
			'Пункт проката'
		];

		$table = [
			'head' => $header,
			'body' => $data
		];

		$filters = [
			'agent_id'		=> [
				'label' => 'Агент',
				'options' 	=> Agent::select(['id', 'name as title'])->get()->toArray(),
			],
			'location_id'	=> [
				'label' => 'Точка проката',
				'options'	=> Location::select(['id', 'address as title'])->get()->toArray()
			],
		];

		return view('reports.base', compact('table', 'filters', 'date_start_input', 'date_end_input'));
	}

	// договоры
	public function agreements()
	{
		$this->authorize('index', \App\Report::class);

		$date_start_input	= request()->date_start ? request()->date_start : Carbon::now()->startOfMonth()->format('Y-m-d');
		$date_end_input		= request()->date_end ? request()->date_end : Carbon::now()->endOfMonth()->format('Y-m-d');

		$date_start 	= $date_start_input . ' 00:00:00';
		$date_end 		= $date_end_input . ' 23:59:59';

		$location_id = $this->user->hasRole('admin') ? $this->user->location_id : request()->location_id;

		$orders = Order::with('client', 'agent', 'location')
			->when($location_id, function ($query) use ($location_id) {
				$query->where('location_id', $location_id);
			})
			->where('created_at', '>=', $date_start)
			->where('created_at', '<=', $date_end)
			->real()
			->oldest()
			->get();

		$data = [];

		foreach ($orders as $order) {
			$data[] = [
				'order_id' 		=> $order->optional_id,
				'client_name'	=> $order->client->last_name,
				'client_phone'	=> $order->client->phone,
				'created_at'	=> $order->created_at->format('d.m.Y'),
				'price'			=> $order->price,
				'discount'		=> $order->discount . '%',
				'amount_paid'	=> ($order->is_paid) ? $order->price : 0,
				'location'		=> $order->location->address
			];
		}

		$header = [
			'Договор',
			'Фамилия Клиента',
			'Телефон клиента',
			'Дата',
			'Сумма договора',
			'Скидка клиента',
			'Оплачено',
			'Пункт проката'
		];

		$table = [
			'head' => $header,
			'body' => $data
		];

		$filters = [
			'location_id'	=> [
				'label' => 'Точка проката',
				'options'	=> Location::select(['id', 'address as title'])->get()->toArray()
			],
		];

		return view('reports.base', compact('table', 'filters', 'date_start_input', 'date_end_input'));
	}

	// ежедневная выручка
	public function daily_revenue()
	{
		$this->authorize('index', \App\Report::class);

		$date_start_input	= request()->date_start ? request()->date_start : Carbon::now()->startOfMonth()->format('Y-m-d');
		$date_end_input		= request()->date_end ? request()->date_end : Carbon::now()->endOfMonth()->format('Y-m-d');

		$date_start 	= $date_start_input . ' 00:00:00';
		$date_end 		= $date_end_input . ' 23:59:59';

		$location_id = $this->user->hasRole('admin') ? $this->user->location_id : request()->location_id;

		$orders = Order::with('reservations.unit.product.brand', 'reservations.unit.product.category', 'location')
			->when($location_id, function ($query) use ($location_id) {
				$query->where('location_id', $location_id);
			})
			->where('created_at', '>=', $date_start)
			->where('created_at', '<=', $date_end)
			->real()
			->oldest()
			->get();

		// распределение результата по дням
		$dates = $orders->groupBy(function ($order) {
			return $order->created_at->format('Y-m-d');
		});

		$data = [];
		foreach ($dates as $date => $orders) {
			// распределение по филиалам
			$orders_by_locations = $orders->groupBy('location_id');

			$rows = [];

			foreach ($orders_by_locations as $location_id => $orders_location) {
				$rows[] = [
					'name' 				=> $orders_location[0]->location->address,
					'revenue' 			=> $orders_location->sum('price'),
					'revenue_cash'		=> $orders_location->where('paid_by', Order::PAID_CASH)->sum('price'),
					'revenue_card'		=> $orders_location->where('paid_by', Order::PAID_CARD)->sum('price'),
					'revenue_invoice'		=> $orders_location->where('paid_by', Order::PAID_INVOICE)->sum('price'),
					'revenue_online'	=> $orders_location->where('paid_by', Order::PAID_ONLINE)->sum('price'),
					'paid_count'		=> $orders_location->filter(function ($order) {
						return $order->is_paid;
					})->count(),
					'units_count'		=> $orders_location->sum(function ($order) {
						return $order->reservations->count();
					}),
					'units_taken'		=> $orders_location->where('status', Order::STATUS_ENDED)->sum(function ($order) {
						return $order->reservations->count();
					}),
					'units_sold'		=> $orders_location->sum(function ($order) {
						return $order->reservations->where('status', Reservation::STATUS_SOLD)->count();
					})
				];
			}

			$rows_collection = collect($rows);

			$data[] = [
				'name' 				=> Carbon::parse($date)->format('d.m.Y'),
				'header'			=> true,
				'revenue' 			=> $rows_collection->sum('revenue'),
				'revenue_cash'		=> $rows_collection->sum('revenue_cash'),
				'revenue_card'		=> $rows_collection->sum('revenue_card'),
				'revenue_invoice'   => $rows_collection->sum('revenue_invoice'),
				'revenue_online'	=> $rows_collection->sum('revenue_online'),
				'paid_count'		=> $rows_collection->sum('paid_count'),
				'units_count'		=> $rows_collection->sum('units_count'),
				'units_taken'		=> $rows_collection->sum('units_taken'),
				'units_sold'		=> $rows_collection->sum('units_sold')
			];

			foreach ($rows as $row) {
				$data[] = $row;
			}
		}

		$header = [
			'Пункт проката',
			'Всего',
			'Наличные',
			'Банк. карта',
			'Счет',
			'Онлайн',
			'Договоров оплачено',
			'Инвентаря выдано',
			'Инвентаря принято',
			'Товаров продано'
		];

		$table = [
			'head' => $header,
			'body' => $data
		];

		$filters = [
			'location_id'	=> [
				'label' => 'Точка проката',
				'options'	=> Location::select(['id', 'address as title'])->get()->toArray()
			],
		];

		return view('reports.base', compact('table', 'filters', 'date_start_input', 'date_end_input'));
	}

	public function inventory()
	{
		$this->authorize('index', \App\Report::class);
		return view('reports.inventory');
	}

	public function general()
	{
		$this->authorize('index', \App\Report::class);
		return view('reports.general');
	}

	public function other()
	{
		$this->authorize('index', \App\Report::class);
		return view('reports.other');
	}
}
