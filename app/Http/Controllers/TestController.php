<?php

namespace App\Http\Controllers;

use App\Location;
use Carbon\Carbon;
use App\Services\OrderService;
use App\Order;
use App\Unit;
use Illuminate\Http\Request;

class TestController extends Controller
{
    public function api()
    {
        /*
        $order = Order::create([
            'location_id'   => 1,
            'user_id'       => auth()->user()->id,
            'client_id'     => 1,
            'status'        => Order::STATUS_DRAFT,
            'duration'      => 4,
            'dimension'     => 'd'
        ]);

        $orderService = new OrderService($order);
        $orderService->calculateOrderTime();

        $unit = Unit::find(1);
        $orderService->attachUnit($unit);
        */

        /*
        $order = Order::find(8);
        $orderService = new OrderService($order);
        //$orderService->calculateReservationPrice($order->reservations()->first());
        $orderService->calculateOrderPrice();
        */

        $order = Order::find(46);
        $orderService = new OrderService($order);
        $reservation = $order->reservations()->first();
        $orderService->calculateReservationExtraPrice($reservation);
        $order->fresh();

        dd($order->reservations()->first()->toArray());
    }

    public function schedule()
    {
        // филиал
        $location = Location::find(1);
        // текущее время с округлением до 5 минут в большую сторону
        // $now = $this->roundUpToMinuteInterval(now(), 5);
        $now = Carbon::parse('2018-11-09 18:00:00');

        // время работы филиала
        $opens_at       = $location->opens_at;       // открытие
        $opened_at      = Carbon::parse($now->format('Y-m-d ' . $opens_at . ':00'));    // открытие сегодня
        $closes_at      = $location->closes_at;      // закрытие
        $closed_at      = Carbon::parse($now->format('Y-m-d ' . $closes_at . ':00'));   // закрытие сегодня

        // если время закрытия после 0
        if($opened_at->gte($closed_at)) $closed_at->addDays(1);

        // настройки филиала
        $return_by      = $location->return_by;      // бесплатный возврат до
        $next_day_at    = $location->next_day_at;    // начало отсчета со следующего дня если в прокат взято после времени
        $all_day        = !$location->all_day;        // круглосуточный режим работы

        // выбраный тариф
        $duration       = 8;
        $dimension      = 'h';
        /*
        $step_m         = 10;
        $step_h         = 1;
        $step_d         = 1;
        $step           = 'h';
        */

        // поправка на hours_max
        if($dimension === 'h' && $duration > $location->hours_max) {
            $dimension  = 'd';
            $duration   = 1;
        }

        // параметр next_day для сегодняшнего дня
        $next_day_by    = Carbon::parse($now->format('Y-m-d ' . $next_day_at . ':00'));
        // начало отсчета со следующего дня ?
        $is_next_day    = $now->gte($next_day_by);
        $is_today       = $next_day_by->gt($now);

        // начало аренды. если начало с сегодня (или круглосуточный) – текущее время, если нет – завтра с открытия филиала
        $starts_at      = ($is_today || $all_day) ? $now : $opened_at->copy()->addDays(1);

        // тариф в часах
        if($dimension === 'h')  $ends_at = $starts_at->copy()->addHours($duration);
        // тариф в днях
        if($dimension === 'd')  $ends_at = $starts_at->copy()->addDays($duration);
        // тариф в минутах
        if($dimension === 'm')  $ends_at = $starts_at->copy()->addMinutes($duration);

        // круглосуточный филиал
        if($all_day) {
            // $ends_at = $ends_at
        }
        // часы работы
        else {
            // если оканчивается до часов работы СЕГОДНЯ – возврат в текущее время
            if($closed_at->gt($ends_at)) {
                // $ends_at = $ends_at
            }

            // если оканчивается после часов работы – возврат через сутки до бесплатного возврата
            else {
                if($ends_at->format('d') === $closed_at->format('d'))
                $ends_at->addDays(1);
                $ends_at = Carbon::parse($ends_at->format('Y-m-d ' . $return_by . ':00'));
            }
        }

        $result = [
            'opens_at'      => $opens_at,
            'closes_at'     => $closes_at,
            'return_by'     => $return_by,
            'next_day_at'   => $next_day_at,
            'all_day'       => $all_day,
            'tariff'        => $duration . ' ' . $dimension,
            'now'           => $now,
            'starts_at'     => $starts_at,
            'ends_at'       => $ends_at,
        ];

        dd($result);
    }

    /*
    public function schedule_old()
    {
    	// текущее время с округлением до 5 минут в большую сторону
    	// $now = $this->roundUpToMinuteInterval(now(), 5);
    	$now = Carbon::parse('2018-11-09 18:30:00');

    	// время работы филиала
    	$opens_at		= '9:00';		// открытие
    	$opened_at		= Carbon::parse($now->format('Y-m-d ' . $opens_at . ':00'));	// открытие сегодня
    	$closes_at		= '23:00';		// закрытие
    	$closed_at		= Carbon::parse($now->format('Y-m-d ' . $closes_at . ':00'));	// закрытие сегодня

    	// настройки филиала
    	$return_by		= '10:20';		// бесплатный возврат до
    	$next_day_at	= '19:00';		// начало отсчета со следующего дня если в прокат взято после времени
    	$day_is			= 'working';	// день это 24 часа или рабочие часы (hours)
    	$all_day		= false;		// круглосуточный режим работы

    	// выбраный тариф
    	$duration		= 12;
    	$dimension		= 'h';
    	$step_m			= 10;
    	$step_h			= 1;
    	$step_d			= 1;
    	$step 			= 'h';

    	// параметр next_day для сегодняшнего дня
    	$next_day_by	= Carbon::parse($now->format('Y-m-d ' . $next_day_at . ':00'));
    	// начало отсчета со следующего дня ?
    	$is_next_day	= $now->gte($next_day_by);
    	$is_today		= $next_day_by->gt($now);

    	// начало аренды. если начало с сегодня (или круглосуточный) – текущее время, если нет – завтра с открытия филиала
    	$starts_at		= (!$is_next_day || $all_day) ? $now : $opened_at->copy()->addDays(1);

    	// окончание аренды
    	// день - 24 часа. ЕСЛИ 24 ЧАСА ЭТО КРУГЛОСУТОЧНЫЙ ПРОКАТ, значит просто прибавляем время и это получается время сдачи стаффа
    	if($day_is === '24') {
    		// тариф в часах
    		if($dimension === 'h')	$ends_at = $starts_at->copy()->addHours($duration);
    		// тариф в днях
    		if($dimension === 'd')	$ends_at = $starts_at->copy()->addDays($duration);
    		// тариф в минутах
    		if($dimension === 'm')	$ends_at = $starts_at->copy()->addMinutes($duration);

    		// если оканчивается сегодня до закрытия
	    	if($closed_at->gt($ends_at)) {
	    		$ends_at = $ends_at;
	    	}

    		// если не круглосуточный – возврат до бесплатного времени возврата
    		else if(!$all_day) {
    			// время открытия в день сдачи
    			// $opened_at_return_day 	= Carbon::parse($ends_at->format('Y-m-d ' . $opens_at . ':00'));
    			$return_by_return_day	= Carbon::parse($ends_at->format('Y-m-d ' . $return_by . ':00'));

    			// return_by_return_day == текущий день, то прибавить один день
    			if($return_by_return_day->format('d') === $starts_at->format('d')) {
    				$return_by_return_day->addDays(1);
    			}

    			// если оканчивается до бесплатного времени
    			if($return_by_return_day->gt($ends_at)) {
    				$ends_at = $return_by_return_day;
    			}
	    	}

	    	// если круглосуточный возврат без корректировки времени
	    	else {
	    		$ends_at = $ends_at;
	    	}
    	}

    	// день - рабочее время
    	// all_day в этом случае не работает
    	else {
    		// подсчет количества арендных часов в день
    		$time_applied = 0;
    		$ends_at = $starts_at->copy();

    		if($step === 'm') $ends_at->addMinutes($duration);
    		if($step === 'd') $ends_at->addDays($duration);

    		// добавляется по периоду, пока не достигнет закрытия
    		// ТОЛЬКО ДЛЯ ПОЧАСОВЫХ
    		if($step === 'h') {
	    		for($i = 0; $i < $duration; $i++) {
	    			$ends_at->addHours(1);

	    			// расчет времени закрытия в текущий день
	    			$closed_at = Carbon::parse($ends_at->format('Y-m-d ' . $closes_at));

	    			// если ends_at > closed_at, перенос даты окончания на начало следующего дня
	    			if($ends_at->gte($closed_at)) {
	    				$ends_at = Carbon::parse($ends_at->addDays(1)->format('Y-m-d ' . $opens_at));
	    			}

	    			// проверка сколько лишнего времени добавили

	    			$time_applied++;
	    		}
	    	}
    	}

    	$result = [
    		'opens_at'		=> $opens_at,
    		'closes_at'		=> $closes_at,
    		'return_by'		=> $return_by,
    		'next_day_at'	=> $next_day_at,
    		'day_is'		=> $day_is,
    		'all_day'		=> $all_day,
    		'tariff'		=> $duration . ' ' . $dimension,
    		'now'			=> $now,
    		'starts_at'		=> $starts_at,
    		'ends_at'		=> $ends_at,
    	];

    	dd($result);
    }
    */

    // округление минут
    public function roundUpToMinuteInterval(\DateTime $dateTime, $minuteInterval = 10)
	{
	    return $dateTime->setTime(
	        $dateTime->format('H'),
	        ceil($dateTime->format('i') / $minuteInterval) * $minuteInterval,
	        0
	    );
	}
}
