<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ExpiredPassport;
use App\Client;

class ExpiredPassportController extends Controller
{
	public function upload()
	{
	    $filename = base_path() . '/list_of_expired_passports.csv';

	    $handle = fopen($filename, "r");
		if ($handle) {
			$i = 0;
		    while (($line = fgets($handle)) !== false) {
		    	if($i == 0){
		    		$i++;
		    		continue;
		    	}

		        $line = str_replace("\n", "", $line);
		        $series = explode(',', $line)[0];
		        $number = explode(',', $line)[1];

		        ExpiredPassport::create([
		        	'series' => $series,
		        	'number' => $number
		        ]);

		        $i ++;
		        if($i == 1000000) break;
		    }

		    fclose($handle);
		}
	}

	public function check(Request $request)
	{
		$series = explode(' ', $request->data)[0];
		$number = explode(' ', $request->data)[1];

		$client = Client::find($request->client_id);

		if(ExpiredPassport::where('series', $series)->where('number', $number)->exists())
		{
			if(!is_null($client))
			{
				$client->update([
					'passport_status' => Client::PASSPORT_STATUS_INVALID,
					'passport' => $series . $number
				]);
			}
			

			return response()->json([
				'success' => false
			]);
		}
		else
		{
			if(!is_null($client))
			{
				$client->update([
					'passport_status' => Client::PASSPORT_STATUS_VALID,
					'passport' => $series . $number
				]);
			}
		}

		return response()->json([
			'success' => true
		]);
	}
}
