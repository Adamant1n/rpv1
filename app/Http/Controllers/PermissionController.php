<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

use App\Tables\PermissionTable;

class PermissionController extends Controller
{

	public function __construct()
	{
		$this->authorizeResource(Permission::class);
	}

    public function index(Request $request)
    {
    	$dataTable = new PermissionTable($request);

    	if($request->ajax())
    	{
    		$data = $dataTable->getData();
    		return response()->json($data);
    	}

        return view('permissions.index', compact('dataTable'));
    }

    public function create(Permission $permission)
    {
        return view('permissions.form', compact('permission'));
    }

    public function store(Request $request)
    {
        $permission = Permission::create($request->all());

        return redirect()->route('permissions.index')->with('notification', ['success', 'Разрешение сохранено']);
    }

    public function edit(Permission $permission)
    {
        return view('permissions.form', compact('permission'));
    }

    public function update(Request $request, Permission $permission)
    {
        $permission->update($request->all());

        return redirect()->route('permissions.index')->with('notification', ['success', 'Разрешение сохранено']);
    }

    public function destroy(Permission $permission)
    {
        $permission->delete();

        return redirect()->route('permissions.index')->with('notification', ['success', 'Разрешение удалено']);
    }
}
