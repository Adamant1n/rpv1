<?php

namespace App\Http\Controllers;

use App\Tariff;
use App\ProductCategory;
use App\PriceList;
use App\Location;
use Illuminate\Http\Request;

class TariffController extends Controller
{
	public function __construct()
	{
		$this->middleware(function ($request, $next) {

			$this->user = auth()->user();

			return $next($request);
		});
	}

	public function index(Request $request)
	{
		$location = Location::find($request->location);

		if ($this->user->hasRole('superadmin'))
		{
			if (empty($request->location))
				$tariffs = Tariff::all();
			else
				$tariffs = $location->tariffs;

			$locations = Location::all();
		}
		else
		{
			$tariffs = $location->tariffs;

			$locations = Location::find($this->user->location_id);
		}

		$categories = ProductCategory::all();

		$price_lists = PriceList::all();

		return view('tariffs.index', compact(
			'tariffs',
			'categories',
			'price_lists',
			'locations',
			'request'
		));
	}

	public function create(Request $request)
	{
		$categories = ProductCategory::all();

		$price_lists = PriceList::all();

		$locations = Location::all();

		return view('tariffs.create', compact(
			'categories',
			'price_lists',
			'request',
			'locations'
		));
	}

	public function store(Request $request)
	{
		request()->validate([
			'price'         => 'required|string',
			'category_id'   => 'required|integer',
			'price_list_id' => 'required|integer',
			'duration'      => 'required|string',
			'dimension'     => 'required|string'
		]);

		Tariff::create($request->except('price_list','location_id'));

		if (empty($request->price_list))
			return redirect()->route('tariffs.index')->with('success', 'Тариф успешно создан');
		else
			return redirect()->route('price_lists.edit', $request->price_list_id)->with('success', 'Тариф успешно создан');
	}

	public function show($id)
	{
		//
	}

	public function edit(Tariff $tariff)
	{
		$categories = ProductCategory::all();

		$price_lists = PriceList::all();

		return view('tariffs.edit', compact(
			'tariff',
			'categories',
			'price_lists'
		));
	}

	public function update(Request $request, Tariff $tariff)
	{
		request()->validate([
			'price'         => 'required|string',
			'category_id'   => 'required|integer',
			'price_list_id' => 'required|integer',
			'duration'      => 'required|string',
			'dimension'     => 'required|string'
		]);

		$tariff->update($request->all());

		return redirect()->route('tariffs.index')->with('success', 'Тариф успешно сохранен');
	}

	public function destroy(Tariff $tariff)
	{
		$tariff->delete();

		return redirect()->route('tariffs.index')->with('success', 'Тариф удален');
	}
}
