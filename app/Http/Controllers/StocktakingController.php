<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Unit;
use App\Category;
use App\Location;
use App\Reservation;
use App\Stocktaking;

class StocktakingController extends Controller
{
    public function index()
    {
        $this->authorize('index', Unit::class);

    	$stocktakings = Stocktaking::with('location')->get();

    	return view('stocktaking.index', compact('stocktakings'));
    }

    public function perform()
    {
        $this->authorize('index', Unit::class);

    	$locations = Location::all();
    	$categories = Category::all();

    	return view('stocktaking.perform', compact('locations', 'categories'));
    }

    public function getUnits(Request $request)
	{
        $this->authorize('index', Unit::class);

		$codes = null;

		if($request->has('file') && $request->file != 'undefined')
		{
			$codes = file_get_contents($request->file->getRealPath());
			$codes = preg_split('/\n|\r\n?/', $codes);
		}

    	$location_id = $request->location_id;

    	if(empty($request->categories) || ($request->categories[0] == null && count($request->categories) == 1))
    	{
    		$categories = false;
    	}
    	else
    	{
    		$categories = explode(',', $request->categories);
    	}


    	$units = Unit::where('location_id', $location_id)
    		->with('product.brand', 'product.category', 'condition')
    		->join('products', 'products.id', 'units.product_id')
    		->when($categories, function ($query) use ($categories) {
    			$query->whereIn('products.category_id', $categories);
    		})
    		->when($codes, function ($query) use ($codes) {
    			$query->whereNotIn('code', $codes);
    		})
    		->select('units.*')
    		->get();

        $totalCount = Unit::where('location_id', $location_id)
            ->with('product.brand', 'product.category', 'condition')
            ->join('products', 'products.id', 'units.product_id')
            ->when($categories, function ($query) use ($categories) {
                $query->whereIn('products.category_id', $categories);
            })
            ->select('units.*')
            ->count();

    	$units->map(function ($unit) {
    		$badges = [
				0 => ['color' => 'danger','text' => 'в наличии'],
				1 => ['color' => 'warning','text' => 'резерв'],
			];

			if($unit->count == 0)
				$unit->badge = '<label class="badge badge-danger">продано</label>';
			else 
				$unit->badge = '<label class="badge badge-'.$badges[$unit->is_reserved]['color'].'">'
					.$badges[$unit->is_reserved]['text'].'</label>';
    	});

    	$output = [];

    	foreach($units as $unit) {
    		$output[] = [
	        	$unit->code,
	        	$unit->product->category->name,
	        	$unit->product->brand->name,
	        	$unit->product->name,
	        	$unit->condition->name,
	        	$unit->count,
	        	$unit->badge,
	        	$unit->is_reserved
	        ];
    	}



    	return response()->json([
            'units' => $output,
            'codes' => $codes,
            'total' => $totalCount,
            'in_stock' => is_null($codes) ? 0 : count($codes),
        ]);
	}

	public function show(Request $request, Stocktaking $stocktaking)
	{
        $this->authorize('index', Unit::class);

		$stocktaking->load('units.unit.product.brand', 'units.unit.product.category', 'units.condition');

		$stocktaking->units->map(function ($unit) {
    		$badges = [
				0 => ['color' => 'danger','text' => 'в наличии'],
				1 => ['color' => 'warning','text' => 'резерв'],
			];

			if($unit->unit->count == 0)
			{
				$unit->badge = '<label class="badge badge-danger">продано</label>';
			}
			else if($unit->unit->is_reserved)
			{

				$unit->badge = '<label class="badge badge-warning">резерв</label>';
			}
			else if($unit->condition_id == Unit::CONDITION_OK)
			{
				$unit->badge = '<label class="badge badge-success">в наличии</label>';
			}
			else if($unit->condition_id == Unit::CONDITION_LOST)
			{
				$unit->badge = '<label class="badge badge-danger">утерян</label>';
			}
			else
			{
				$unit->badge = '<label class="badge badge-info">undefined</label>';
			}
    	});

		return view('stocktaking.show', compact('stocktaking'));
	}

	public function store(Request $request)
	{
        $this->authorize('index', Unit::class);
        
		$codes = json_decode($request->codes);
    	$location_id = $request->location_id;

    	if(($request->categories[0] == null && count($request->categories) == 1) || empty($request->categories))
    	{
    		$categories = false;
    	}
    	else
    	{
    		$categories = $request->categories;
    	}

    	$lostUnitsCurrent = Unit::where('location_id', $location_id)
    		->with('product.brand', 'product.category', 'condition')
    		->join('products', 'products.id', 'units.product_id')
    		->when($categories, function ($query) use ($categories) {
    			$query->whereIn('products.category_id', $categories);
    		})
    		->whereNotIn('code', $codes)
    		->select('units.*')
    		->get();

    	$reserved_count = 0;

    	foreach($lostUnitsCurrent as $unit) {
    		if($unit->is_reserved)
    		{
    			$reserved_count++;
    			continue;
    		}

    		$unit->update([
	    		'condition_id' => Unit::CONDITION_LOST
	    	]);
    	}

    	$lostUnitsCurrent->map(function ($unit) {
    		$badges = [
				0 => ['color' => 'danger','text' => 'в наличии'],
				1 => ['color' => 'warning','text' => 'резерв'],
			];

			if($unit->count == 0)
				$unit->badge = '<label class="badge badge-danger">продано</label>';
			else 
				$unit->badge = '<label class="badge badge-'.$badges[$unit->is_reserved]['color'].'">'
					.$badges[$unit->is_reserved]['text'].'</label>';
    	});

    	$unitsCurrent = Unit::where('location_id', $location_id)
    		->join('products', 'products.id', 'units.product_id')
    		->when($categories, function ($query) use ($categories) {
    			$query->whereIn('products.category_id', $categories);
    		})
    		->whereIn('code', $codes)
    		->select('units.*')
    		->get();

		$units = Unit::where('location_id', $location_id)
    		->join('products', 'products.id', 'units.product_id')
    		->when($categories, function ($query) use ($categories) {
    			$query->whereIn('products.category_id', $categories);
    		})
    		->select('units.*')
    		->get();    	

    	$result = [
    		'overall' => [
    			'lost' => $units->where('condition_id', Unit::CONDITION_LOST)->count(),
    			'ok' => $units->where('condition_id', Unit::CONDITION_OK)->count(),
    			'other' => $units->where('condition_id', '<>', Unit::CONDITION_OK)->where('condition_id', '<>', Unit::CONDITION_LOST)->count(),
    		],
    		'current' => [
    			'lost' => $lostUnitsCurrent->count() - $reserved_count,
    			'ok' => $unitsCurrent->where('condition_id', Unit::CONDITION_OK)->count(),
    			'other' => $unitsCurrent->where('condition_id', '<>', Unit::CONDITION_OK)->where('condition_id', '<>', Unit::CONDITION_LOST)->count(),
    		]
    	];

    	$stocktaking = Stocktaking::create([
    		'location_id'	=> $location_id,
    		'count_lost'	=> $lostUnitsCurrent->count() - $reserved_count,
    		'count_ok'		=> $unitsCurrent->where('condition_id', Unit::CONDITION_OK)->count(),
    		'count_other'	=> $unitsCurrent->where('condition_id', '<>', Unit::CONDITION_OK)->where('condition_id', '<>', Unit::CONDITION_LOST)->count(),
    	]);

    	foreach($lostUnitsCurrent as $unit)
    	{
    		if($unit->is_reserved)
    		{
    			continue;
    		}

    		$stocktaking->units()->create([
    			'unit_id'	=> $unit->id,
    			'condition_id'	=> $unit->condition_id,
    		]);
    	}

    	foreach($unitsCurrent as $unit)
    	{
    		$stocktaking->units()->create([
    			'unit_id'	=> $unit->id,
    			'condition_id'	=> $unit->condition_id,
    		]);
    	}

		return view('stocktaking.result', compact('result', 'lostUnitsCurrent'));
	}
}
