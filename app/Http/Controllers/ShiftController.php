<?php

namespace App\Http\Controllers;

use App\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Schema;
use Webpatser\Uuid\Uuid;
use App\Shift;
use App\User;
use App\Order;
use Illuminate\Support\Facades\Auth;

class ShiftController extends Controller
{

    public function xreport()
    {
        $uuid = Uuid::generate()->string;

        $request = [
            'uuid'      => $uuid,
            'request'   => [
                'type'      => 'reportX',
                'operator'  => [
                    'name'  => Auth::user()->last_name,
                    'vatin' => Auth::user()->atol_id,
                ]
            ]
        ];

        $client = new \GuzzleHttp\Client();
        $url = config('services.atol.url') . 'requests';
        try {
            $response = $client->request('POST', $url, ['json' => $request]);

            return redirect()->route('shifts.index')->with('success', 'Отчет отправлен на печать');
        } catch (\Exception $e) {
            info('X-Report Error: ' . $e->getMessage());

            return redirect()->route('shifts.index')->with('success', 'Произошла ошибка: ' . $e->getMessage());
        }
    }

    public function report($shiftId)
    {
        $shift = Shift::findOrFail($shiftId);
        $shift->load('user.location');

        $user = $shift->user;
        $location = $user->location;
        $closed_at = $shift->closed_at ?? now();

        $html = file_get_contents(resource_path('documents/report.html'));
        $table = file_get_contents(resource_path('documents/table.html'));
        $row = file_get_contents(resource_path('documents/row.html'));


        $orders = Order::where('payment_accepted_by', $user->id)
            ->where('paid_at', '>=', $shift->opened_at)
            ->where('paid_at', '<=', $closed_at)
            ->where(function ($query) {
                $query->where('status', 'paid')
                    ->orWhere('status', 'ended');
            })
            ->with('reservations', 'payments')
            ->get();

        $tableCash = $table;
        $tableCashRows = '';
        $tableCard = $table;
        $tableCardRows = '';
        $tableInvoice = $table;
        $tableInvoiceRows = '';

        $tableSalesCash = $table;
        $tableSalesCashRows = '';
        $tableSalesCard = $table;
        $tableSalesCardRows = '';
        $tableSalesInvoice = $table;
        $tableSalesInvoiceRows = '';

        $tableDebts = $table;
        $tableDebtsRows = '';

        $cashRevenue = 0;
        $cardRevenue = 0;
        $invoiceRevenue = 0;
        $revenue = 0;

        $cashSalesRevenue = 0;
        $cardSalesRevenue = 0;
        $invoiceSalesRevenue = 0;

        $cashDebtRevenue = 0;
        $cardDebtRevenue = 0;
        $invoiceDebtRevenue = 0;
        $debts = 0;

        foreach ($orders as $order) {
            $orderPrice = 0;

            $orderCash = 0;
            $orderCard = 0;
            $orderInvoice = 0;
            $orderSalesCash = 0;
            $orderSalesCard = 0;
            $orderSalesInvoice = 0;
            $orderRefund = 0;
            $orderDebts = 0;

            foreach ($order->payments->where('type', 'debt') as $debt) {
                $newRow = $row;
                $variables = [
                    '{{price}}'         => $debt->sum,
                    '{{refund}}'        => '–',
                    '{{time}}'          => $debt->created_at->format('H:i'),
                    '{{last_name}}'     => $order->client->last_name,
                    '{{customer_id}}'   => $order->client->optional_id,
                    '{{login}}'         => '–',
                ];

                $debts += $debt->sum;

                if ($order->paid_by == Order::PAID_CASH) {
                    $cashDebtRevenue += $debt->sum;
                } elseif ($order->paid_by == Order::PAID_CARD) {
                    $cardDebtRevenue += $debt->sum;
                } elseif ($order->paid_by == Order::PAID_INVOICE) {
                    $invoiceDebtRevenue += $debt->sum;
                }

                $newRow = str_replace(array_keys($variables), array_values($variables), $newRow);
                $tableDebtsRows .= $newRow;
            }

            foreach ($order->reservations as $reservation) {
                if ($reservation->status == 'sold') {
                    if ($order->paid_by == Order::PAID_CASH) {
                        $orderSalesCash += $reservation->price;
                        $cashSalesRevenue += $reservation->price;
                    } elseif ($order->paid_by == Order::PAID_CARD) {
                        $orderSalesCard += $reservation->price;
                        $cardSalesRevenue += $reservation->price;
                    } elseif ($order->paid_by == Order::PAID_INVOICE) {
                        $orderSalesInvoice += $reservation->price;
                        $invoiceSalesRevenue += $reservation->price;
                    }
                } else if ($reservation->status == 'rent' || $reservation->status == 'returned') {
                    if ($order->paid_by == Order::PAID_CASH) {
                        $orderCash += $reservation->price;
                        $cashRevenue += $reservation->price;
                    } elseif ($order->paid_by == Order::PAID_CARD) {
                        $orderCard += $reservation->price;
                        $cardRevenue += $reservation->price;
                    } elseif ($order->paid_by == Order::PAID_INVOICE) {
                        $orderInvoice += $reservation->price;
                        $invoiceRevenue += $reservation->price;
                    }
                }

                $orderRefund += $reservation->refund;
            }

            if ($orderCash != 0) {
                $newRow = $row;
                $variables = [
                    '{{price}}'         => $orderCash,
                    '{{refund}}'        => $orderRefund,
                    '{{time}}'          => $order->paid_at->format('H:i'),
                    '{{last_name}}'     => $order->client->last_name,
                    '{{customer_id}}'   => $order->client->optional_id,
                    '{{login}}'         => '–',
                ];

                $newRow = str_replace(array_keys($variables), array_values($variables), $newRow);
                $tableCashRows .= $newRow;
            }

            if ($orderCard != 0) {
                $newRow = $row;
                $variables = [
                    '{{price}}'         => $orderCard,
                    '{{refund}}'        => $orderRefund,
                    '{{time}}'          => $order->paid_at->format('H:i'),
                    '{{last_name}}'     => $order->client->last_name,
                    '{{customer_id}}'   => $order->client->optional_id,
                    '{{login}}'         => '–',
                ];

                $newRow = str_replace(array_keys($variables), array_values($variables), $newRow);
                $tableCardRows .= $newRow;
            }

            if ($orderInvoice != 0) {
                $newRow = $row;
                $variables = [
                    '{{price}}'         => $orderInvoice,
                    '{{refund}}'        => $orderRefund,
                    '{{time}}'          => $order->paid_at->format('H:i'),
                    '{{last_name}}'     => $order->client->last_name,
                    '{{customer_id}}'   => $order->client->optional_id,
                    '{{login}}'         => '–',
                ];

                $newRow = str_replace(array_keys($variables), array_values($variables), $newRow);
                $tableInvoiceRows .= $newRow;
            }

            if ($orderSalesCash != 0) {
                $newRow = $row;
                $variables = [
                    '{{price}}'         => $orderSalesCash,
                    '{{time}}'          => $order->paid_at->format('H:i'),
                    '{{last_name}}'     => $order->client->last_name,
                    '{{customer_id}}'   => $order->client->optional_id,
                    '{{login}}'         => '–',
                ];

                $newRow = str_replace(array_keys($variables), array_values($variables), $newRow);
                $tableSalesCashRows .= $newRow;
            }

            if ($orderSalesCard != 0) {
                $newRow = $row;
                $variables = [
                    '{{price}}'         => $orderSalesCard,
                    '{{time}}'          => $order->paid_at->format('H:i'),
                    '{{last_name}}'     => $order->client->last_name,
                    '{{customer_id}}'   => $order->client->optional_id,
                    '{{login}}'         => '–',
                ];

                $newRow = str_replace(array_keys($variables), array_values($variables), $newRow);
                $tableSalesCardRows .= $newRow;
            }

            if ($orderSalesInvoice != 0) {
                $newRow = $row;
                $variables = [
                    '{{price}}'         => $orderSalesInvoice,
                    '{{time}}'          => $order->paid_at->format('H:i'),
                    '{{last_name}}'     => $order->client->last_name,
                    '{{customer_id}}'   => $order->client->optional_id,
                    '{{login}}'         => '–',
                ];

                $newRow = str_replace(array_keys($variables), array_values($variables), $newRow);
                $tableSalesInvoiceRows .= $newRow;
            }
        }

        $tableCash = str_replace('{{rows}}', $tableCashRows, $tableCash);
        $tableCash = str_replace('{{total}}', $cashRevenue, $tableCash);
        $tableCard = str_replace('{{rows}}', $tableCardRows, $tableCard);
        $tableCard = str_replace('{{total}}', $cardRevenue, $tableCard);
        $tableInvoice = str_replace('{{rows}}', $tableInvoiceRows, $tableInvoice);
        $tableInvoice = str_replace('{{total}}', $invoiceRevenue, $tableInvoice);
        $tableSalesCash = str_replace('{{rows}}', $tableSalesCashRows, $tableSalesCash);
        $tableSalesCash = str_replace('{{total}}', $cashSalesRevenue, $tableSalesCash);
        $tableSalesCard = str_replace('{{rows}}', $tableSalesCardRows, $tableSalesCard);
        $tableSalesCard = str_replace('{{total}}', $cardSalesRevenue, $tableSalesCard);
        $tableSalesInvoice = str_replace('{{rows}}', $tableSalesInvoiceRows, $tableSalesInvoice);
        $tableSalesInvoice = str_replace('{{total}}', $invoiceSalesRevenue, $tableSalesInvoice);

        $tableDebts = str_replace('{{rows}}', $tableDebtsRows, $tableDebts);
        $tableDebts = str_replace('{{total}}', $debts, $tableDebts);

        $totalCashRevenue = $cashRevenue + $cashSalesRevenue + $cashDebtRevenue;
        $totalCardRevenue = $cardRevenue + $cardSalesRevenue + $cardDebtRevenue;
        $totalInvoiceRevenue = $invoiceRevenue + $invoiceSalesRevenue + $invoiceDebtRevenue;
        $totalRevenue = $totalCashRevenue + $totalCardRevenue + $totalInvoiceRevenue;

        $variables = [
            '{{shift_number}}'  => $shift->id,
            '{{location_name}}' => $location->address,
            '{{legal_name}}'    => $location->legal_name,
            '{{opened_at}}'     => $shift->opened_at->format('H:i d.m.Y'),
            '{{closed_at}}'     => $closed_at->format('H:i d.m.Y'),
            '{{cashier_name}}'  => $user->name_short,
            '{{table_cash}}'    => $tableCash,
            '{{table_card}}'    => $tableCard,
            '{{table_invoice}}'    => $tableInvoice,
            '{{table_sale_cash}}' => $tableSalesCash,
            '{{table_sale_card}}' => $tableSalesCard,
            '{{table_sale_invoice}}' => $tableSalesInvoice,
            '{{revenue}}'       => $totalRevenue,
            '{{revenue_cash}}'  => $totalCashRevenue,
            '{{revenue_card}}'  => $totalCardRevenue,
            '{{revenue_invoice}}'  => $totalInvoiceRevenue,
            '{{table_debts}}' => $tableDebts,
            '{{kkt_serial}}' => '–',
        ];

        $html = str_replace(array_keys($variables), array_values($variables), $html);

        echo $html;
    }

    public function index()
    {
        if (auth()->user()->role->name == 'admin' || auth()->user()->role->name == 'superadmin') {
            $shifts = Shift::with('user')->latest()->get();
        } else {
            $shifts = auth()->user()->shifts()->with('user')->latest()->get();
        }

        return view('shifts.index', compact('shifts'));
    }

    public function open(Request $request)
    {
        $user = auth()->user();

        if ($user->shifts()->opened()->count() > 0) {
            return redirect()->route('shifts.index')->with('error', 'Невозможно открыть смену, пока не закрыта текущая');
        }

        $uuid = Uuid::generate()->string;

        $request = [
            'uuid'      => $uuid,
            'request'   => [
                'type'      => 'openShift',
                'operator'  => [
                    'name'  => $user->last_name,
                    'vatin' => $user->atol_id
                ]
            ]
        ];

        $client = new \GuzzleHttp\Client();
        $url = config('services.atol.url') . 'requests';
        try {
            info('Shift controller – try');
            $response = $client->request('POST', $url, ['json' => $request]);
            info('Shift controller – response');
            info((string) $response->getBody());
        } catch (\Exception $e) {
            info('Shift controller – error');
            info($e->getMessage());
        }

        $user->shifts()->create([
            'uuid'      => $uuid,
            'opened_at' => now()
        ]);

        return redirect()->route('shifts.index')->with('success', 'Смена открыта');
    }

    public function close(Request $request, $shiftId)
    {
        $shift = Shift::findOrFail($shiftId);

        $uuid = Uuid::generate()->string;

        $request = [
            'uuid'      => $uuid,
            'request'   => [
                'type'      => 'closeShift',
                'operator'  => [
                    'name'  => $shift->user->last_name,
                    'vatin' => $shift->user->atol_id
                ]
            ]
        ];

        $client = new \GuzzleHttp\Client();
        $url = config('services.atol.url') . 'requests';
        $response = null;
        try {
            $response = $client->request('POST', $url, ['json' => $request]);
        } catch (\Exception $e) {
        }

        $shift->update([
            'closed_at' => now()
        ]);

        return redirect()->route('shifts.index')->with('success', 'Смена успешно закрыта');
    }
}
