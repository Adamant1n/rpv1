<?php

namespace App\Http\Controllers;

use App\Agent;
use App\Product;
use App\Category;
use Illuminate\Http\Request;

class AgentController extends Controller
{
	public function __construct()
	{
		// $this->authorizeResource(Agent::class);
	}

	public function index(Request $request)
	{
		$this->authorize('index', Agent::class);

		if ($request->has('trashed'))
			$agents = Agent::onlyTrashed()->get();
		else
			$agents = Agent::all();

		return view('agents.index', compact('agents', 'request'));
	}

	public function create()
	{
		$this->authorize('create', Agent::class);

		return view('agents.create');
	}

	public function store(Request $request)
	{
		$this->authorize('create', Agent::class);

		request()->validate([
			'name'       => 'required|string|unique:agents',
			'promocode'  => 'required|string|unique:agents',
			'discount'   => 'required|integer',
			'commission' => 'required|integer'
		]);

		Agent::create($request->all());

		return redirect()->route('agents.index')->with('success', 'Агент успешно создан');
	}

	public function show($id)
	{
		//
	}

	public function edit($id)
	{
		$this->authorize('update', Agent::class);

		$agent = Agent::with('products')->withTrashed()->find($id);
		$categories = Category::with('products')->get();

		return view('agents.edit', compact('agent', 'categories'));
	}

	public function update(Request $request, Agent $agent)
	{
		$this->authorize('update', Agent::class);

		request()->validate([
			'name'       => 'required|string',
			'promocode'  => 'required|string',
			'discount'   => 'required|integer',
			'commission' => 'required|integer'
		]);

		$agent->update($request->except('products'));

		$products = [];
		if ($request->has('products')) {
			foreach ($request->products as $product) {
				$products[$product['id']] = ['discount' => $product['discount']];
			}

			$agent->products()->sync($products);
		}

		return redirect()->route('agents.index')->with('success', 'Агент успешно сохранен');
	}

	public function destroy(Agent $agent)
	{
		$this->authorize('delete', Agent::class);

		$agent->delete();

		return redirect()->route('agents.index')->with('success', 'Агент заблокирован');
	}

	public function restore($id)
	{
		$this->authorize('restore', Agent::class);

		Agent::withTrashed()->findOrFail($id)->restore();

		return redirect()->route('agents.index')->with('success', 'Агент разблокирован');
	}
}
