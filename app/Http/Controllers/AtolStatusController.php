<?php

namespace App\Http\Controllers;

use App\AtolStatus;
use Exception;
use Illuminate\Support\Facades\Log;
use Illuminate\support\Str;

class AtolStatusController extends Controller
{
    public function request()
    {
        $command = AtolStatus::create([
            'uuid' => Str::uuid(),
        ]);

        $request = [
            'uuid' => $command->uuid,
            'request' => [
                'type' => 'getDeviceStatus',
            ],
        ];

        $client = new \GuzzleHttp\Client;
        $url = config('services.atol.url') . 'requests';

        try {
            $client->request('POST', $url, ['json' => $request]);
        } catch (\Exception $e) {
            $command->update([
                'status' => false,
            ]);
        }

        return [
            'uuid' => $command->uuid,
        ];
    }

    public function ping($uuid)
    {
        $command = AtolStatus::latest()
            ->where('uuid', $uuid)
            ->first();

        Log::info('ping ' . $uuid);

        if ($command && !is_null($command->status)) {
            Log::info('command, !is_null');

            return [
                'active' => $command->status,
            ];
        }

        if (!$command) {
            Log::info('!command');

            return [
                'active' => null,
            ];
        }

        $client = new \GuzzleHttp\Client();
        $url = config('services.atol.url') . 'requests/' . $command->uuid;

        try {
            $response = $client->get($url);
            $body = json_decode($response->getBody()->getContents());

            $deviceStatus = $body->results[0]->result->deviceStatus;

            $status = !$deviceStatus->blocked && $deviceStatus->fiscal && $deviceStatus->paperPresent;

            if ($status) {
                Log::info('status true');

                $command->update([
                    'status' => true,
                ]);
            } else {
                Log::info('status false');

                $command->update([
                    'status' => false,
                ]);
            }
        } catch (Exception $e) {
            Log::info('exception status false');

            $command->update([
                'status' => false,
            ]);
        }

        return response()->json([
            'active' => $command ? $command->status : false,
        ]);
    }
}
