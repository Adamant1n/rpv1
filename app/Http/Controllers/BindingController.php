<?php

namespace App\Http\Controllers;

use App\Binding;
use Illuminate\Http\Request;

class BindingController extends Controller
{
    public function __construct()
    {
        
    }

    public function index()
    {
        $this->authorize('manageProperties', \App\Setting::class);

        $bindings = Binding::all();

        return view('bindings.index', compact('bindings'));
    }

    public function create()
    {
        $this->authorize('manageProperties', \App\Setting::class);

        return view('bindings.create');
    }

    public function store(Request $request)
    {
        $this->authorize('manageProperties', \App\Setting::class);

        request()->validate([
            'name' => 'required|string|max:100'
        ]);

        Binding::create($request->all());

        return redirect()->route('bindings.index')->with('success', 'Крепление успешно создано');
    }

    public function show($id)
    {
        //
    }

    public function edit(Binding $binding)
    {
        $this->authorize('manageProperties', \App\Setting::class);

        return view('bindings.edit', compact('binding'));
    }

    public function update(Request $request, Binding $binding)
    {
        $this->authorize('manageProperties', \App\Setting::class);

        request()->validate([
            'name' => 'required|string|max:100'
        ]);

        $binding->update($request->all());

        return redirect()->route('bindings.index')->with('success', 'Крепление успешно сохранено');
    }

    public function destroy(Binding $binding)
    {
        $this->authorize('manageProperties', \App\Setting::class);
        
        $binding->delete();

        return redirect()->route('bindings.index')->with('success', 'Крепление удалено');
    }
}
