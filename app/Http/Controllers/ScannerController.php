<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Client;
use App\Order;
use App\Unit;
use App\Agent;
use App\Location;
use App\Services\OrderService;
use App\Reservation;
use App\OrderGroup;
use App\Condition;
use App\Debt;
use App\Payment;
use App\Services\ShiftService;

class ScannerController extends Controller
{
	public function __construct()
	{
	}

	public function refund(Request $request)
	{
		$order_id = $request->order_id;
		$order = Order::findOrFail($order_id);

		$shiftService = new ShiftService(auth()->user());
		$shiftService->openShiftIfNeeded();

		if ($order->paid_by != Order::PAID_INVOICE) {
			$orderService = new OrderService($order);
			$orderService->printRefundReceipt();
		}

		$order->update([
			'refunded' => $order->refund,
		]);

		$order->load('reservations.unit.product.category', 'agent', 'group.reservations', 'group.orders.client.feature');
		$order->loadMissing('client.feature');

		return $order;
	}

	public function setPrice(Request $request)
	{
		$reservation = Reservation::findOrFail($request->pk);
		$reservation->update([
			'price_manual'	=> $request->value
		]);

		$order = $reservation->order;

		$orderService = new OrderService($order);
		$orderService->calculateOrderPrice();

		$order->load('reservations.unit.product.category', 'agent', 'group.reservations', 'group.orders.client.feature');

		return $order;
	}

	public function setPriceExtra(Request $request)
	{
		$reservation = Reservation::findOrFail($request->pk);

		$extraPrev = $reservation->price_extra;

		$reservation->update([
			'price_extra' => $request->value
		]);

		$order = $reservation->order;
		$priceDiff = $extraPrev - $request->value;

		$order->update([
			'price_extra' => $order->price_extra - $priceDiff,
		]);

		$order->load('reservations.unit.product.category', 'agent', 'group.reservations', 'group.orders.client.feature');

		return $order;
	}

	public function getClients(Request $request)
	{
		$order_id = $request->order_id;
		$order = Order::findOrFail($order_id);
		$order->load('group.orders');

		$currentClients = [];
		$currentClients[] = $order->client_id;
		if (!is_null($order->group)) {
			foreach ($order->group->orders as $order) {
				$currentClients[] = $order->client_id;
			}
		}

		$clients = Client::whereNotIn('id', $currentClients)->get();
		$collection = [];

		foreach ($clients as $client) {
			// $collection["{$client->optional_id}"] = $client->full_name;
			$collection[] = [
				'id' => $client->optional_id,
				'name' => $client->full_name
			];
		}

		return response()->json($collection);
	}

	public function updateCondition(Request $request)
	{
		$unit = Unit::where('code', $request->code)->first();
		$condition = Condition::findOrFail($request->condition_id);
		$unit->update(['condition_id' => $request->condition_id]);

		return response()->json(['success' => true]);
	}

	public function replace(Request $request)
	{
		$order = Order::findOrFail($request->order_id);
		$orderService = new OrderService($order);

		$oldUnit = Unit::where('code', $request->unit_1)->first();
		$newUnit = Unit::where('code', $request->unit_2)->first();

		if (!$oldUnit || !$newUnit) {
			return response()->json([
				'success' => false,
				'message' => 'Инвентарь отсутствует в базе данных'
			]);
		} else if ($oldUnit->product->category_id != $newUnit->product->category_id) {
			return response()->json([
				'success' => false,
				'message' => 'Категории нового и заменяемого инвентаря не соответствуют'
			]);
		} else {
			$reservation = $order->reservations()->where('unit_id', $oldUnit->id)->first();

			$orderService->attachUnit($newUnit);
			$orderService->takeUnit($reservation, true);

			// $reservation->update(['unit_id' => $newUnit->id]);

			if ($order->status === Order::STATUS_DRAFT) {
				$orderService->calculateOrderPrice();
			}
			$order->fresh();

			$order->load('reservations.unit.product.category', 'agent', 'group.reservations', 'group.orders.client.feature');

			return response()->json([
				'success' => true,
				'message' => 'Инвентарь успешно заменен',
				'clientData' => $order
			]);
		}
	}

	public function removeFromGroup(Request $request)
	{
		$client_id	= $request->client_id;
		$client = Client::findOrFail($client_id);

		$order = Order::findOrFail($request->order_id);
		$group = $order->group;

		$order->group_id = null;
		$order->update();

		$order->fresh();

		if ($group->orders()->count() == 0) {
			$client->load('orders', 'feature');
			$order->load('reservations.unit.product.category', 'agent', 'group.reservations', 'group.orders.client.feature');
			$client->current_order = $order;

			return $client;
		} else {
			$order = $group->orders()->with('reservations.unit.product.category', 'agent', 'group.reservations', 'group.orders.client.feature')->first();
			$client = $order->client()->with('feature', 'orders')->first();
			$client->current_order = $order;

			return $client;
		}
	}

	public function addToGroup(Request $request)
	{
		$barcode = $request->client_id;
		$client_id	= intval(substr($barcode, 1, strlen($barcode)));
		$client = Client::findOrFail($client_id);

		if ($client->in_group) {
			return response()->json(false);
		}

		$order = Order::findOrFail($request->order_id);
		$group = $order->group;
		// создать группу, если нет
		if (is_null($order->group_id)) {
			$group = OrderGroup::create([
				"client_id" => $order->client_id
			]);

			$order->update(["group_id" => $group->id]);
		}

		$orderService = new OrderService(new Order);
		$order = $orderService->createOrder($client);

		$order->group_id = $group->id;
		$order->update();

		$order->fresh();

		$client->load('orders', 'feature');
		$order->load('reservations.unit.product.category', 'agent', 'group.reservations', 'group.orders.client.feature');
		$client->current_order = $order;

		return $client;
	}

	// статус завершено
	public function end(Request $request)
	{
		$order = Order::findOrFail($request->order_id);
		$order->end();

		if ($order->group) {
			$group = OrderGroup::where('id', $order->group_id)->first()->append('price');
			$order->price = $group->price;
			$order->price_extra = $group->price_extra;
		}

		$order->load('reservations.unit.product.category', 'agent', 'group.reservations', 'group.orders.client.feature');

		return response()->json($order);
	}

	public function payExtra(Request $request)
	{
		$order = Order::findOrFail($request->order_id);
		$group = $order->group;

		$shiftService = new ShiftService(auth()->user());
		$shiftService->openShiftIfNeeded();

		if ($order->paid_by != Order::PAID_INVOICE) {
			$orderService = new OrderService($order);
			$orderService->printExtraReceipt();
		}

		$order->payments()->create([
			'sum'	=> $order->price_extra,
			'type'	=> Payment::TYPE_DEBT
		]);

		$order->update([
			'paid_extra' => $order->price_extra
		]);

		// если есть группа
		if (!is_null($order->group_id)) {
			foreach ($group->orders as $_order) {
				$_order->update([
					'paid_extra' => $order->price_extra
				]);

				if ($_order->id != $order->id) {
					$_order->payments()->create([
						'sum'	=> $order->price_extra,
						'type'	=> Payment::TYPE_DEBT
					]);
				}
			}
		}

		$order->load('reservations.unit.product.category', 'agent', 'group.reservations', 'group.orders.client.feature');
		$order->loadMissing('client.feature');

		if ($order->group) {
			$order->price = $group->price;
			$order->price_extra = $group->price_extra;
		}

		return response()->json($order);
	}

	// статус оплачено
	public function pay(Request $request)
	{
		$shiftService = new ShiftService(auth()->user());
		$shiftService->openShiftIfNeeded();

		$order = Order::findOrFail($request->order_id);
		$group = $order->group;

		if (auth()->user()->role->name != 'delivery' && $order->paid_by != Order::PAID_INVOICE) {
			$orderService = new OrderService($order);
			$orderService->printReceipt();
		} else {
			// $order->markAsPaid();
		}

		// temp
		$order->markAsPaid();

		$order->load('reservations.unit.product.category', 'agent', 'group.reservations', 'group.orders.client.feature');
		$order->loadMissing('client.feature');

		if ($order->group) {
			$order->price = $group->price;
			$order->price_extra = $group->price_extra;
		}

		return response()->json($order);
	}

	// принудительно отметить как оплаченный
	public function forcePay(Request $request)
	{
		$order = Order::findOrFail($request->order_id);
		$order->markAsPaid();

		$order->load('reservations.unit.product.category', 'agent', 'group.reservations', 'group.orders.client.feature');
		$order->loadMissing('client.feature');

		if ($order->group) {
			$order->price = $order->group->price;
			$order->price_extra = $order->group->price_extra;
		}

		return response()->json($order);
	}

	public function printReceipt(Request $request)
	{
		$order = Order::findOrFail($request->order_id);
		$orderService = new OrderService($order);

		$receipt = $orderService->printReceipt();

		return $receipt;
	}

	// ввод промокода
	public function promocode(Request $request)
	{
		$order = Order::findOrFail($request->order_id);

		if ($order->status === Order::STATUS_DRAFT || $order->status === Order::STATUS_STAFFED) {
			$agent = Agent::where('promocode', $request->promocode)->firstOrFail();

			if ($agent) {
				$order->update([
					'agent_id' => $agent->id,
					'discount' => $agent->discount
				]);
			}

			$orderService = new OrderService($order);
			$orderService->calculateOrderPrice();
		}

		$order->load('reservations.unit.product.category', 'agent', 'group.reservations', 'group.orders.client.feature', 'client.feature');

		return response()->json($order);
	}

	// отмена заказа
	public function cancelOrder(Request $request)
	{
		$order = Order::findOrFail($request->order_id);
		$order->load('reservations.unit.product.category', 'agent', 'group.reservations', 'group.orders.client.feature');
		$order->cancel();

		return response()->json($order);
	}

	// создание заказа
	public function createOrder(Request $request)
	{
		$user	= auth()->user();

		$client = Client::findOrFail($request->client_id);

		$location = $user->location;
		if ($location->tariff_step == Location::STEP_MINUTE) {
			$dimension = 'm';
			$duration = 10;
		} else if ($location->tariff_step == Location::STEP_HOUR) {
			$dimension = 'h';
			$duration = 1;
		} else {
			$dimension = 'd';
			$duration = 1;
		}

		if ($request->has('booking')) {
			$type = Order::TYPE_BOOK;
		} else if ($request->has('delivery')) {
			$type = Order::TYPE_DELIVERY;
		} else {
			$type = Order::TYPE_RENT;
		}

		$order 	= $client->orders()->create([
			'user_id'		=> $user->id,
			'location_id'	=> $user->location_id,
			'dimension'		=> $dimension,
			'duration'		=> $duration,
			'type'			=> $type
		]);

		$order = $order->fresh();
		$orderService = new OrderService($order);
		$order = $orderService->calculateOrderTime();

		$client->load('orders', 'feature');
		$order->load('reservations.unit.product.category', 'agent', 'group.reservations', 'group.orders.client.feature');
		$client->current_order = $order;

		return response()->json($client);
	}

	// получение заказа
	public function order(Request $request)
	{
		$order = Order::findOrFail($request->order_id);

		$order->load('reservations.unit.product.category', 'agent', 'group.reservations', 'group.orders.client.feature', 'client.feature', 'client.orders');

		// кассир, обработка группы
		if ($request->cashier == 1) {
			if ($order->group) {
				$group = OrderGroup::where('id', $order->group_id)->first()->append('price');
				$order->group = $group;
				$order->price = $group->price;
				$order->price_extra = $group->price_extra;
			}
		}

		return response()->json($order);
	}

	// получение заказа по штриху клиента
	public function client(Request $request)
	{
		if ($request->has('order_id')) {
			$order = Order::findOrFail($request->order_id);
			$client = Client::findOrFail($order->client_id);

			$client->load('orders', 'feature');
			$order->load('reservations.unit.product.category', 'agent', 'group.reservations', 'group.orders.client.feature');
			$client->current_order = $order;

			return response()->json($client);
			exit;
		}

		if ($request->has('event_id')) {
			$event = \App\Instructors\Event::find($request->event_id);

			if ($event->order_id) {
				$order = Order::findOrFail($event->order_id);
				$barcode    = $request->barcode;
				$client_id  = intval(substr($barcode, 1, strlen($barcode)));
				$client     = Client::findOrFail($client_id);

				$client->load('orders', 'feature');
				$order->load('reservations.unit.product.category', 'agent', 'group.reservations', 'group.orders.client.feature');
				$client->current_order = $order;

				return response()->json($client);
				exit;
			} else {
				$barcode    = $request->barcode;
				$client_id  = intval(substr($barcode, 1, strlen($barcode)));
				$client     = Client::findOrFail($client_id);

				$order  = $client->orders()->create([
					'user_id'       => auth()->user()->id,
					'location_id'   => auth()->user()->location_id,
					'dimension'     => 'd',
					'duration'      => 1,
				]);

				$event->update(['order_id' => $order->id]);

				$order = $order->fresh();
				$orderService = new OrderService($order);
				$order = $orderService->calculateOrderTime();

				$client->load('orders', 'feature');
				$order->load('reservations.unit.product.category', 'agent', 'group.reservations', 'group.orders.client.feature');
				$client->current_order = $order;

				return response()->json($client);
				exit;
			}
		}

		if ($request->has('unit_id')) {
			$unit = Unit::where('code', $request->unit_id)->first();

			if ($unit->current_order) {

				if (!$unit->current_order->reservations->where('unit_id', $unit->id)->where('status', '<>', Reservation::STATUS_RETURNED)->first()) {
					return response()->json(false);
					exit;
				}

				/*
				if ($unit->is_not_unique) {
					return response()->json(false);
					exit;
				}
				*/

				$order = $unit->current_order;
				$client = Client::findOrFail($order->client_id);

				$client->load('orders', 'feature');
				$order->load('reservations.unit.product.category', 'agent', 'group.reservations', 'group.orders.client.feature');
				$client->current_order = $order;

				return response()->json($client);
				exit;
			}

			return response()->json(false);
			exit;
		}

		$user		= auth()->user();
		$barcode	= $request->barcode;

		// id клиента без литеры
		$client_id	= intval(substr($barcode, 1, strlen($barcode)));
		$client 	= Client::findOrFail($client_id);

		$order 		= $client->currentOrder;

		if (!$order) {
			$location = $user->location;
			if ($location->tariff_step == Location::STEP_MINUTE) {
				$dimension = 'm';
				$duration = 10;
			} else if ($location->tariff_step == Location::STEP_HOUR) {
				$dimension = 'h';
				$duration = 1;
			} else {
				$dimension = 'd';
				$duration = 1;
			}

			$order 	= $client->orders()->create([
				'user_id'		=> $user->id,
				'location_id'	=> $user->location_id,
				'dimension'		=> $dimension,
				'duration'		=> $duration
			]);

			$order = $order->fresh();
			$orderService = new OrderService($order);
			$order = $orderService->calculateOrderTime();
		}

		$client->load('orders', 'feature');
		$order->load('reservations.unit.product.category', 'agent', 'group.reservations', 'group.orders.client.feature');

		// кассир, обработка группы
		if ($request->cashier == 1) {
			if ($order->group) {
				$group = OrderGroup::where('id', $order->group_id)->first()->append('price');
				$order->group = $group;
				$order->price = $group->price;
				$order->price_extra = $group->price_extra;
			}
		}

		$data = $client->toArray();
		$data['current_order'] = $order->toArray();

		return response()->json($data);
	}

	// обновить заказ
	public function updateOrder(Request $request)
	{
		$order = Order::findOrFail($request->order_id);

		if ($order->type == Order::TYPE_BOOK && $request->type == Order::TYPE_RENT) {
			foreach ($order->reservations as $reservation) {
				$reservation->update([
					'created_at' => now()
				]);
			}
		}

		if (($request->duration != $order->duration || $request->dimension != $order->dimension) && $request->duration != 0) {
			foreach ($order->reservations as $reservation) {
				if ($reservation->status == Reservation::STATUS_RENT) {
					$reservation->update([
						'duration'	=> $request->duration,
						'dimension' => $request->dimension
					]);
				}
			}
		}

		$doStaff = false;
		if ($order->status != Order::STATUS_STAFFED && $request->status == Order::STATUS_STAFFED) {
			$doStaff = true;
		}

		$order->update($request->except('order_id', 'book_at'));
		if ($request->has('book_at')) {
			$book_at = \Carbon\Carbon::parse($request->book_at)->toDateTimeString();
			$order->update(['book_at' => $book_at]);
		}

		$orderService = new OrderService($order);
		$orderService->calculateOrderTime();
		$orderService->calculateOrderPrice();

		if ($doStaff) {
			$order->loadMissing('onlineOrder');

			if ($order->onlineOrder) {
				if ($order->onlineOrder->payment_type == 'full') {
					$order->status = Order::STATUS_PAID;
					$order->save();
				} else {
				}
			}
		}

		$client = $order->client;

		$client->load('currentOrder.reservations.unit.product.category', 'currentOrder.agent', 'orders', 'feature', 'currentOrder.group.orders.client.feature');

		return response()->json($client);
	}

	// добавить / снять единицу товара
	public function unit(Request $request)
	{
		// manager
		$user = auth()->user();

		$barcode = $request->barcode;
		// $unit_id = intval(substr($barcode, 1, strlen($barcode)));
		$unit_id = $barcode;
		// $unit = Unit::findOrFail($unit_id);

		$unit = Unit::where('code', $unit_id)->firstOrFail();

		if ($unit->condition_id != Unit::CONDITION_OK) {
			return response()->json([
				'success' => false,
				'message' => 'Невозможно добавить утерянный и сломанный инвентарь'
			]);
		}

		$client = Client::findOrFail($request->client_id);

		// $order = $client->currentOrder;
		$order = Order::findOrFail($request->order_id);

		// неуникальный и не указано кол-во
		if ($unit->is_not_unique && !$request->has('count') && $order->status === Order::STATUS_DRAFT) {
			if ($order->reservations()->where('unit_id', $unit->id)->rent()->exists()) {
				return response()->json(['is_unique' => false]);
			} else {
				$request->count = 1;
			}
		}

		$orderService = new OrderService($order);

		// возврат товара, только take
		if ($order->status === Order::STATUS_PAID) {
			$reservation = $order->reservations()
				->where('unit_id', $unit->id)
				->where(function ($query) {
					$query->rent()
						->orWhere('status', Reservation::STATUS_SOLD);
				})
				->first();

			// info('sc 1: ' . $reservation->status);

			if ($reservation) {
				// info('sc 2: ' . $reservation->status);
				$orderService->takeUnit($reservation);
				// info('sc 3: ' . $reservation->status);
			} else {
				$reservation = Reservation::where('unit_id', $unit->id)->latest()->whereStatus(Reservation::STATUS_RENT)->first();
				if (optional($reservation)->order_id != $order->id) {
					$order = $unit->current_order;
					$client = Client::findOrFail($order->client_id);

					$client->load('orders', 'feature');
					$order->load('reservations.unit.product.category', 'agent', 'group.reservations', 'group.orders.client.feature');
					$client->current_order = $order;

					return response()->json($client);
					exit;
				}
			}
		}

		// оформление договора
		else if ($order->status === Order::STATUS_DRAFT || $order->status === Order::STATUS_STAFFED) {
			info('sc 4');
			$reservation = $order->reservations()->where('unit_id', $unit->id)->latest('id')->whereStatus(Reservation::STATUS_RENT)->first();

			if ($unit->is_not_unique && $request->action == 'remove') {
				info('sc 5');
				$count = $request->count;
				$realCount = $order->reservations()->where('unit_id', $unit->id)->rent()->count();

				if ($count > $realCount) {
					$count = $realCount;
				}

				for ($i = 0; $i < $count; $i++) {
					info('sc 6');
					$reservation = $order->reservations()->where('unit_id', $unit->id)->rent()->first();
					$orderService->takeUnit($reservation);
				}
			} else if ($unit->is_not_unique && $request->action == 'add') {
				info('sc 7');
				$count = $request->count;
				$realCount = $unit->count - Reservation::where('unit_id', $unit->id)->rent()->count();

				if ($count > $realCount) {
					$count = $realCount;
				}

				for ($i = 0; $i < $count; $i++) {
					$orderService->attachUnit($unit);
				}
			} else if ($reservation) {
				info('sc 8');
				if ($reservation->status === Reservation::STATUS_RETURNED) {
					info('sc 9');
					// $orderService->untakeUnit($reservation);
					$orderService->attachUnit($unit);
				} else {
					$orderService->takeUnit($reservation);
				}
			} else {
				info('sc 10');
				if (!$unit->is_reserved || $unit->is_not_unique) {
					if ($unit->is_not_unique) {
						for ($i = 0; $i < $request->count; $i++) {
							$orderService->attachUnit($unit);
						}
					} else {
						$orderService->attachUnit($unit);
					}
				} else {
					info('sc 11');
					$reservation = Reservation::where('unit_id', $unit->id)->latest()->whereStatus(Reservation::STATUS_RENT)->first();
					if (optional($reservation)->order_id != $order->id) {
						$order = $unit->current_order;
						$client = Client::findOrFail($order->client_id);

						$client->load('orders', 'feature');
						$order->load('reservations.unit.product.category', 'agent', 'group.reservations', 'group.orders.client.feature');
						$client->current_order = $order;

						return response()->json($client);
						exit;
					}
				}
			}
		}

		if ($order->status === Order::STATUS_DRAFT || $order->status === Order::STATUS_STAFFED) {
			info('sc 12');
			$orderService->calculateOrderPrice();
		}

		info('sc 13');
		$order->fresh();

		$client->load('orders', 'feature');
		$order->load('reservations.unit.product.category', 'agent', 'group.reservations', 'group.orders.client.feature');
		$client->current_order = $order;

		return response()->json($client);
	}
}
