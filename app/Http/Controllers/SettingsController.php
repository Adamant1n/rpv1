<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Setting;

class SettingsController extends Controller
{
    public function edit()
    {
        $this->authorize('manageSettings', Setting::class);

    	$template = Setting::where('name', 'template')->first();
        $pledgeTemplate = Setting::where('name', 'pledge_template')->first();

    	return view('settings.edit', compact('template', 'pledgeTemplate'));
    }

    public function update(Request $request)
    {
        $this->authorize('manageSettings', Setting::class);
        
        if($request->template)
        {
        	$template = $request->template->store('templates', 'public');

        	Setting::updateOrCreate(
        		['name' => 'template'],
        		['value' => $template]
        	);
        }

        if($request->pledge_template)
        {
            $pledgeTemplate = $request->pledge_template->store('templates', 'public');

            Setting::updateOrCreate(
                ['name' => 'pledge_template'],
                ['value' => $pledgeTemplate]
            );
        }

    	return redirect()->back()->with('success', 'Настройки обновлены');
    }
}
