<?php

namespace App\Http\Controllers;

use App\Location;
use App\PriceList;
use Illuminate\Http\Request;

class LocationController extends Controller
{
	public function __construct()
	{
		$this->authorizeResource(Location::class);
	}

	public function index()
	{
		$locations = Location::all();

		return view('locations.index', compact('locations'));
	}

	public function create()
	{
		return view('locations.create');
	}

	public function store(Request $request)
	{
		request()->validate([
			'address' => 'required|string|max:100'
		]);

		$data = $request->all();
		if(empty($request->active)) $data['active'] = false;
		$data['kkt'] = $request->has('kkt');

		$location = Location::create($data);

		return redirect()->route('locations.edit', $location)->with('success', 'Филиал успешно создан');
	}

	public function show($id)
	{
		//
	}

	public function edit(Location $location)
	{
		// $location->load('price_lists');
		$location->price_lists = PriceList::all();

		return view('locations.edit', compact('location'));
	}

	public function update(Request $request, Location $location)
	{
		request()->validate([
			'address' => 'required|string|max:100'
		]);

		$data = $request->except('weekends');
		$data['active'] = $request->has('active') ? true : false;
		$data['kkt'] = $request->has('kkt');
		//$data['all_day'] = $request->has('all_day') ? true : false;
		$data['weekends'] = [];
		foreach($request->weekends as $weekend) {
			$data['weekends'][] = intval($weekend);
		}

		$location->update($data);

		return redirect()->route('locations.edit', $location)->with('success', 'Филиал успешно сохранен');
	}

	public function destroy(Location $location)
	{
		$location->delete();

		return redirect()->route('locations.index')->with('success', 'Филиал удален');
	}
}
