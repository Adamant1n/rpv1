<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \Carbon\Carbon;
use App\Order;
use App\Unit;
use App\Location;
use App\Client;
use App\Reservation;
use App\Services\OrderService;
use App\Services\OnlineOrderService;
use App\Agent;
use App\OnlineOrder;
use YandexCheckout\Client as Yandex;

class PublicController extends Controller
{
    public function index()
    {
        return view('public.index');
    }

    public function process(OnlineOrder $onlineOrder)
    {
        $service = new OnlineOrderService($onlineOrder);

        return $service->process('tinkoff');
    }

    public function success(OnlineOrder $onlineOrder)
    {
        return view('public.success', compact('onlineOrder'));
    }

    public function store(Request $request)
    {
        $order = $request->order;
        $starts_at = Carbon::createFromFormat('d.m.Y H:i', $order['date'] . ' ' . $order['time']);

        $fakeOrder = new Order;
        $fakeOrder->location_id = $order['location']['id'];
        $fakeOrder->duration = $order['duration'];
        $fakeOrder->dimension = $order['dimension'];
        $fakeOrder->starts_at = $starts_at;

        // agent + discount

        if(isset($order['promocode']))
        {
            $agent = Agent::where('promocode', $order['promocode'])->first();

            if(isset($agent->id))
            {
                $fakeOrder->agent = $agent;
                $fakeOrder->discount = $agent->discount;
            }
        }

        $orderService = new OrderService($fakeOrder);

        $additionalProductsIds = [];

        foreach($order['kits'] as $kit)
        {
            // regular products

            $productsCategoriesIds = [];

            if($kit['type'] == 'ski')
            {
                if($kit['ski'])
                {
                    $productsCategoriesIds[] = 1;
                }

                if($kit['poles'])
                {
                    $productsCategoriesIds[] = 7;
                }

                if($kit['shoes'])
                {
                    $productsCategoriesIds[] = 2;
                }
            }
            elseif($kit['type'] == 'snowboard')
            {
                if($kit['snowboard'])
                {
                    $productsCategoriesIds[] = 4;
                }

                if($kit['shoes'])
                {
                    $productsCategoriesIds[] = 5;
                }
            }

            foreach($productsCategoriesIds as $category_id)
            {
                $reservation = new Reservation;
                $reservation->unit = Unit::whereHas('product', function ($query) use ($category_id) {
                    $query->where('category_id', $category_id);
                })->first();

                $reservation->created_at = $starts_at;
                $reservation->dimension = $order['dimension'];
                $reservation->duration = $order['duration'];
                $reservation->starts_at = $starts_at;

                $reservation = $orderService->calculateReservationTime($reservation, false);
                $reservation = $orderService->calculateReservationPrice($reservation, false);

                $fakeOrder->reservations[] = $reservation;
            }

            // additional products

            foreach($kit['additionals'] as $additional)
            {
                $reservation = new Reservation;
                $reservation->unit = Unit::whereHas('product', function ($query) use ($additional) {
                    $query->where('category_id', $additional['category_id']);
                })->first();

                $reservation->created_at = $starts_at;
                $reservation->dimension = $order['dimension'];
                $reservation->duration = $order['duration'];
                $reservation->starts_at = $starts_at;

                $reservation = $orderService->calculateReservationTime($reservation, false);
                $reservation = $orderService->calculateReservationPrice($reservation, false);

                $fakeOrder->reservations[] = $reservation;
                $additionalProductsIds[] = [
                    'category_id'   => $additional['category_id'],
                    'size'          => $additional['size'],
                ];
            }
        }

        $fakeOrder->reservations = collect($fakeOrder->reservations);
        $fakeOrder = $orderService->calculateOrderPrice(false);
        
        $client = Client::firstOrCreate(
            [
                'phone'         => $order['client']['phone'],
            ],
            [
                'first_name'    => $order['client']['first_name'],
                'last_name'     => isset($order['client']['last_name']) ? $order['client']['last_name'] : 'test',
            ]
        );

        $client->feature()->updateOrCreate([
            'height'    => $order['client']['height'],
            'weight'    => $order['client']['weight'],
            'shoe_size' => $order['client']['shoe_size'],
        ]);

        $onlineOrder = new OnlineOrder([
            'client_id'     => $client->id,
            'location_id'   => $order['location']['id'],
            'agent_id'      => isset($agent->id) ? $agent->id : null,
            'order_id'      => null,
            'date'          => $starts_at->format('Y-m-d'),
            'time'          => $starts_at->format('H:i'),
            'starts_at'     => $reservation->starts_at,
            'ends_at'       => $reservation->ends_at,
            'delivery_type' => $order['delivery']['value'],
            'address'       => isset($order['address']) ? $order['address'] : '',
            'duration'      => $order['duration'],
            'dimension'     => $order['dimension'],
            'promocode'     => $order['promocode'],
            'payment_type'  => $order['payment_type'],
            'discount'      => $fakeOrder->discount ?? 0,
            'price'         => $fakeOrder->full_price,
            'total'         => $fakeOrder->price,
            'products'      => $productsCategoriesIds,
            'additional_products' => $additionalProductsIds,
        ]);

        $onlineOrder->save();

        $redirectUrl = $this->proceedYandex($onlineOrder);

        return response()->json([
            'order' => $onlineOrder,
            'url'   => $redirectUrl, // route('public.process', $onlineOrder),
        ]);
    }

    /** payments */
    public function proceedYandex(OnlineOrder $payment)
    {
        $client = new Yandex();
        $client->setAuth(config('services.yandex.shopId'), config('services.yandex.secret'));

        $idempotenceKey = uniqid($payment->id, true);
        $amount = $payment->total;

        $response = $client->createPayment([
            'amount' => [
                'value'     => (float) $amount,
                'currency'  => 'RUB',
            ],
            'payment_method_data' => [
                'type' => 'bank_card',
            ],
            'confirmation' => [
                'type'          => 'redirect',
                'return_url'    => route('public.success', $payment->id),
            ],
            'description'   => 'Предоплата аренды №' . $payment->id,
            'receipt'       => [
                'customer'  => $payment->client->full_name,
                'email'     => 'customer@test.com',//$payment->client->email,
                'items'     => [
                    [
                        'description'   => 'Предоплата аренды №' . $payment->id,
                        'quantity'      => 1,
                        'amount'        => [
                            'value'     => (float) $amount,
                            'currency'  => 'RUB',
                        ],
                        'vat_code'      => '1',
                    ]
                ],
            ],
        ], $idempotenceKey);

        $paymentId = $response->getId();

        $payment->update([
            'external_id'   => $paymentId,
        ]);

        $redirectUrl = $response->confirmation->getConfirmationUrl();

        return $redirectUrl;
    }

    public function checkResult()
    {
        $client = new Client();
        $client->setAuth(config('services.yandex.shopId'), config('services.yandex.secret'));

        $idempotenceKey = uniqid($this->payment->id, true);

        $payment = $client->getPaymentInfo($this->payment->external_id);
        $status = $payment->getStatus();

        if($status == 'waiting_for_capture')
        {
            $paymentId = $this->payment->external_id;
            $idempotenceKey = uniqid($this->payment->id, true);

            $response = $client->capturePayment([
                'amount' => [
                    'value'     => (float) $this->payment->amount,
                    'currency'  => 'RUB',
                ],
            ], $paymentId, $idempotenceKey);

            $status = $response->getStatus();
        }

        if($status == 'succeeded')
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    /** endpayments */

    public function calculate(Request $request)
    {
        $order = $request->order;
        $starts_at = Carbon::createFromFormat('d.m.Y H:i', $order['date'] . ' ' . $order['time']);

        $fakeOrder = new Order;
        $fakeOrder->location_id = $order['location']['id'];
        $fakeOrder->duration = $order['duration'];
        $fakeOrder->dimension = $order['dimension'];
        $fakeOrder->starts_at = $starts_at;

        // agent + discount

        if(isset($order['promocode']))
        {
            $agent = Agent::where('promocode', $order['promocode'])->first();

            if(isset($agent->id))
            {
                $fakeOrder->agent = $agent;
                $fakeOrder->discount = $agent->discount;
            }
        }

        $orderService = new OrderService($fakeOrder);

        foreach($order['kits'] as $kit)
        {
            // regular products

            $productsCategoriesIds = [];

            if($kit['type'] == 'ski')
            {
                if($kit['ski'])
                {
                    $productsCategoriesIds[] = 1;
                }

                if($kit['poles'])
                {
                    $productsCategoriesIds[] = 7;
                }

                if($kit['shoes'])
                {
                    $productsCategoriesIds[] = 2;
                }
            }
            elseif($kit['type'] == 'snowboard')
            {
                if($kit['snowboard'])
                {
                    $productsCategoriesIds[] = 4;
                }

                if($kit['shoes'])
                {
                    $productsCategoriesIds[] = 5;
                }
            }

            foreach($productsCategoriesIds as $category_id)
            {
                $reservation = new Reservation;
                $reservation->unit = Unit::whereHas('product', function ($query) use ($category_id) {
                    $query->where('category_id', $category_id);
                })->first();

                $reservation->created_at = $starts_at;
                $reservation->dimension = $order['dimension'];
                $reservation->duration = $order['duration'];
                $reservation->starts_at = $starts_at;

                $reservation = $orderService->calculateReservationTime($reservation, false);
                $reservation = $orderService->calculateReservationPrice($reservation, false);

                $fakeOrder->reservations[] = $reservation;
            }

            // additional products

            foreach($kit['additionals'] as $additional)
            {
                $reservation = new Reservation;
                $reservation->unit = Unit::whereHas('product', function ($query) use ($additional) {
                    $query->where('category_id', $additional['category_id']);
                })->first();

                $reservation->created_at = $starts_at;
                $reservation->dimension = $order['dimension'];
                $reservation->duration = $order['duration'];
                $reservation->starts_at = $starts_at;

                $reservation = $orderService->calculateReservationTime($reservation, false);
                $reservation = $orderService->calculateReservationPrice($reservation, false);

                $fakeOrder->reservations[] = $reservation;
            }
        }

        $fakeOrder->reservations = collect($fakeOrder->reservations);
        $fakeOrder = $orderService->calculateOrderPrice(false);
        $fakeOrder->from = $reservation->starts_at->format('H:i d.m.Y');
        $fakeOrder->to = $reservation->ends_at->format('H:i d.m.Y');

        return $fakeOrder;        
    }

    public function getLocations ()
    {
        return Location::all();
    }

}
