<?php

namespace App\Http\Controllers;

use App\Brand;
use Illuminate\Http\Request;

class BrandController extends Controller
{
	public function __construct()
	{
	}

	public function index()
	{
		$this->authorize('manageProperties', \App\Setting::class);

		$brands = Brand::all();

		return view('brands.index', compact('brands'));
	}

	public function create()
	{
		$this->authorize('manageProperties', \App\Setting::class);

		return view('brands.create');
	}

	public function store(Request $request)
	{
		$this->authorize('manageProperties', \App\Setting::class);

		request()->validate([
			'name' => 'required|string|max:100'
		]);

		Brand::create($request->all());

		return redirect()->route('brands.index')->with('success', 'Бренд успешно создан');
	}

	public function show($id)
	{
		//
	}

	public function edit(Brand $brand)
	{
		$this->authorize('manageProperties', \App\Setting::class);

		return view('brands.edit', compact('brand'));
	}

	public function update(Request $request, Brand $brand)
	{
		$this->authorize('manageProperties', \App\Setting::class);

		request()->validate([
			'name' => 'required|string|max:100'
		]);

		$brand->update($request->all());

		return redirect()->route('brands.index')->with('success', 'Бренд успешно сохранен');
	}

	public function destroy(Brand $brand)
	{
		$this->authorize('manageProperties', \App\Setting::class);
		
		$brand->delete();

		return redirect()->route('brands.index')->with('success', 'Бренд удален');
	}
}
