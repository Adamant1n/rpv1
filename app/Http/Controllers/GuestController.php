<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Client;
use App\Http\Requests\ClientRequest;
use Carbon\Carbon;

class GuestController extends Controller
{
	public function index()
	{
		return view('guest.index');
	}

	public function find(Request $request)
	{
		$client = Client::where('phone', $request->phone)->first();

		if(!$client)
		{
			return redirect()->route('guest.create', ['phone' => $request->phone]);
		}
		else
		{
			return redirect()->route('guest.show', $client->id);
		}
	}

	public function show($client_id)
	{
		$client = Client::findOrFail($client_id);
		$client->load('feature');

		return view('guest.show', compact('client'));
	}

	public function create()
	{
		return view('guest.create');
	}

    public function store(Request $request)
    {
    	$data = $request->except('profile');
    	// $data['birthday'] = Carbon::parse($data['birthday'])->format('Y-m-d');

    	$_client = Client::where('phone', 'like', '%' . $request->phone . '%')->first();

    	if(!$_client)
    	{
	    	$client = Client::create($data);

	    	$client->feature()->create($request->profile);

	    	return redirect()->route('guest.show', $client)->with('success', 'Карточка клиента успешно создана');
	    }
	    else
	    {
	    	return redirect()->route('guest.show', $_client);
	    }
    }
}
