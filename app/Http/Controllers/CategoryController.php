<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
	public function __construct()
	{
	}

	public function index()
	{
		$this->authorize('manageProperties', \App\Setting::class);

		$categories = Category::all();

		return view('categories.index', compact('categories'));
	}

	public function create()
	{
		$this->authorize('manageProperties', \App\Setting::class);

		$available_params = Category::getAvailableParams();

		return view('categories.create', compact('available_params'));
	}

	public function store(Request $request)
	{
		$this->authorize('manageProperties', \App\Setting::class);

		request()->validate([
			'name' => 'required|string|max:100'
		]);

		$data = $request->except('params');
		$data['group'] = $request->has('group');

		$available_params = Category::getAvailableParams();
		if ($request->has('params')) {
			foreach ($available_params as $param_id => $param_name) {
				if (array_key_exists($param_id, $request->params)) {
					$data['params'][$param_id] = 1;
				} else
					$data['params'][$param_id] = 0;
			}
		}

		Category::create($data);

		return redirect()->route('categories.index')->with('success', 'Категория успешно создана');
	}

	public function show($id)
	{
		//
	}

	public function edit(Category $category)
	{
		$this->authorize('manageProperties', \App\Setting::class);

		$available_params = Category::getAvailableParams();

		return view('categories.edit', compact('category', 'available_params'));
	}

	public function update(Request $request, Category $category)
	{
		$this->authorize('manageProperties', \App\Setting::class);

		request()->validate([
			'name' => 'required|string|max:100'
		]);

		$data = $request->except('params');
		$data['group'] = $request->has('group');

		$available_params = Category::getAvailableParams();
		foreach ($available_params as $param_id => $param_name) {
			if (array_key_exists($param_id, $request->params)) {
				$data['params'][$param_id] = 1;
			} else
				$data['params'][$param_id] = 0;
		}

		$category->update($data);

		return redirect()->route('categories.index')->with('success', 'Категория успешно сохранена');
	}

	public function destroy(Category $category)
	{
		$this->authorize('manageProperties', \App\Setting::class);

		$category->delete();

		return redirect()->route('categories.index')->with('success', 'Категория удалена');
	}
}
