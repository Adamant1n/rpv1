<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Auth;

class BarcodeLogin extends Controller
{
    public function handle(Request $request)
    {
    	$code = $request->code;
    	$char = strtolower(substr($code, 0, 1));

        /*
    	if($char != 'x')
    	{
    		return response()->json([
    			'success' => false,
    			'message' => 'x'
    		]);
    	}
        */

        if($char == 'x')
        {
        	$user_id = intval(substr($code, 1, strlen($code)));
        }
        else
        {
            $user_id = strtolower($code);
        }

    	$user = User::where('id', $user_id)->orWhere('barcode', $user_id)->firstOrFail();

    	if(!is_null($user))
    	{	
    		Auth::loginUsingId($user->id);

    		return response()->json([
    			'success' => true,
    		]);
    	}
    	else
    	{
    		return response()->json([
    			'success' => false,
    			'message' => 'y'
    		]);
    	}
    }
}
