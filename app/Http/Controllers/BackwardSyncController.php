<?php

namespace App\Http\Controllers;

use App\Agent;
use App\Brand;
use App\Category;
use App\Client;
use App\ClientFeature;
use App\Location;
use App\PriceList;
use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class BackwardSyncController extends Controller
{
    private $location;

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->location = Location::find(config('app.location_id')); //auth()->user()->location;
            return $next($request);
        });
    }

    public function request()
    {
        $url = config('app.server_url') . '/sync/response?location_id=' . $this->location->id;
        $data = json_decode(file_get_contents($url), true);

        /** price lists */
        foreach ($data['price_lists'] as $item) {
            PriceList::unguard();
            PriceList::updateOrCreate(
                [
                    'id' => $item['id'],
                    'location_id' => $item['location_id']
                ],
                Arr::except($item, ['id', 'location_id', 'created_at', 'updated_at'])
            );
        }

        /** agents */
        foreach ($data['agents'] as $item) {
            Agent::unguard();
            Agent::updateOrCreate(
                [
                    'id' => $item['id'],
                ],
                Arr::except($item, ['id', 'created_at', 'updated_at'])
            );
        }

        /** location */
        $this->location->update(Arr::except($data['location'], ['id']));

        /** product categories */
        foreach ($data['product_categories'] as $item) {
            Category::unguard();
            Category::updateOrCreate(
                [
                    'id' => $item['id'],
                ],
                Arr::except($item, ['id', 'created_at', 'updated_at'])
            );
        }

        /** product brands */
        foreach ($data['brands'] as $item) {
            Brand::unguard();
            Brand::updateOrCreate(
                [
                    'id' => $item['id'],
                ],
                Arr::except($item, ['id', 'created_at', 'updated_at'])
            );
        }

        /** products */
        foreach ($data['products'] as $item) {
            $productData = Arr::except($item, ['id', 'created_at', 'updated_at', 'photo_url']);
            $productData['photo'] = $item['photo_url'];
            Product::unguard();
            Product::updateOrCreate(
                [
                    'id' => $item['id'],
                ],
                $productData
            );
        }

        /** clients */
        foreach ($data['clients'] as $item) {
            $clientData = Arr::except($item, ['id', 'created_at', 'updated_at', 'full_name', 'optional_id', 'images', 'birthday_short', 'feature']);
            if ($item['images']['passport']) {
                $clientData['passport_scan'] = $item['images']['passport'];
            }
            if ($item['images']['photo']) {
                $clientData['photo'] = $item['images']['photo'];
            }

            Client::unguard();
            Client::updateOrCreate(
                [
                    'id' => $item['id'],
                ],
                $clientData
            );

            if (isset($item['feature'])) {
                ClientFeature::updateOrCreate(
                    [
                        'client_id' => $item['id'],
                    ],
                    Arr::except($item['feature'], ['foot_length', 'id'])
                );
            }
        }

        return response()->json($data);
    }

    public function response(Request $request)
    {
        $location = Location::find($request->location_id);
        $priceLists = PriceList::all();
        $productCategories = Category::all();
        $products = Product::all();
        $agents = Agent::all();
        $clients = Client::with('feature')->get();
        $brands = Brand::all();

        return response()->json([
            'brands' => $brands,
            'price_lists' => $priceLists,
            'location' => $location,
            'clients' => $clients,
            'agents' => $agents,
            'product_categories' => $productCategories,
            'products' => $products,
        ]);
    }
}
