<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Location;
use App\Order;
use App\OrderGroup;
use App\Client;
use App\Product;
use App\Unit;
use App\Agent;
use App\Category;
use App\Brand;
use App\Binding;
use App\PriceList;
use App\Condition;
use App\Reservation;
use App\Synchronization;
use App\User;
use Exception;
use GuzzleHttp\Exception\ServerException;
use PragmaRX\Version\Package\Version;

use Illuminate\Http\Request;

class SynchronizationController extends Controller
{
	private $location;

	public function __construct()
	{
		$this->middleware(function ($request, $next) {
			$this->location = Location::find(config('app.location_id')); //auth()->user()->location;
			return $next($request);
		});
	}

	public function index()
	{
		$version = (new Version)->compact();
		$synchronizations = Synchronization::latest('id')->get();
		$latestVersion = optional($synchronizations->first())->version;
		$needUpdate = ($version != $latestVersion);

		$formattedVersion = str_replace(['.', '-', 'v'], '', $version);
		$formattedLatestVersion = str_replace(['.', '-', 'v'], '', $latestVersion);

		return view('synchronizations.index', compact('synchronizations', 'version', 'needUpdate', 'latestVersion'));
	}

	public function send()
	{
		$synchronization = Synchronization::create(['started_at' => now()]);

		$data = $this->prepareData();
		$url = config('app.server_url') . '/sync/receive';

		$client = new \GuzzleHttp\Client();

		try {
			$response = $client->request('POST', $url, [
				'json'	=> $data,
			]);
		} catch (ServerException $e) {
			dd($e->getResponse()->getBody());
		}

		$data = json_decode($response->getBody()->getContents(), true);

		Location::unguard();
		foreach ($data['locations'] as $item) {
			Location::updateOrCreate(
				['id' => intval($item['id'])],
				array_except($item, ['id'])
			);
		}

		User::unguard();
		foreach ($data['users'] as $item) {
			User::updateOrCreate(
				['id' => intval($item['id'])],
				array_except($item, ['id'])
			);
		}

		PriceList::unguard();
		foreach ($data['price_lists'] as $item) {
			PriceList::updateOrCreate(
				['id' => intval($item['id'])],
				array_except($item, ['id'])
			);
		}

		Binding::unguard();
		foreach ($data['bindings'] as $item) {
			Binding::updateOrCreate(
				['id' => intval($item['id'])],
				array_except($item, ['id'])
			);
		}

		Condition::unguard();
		foreach ($data['conditions'] as $item) {
			Condition::updateOrCreate(
				['id' => intval($item['id'])],
				array_except($item, ['id'])
			);
		}

		Brand::unguard();
		foreach ($data['brands'] as $item) {
			Brand::updateOrCreate(
				['id' => intval($item['id'])],
				array_except($item, ['id'])
			);
		}

		Category::unguard();
		foreach ($data['categories'] as $item) {
			Category::updateOrCreate(
				['id' => intval($item['id'])],
				array_except($item, ['id'])
			);
		}

		Agent::unguard();
		foreach ($data['agents'] as $item) {
			Agent::updateOrCreate(
				['id' => intval($item['id'])],
				array_except($item, ['id'])
			);
		}

		Product::unguard();
		foreach ($data['products'] as $item) {
			Product::updateOrCreate(
				['id' => intval($item['id'])],
				array_except($item, ['id', 'photo_url'])
			);
		}

		Unit::unguard();
		foreach ($data['units'] as $item) {
			Unit::updateOrCreate(
				['id' => intval($item['id'])],
				array_except($item, ['id'])
			);
		}

		Client::unguard();
		foreach ($data['clients'] as $item) {
			$item['created_at'] = Carbon::createFromFormat('H:i d.m.Y', $item['created_at']);
			$item['updated_at'] = Carbon::createFromFormat('H:i d.m.Y', $item['updated_at']);

			$client = Client::updateOrCreate(
				['id' => intval($item['id'])],
				array_except($item, ['id', 'feature', 'optional_id', 'full_name', 'images', 'birthday_short'])
			);

			$client->feature()->updateOrCreate(
				['client_id' => $item['id']],
				array_except($item['feature'], ['id', 'client_id', 'foot_length'])
			);
		}

		OrderGroup::unguard();
		foreach ($data['groups'] as $item) {
			OrderGroup::updateOrCreate(
				['id' => intval($item['id'])],
				array_except($item, ['id', 'optional_id', 'price', 'pledge_amount', 'active_reservations_count', 'payments', 'orders'])
			);
		}

		Order::unguard();
		Reservation::unguard();

		foreach ($data['orders'] as $item) {
			$order = Order::updateOrCreate(
				['id' => intval($item['id'])],
				array_except($item, ['optional_id', 'z_num', 'reservations', 'client', 'price_paid', 'current_price_list', 'payments', 'location', 'units', 'active_reservations_count', 'term', 'pledge_amount', 'refund', 'online_order'])
			);

			foreach ($item['reservations'] as $reservation) {
				$reservation = Reservation::find($reservation['id']);

				if ($reservation) {
					$reservation->update(
						['id' => $reservation['id']],
						array_except($reservation->toArray(), ['id', 'order_id', 'term'])
					);
				} else {
					if (!is_null($reservation)) {
						$order->reservations()->create(
							array_except($reservation, ['order_id', 'term'])
						);
					}
				}

				/*
				$order->reservations()->updateOrCreate(
					['id' => $reservation['id']],
					array_except($reservation, ['id', 'order_id', 'term'])
				);
				*/
			}
		}

		$synchronization->update(['ended_at' => now(), 'result' => $data['stats'], 'version' => $data['version']]);

		return response()->json($data);
	}

	public function receive(Request $request)
	{
		$stats = [];

		$incomingData = $request->all();

		$location_id = $incomingData['location_id'];

		$incomingBindings = $incomingData['bindings'];
		$bindingData = $this->handleBindings($incomingBindings);
		$stats['bindings'] = $bindingData['stats'];

		$incomingBrands = $incomingData['brands'];
		$brandData = $this->handleBrands($incomingBrands);
		$stats['brands'] = $brandData['stats'];


		$incomingProducts = $incomingData['products'];
		$productData = $this->handleProducts($incomingProducts);
		$stats['products'] = $productData['stats'];

		$incomingUnits = $incomingData['units'];
		$unitData = $this->handleUnits($incomingUnits);
		$stats['units'] = $productData['stats'];

		$incomingClients = $incomingData['clients'];
		$clientData = $this->handleClients($incomingClients);
		$stats['clients'] = $clientData['stats'];

		$incomingGroups = $incomingData['groups'];
		$groupData = $this->handleGroups($incomingGroups);
		$stats['groups'] = $groupData['stats'];

		$incomingOrders = $incomingData['orders'];
		$orderData = $this->handleOrders($incomingOrders);
		$stats['orders'] = $orderData['stats'];

		$categories = Category::all();
		$conditions = Condition::all();
		$price_lists = PriceList::all();
		$locations = Location::all();
		$agents = Agent::all();
		$users = User::all();
		$users->map(function ($user) {
			return $user->setAppends([]);
		});

		$version = new Version;

		return response()->json([
			'version'	=> $version->compact(),
			'stats'		=> $stats,
			// 'orders'	=> $orderData['items'],
			'bindings'	=> $bindingData['items'],
			'brands'	=> $brandData['items'],
			'products'	=> $productData['items'],
			'units'		=> $unitData['items'],
			'clients'	=> $clientData['items'],
			'groups'	=> $groupData['items'],
			'categories' => $categories,
			'conditions' => $conditions,
			'locations'	=> $locations,
			'price_lists' => $price_lists,
			'agents' => $agents,
			'orders' => $orderData['items'],
			'users' => $users,
		]);
	}

	public function handleClients($incomingItems)
	{
		Client::unguard();
		$incomingIds = array_pluck($incomingItems, 'id');
		$items = Client::whereIn('id', $incomingIds)->get();

		$turnedIds = [];
		$stats = [
			'created' => 0,
			'updated' => 0,
			'skipped' => 0,
			'turned'  => 0,
		];

		foreach ($incomingItems as $incomingItem) {
			$incomingItem['created_at'] = Carbon::createFromFormat('H:i d.m.Y', $incomingItem['created_at']);
			$incomingItem['updated_at'] = Carbon::createFromFormat('H:i d.m.Y', $incomingItem['updated_at']);

			$rawItem = [];
			// $except = ['optional_id', 'z_num'];
			$except = [];

			foreach ($incomingItem as $field => $value) {
				if (!is_array($value) && !in_array($field, $except)) $rawItem[$field] = $value;
			}

			$item = $items->where('id', $incomingItem['id'])->first();

			//$item->updated_at =  $item->updated_at);
			//$item->created_at =  Carbon::createFromFormat('H:i d.m.Y', $item->created_at);

			// file_put_contents('debug.txt', json_encode($item));

			if (empty($item)) {
				$item = Client::create(array_except($rawItem, ['feature', 'optional_id', 'full_name', 'images', 'birthday_short']));
				$item->feature()->updateOrCreate(
					['client_id' => $incomingItem['id']],
					array_except($incomingItem['feature'], ['id', 'client_id', 'foot_length'])
				);

				$stats['created']++;
			} else if ($incomingItem['updated_at']->gt($item->updated_at)) {
				$item = Client::updateOrCreate(
					['id' => intval($item['id'])],
					array_except($rawItem, ['feature', 'optional_id', 'full_name', 'images', 'birthday_short'])
				);

				$item->feature()->updateOrCreate(
					['client_id' => $incomingItem['id']],
					array_except($incomingItem['feature'], ['id', 'client_id', 'foot_length'])
				);

				$stats['updated']++;
			} else if (Carbon::createFromFormat('H:i d.m.Y', $item->updated_at)->gt($incomingItem['updated_at'])) {
				$turnedIds[] = $incomingItem['id'];

				$stats['turned']++;
			} else {
				$stats['skipped']++;
			}
		}

		$items = Client::with('feature')->whereNotIn('id', $incomingIds)->orWhereIn('id', $turnedIds)->get();

		return [
			'items'	=> $items,
			'stats'	=> $stats
		];
	}

	public function handleUnits($incomingItems)
	{
		Unit::unguard();
		$incomingIds = array_pluck($incomingItems, 'id');
		$items = Unit::whereIn('id', $incomingIds)->get();

		$turnedIds = [];
		$stats = [
			'created' => 0,
			'updated' => 0,
			'skipped' => 0,
			'turned'  => 0,
		];

		foreach ($incomingItems as $incomingItem) {
			$incomingItem['created_at'] = Carbon::parse($incomingItem['created_at']);
			$incomingItem['updated_at'] = Carbon::parse($incomingItem['updated_at']);

			$rawItem = [];
			// $except = ['optional_id', 'z_num'];
			$except = [];

			foreach ($incomingItem as $field => $value) {
				if (!is_array($value) && !in_array($field, $except)) $rawItem[$field] = $value;
			}

			$item = $items->where('id', $incomingItem['id'])->first();

			if (empty($item)) {
				$item = Unit::create($rawItem);

				$stats['created']++;
			} else if ($incomingItem['updated_at']->gt($item->updated_at)) {
				$item->update(array_except($rawItem, 'optional_id'));

				$stats['updated']++;
			} else if ($item->updated_at->gt($incomingItem['updated_at'])) {
				$turnedIds[] = $incomingItem['id'];

				$stats['turned']++;
			} else {
				$stats['skipped']++;
			}
		}

		$items = Unit::whereNotIn('id', $incomingIds)->orWhereIn('id', $turnedIds)->get();

		return [
			'items'	=> $items,
			'stats'	=> $stats
		];
	}

	public function handleGroups($incomingItems)
	{
		OrderGroup::unguard();
		$incomingIds = array_pluck($incomingItems, 'id');
		$items = OrderGroup::whereIn('id', $incomingIds)->get();

		$turnedIds = [];
		$stats = [
			'created' => 0,
			'updated' => 0,
			'skipped' => 0,
			'turned'  => 0,
		];

		foreach ($incomingItems as $incomingItem) {
			$incomingItem['created_at'] = Carbon::parse($incomingItem['created_at']);
			$incomingItem['updated_at'] = Carbon::parse($incomingItem['updated_at']);

			$rawItem = [];
			// $except = ['optional_id', 'z_num'];
			$except = [];

			foreach ($incomingItem as $field => $value) {
				if (!is_array($value) && !in_array($field, $except)) $rawItem[$field] = $value;
			}

			$item = $items->where('id', $incomingItem['id'])->first();

			if (empty($item)) {
				// $item = OrderGroup::create($rawItem);
				$item = OrderGroup::create(
					array_except($rawItem, ['optional_id', 'price', 'pledge_amount', 'active_reservations_count', 'payments', 'orders'])
				);

				$stats['created']++;
			} else if ($incomingItem['updated_at']->gt($item->updated_at)) {
				$item->update(
					array_except($rawItem, ['optional_id', 'price', 'pledge_amount', 'active_reservations_count', 'payments', 'orders'])
				);

				$stats['updated']++;
			} else if ($item->updated_at->gt($incomingItem['updated_at'])) {
				$turnedIds[] = $incomingItem['id'];

				$stats['turned']++;
			} else {
				$stats['skipped']++;
			}
		}

		$items = OrderGroup::whereNotIn('id', $incomingIds)->orWhereIn('id', $turnedIds)->get();

		return [
			'items'	=> $items,
			'stats'	=> $stats
		];
	}


	public function handleProducts($incomingItems)
	{
		Product::unguard();
		$incomingIds = array_pluck($incomingItems, 'id');
		$items = Product::whereIn('id', $incomingIds)->get();

		$turnedIds = [];
		$stats = [
			'created' => 0,
			'updated' => 0,
			'skipped' => 0,
			'turned'  => 0,
		];

		foreach ($incomingItems as $incomingItem) {
			$incomingItem['created_at'] = Carbon::parse($incomingItem['created_at']);
			$incomingItem['updated_at'] = Carbon::parse($incomingItem['updated_at']);

			$rawItem = [];
			// $except = ['optional_id', 'z_num'];
			$except = [
				'photo_url'
			];

			foreach ($incomingItem as $field => $value) {
				if (!is_array($value) && !in_array($field, $except)) $rawItem[$field] = $value;
			}

			$item = $items->where('id', $incomingItem['id'])->first();

			if (empty($item)) {
				$item = Product::create($rawItem);

				$stats['created']++;
			} else if ($incomingItem['updated_at']->gt($item->updated_at)) {
				$item->update(array_except($rawItem, ['optional_id', 'photo_url']));

				$stats['updated']++;
			} else if ($item->updated_at->gt($incomingItem['updated_at'])) {
				$turnedIds[] = $incomingItem['id'];

				$stats['turned']++;
			} else {
				$stats['skipped']++;
			}
		}

		$items = Product::whereNotIn('id', $incomingIds)->orWhereIn('id', $turnedIds)->get();

		return [
			'items'	=> $items,
			'stats'	=> $stats
		];
	}

	public function handleBrands($incomingItems)
	{
		Brand::unguard();
		$incomingIds = array_pluck($incomingItems, 'id');
		$items = Brand::whereIn('id', $incomingIds)->get();

		$turnedIds = [];
		$stats = [
			'created' => 0,
			'updated' => 0,
			'skipped' => 0,
			'turned'  => 0,
		];

		foreach ($incomingItems as $incomingItem) {
			$incomingItem['created_at'] = Carbon::parse($incomingItem['created_at']);
			$incomingItem['updated_at'] = Carbon::parse($incomingItem['updated_at']);

			$rawItem = [];
			// $except = ['optional_id', 'z_num'];
			$except = [];

			foreach ($incomingItem as $field => $value) {
				if (!is_array($value) && !in_array($field, $except)) $rawItem[$field] = $value;
			}

			$item = $items->where('id', $incomingItem['id'])->first();

			if (empty($item)) {
				$item = Brand::create($rawItem);

				$stats['created']++;
			} else if ($incomingItem['updated_at']->gt($item->updated_at)) {
				$item->update(array_except($rawItem, 'optional_id'));

				$stats['updated']++;
			} else if ($item->updated_at->gt($incomingItem['updated_at'])) {
				$turnedIds[] = $incomingItem['id'];

				$stats['turned']++;
			} else {
				$stats['skipped']++;
			}
		}

		$items = Brand::whereNotIn('id', $incomingIds)->orWhereIn('id', $turnedIds)->get();

		return [
			'items'	=> $items,
			'stats'	=> $stats
		];
	}

	public function handleBindings($incomingItems)
	{
		Binding::unguard();
		$incomingIds = array_pluck($incomingItems, 'id');
		$items = Binding::whereIn('id', $incomingIds)->get();

		$turnedIds = [];
		$stats = [
			'created' => 0,
			'updated' => 0,
			'skipped' => 0,
			'turned'  => 0,
		];

		foreach ($incomingItems as $incomingItem) {
			$incomingItem['created_at'] = Carbon::parse($incomingItem['created_at']);
			$incomingItem['updated_at'] = Carbon::parse($incomingItem['updated_at']);

			$rawItem = [];
			// $except = ['optional_id', 'z_num'];
			$except = [];

			foreach ($incomingItem as $field => $value) {
				if (!is_array($value) && !in_array($field, $except)) $rawItem[$field] = $value;
			}

			$item = $items->where('id', $incomingItem['id'])->first();

			if (empty($item)) {
				$item = Binding::create($rawItem);

				$stats['created']++;
			} else if ($incomingItem['updated_at']->gt($item->updated_at)) {
				$item->update(array_except($rawItem, 'optional_id'));

				$stats['updated']++;
			} else if ($item->updated_at->gt($incomingItem['updated_at'])) {
				$turnedIds[] = $incomingItem['id'];

				$stats['turned']++;
			} else {
				$stats['skipped']++;
			}
		}

		$items = Binding::whereNotIn('id', $incomingIds)->orWhereIn('id', $turnedIds)->get();

		return [
			'items'	=> $items,
			'stats'	=> $stats
		];
	}

	public function handleOrders($incomingOrders)
	{
		Order::unguard();
		Reservation::unguard();

		$incomingOrdersIds = array_pluck($incomingOrders, 'id');
		$orders = Order::whereIn('id', $incomingOrdersIds)->get();

		$turnedOrdersIds = [];
		$stats = [
			'created' => 0,
			'updated' => 0,
			'skipped' => 0,
			'turned'  => 0,
		];

		foreach ($incomingOrders as $incomingOrder) {
			$incomingOrder['created_at'] = Carbon::parse($incomingOrder['created_at']);
			$incomingOrder['updated_at'] = Carbon::parse($incomingOrder['updated_at']);

			$rawOrder = [];
			$except = ['optional_id', 'z_num', 'client'];

			foreach ($incomingOrder as $field => $value) {
				if (!is_array($value) && !in_array($field, $except)) $rawOrder[$field] = $value;
			}

			$order = $orders->where('id', $incomingOrder['id'])->first();

			if (empty($order)) {
				$order = Order::create(array_except($rawOrder, ['optional_id', 'price_paid', 'active_reservations_count', 'term', 'pledge_amount', 'refund', 'online_order']));

				foreach ($incomingOrder['reservations'] as $reservation) {
					$order->reservations()->updateOrCreate(
						['id' => $reservation['id']],
						array_except($reservation, ['id', 'order_id', 'term'])
					);
				}

				$stats['created']++;
			} else if ($incomingOrder['updated_at']->gt($order->updated_at)) {
				$order->update(array_except($rawOrder, ['optional_id', 'price_paid', 'active_reservations_count', 'term', 'pledge_amount', 'refund', 'online_order']));

				foreach ($incomingOrder['reservations'] as $reservation) {
					$order->reservations()->updateOrCreate(
						['id' => $reservation['id']],
						array_except($reservation, ['id', 'order_id', 'term'])
					);
				}

				$stats['updated']++;
			} else if ($order->updated_at->gt($incomingOrder['updated_at'])) {
				$incomingOrdersIdsBef = $incomingOrdersIds;
				$turnedOrdersIds[] = $incomingOrder['id'];

				$stats['turned']++;
			} else {
				$stats['skipped']++;
			}
		}

		$orders = Order::with('reservations')->whereNotIn('id', $incomingOrdersIds)->orWhereIn('id', $turnedOrdersIds)->get();

		return [
			'items'	=> $orders,
			'stats'		=> $stats
		];
	}

	public function prepareData()
	{
		/*
			обработка снизу вверх
			1 клиенты с атрибутами
			2 юниты
			3 заказы с резервациями
		*/

		$products	= Product::whereRaw('id BETWEEN ' . $this->location->base_id . ' AND ' . $this->location->max_id)->get();
		$units 		= $this->location->units()->latest('updated_at')->get()->toArray();

		$brands		= Brand::whereRaw('id BETWEEN ' . $this->location->base_id . ' AND ' . $this->location->max_id)->get();
		$agents		= Agent::whereRaw('id BETWEEN ' . $this->location->base_id . ' AND ' . $this->location->max_id)->get();
		$bindings	= Binding::whereRaw('id BETWEEN ' . $this->location->base_id . ' AND ' . $this->location->max_id)->get();

		$groups 	= OrderGroup::whereIn('id', $this->location->orders()->whereNotNull('group_id')->get()->pluck('group_id')->toArray())->get();
		$orders 	= $this->location->orders()->with('reservations')->latest('updated_at')->get()->toArray();
		$clientIds 	= array_pluck($orders, 'client_id');
		$clients	= Client::whereIn('id', $clientIds)->with('feature')->latest('updated_at')->get()->toArray();

		$data = [
			'location_id'	=> $this->location->id,
			'clients'		=> $clients,
			'units'			=> $units,
			'orders'		=> $orders,
			'brands'		=> $brands,
			'bindings'		=> $bindings,
			'products'		=> $products,
			'groups'		=> $groups,
		];

		return $data;
	}
}
