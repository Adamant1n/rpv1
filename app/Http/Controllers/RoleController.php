<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

use App\Tables\RoleTable;

class RoleController extends Controller
{
	private $colors = [
		'danger' 			=> 'Danger',
		'warning' 			=> 'Warning',
		'primary' 			=> 'Primary',
		'info' 				=> 'Info',
		'success' 			=> 'Success',
		'dark' 				=> 'Dark',
		'outline-danger' 	=> 'Outline Danger',
		'outline-warning' 	=> 'Outline Warning',
		'outline-primary' 	=> 'Outline Primary',
		'outline-info' 		=> 'Outline Info',
		'outline-success' 	=> 'Outline Success',
		'outline-dark' 		=> 'Outline Dark'
	];

	public function __construct()
	{
		return $this->authorizeResource(Role::class);
	}

    public function index(Request $request)
    {
    	$dataTable = new RoleTable($request);

    	if($request->ajax())
    	{
    		$data = $dataTable->getData();
    		return response()->json($data);
    	}

        return view('roles.index', compact('dataTable'));
    }

    public function create(Role $role)
    {
    	$permissions = Permission::all();
    	$role->load('permissions');
    	$colors = $this->colors;

        return view('roles.form', compact('role', 'colors', 'permissions'));
    }

    public function store(Request $request)
    {
        $role = Role::create($request->except('permissions'));
        $role->syncPermissions($request->permissions);

        return redirect()->route('roles.index')->with('notification', ['success', 'Роль сохранена']);
    }

    public function edit(Role $role)
    {
    	$permissions = Permission::all();
    	$role->load('permissions');
    	$colors = $this->colors;

        return view('roles.form', compact('role', 'colors', 'permissions'));
    }

    public function update(Request $request, Role $role)
    {
        $role->update($request->except('permissions'));
        $role->syncPermissions($request->permissions);

        return redirect()->route('roles.index')->with('notification', ['success', 'Роль сохранена']);
    }

    public function destroy(Role $role)
    {
        $role->delete();

        return redirect()->route('roles.index')->with('notification', ['success', 'Роль удалена']);
    }
}
