<?php

namespace App\Http\Controllers;

use App\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Carbon;
use UploadImage;
use Storage;
use Dan\UploadImage\Exceptions\UploadImageException;

class ClientController extends Controller
{
	public function __construct()
	{
		
	}

    public function ajaxUpdate(Request $request, Client $client)
    {
        $request->merge(['birthday' => Carbon::parse($request->birthday)->format('Y-m-d')]);
        $data = $request->except('weight','height','shoe_size','file_photo','file_passport', 'photo_webcam');
        if(empty($request->is_signed)) $data['is_signed'] = false;
        $client->update($data);
        $client->feature()->update($request->only('weight','height','shoe_size'));

        return response()->json($client, 201);
    }

	public function index()
	{
		$this->authorize('index', Client::class);
		$clients = [];
		return view('clients.index', compact('clients'));
	}

	public function webcam(Request $request)
	{
		$path = UploadImage::upload($request->file('webcam'), 'client', false, false, true)->getImageName();

		return response()->json([
			'success' => true,
			'path' => $path
		]);
	}

	public function webcamSave(Request $request, $client_id)
	{
		$client = Client::findOrFail(intval($client_id));
		$path = UploadImage::upload($request->file('webcam'), 'client', false, false, true)->getImageName();

		$client->update(['photo' => $path]);

		return response()->json([
			'success' => true,
			'path' => $path
		]);
	}

	public function barcode(Client $client)
	{
		$font = base_path() . '/resources/fonts/arial-c.ttf';

		header('Content-Type: image/png');

		$im = imagecreatetruecolor(380, 400);
		$white = imagecolorallocate($im, 255, 255, 255);
		$grey = imagecolorallocate($im, 128, 128, 128);
		$black = imagecolorallocate($im, 0, 0, 0);

		imagefilledrectangle($im, 0, 0, 380, 400, $white);


		$text = $client->full_name;
		imagettftext($im, 16, 0, 20, 30, $black, $font, $text);
		$text = 'Телефон: ' . $client->phone;
		imagettftext($im, 16, 0, 20, 60, $black, $font, $text);

		// биометрия
		$text = 'Возраст: ' . $client->age;
		imagettftext($im, 16, 0, 20, 90, $black, $font, $text);
		$text = 'Рост: ' . $client->feature->height;
		imagettftext($im, 16, 0, 20, 120, $black, $font, $text);
		$text = 'Вес: ' . $client->feature->weight;
		imagettftext($im, 16, 0, 20, 150, $black, $font, $text);
		$text = 'Размер: ' . $client->feature->shoe_size;
		imagettftext($im, 16, 0, 20, 180, $black, $font, $text);

		if($client->skill) {
			$skills = [
				'beginner' => 'Новичок',
				'mid' => 'Средний',
				'pro' => 'Профессионал',
			];

			$text = 'Навык: ' . $skills[$client->skill];
			imagettftext($im, 16, 0, 20, 210, $black, $font, $text);
		}

		// штрихкод
		$text = $client->optional_id;
		imagettftext($im, 24, 0, 120, 390, $black, $font, $text);

		$generator = new \Picqer\Barcode\BarcodeGeneratorJPG();
		$file = $generator->getBarcode($client->optional_id, $generator::TYPE_CODE_128, 3, 92);

		$barcode = imagecreatefromstring($file);
		imagecopy($im, $barcode, 55, 260, 0, 0, 270, 92);

		imagepng($im);
		imagedestroy($im);

		/*
		$generator = new \Picqer\Barcode\BarcodeGeneratorJPG();

		$file = $generator->getBarcode($client->optional_id, $generator::TYPE_CODE_128, 3, 92);

		header("Content-Disposition: attachment; filename='" . $client->optional_id . ".jpg'");
        echo $file;
        */
	}

	public function json(Request $request)
	{
		$columns = Schema::getColumnListing('clients');

		$clients = Client::where([])
						->orderBy(
							$columns[$request->order[0]['column']],
							$request->order[0]['dir']
						);

		if ($request->search['value'] !== null)
		{

			$pattern = "/K[0-9]{6}/";
			if(preg_match($pattern, $request->search['value'], $matches)) {
				// dd('DOGOVOR');
				$id = intval(substr($request->search['value'], 1, strlen($request->search['value'])));
				$clients->where('id', $id);
			}
			else {
				foreach ($columns as $column)
					$clients->orWhere($column, 'like', '%'.$request->search['value'].'%');
			}
		}

        $filteredCount = $clients->count();
		$clients = $clients->offset($request->start)
            ->limit($request->length)
            ->get();

		$output = [];

		foreach ($clients as $client)
		{
			$output[] = [
				$client->optional_id,
				$client->full_name,
				$client->phone,
				$client->email,
				$client->created_at,
				$client->updated_at
			];
		}

		return json_encode([
			'draw' => $request->draw,
			'recordsTotal' => Client::count(),
			'recordsFiltered' => $filteredCount,
			'data' => $output
		]);
	}

	public function create()
	{
		$this->authorize('create', Client::class);
		return view('clients.create');
	}

	public function store(Request $request)
	{
		$this->authorize('create', Client::class);
		request()->validate([
			'last_name'     => 'required|string|max:50',
			'first_name'    => 'required|string|max:50',
			'mid_name'      => 'nullable|string|max:50',
			'file_photo'    => 'mimes:jpeg,jpg,png',
			'file_passport' => 'mimes:jpeg,jpg,png'
		]);

		if($request->photo_webcam)
		{
			$request->merge(['photo' => $request->photo_webcam]);
		}

		else if ($request->hasFile('file_photo'))
		{
			$request->merge(['photo' => UploadImage::upload(
				$request->file('file_photo'), 'client', false, false, true)->getImageName()
			]);
		}

		if ($request->hasFile('file_passport'))
		{
			$request->merge(['passport_scan' => UploadImage::upload(
				$request->file('file_passport'), 'passport', false, false, true)->getImageName()
			]);
		}

		$request->merge(['birthday' => Carbon::parse($request->birthday)->format('Y-m-d')]);

		$client = Client::create(
			$request->except('weight','height','shoe_size','file_photo','file_passport')
		);

		// $client->fresh();

		$client->feature()->create($request->only('weight','height','shoe_size'));

		return redirect()->route('clients.edit', $client)->with('success', 'Клиент успешно добавлен');
	}

	public function show()
	{
		//
	}

	public function edit($id)
	{
		$client = Client::where('id', $id)->with('feature', 'orders')->firstOrFail();
		$this->authorize('update', $client);

		$client->discounts = $client->orders()
			->with('agent')
			->whereNotNull('agent_id')
			->get();

		$client->discounts = $client->discounts->map(function($order) {
			$order->real_price = $order->price / (1 - $order->agent->discount / 100);
			$order->discount = $order->real_price - $order->price;

			return $order;
		});

		return view('clients.edit', compact('client'));
	}

	public function update(Request $request, Client $client)
	{
		$this->authorize('update', $client);
		request()->validate([
			'last_name'     => 'required|string|max:50',
			'first_name'    => 'required|string|max:50',
			'mid_name'      => 'nullable|string|max:50',
			'file_photo'    => 'mimes:jpeg,png',
			'file_passport' => 'mimes:jpeg,png'
		]);

		if($request->photo_webcam)
		{
			$request->merge(['photo' => $request->photo_webcam]);
		}

		else if ($request->hasFile('file_photo'))
		{
			$request->merge(['photo' => UploadImage::upload(
				$request->file('file_photo'), 'client', false, false, true)->getImageName()
			]);
		}

		if ($request->hasFile('file_passport'))
		{
			$request->merge(['passport_scan' => UploadImage::upload(
				$request->file('file_passport'), 'passport', false, false, true)->getImageName()
			]);
		}

		$request->merge(['birthday' => Carbon::parse($request->birthday)->format('Y-m-d')]);

		// договор подписан
		$data = $request->except('weight','height','shoe_size','file_photo','file_passport', 'photo_webcam');

		if(empty($request->is_signed)) $data['is_signed'] = false;
		// else $data['is_signed'] = true;

		$client->update($data);

		$client->feature()->update($request->only('weight','height','shoe_size'));

		return redirect()->route('clients.edit', $client)
			->with('success', 'Данные успешно сохранены');
	}

	public function destroy(Client $client)
	{
		$this->authorize('delete', $client);
		$client->feature()->delete();
		$client->delete();

		return redirect()->route('clients.index')->with('success', 'Клиент удален');
	}
}
