<?php

namespace App\Http\Controllers;

use App\User;
use App\Location;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;

class UserController extends Controller
{
	public function __construct()
	{
		$this->middleware(function ($request, $next) {

			$this->user = auth()->user();

			return $next($request);
		});
	}

	public function barcode($user_id)
	{
		$user = User::findOrFail($user_id);
		$font = base_path() . '/resources/fonts/arial-c.ttf';

		header('Content-Type: image/png');

		$im = imagecreatetruecolor(380, 240);
		$white = imagecolorallocate($im, 255, 255, 255);
		$grey = imagecolorallocate($im, 128, 128, 128);
		$black = imagecolorallocate($im, 0, 0, 0);

		imagefilledrectangle($im, 0, 0, 380, 240, $white);

		$text = $user->full_name;
		imagettftext($im, 16, 0, 55, 50, $black, $font, $text);

		// штрихкод
		$text = $user->optional_id;
		imagettftext($im, 24, 0, 120, 210, $black, $font, $text);

		$generator = new \Picqer\Barcode\BarcodeGeneratorJPG();
		$file = $generator->getBarcode($user->optional_id, $generator::TYPE_CODE_128, 3, 92);

		$barcode = imagecreatefromstring($file);
		imagecopy($im, $barcode, 55, 80, 0, 0, 270, 92);

		imagepng($im);
		imagedestroy($im);

		/*
		$generator = new \Picqer\Barcode\BarcodeGeneratorJPG();

		$file = $generator->getBarcode($client->optional_id, $generator::TYPE_CODE_128, 3, 92);

		header("Content-Disposition: attachment; filename='" . $client->optional_id . ".jpg'");
        echo $file;
        */
	}

	public function index(Request $request)
	{
		$this->authorize('index', User::class);

		$where = [];

		if ($this->user->hasRole('superadmin'))
		{
			$locations = Location::all();

			if (!empty($request->location))
				$where[] = ['location_id', $request->location];
		}
		else
		{
			$locations = Location::find($this->user->location_id);

			$where[] = ['location_id', $this->user->location_id];
		}

		if ($request->has('trashed'))
			$users = User::onlyTrashed()->where($where)->get();
		else
			$users = User::where($where)->get();

		return view('users.index', compact('users','locations','request'));
	}

	public function create()
	{
		$this->authorize('create', User::class);

		if ($this->user->hasRole('superadmin'))
		{
			$roles = Role::all();

			$locations = Location::all();
		}
		else
		{
			$roles = Role::where('name', 'manager')->get();

			$locations = [];
		}

		return view('users.create', compact('roles','locations'));
	}

	public function store(Request $request)
	{
		$this->authorize('create', User::class);

		request()->validate([
			'last_name'  => 'required|string|max:50',
			'first_name' => 'required|string|max:50',
			'mid_name'   => 'required|string|max:50',
			'email'      => 'required|email|unique:users',
			'password'   => 'required|min:6'
		]);

		if ($request->has('location_id') && !$this->user->hasRole('superadmin'))
			$request->merge(['location_id' => $this->user->location_id]);

		$request->merge(['password' => Hash::make($request->password)]);

		$user = User::create($request->except('role'));

		$user->assignRole($request->role);

		return redirect()->route('users.index')->with('success', 'Пользователь успешно создан');
	}

	public function show($id)
	{
		//
	}

	public function edit($id)
	{
		if ($this->user->hasRole('superadmin'))
		{
			$roles = Role::all();

			$locations = Location::all();
		}
		else
		{
			$roles = Role::where('name', 'manager')->get();

			$locations = [];
		}

		$user = User::withTrashed()->find($id);
		$this->authorize('update', $user);

		return view('users.edit', compact('user','locations','roles'));
	}

	public function update(Request $request, User $user)
	{
		$this->authorize('update', $user);

		request()->validate([
			'last_name'   => 'required|string|max:50',
			'first_name'  => 'required|string|max:50',
			'mid_name'    => 'required|string|max:50',
			'email'       => 'required|email',
		]);

		if ($user->getRoleNames()[0] != $request->role)
		{
			$user->removeRole($user->getRoleNames()[0]);
			$user->assignRole($request->role);
		}

		// $data = $request->all();
		$data = $request->except('password', 'role');

		if (!empty($request->password)){
			$data['password'] = bcrypt($request->password);
		}

		$user->update($data);

		return redirect()->route('users.index')->with('success', 'Данные успешно обновлены');
	}

	public function destroy(User $user)
	{
		$this->authorize('delete', $user);
		$user->delete();

		return redirect()->route('users.index')->with('success', 'Пользователь заблокирован');
	}

	public function restore($id)
	{
		$user = User::withTrashed()->find($id);
		$this->authorize('restore', $user);

		$user->restore();

		return redirect()->route('users.index')->with('success', 'Пользователь разблокирован');
	}
}
