<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Condition;
use App\Location;
use App\Unit;

class MassEditingController extends Controller
{
	public function getUnit(Request $request)
	{
		$this->authorize('index', Unit::class);

		$units = Unit::with('product.brand', 'product.category', 'condition')
    		->where('code', $request->code)
    		->get();

    	$units->map(function ($unit) {
    		$badges = [
				0 => ['color' => 'success','text' => 'в наличии'],
				1 => ['color' => 'warning','text' => 'резерв'],
			];

			if($unit->count == 0)
				$unit->badge = '<label class="badge badge-danger">продано</label>';
			else 
				$unit->badge = '<label class="badge badge-'.$badges[$unit->is_reserved]['color'].'">'
					.$badges[$unit->is_reserved]['text'].'</label>';
    	});

    	$output = [];

    	foreach($units as $unit) {
    		$output[] = [
	        	$unit->code,
	        	$unit->product->category->name,
	        	$unit->product->brand->name,
	        	$unit->product->name,
	        	$unit->condition->name,
	        	$unit->count,
	        	$unit->badge
	        ];
    	}

    	return response()->json($output);
	}

	public function getUnits(Request $request)
	{
		$this->authorize('index', Unit::class);

		$codes = file($request->file->getRealPath());
		// $codes = explode("\r\n", $codes);
		foreach($codes as &$code)
		{
			$code = trim($code);
		}

		$units = Unit::with('product.brand', 'product.category', 'condition')
    		->whereIn('code', $codes)
    		->get();

    	$units->map(function ($unit) {
    		$badges = [
				0 => ['color' => 'success','text' => 'в наличии'],
				1 => ['color' => 'warning','text' => 'резерв'],
			];

			if($unit->count == 0)
				$unit->badge = '<label class="badge badge-danger">продано</label>';
			else 
				$unit->badge = '<label class="badge badge-'.$badges[$unit->is_reserved]['color'].'">'
					.$badges[$unit->is_reserved]['text'].'</label>';
    	});

    	$output = [];

    	foreach($units as $unit) {
    		$output[] = [
	        	$unit->code,
	        	$unit->product->category->name,
	        	$unit->product->brand->name,
	        	$unit->product->name,
	        	$unit->condition->name,
	        	$unit->count,
	        	$unit->badge
	        ];
    	}

    	return response()->json($output);
	}

    public function index()
    {
    	$this->authorize('index', Unit::class);

    	$conditions = Condition::all();
		$locations  = Location::all();

    	return view('massediting.index', compact('conditions', 'locations'));
    }

    public function store(Request $request)
    {
    	$this->authorize('index', Unit::class);
    	
    	// $codes = explode("\r\n", $request->codes);
    	$codes = json_decode($request->codes);
    	$location_id = $request->location_id;
    	$condition_id = $request->condition_id;
    	$main_location = $request->main_location;

    	$count = Unit::whereIn('code', $codes)->update([
    		'location_id' => $location_id,
    		'main_location' => $main_location,
    		'condition_id' => $condition_id
    	]);

    	return redirect()->route('massediting.index')->with('success', 'Обновлено ' . $count . ' единиц инвентаря');
    }
}
