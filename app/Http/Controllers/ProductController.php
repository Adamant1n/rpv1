<?php

namespace App\Http\Controllers;

use App\Product;
use App\Category;
use App\Brand;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Schema;
use UploadImage;
use Dan\UploadImage\Exceptions\UploadImageException;

class ProductController extends Controller
{
	public function index()
	{
		$this->authorize('manageProperties', \App\Setting::class);

		$products = Product::with('brand', 'category')->get();

		$categories = Category::all();

		$brands = Brand::all();

		return view('products.index', compact('products', 'categories', 'brands'));
	}

	public function json(Request $request)
	{
		$columns = Schema::getColumnListing('products');

		$products = Product::where([])
			->offset($request->start)
			->limit($request->length)
			->orderBy(
				$columns[$request->order[0]['column']],
				$request->order[0]['dir']
			);

		if ($request->search['value'] !== null) {
			foreach ($columns as $column)
				$products->orWhere($column, 'like', '%' . $request->search['value'] . '%');
		}

		$products = $products->get();

		$output = [];

		$categories = ProductCategory::all();

		$brands = Brand::all();

		foreach ($products as $product) {
			$output[] = [
				$product->id,
				$product->name,
				$brands->find($product->brand_id)->name,
				$categories->find($product->category_id)->name,
				$product->created_at->format('d.m.Y H:i'),
				$product->updated_at->format('d.m.Y H:i')
			];
		}

		return json_encode([
			'draw' => $request->draw,
			'recordsTotal' => Product::count(),
			'recordsFiltered' => count($output),
			'data' => $output
		]);
	}

	public function create()
	{
		$this->authorize('manageProperties', \App\Setting::class);

		$categories = Category::all();

		$brands = Brand::all();

		return view('products.create', compact('categories', 'brands'));
	}

	public function store(Request $request)
	{
		$this->authorize('manageProperties', \App\Setting::class);

		request()->validate([
			'name'       => 'required|string|max:100',
			// 'file_photo' => 'mimes:jpeg,png'
		]);

		$data = $request->except('file_photo');
		$data['for_sale'] = $request->has('for_sale');

		// бренд
		if (!is_numeric($request->brand_id)) {
			$data['brand_id'] = Brand::firstOrCreate(['name' => $request->brand_id])->id;
		}

		// фото
		if ($request->hasFile('file_photo')) {
			$data['photo'] = UploadImage::upload($request->file('file_photo'), 'product', false, false, true)->getImageName();
		}

		$product = Product::create($data);

		return redirect()->route('products.index')->with('success', 'Модель успешно создана');
	}

	public function show($id)
	{
		//
	}

	public function edit(Product $product)
	{
		$this->authorize('manageProperties', \App\Setting::class);

		$categories = Category::all();

		$brands = Brand::all();

		return view('products.edit', compact('product', 'categories', 'brands'));
	}

	public function update(Request $request, Product $product)
	{
		$this->authorize('manageProperties', \App\Setting::class);

		request()->validate([
			'name'       => 'required|string|max:100',
			// 'file_photo' => 'mimes:jpeg,png,jpg'
		]);

		$data = $request->except('file_photo');
		$data['for_sale'] = $request->has('for_sale');

		// бренд
		if (!is_numeric($request->brand_id)) {
			$data['brand_id'] = Brand::firstOrCreate(['name' => $request->brand_id])->id;
		}

		// фото
		if ($request->hasFile('file_photo')) {
			$data['photo'] = UploadImage::upload($request->file('file_photo'), 'product', false, false, true)->getImageName();
		}

		$product->update($data);

		return redirect()->route('products.index')->with('success', 'Модель успешно сохранена');
	}

	public function destroy(Product $product)
	{
		$this->authorize('manageProperties', \App\Setting::class);

		$product->delete();

		return redirect()->route('products.index')->with('success', 'Модель удалена');
	}
}
