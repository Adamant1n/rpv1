<?php

namespace App\Http\Controllers;

use App\Product;
use App\Unit;
use App\Category;
use App\Condition;
use App\Brand;
use App\Location;
use App\Binding;
use App\Imports\UnitImport;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Carbon;

class StockController extends Controller
{
	public function __construct()
	{
		$this->middleware(function ($request, $next) {
			$this->user = auth()->user();
			return $next($request);
		});
	}

	public function index(Request $request)
	{
		$this->authorize('index', Unit::class);

		$locations  = Location::all();
		$brands     = Brand::all();
		$categories = Category::all();
		$conditions = Condition::all();

		return view('stock.index', compact(
			'locations',
			'brands',
			'categories',
			'conditions',
			'request'
		));
	}

	public function json(Request $request)
	{
		// $columns = Schema::getColumnListing('units');

		$columns = [
			'code',
			'category_id',
			'brand_id',
			'name',
			'condition_id',
			'binding_id',
			'feature_1',
			'size',
			'feature_4',
			'count',
		];

		$location = $this->user->location_id;

		$product_units = Unit::with('product.brand', 'product.category', 'condition')
			->when(!$this->user->hasRole('superadmin'), function ($query) {
				global $location;
				$query->where('location_id', $location);
			})
			->when(
				$this->user->hasRole('superadmin') && $request->has('location'),
				function ($query) {
					global $request;
					$query->where('location_id', $request->location);
				}
			);

		$recordsTotal = $product_units->count();

		$product_units = $product_units->when(
			$request->search['value'] !== null,
			function ($query) {
				global $request;
				$query->where('code', 'like', $request->search['value'] . '%');
			}
		)
			->when($request->has('brand'), function ($query) {
				$query->whereHas('product.brand', function ($query) {
					global $request;
					$query->where('id', $request->brand);
				});
			})
			->when($request->has('category'), function ($query) {
				$query->whereHas('product.category', function ($query) {
					global $request;
					$query->where('id', $request->category);
				});
			})
			->when($request->has('condition'), function ($query) {
				$query->whereHas('condition', function ($query) {
					global $request;
					$query->where('id', $request->condition);
				});
			});

		$recordsFiltered = $product_units->count();

		$sortBy = $columns[$request->order[0]['column']];
		if ($sortBy == 'category_id') {
			$product_units->join('products', 'products.id', 'units.product_id')
				->orderBy('products.category_id', $request->order[0]['dir']);
		} elseif ($sortBy == 'brand_id') {
			$product_units->join('products', 'products.id', 'units.product_id')
				->orderBy('products.brand_id', $request->order[0]['dir']);
		} elseif ($sortBy == 'name') {
			$product_units->join('products', 'products.id', 'units.product_id')
				->orderBy('products.name', $request->order[0]['dir']);
		} elseif ($sortBy == 'size') {
			$product_units->orderBy('feature_5', $request->order[0]['dir'])
				->orderBy('feature_3', $request->order[0]['dir']);
		} else {
			$product_units->orderBy($sortBy, $request->order[0]['dir']);
		}

		$product_units = $product_units->offset($request->start)
			->limit($request->length)
			/*
			->orderBy(
				$columns[$request->order[0]['column']],
				$request->order[0]['dir']
			)
			*/
			->get();

		$output = [];

		$s = [
			0 => ['color' => 'success', 'text' => 'в наличии'],
			1 => ['color' => 'warning', 'text' => 'резерв'],
		];

		foreach ($product_units as $unit) {
			// чтобы в разные столбцы не ставить SML и обувь
			$unit->size = $unit->feature_5 ? $unit->feature_5 : $unit->feature_3;

			if ($unit->count == 0) {
				$badge = '<label class="badge badge-danger">продано</label>';
			} else {
				$badge = '<label class="badge badge-' . $s[$unit->is_reserved]['color'] . '">'
					. $s[$unit->is_reserved]['text'] . '</label>';
			}

			$output[] = [
				$unit->code,
				optional(optional($unit->product)->category)->name ?? '–',
				optional(optional($unit->product)->brand)->name ?? '–',
				optional($unit->product)->name ?? '–',
				optional($unit->condition)->name ?? '–',

				optional($unit->binding)->name ?? '–',
				$unit->feature_1,
				$unit->size,
				$unit->feature_4,
				optional(optional($unit->product)->category)->group ? $unit->count : '',

				$badge
			];
		}

		return json_encode([
			'draw'            => $request->draw,
			'recordsTotal'    => $recordsTotal,
			'recordsFiltered' => $recordsFiltered,
			'data'            => $output
		]);
	}

	public function import(Request $request)
	{
		$cols = [
			['id' => 'code',     	'name' => 'Артикул'],
			['id' => 'condition',	'name' => 'Статус'],
			['id' => 'ft_1',     	'name' => 'Хар.1'],
			['id' => 'ft_2', 	  	'name' => 'Хар.2'],
			['id' => 'ft_3', 	  	'name' => 'Хар.3'],
			['id' => 'binding',	  	'name' => 'Крепление'],
			['id' => 'product',   	'name' => 'Наименование'],
			['id' => 'category',  	'name' => 'Гр.aрт.проката'],
			['id' => 'brand',     	'name' => 'Марка']
		];

		if ($request->has('excel')) {
			if ($this->user->hasRole('superadmin'))
				$location_id = $request->location_id;
			else
				$location_id = $this->user->location_id;

			$i = 0;

			foreach (json_decode($request->excel) as $row) {
				$i++;
				if ($i == 1) continue;

				$condition = trim($row[array_search('condition', $request->col)]);

				if (!empty($condition))
					$condition = Condition::firstOrCreate(['name' => $condition]);

				$binding = trim($row[array_search('binding', $request->col)]);

				if (!empty($binding))
					$binding = Binding::firstOrCreate(['name' => $binding]);

				$category = trim($row[array_search('category', $request->col)]);

				if (!empty($category))
					$category = Category::firstOrCreate(['name' => $category]);

				$brand = trim($row[array_search('brand', $request->col)]);

				if (!empty($brand))
					$brand = Brand::firstOrCreate(['name' => $brand]);

				$code = trim($row[array_search('code', $request->col)]);

				$product = trim($row[array_search('product', $request->col)]);

				// features
				$ft_1 = trim($row[array_search('ft_1', $request->col)]);
				$ft_2 = trim($row[array_search('ft_2', $request->col)]);
				$ft_3 = trim($row[array_search('ft_3', $request->col)]);
				$ft_4 = trim($row[array_search('ft_4', $request->col)]);
				$ft_5 = trim($row[array_search('ft_5', $request->col)]);

				$ft_1 = str_replace(['=', '>', '<', '-'], ['', '', '', ''], $ft_1);
				$ft_2 = str_replace(['=', '>', '<', '-'], ['', '', '', ''], $ft_2);
				$ft_3 = str_replace(['=', '>', '<', '-'], ['', '', '', ''], $ft_3);
				$ft_4 = str_replace(['=', '>', '<', '-'], ['', '', '', ''], $ft_4);
				$ft_5 = str_replace(['=', '>', '<', '-'], ['', '', '', ''], $ft_5);

				$feature_1 = null;
				$feature_2 = null;
				$feature_3 = null;
				$feature_4 = null;
				$feature_5 = null;

				$categoryParams = $category->params;
				$ft_n = 1;
				foreach ($categoryParams as $n => $value) {
					if ($value == 1) {
						if ($ft_n == 1) $featureValue = $ft_1;
						if ($ft_n == 2) $featureValue = $ft_2;
						if ($ft_n == 3) $featureValue = $ft_3;
						if ($ft_n == 4) $featureValue = $ft_4;
						if ($ft_n == 5) $featureValue = $ft_5;

						if ($n == 1) $feature_1 = $featureValue;
						if ($n == 2) $feature_2 = $featureValue;
						if ($n == 3) $feature_3 = $featureValue;
						if ($n == 4) $feature_4 = $featureValue;
						if ($n == 5) $feature_5 = $featureValue;

						$ft_n++;
					}
				}

				$v = \Validator::make(
					[
						'product' => $product,
						'code'    => $code
					],
					[
						'product' => 'required',
						'code'    => 'required|unique:units'
					]
				);

				if ($v->fails()) continue;

				$product = Product::firstOrCreate([
					'name'        => $product,
					'category_id' => $category->id ? $category->id : null,
					'brand_id'    => $brand->id ? $brand->id : null
				]);

				Unit::create([
					'location_id'  => $location_id,
					'product_id'   => $product->id,
					'condition_id' => $condition->id,
					'binding_id'   => optional($binding)->id,
					'code'         => $code,
					'feature_1'	   => $feature_1,
					'feature_2'	   => $feature_2,
					'feature_3'	   => $feature_3,
					'feature_4'	   => $feature_4,
					'feature_5'	   => $feature_5,
				]);
			}

			return redirect()->route('stock.index')
				->with('success', 'Данные успешно импортированы');
		}

		$excel = [];

		if ($request->hasFile('excel_file')) {
			$data = (new UnitImport)->toCollection($request->file('excel_file'));

			$i = 0;

			foreach ($data[0] as $row) {
				if ($row[0] === null) continue;
				$i++;
				$excel[] = $row;
			}
		}

		$locations = Location::all();

		return view('stock.import', compact('locations', 'cols', 'request', 'excel'));
	}

	public function create()
	{
		$this->authorize('create', Unit::class);

		$products   = Product::all();
		$conditions = Condition::all();
		$locations  = Location::all();
		$bindings 	= Binding::all();
		$categories	= Category::with('products')->get();
		$features	= Category::getAvailableParams();
		$clone = false;

		if (request()->clone) {
			$unit = Unit::find(request()->clone);
			$clone = true;
			return view('stock.edit', compact('products', 'conditions', 'locations', 'bindings', 'categories', 'features', 'unit', 'clone'));
		}

		return view('stock.create', compact('products', 'conditions', 'locations', 'bindings', 'categories', 'features', 'clone'));
	}

	public function checkIfUnitUnique(Request $request)
	{
		if (Unit::where('code', $request->code)->where('location_id', $request->location_id)->exists()) {
			return response()->json(['result' => false]);
		} else return response()->json(['result' => true]);
	}

	public function store(Request $request)
	{
		$this->authorize('create', Unit::class);

		$request->validate([
			'code' => 'unique:units'
		]);

		$codes = [];
		if ($request->code)
			$codes[] = $request->code;
		else
			$codes = explode("\r\n", $request->codes);

		$data = $request->except('code', 'codes', 'category_id');

		foreach ($codes as $code) {
			if (empty($code)) break;
			$data['code'] = $code;

			$unit = Unit::create($data);
		}

		return redirect()->route('stock.index')->with('success', 'Добавлено ' . count($codes) . ' товаров');
	}

	public function show($id)
	{
		//
	}

	public function edit($code)
	{
		$products   = Product::all();
		$conditions = Condition::all();
		$locations  = Location::all();
		$unit       = Unit::where('code', $code)->first();
		$bindings	= Binding::all();
		$clone 		= false;

		$this->authorize('update', $unit);

		$categories	= Category::with('products')->get();
		$features	= Category::getAvailableParams();

		return view('stock.edit', compact('unit', 'products', 'conditions', 'locations', 'bindings', 'categories', 'features', 'clone'));
	}

	public function update(Request $request, $unit_id)
	{
		$unit = Unit::where('id', $unit_id)->firstOrFail();
		$this->authorize('update', $unit);
		$code = $unit->code;

		$data = $request->except('category_id', '_method', '_token');
		// $data['for_sale'] = $request->has('for_sale');

		// $unit->update($data);
		$units = Unit::where('code', $code)->update($data);

		return redirect()->route('stock.edit', $code)->with('success', 'Единица товара сохранена');
	}

	public function destroy($unitId)
	{
		$unit = Unit::findOrFail($unitId);
		$unit->delete();

		return redirect()->route('stock.index')->with('success', 'Единица товара удалена');
	}
}
