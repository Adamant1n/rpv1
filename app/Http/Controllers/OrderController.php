<?php

namespace App\Http\Controllers;

use App\Order;
use App\Client;
use App\Location;
use App\Product;
use App\Category;
use App\Condition;
use App\Unit;
use App\Tariff;
use App\PriceList;
use App\Brand;
use App\Agent;
use Illuminate\Http\Request;
use UploadImage;
use App\Setting;
use App\Services\PledgeService;

use PhpOffice\PhpWord\IOFactory;
use PhpOffice\PhpWord\Settings;

class OrderController extends Controller
{
	public function __construct()
	{
		$this->middleware(function ($request, $next) {
			$this->user = auth()->user();
			return $next($request);
		});

		$this->authorizeResource(Order::class);
	}

	public function pledgeAgreement(Order $order)
	{
		$service = new PledgeService($order);

		return $service->generateDocument();
	}

	public function sticker(Order $order)
	{
		$font = base_path() . '/resources/fonts/arial-c.ttf';

		header('Content-Type: image/png');

		$im = imagecreatetruecolor(472, 293);
		$white = imagecolorallocate($im, 255, 255, 255);
		$grey = imagecolorallocate($im, 128, 128, 128);
		$black = imagecolorallocate($im, 0, 0, 0);

		imagefilledrectangle($im, 0, 0, 472, 293, $white);

		$text = $order->client->last_name;
		imagettftext($im, 48, 0, 50, 90, $black, $font, $text);

		// штрихкод
		$text = $order->optional_id;
		imagettftext($im, 32, 0, 140, 280, $black, $font, $text);

		$generator = new \Picqer\Barcode\BarcodeGeneratorJPG();
		$file = $generator->getBarcode($order->optional_id, $generator::TYPE_CODE_128, 4, 120);

		$barcode = imagecreatefromstring($file);
		imagecopy($im, $barcode, 56, 110, 0, 0, 360, 120);

		imagepng($im);
		imagedestroy($im);
	}

	public function agreement(Order $order)
	{
		$template = Setting::where('name', 'template')->first()->value;

		$templateProcessor = new \PhpOffice\PhpWord\TemplateProcessor(base_path() . '/storage/app/public/' . $template);
		$templateProcessor->setValue('order_id', $order->optional_id);
		$templateProcessor->setValue('today', now()->format('d.m.Y'));
		$templateProcessor->setValue('legal_name', $order->location->legal_name);
		$templateProcessor->setValue('client_age', $order->client->age);
		$templateProcessor->setValue('client_name', $order->client->full_name);
		$templateProcessor->setValue('client_phone', $order->client->phone);
		$templateProcessor->setValue('client_weight', $order->client->feature->weight);
		$templateProcessor->setValue('client_height', $order->client->feature->height);
		$templateProcessor->setValue('client_shoe', $order->client->feature->shoe_size);
		$templateProcessor->setValue('client_name', $order->client->name);
		$templateProcessor->setValue('date_start', optional($order->starts_at)->format('H:i d.m.Y'));
		$templateProcessor->setValue('date_end', optional($order->ends_at)->format('H:i d.m.Y'));
		$templateProcessor->setValue('price', $order->price);

		$temp_file = tempnam(sys_get_temp_dir(), 'PHPWord');
		$templateProcessor->saveAs($temp_file);

		$phpWord = \PhpOffice\PhpWord\IOFactory::load($temp_file);


		// генерация штрихкода
		$generator = new \Picqer\Barcode\BarcodeGeneratorJPG();
		$file = $generator->getBarcode($order->optional_id, $generator::TYPE_CODE_128_A);
		$barcode_file = base_path() . '/storage/app/public/' . $order->optional_id .  '.jpg';
		file_put_contents($barcode_file, $file);

		// добавить штрихкод
		/*
		$header = $sections[0]->addHeader();

		$header->addImage($barcode_file, ['align' => 'center']);
		*/

		$sections = $phpWord->getSections();
		$section = $sections[0];
		$element = $section->getElements()[0];
		$element->addImage($barcode_file, [
			'width' => 80,
			'height' => 40,
			//'wrappingStyle' => 'square',
			'positioning' => 'absolute',
			'posHorizontal'    => \PhpOffice\PhpWord\Style\Image::POSITION_HORIZONTAL_CENTER,
			'posHorizontalRel' => 'margin',
			'posVerticalRel' => 'margin',
			'posVertical'    => 0,
		]);

		if ($order->client->photo) {
			try {
			$element->addImage(base_path() . '/public/' . UploadImage::load('client', 150) . $order->client->photo, [
				'width' => 76,
				'height' => 108,
				'wrappingStyle' => 'square',
				'positioning' => 'absolute',
				'posHorizontal'    => \PhpOffice\PhpWord\Style\Image::POSITION_HORIZONTAL_RIGHT,
				'posHorizontalRel' => 'margin',
				'posVerticalRel' => 'margin',
				'posVertical'    => 0,
			]);
			}
			catch(\Exception $e) {
				// 
			}
		}

		// страница инвентаря
		$section = $phpWord->addSection();
		$section->addText('Приложение №1 к договору ' . $order->optional_id, ['align' => 'center']);

		$styleCell = [
			'borderTopSize'		=> 0,
			'borderTopColor' 	=> 'white',
			'borderLeftSize'	=> 0,
			'borderLeftColor' 	=> 'white',
			'borderRightSize'	=> 0,
			'borderRightColor'	=> 'white',
			'borderBottomSize' 	=> 0,
			'borderBottomColor'	=> 'white'
		];

		$styleCell = [];

		$table = $section->addTable('myOwnTableStyle', [
			'borderSize'	=> 0,
			'borderColor' 	=> 'white',
			'afterSpacing' 	=> 0,
			'Spacing'		=> 0,
			'cellMargin' 	=> 0,
			'unit' => \PhpOffice\PhpWord\Style\Table::WIDTH_PERCENT,
			'width' => 100 * 50,
		]);

		// штрих наименование бред модель сумма

		$table->addRow(-0.5, array('exactHeight' => -5));
		$table->addCell(2500, $styleCell)->addText('Штрих-код', [
			'align' => 'left', 'spaceAfter' => 0, 'bold' => true
		]);
		$table->addCell(2500, $styleCell)->addText('Категория', [
			'align' => 'left', 'spaceAfter' => 0, 'bold' => true
		]);
		$table->addCell(2500, $styleCell)->addText('Бренд', [
			'align' => 'left', 'spaceAfter' => 0, 'bold' => true
		]);
		$table->addCell(2500, $styleCell)->addText('Модель', [
			'align' => 'left', 'spaceAfter' => 0, 'bold' => true
		]);
		$table->addCell(2500, $styleCell)->addText('Стоимость, руб.', [
			'align' => 'left', 'spaceAfter' => 0, 'bold' => true
		]);

		$table->addCell(2500, $styleCell)->addText('Начало', [
			'align' => 'left', 'spaceAfter' => 0, 'bold' => true
		]);
		$table->addCell(2500, $styleCell)->addText('Окончание', [
			'align' => 'left', 'spaceAfter' => 0, 'bold' => true
		]);

		if (!empty($order->agent_id)) {
			$agent = Agent::findOrFail($order->agent_id);
		}

		$reservations = $order->reservations()->with('unit.product.category', 'unit.product.brand')->get();


		foreach ($reservations as $reservation) {
			// строка
			$table->addRow(-0.5, array('exactHeight' => -5));

			// артикул
			$table->addCell(2500, $styleCell)->addText($reservation->unit->code, [
				'align' => 'left', 'spaceAfter' => 0
			]);

			// категория
			$table->addCell(2500, $styleCell)->addText($reservation->unit->product->category->name, [
				'align' => 'left', 'spaceAfter' => 0
			]);

			// бренд
			$table->addCell(2500, $styleCell)->addText($reservation->unit->product->brand->name, [
				'align' => 'left', 'spaceAfter' => 0
			]);

			// модель
			$table->addCell(2500, $styleCell)->addText($reservation->unit->product->name, [
				'align' => 'left', 'spaceAfter' => 0
			]);

			$table->addCell(2500, $styleCell)->addText($reservation->price . ' руб.', [
				'align' => 'left', 'spaceAfter' => 0
			]);

			$table->addCell(2500, $styleCell)->addText(optional($reservation->starts_at)->format('H:i d.m.Y'), [
				'align' => 'left', 'spaceAfter' => 0
			]);
			$table->addCell(2500, $styleCell)->addText(optional($reservation->ends_at)->format('H:i d.m.Y'), [
				'align' => 'left', 'spaceAfter' => 0
			]);
		}

		// duplicate

		$section = $phpWord->addSection();
		$section->addPageBreak();
		$section->addText('test');

		$phpWord->save($temp_file, 'HTML');

		/*
		header("Content-Disposition: attachment; filename=\"" . $order->optional_id . ".docx\"");
        readfile($temp_file);
        exit;
        */

		$html = file_get_contents($temp_file);

		$html = str_replace('<img', '<img onload="window.print();setTimeout(window.close, 1000);"', $html);
		echo $html . '<p style="page-break-before: always"></p>' . $html;

		//header("Content-Disposition: attachment; filename=\"" . $order->optional_id . ".html\"");

		// header("Content-Disposition: attachment; filename=\"" . $order->optional_id . ".docx\"");
		//readfile($temp_file);
		unlink($temp_file);
	}

	public function barcode(Order $order)
	{
		$generator = new \Picqer\Barcode\BarcodeGeneratorJPG();
		$file = $generator->getBarcode($order->optional_id, $generator::TYPE_CODABAR, 1.6, 80);

		header("Content-Disposition: attachment; filename='" . $order->optional_id . ".jpg'");
		// readfile($file);
		echo $file;
		// unlink($temp_file);
	}

	public function index(Request $request)
	{
		$locations = Location::all();

		$is_superadmin = $this->user->hasRole('superadmin') ? true : false;

		if ($this->user->role->name == 'delivery' && $request->type != 'delivery') {
			return redirect('/orders?type=delivery');
		}

		$orderType = $request->type ? $request->type : Order::TYPE_RENT;

		$orders = Order::with('client', 'reservations', 'onlineOrder')
			// если не суперадмин, филиал пользователя
			->when(!$is_superadmin, function ($query) {
				$query->where('location_id', $this->user->location_id);
			})
			// если суперадмин и указана локация
			->when(!$is_superadmin && !empty($request->location_id), function ($query) {
				$query->where('location_id', request()->location_id);
			})
			// только завершенные заказы
			->when($request->has('ended'), function ($query) {
				$query->ended();
			})
			->when(!$request->has('ended'), function ($query) {
				$query->active();
			})
			->when($request->location, function ($query) use ($request) {
				$query->where('location_id', $request->location);
			})
			->when($this->user->hasRole('cashier'), function ($query) {
				$query->where('type', 'rent')
					->orWhere('status', 'staffed');
			})
			->where('type', $orderType)
			->where(function ($query) {
				$query->whereHas('reservations')
					->orWhereHas('onlineOrder');
			})
			->get();

		$orders = $orders->map(function ($order) {
			if ($order->onlineOrder) {
				$order->total_price = $order->price - $order->onlineOrder->paid_amount;
			} else {
				$order->total_price = $order->price;
			}

			return $order;
		});

		return view('orders.index', compact(
			'orders',
			'locations',
			'request'
		));
	}

	public function create(Request $request)
	{
		if ($request->has('client_id')) {
			$client = Client::findOrFail($request->client_id);
			$clients = [];
		} else $clients = Client::all();

		$tariffs = Tariff::all();

		$agents = Agent::all();

		if ($this->user->hasRole('superadmin')) {
			if (empty($request->location_id))
				$price_lists = PriceList::all();
			else
				$price_lists = PriceList::where('location_id', $request->location_id)->get();

			$locations = Location::all();
		} else {
			$locations = Location::find($this->user->location_id);
			$price_lists = $locations->price_lists;
		}

		$product_units = Unit::with('product.brand', 'product.category', 'condition')
			->when(!$this->user->hasRole('superadmin'), function ($query) {
				$query->where('location_id', $this->user->location_id);
			})
			->when(
				$this->user->hasRole('superadmin') && !empty($request->location_id),
				function ($query) {
					global $request;
					$query->where('location_id', $request->location_id);
				}
			)
			->notReserved()->get();

		return view('orders.create', compact(
			'locations',
			'request',
			'tariffs',
			'client',
			'clients',
			'product_units',
			'price_lists',
			'znum',
			'agents'
		));
	}

	public function store(Request $request)
	{
		request()->validate([
			'order_units' => 'required',
			'duration'    => 'required',
			'price_list'  => 'required',
			'start_date'  => 'required'
		]);

		$price_list = PriceList::findOrFail($request->price_list);

		$order_price = 0;

		$duration = explode(',', $request->duration);

		foreach ($request->order_units as $order_unit) {
			$unit = Unit::with('product.category')->findOrFail($order_unit);
			$tariff  = Tariff::where([
				['category_id', $unit->product->category_id],
				['price_list_id', $request->price_list],
				['duration', $duration[0]],
				['dimension', $duration[1]]
			])->firstOrFail();
			$order_price += $tariff->price;
		}

		$full_price = $order_price;
		$discount 	= 0;


		$agent_id = null;
		if (!empty($request->agent_id)) {
			$agent = Agent::findOrFail($request->agent_id);
			$order_price = $order_price - ($order_price / 100 * $agent->discount);
			$discount = $agent->discount;
			$agent_id = $agent->id;
		}

		$order = Order::create([
			'location_id'   => $price_list->location_id,
			'user_id'       => $this->user->id,
			'client_id'     => $request->client_id,
			'price_list_id' => $request->price_list,
			'duration'      => $duration[0],
			'dimension'     => $duration[1],
			'price'         => $order_price,
			'start_date'    => $request->start_date . ':00',
			'full_price'	=> $full_price,
			'discount'		=> $discount,
			'paid_at'		=> now(),
			'agent_id'		=> $agent_id
		]);

		foreach ($request->order_units as $order_unit) {
			$order->units()->attach(['product_unit_id' => $order_unit]);

			$unit = Unit::findOrFail($order_unit);

			$unit->update([
				'reserved' => true,
				'counter' => $unit->counter + 1
			]);
		}

		return redirect()->route('orders.index')->with('success', 'Заказ успешно создан');
	}

	public function show($id)
	{
		$order = Order::where('id', $id)
			->with('client', 'user', 'reservations', 'agent')->firstOrFail();

		return view('orders.show', compact('order'));
	}

	public function edit($id)
	{
		$order = Order::where('id', $id)->with('units')->firstOrFail();

		return view('orders.edit', compact('order'));
	}

	public function update(Order $order, Request $request)
	{
		request()->validate(['units' => 'required']);

		foreach ($request->units as $unit) {
			Unit::find($unit)->update(['reserved' => false]);
		}

		// $order->delete();
		$order->setStatusEnded();

		return redirect()->route('orders.index')->with('success', 'Заказ завершен');
	}

	public function destroy($id)
	{
		//
	}
}
