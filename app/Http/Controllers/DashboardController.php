<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Order;
use App\Reservation;
use App\Location;
use App\Services\OrderService;
use App\Instructors\Event;

class DashboardController extends Controller
{
    public function dashboard(Request $request)
    {
        /*
        if(! auth()->user()->can('access-dashboard'))
        {
            return redirect('/orders');
        }
        */

        $user = auth()->user();
        $location = $user->location;

        $today = now();
        $tomorrow = now()->addDays(1);

        $rentOrdersCount = Order::where('location_id', $location->id)
            ->paid()
            ->count();

        $reservedInventoryCount = Reservation::rent()
            ->whereHas('order', function ($query) use ($location) {
                $query->where('status', Order::STATUS_PAID)
                    ->where('location_id', $location->id);
            })
            ->count();

        $reservedInventoryUntilToday = Reservation::rent()
            ->whereHas('order', function ($query) use ($location) {
                $query->where('status', Order::STATUS_PAID)
                    ->where('location_id', $location->id);
            })
            ->whereDate('ends_at', '<=', $today->format('Y-m-d'))
            ->count();

        $bookedForToday = Order::where('location_id', $location->id)
            ->book()
            ->whereDate('ends_at', '<=', $tomorrow->format('Y-m-d'))
            ->get();

        $bookedCount = $bookedForToday->count();
        $bookedStaffedCount = $bookedForToday->where('status', Order::STATUS_STAFFED)->count();
        $bookedNotStaffedCount = $bookedForToday->where('status', '<>', Order::STATUS_STAFFED)->count();

        $deliverToday = Order::where('location_id', $location->id)
            ->delivery()
            ->whereDate('ends_at', '<=', $tomorrow->format('Y-m-d'))
            ->get();

        $deliverTodayCount = $deliverToday->count();
        $deliverTodayStaffedCount = $deliverToday->where('status', Order::STATUS_STAFFED)->count();
        $deliverTodayNotStaffedCount = $deliverToday->where('status', '<>', Order::STATUS_STAFFED)->count();

        $dayOfWeek = now()->format('N');
        $isWeekend = in_array($dayOfWeek, $location->weekends);

        $priceList = $isWeekend ? $location->priceWeekends : $location->priceWeekdays;

        $expiredOrders = Order::paid()->where('ends_at', '<', now())->with('reservations')->get();
        $debt = 0;
        foreach ($expiredOrders as $order) {
            $service = new OrderService($order);

            foreach ($order->reservations as $reservation) {
                $orderDebt = 0;
                if ($reservation->status == Reservation::STATUS_RENT) {
                    $orderDebt = $service->calculateReservationExtraPrice($reservation, true, false);
                } elseif ($reservation->price_extra) {
                    $orderDebt += $reservation->price_extra;
                }
                $debt += $orderDebt;
            }
        }

        $view = $request->has('ajax') ? 'dashboard.main' : 'dashboard.index';

        return view($view, compact(
            'rentOrdersCount',
            'reservedInventoryCount',
            'reservedInventoryUntilToday',
            'bookedCount',
            'bookedStaffedCount',
            'bookedNotStaffedCount',
            'deliverTodayCount',
            'deliverTodayStaffedCount',
            'deliverTodayNotStaffedCount',
            'priceList',
            'debt'
        ));
    }

    public function analytics(Request $request)
    {
        if (!auth()->user()->can('access-analytics')) {
            return redirect('/orders');
        }

        $locationsByDates = Location::with(['orders' => function ($query) {
            $query->where('status', Order::STATUS_PAID)
                ->whereDate('starts_at', '>=', now()->subDays(2)->format('Y-m-d'))
                ->whereDate('starts_at', '<=', now()->format('Y-m-d'));
        }])
            ->get();

        $revenueByDates = $locationsByDates->map(function ($location) {
            $location->revenue = $location->orders->groupBy(function ($order) {
                return $order->starts_at->format('Y-m-d');
            })
                ->map(function ($date) {
                    return $date->sum('price');
                });

            return $location;
        });

        $locationsToday = Location::with(['orders' => function ($query) {
            $query->where('status', Order::STATUS_PAID)
                ->whereDate('starts_at', '=', now()->format('Y-m-d'));
        }])
            ->get();

        $revenueByPaymentType = $locationsToday->map(function ($location) {
            $location->revenue = $location->orders->groupBy('paid_by')
                ->map(function ($type) {
                    return $type->sum('price');
                });

            return $location;
        });

        $avgPayment = $locationsToday->map(function ($location) {
            $location->avg = $location->orders->avg('price');

            return $location;
        });

        $eventsToday = \App\Instructors\Location::with(['events' => function ($query) {
            $query->whereDate('paid_at', now()->format('Y-m-d'));
        }])
            ->get();

        $view = $request->has('ajax') ? 'analytics.main' : 'analytics.index';

        return view($view, compact(
            'revenueByDates',
            'revenueByPaymentType',
            'avgPayment',
            'locationsToday',
            'eventsToday'
        ));
    }
}
