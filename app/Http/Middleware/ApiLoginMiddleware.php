<?php

namespace App\Http\Middleware;

use Auth;
use Closure;

class ApiLoginMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
    	if($request->has('user_id'))
    	{
    		Auth::loginUsingId($request->user_id);
    	}
        return $next($request);
    }
}
