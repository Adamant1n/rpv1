<?php

namespace App\Http\Middleware;

use Closure;

class IsLocationActive
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = auth()->user();
        $user->update(['session_updated_at' => now()]);

        if(!$user->hasRole('superadmin')) {
            if(!$user->location->active) {
                abort(500, 'Филиал не активирован');
            }
        }

        return $next($request);
    }
}
