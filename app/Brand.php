<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
	use \App\Traits\LocationIdentificator;

	public $incrementing = false;
	protected $primaryKey = 'id';
	// protected $guarded = ['id'];

	protected $fillable = [
		'id',
		'name',
		'created_at',
		'updated_at',
	];

	protected $dates = [
		'created_at',
		'updated_at'
	];

	public function products()
	{
		return $this->hasMany(Product::class);
	}
}
