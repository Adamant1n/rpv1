<?php


namespace App\Mailers\Devino;


class SMSClientError
{
    const ERROR_OK = 0;
    const ERROR_ArgumentCanNotBeNullOrEmpty = 1;
    const ERROR_InvalidAgrument = 2;
    const ERROR_InvalidSessionID = 3;
    const ERROR_UnauthorizedAccess = 4;
    const ERROR_NotEnoughCredits = 5;
    const ERROR_InvalidOperation = 6;
    const ERROR_Forbidden = 7;
    const ERROR_GatewayError = 8;
    const ERROR_InternalServerError = 9;

}
