<?php

namespace App\Instructors;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
	use Notifiable;
	use HasRoles;
	use SoftDeletes;

	protected $guarded = ['id'];

	protected $dates = [
		'created_at',
		'updated_at',
		'deleted_at'
	];

	protected $hidden = [
		'password',
		'remember_token'
	];

	protected $appends = [
		'role',
		'full_name'
	];

	const ROLE_TRAINER = 'trainer';

	/** relationships **/

	public function locations()
	{
		return $this->belongsToMany(Location::class, 'location_trainer', 'trainer_id', 'location_id');
	}

	public function location()
	{
		return $this->belongsTo(Location::class);
	}

	/** accessors **/

	public function getFullNameAttribute()
	{
		return $this->last_name . ' ' . $this->first_name;
	}

	public function getRoleAttribute()
	{
		return $this->roles->first();
	}
}
