<?php

namespace App\Instructors;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
	protected $table = 'services';

	protected $guarded = ['id'];

	protected $dates = [
		'created_at',
		'updated_at'
	];

	public function tariffs()
	{
		return $this->hasMany(Tariff::class, 'service_id');
	}

	public function service()
	{
		return $this->belongsTo(Service::class, 'service_id');
	}
}
