<?php

namespace App\Instructors;

use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
	protected $table = 'locations';

	protected $guarded = ['id'];

	protected $dates = [
		'created_at',
		'updated_at'
	];

    protected $casts = [
        'active'    => 'boolean',
        'weekends'  => 'array',
        'all_day'   => 'boolean',
        'kkt'       => 'boolean'
    ];

    /** CONSTANTS **/

    public const TYPE_BOTH = 'both';
    public const TYPE_INSTRUCTORS = 'instructors';
    public const TYPE_RENT = 'rent';

    public static function boot()
    {
        parent::boot();

        static::addGlobalScope(function ($query) {
            $query->where(function ($query) {
                $query->where('type', self::TYPE_BOTH)
                    ->orWhere('type', self::TYPE_INSTRUCTORS);
            });
        });
    }

	public function trainers()
	{
		return $this->belongsToMany(User::class, 'location_trainer', 'location_id', 'trainer_id');
	}

    public function events()
    {
        return $this->hasMany(Event::class);
    }
}
