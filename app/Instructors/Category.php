<?php

namespace App\Instructors;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
	protected $table = 'event_categories';

	protected $guarded = ['id'];

	protected $dates = [
		'created_at',
		'updated_at'
	];

	const SKI = 1;
	const SNOWBOARD = 2;
}
