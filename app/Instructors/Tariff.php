<?php

namespace App\Instructors;

use Illuminate\Database\Eloquent\Model;

class Tariff extends Model
{
	protected $table = 'service_tariffs';

	protected $guarded = ['id'];

	protected $dates = [
		'created_at',
		'updated_at'
	];
}
