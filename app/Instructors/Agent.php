<?php

namespace App\Instructors;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Agent extends Model
{
	public $incrementing = false;
	protected $primaryKey = 'id';
	
	use SoftDeletes;

	protected $guarded = ['id'];

	protected $dates = [
		'created_at',
		'updated_at'
	];

	public function orders()
	{
		return $this->hasMany(Order::class);
	}
}
