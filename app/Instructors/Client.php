<?php

namespace App\Instructors;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
	protected $table = 'clients';

	protected $guarded = ['id'];

	protected $appends = ['full_name'];

	protected $dates = [
		'created_at',
		'updated_at'
	];

	public function getFullNameAttribute()
	{
		return $this->last_name . ' ' . $this->first_name . ' ' . $this->mid_name;
	}

	public function events()
	{
		return $this->hasMany(Event::class, 'client_id');
	}

    public function getOptionalIdAttribute()
    {
        return 'L'.sprintf("%06d", $this->id);
    }
}
