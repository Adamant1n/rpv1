<?php

namespace App\Instructors;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
	protected $table = 'events';

	protected $guarded = ['id'];

	protected $dates = [
		'created_at',
		'updated_at',
		'event_date',
		'sale_date',
		'paid_at'
	];

	public function manager()
	{
		return $this->belongsTo(User::class, 'manager_id');
	}

	public function trainer()
	{
		return $this->belongsTo(User::class, 'trainer_id');
	}

	public function client()
	{
		return $this->belongsTo(Client::class, 'client_id');
	}

	public function service()
	{
		return $this->belongsTo(Service::class, 'service_id');
	}

	public function category()
	{
		return $this->belongsTo(Service::class, 'category_id');
	}

	public function tariff()
	{
		return $this->belongsTo(Tariff::class);
	}

	public function location()
	{
		return $this->belongsTo(Location::class, 'location_id');
	}

	public function agent()
	{
		return $this->belongsTo(Agent::class);
	}

	public function getIsPaidAttribute()
	{
		return $this->payment_status == 1;
	}
}
