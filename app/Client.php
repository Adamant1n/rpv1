<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use UploadImage;

class Client extends Model
{
	use \App\Traits\LocationIdentificator;
	public $incrementing = false;
	// protected $guarded = ['id'];

	protected $fillable = [
		'id',
		'user_id',
		'first_name',
		'last_name',
		'skill',
		'age',
		'mid_name',
		'phone',
		'email',
		'passport',
		'passport_status',
		'description',
		'birthday',
		'photo',
		'passport_scan',
		'is_signed',
		'created_at',
		'updated_at',
	];

	protected $appends = ['full_name', 'optional_id', 'images', 'birthday_short'];

	protected $dates = [
		'birthday',
		'created_at',
		'updated_at'
	];

	protected $casts = [
		'is_signed' => 'boolean'
	];

	/** constants **/

	const PASSPORT_STATUS_PENDING = 'pending';
	const PASSPORT_STATUS_VALID	  = 'valid';
	const PASSPORT_STATUS_INVALID = 'invalid';

	/** RELATIONS **/

	public function feature()
	{
		return $this->hasOne(ClientFeature::class);
	}

	public function user()
	{
		return $this->belongsTo(User::class);
	}

	public function orders()
	{
		return $this->hasMany(Order::class);
	}

	public function currentOrder()
	{
		return $this->hasOne(Order::class)->active()->latest('id');
	}

	/** ATTRIBUTES **/

	public function getFullNameAttribute()
	{
		$fullName = $this->last_name . ' ' . $this->first_name;
		if ($this->mid_name) {
			$fullName .= ' ' . $this->mid_name;
		}

		return $fullName;
	}

	public function getAgeAttribute($value)
	{
		return $value ?? ($this->birthday ? $this->birthday->diffInYears(\Carbon\Carbon::now()) : null);
	}

	public function getBirthdayShortAttribute()
	{
		return $this->birthday ? $this->birthday->format('d.m.Y') : '';
	}

	public function getOptionalIdAttribute()
	{
		return 'L' . sprintf("%06d", $this->id);
	}

	public function getZNumAttribute()
	{
		$weights = [13, 17, 21, 25, 30, 35, 41, 48, 57, 66, 78, 94, 200];
		$heights = [0, 0, 0, 0, 0, 0, 0, 0, 157, 166, 178, 194, 250];
		$sizes = [
			250 => ['0.75', '1', '1.5', '1.75', '2.25', '2.75', '3.5', '', '', '', '', '', '', ''],
			270 => ['0.75', '1', '1.25', '1.5', '2', '2.5', '3', '3.5', '4.5', '5.5', '6.5', '7.5', '', ''],
			290 => ['', '0.75', '1', '1.5', '1.75', '2.25', '2.75', '3', '4', '5', '6', '7', '8.5', '10'],
			310 => ['', '', '', '1.25', '1.5', '2', '2.5', '3', '3.5', '4.5', '5.5', '6.5', '8', '9.5'],
			330 => ['', '', '', '', '1.5', '1.75', '2.25', '2.75', '3.5', '4', '5', '6', '7', '8.5'],
			600 => ['', '', '', '', '', '1.75', '2', '2.5', '3', '3.5', '4.5', '5.5', '6.5', '8']
		];

		// длина подошвы
		// $feet_length = 310;
		// $this->feature->shoe_size
		$shoe_size = $this->feature->shoe_size;
		$feet_length = (20 / 3) * ($shoe_size - 2);

		if (
			$this->feature()->exists()
			&& $this->feature->weight
			&& $this->feature->height
			&& $this->feature->shoe_size
		) {
			foreach ($weights as $w_key => $weight)
				if ($this->feature->weight <= $weight) break;

			foreach ($heights as $h_key => $height)
				if ($this->feature->height <= $height) break;

			foreach ($sizes as $size => $znums)
				if ($feet_length <= $size) break;

			$znum = $znums[min($w_key, $h_key)];
		} else $znum = '100';

		return $znum;
	}

	public function getUpdatedAtAttribute($value)
	{
		return \Carbon\Carbon::parse($value)->format('H:i d.m.Y');
	}

	public function getCreatedAtAttribute($value)
	{
		return \Carbon\Carbon::parse($value)->format('H:i d.m.Y');
	}

	public function getInGroupAttribute()
	{
		return $this->orders()->active()->whereNotNull('group_id')->count() > 0;
	}

	public function getImagesAttribute()
	{
		if ($this->passport_scan) {
			if (strpos($this->passport_scan, 'http://') !== false || strpos($this->passport_scan, 'https://') !== false) {
				$passport = $this->passport_scan;
			} else {
				$passport = asset(UploadImage::load('passport') . $this->passport_scan);
			}
		} else {
			$passport = '';
		}

		if ($this->photo) {
			if (strpos($this->photo, 'http://') !== false || strpos($this->photo, 'https://') !== false) {
				$photo = $this->photo;
			} else {
				$photo = asset(UploadImage::load('client') . $this->photo);
			}
		} else {
			$photo = '';
		}

		return [
			'passport' => $passport,
			'photo' => $photo,
		];
	}
}
