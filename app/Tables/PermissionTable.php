<?php

namespace App\Tables;

use App\Services\DataTable\DataTable;
use Spatie\Permission\Models\Permission;

class PermissionTable extends DataTable
{
	public $tableName = 'permissions';
	public $routeName = 'permissions.index';
	public $routeEdit = 'permissions.edit';

    public $columns = [
		[
			'data'			=> 'id',
			'name' 			=> 'ID',
			'orderable'		=> true,
			'visible'		=> true,
		],
		[
			'data'			=> 'name',
			'name' 			=> 'Название',
			'orderable'		=> true,
			'visible'		=> true,
			'filter'		=> [
				'type'		=> 'input',
				'custom'	=> false,
			],
			'searchable'	=> true,
		],
		[
			'data'			=> 'display',
			'name' 			=> 'Заголовок',
			'orderable'		=> true,
			'visible'		=> true,
			'filter'		=> [
				'type'		=> 'input',
				'custom'	=> false,
			],
			'searchable'	=> true,
		],
		[
			'data'			=> 'guard_name',
			'name' 			=> 'Guard',
			'orderable'		=> false,
			'visible'		=> true,
		],
		[
			'data'			=> 'action',
			'name'			=> '',
			'orderable'		=> false,
			'visible'		=> true
		]
	];

	public function setBaseCollection()
	{
		$this->baseCollection = (new Permission);
	}

	public function mapItem($item)
	{
		return [
			'id'			=> $item->id,
			'name'			=> $item->name,
			'display'		=> $item->display,
			'guard_name'	=> $item->guard_name,
			'action'		=> bladeTableButton('Изменить', route($this->routeEdit, $item->id), 'outline-info')
		];
	}

	public function loadDynamicFilters()
	{
		//
	}
}
