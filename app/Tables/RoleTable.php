<?php

namespace App\Tables;

use App\Services\DataTable\DataTable;
use Spatie\Permission\Models\Role;

class RoleTable extends DataTable
{
	public $tableName = 'roles';
	public $routeName = 'roles.index';
	public $routeEdit = 'roles.edit';

    public $columns = [
		[
			'data'			=> 'id',
			'name' 			=> 'ID',
			'orderable'		=> true,
			'visible'		=> true,
		],
		[
			'data'			=> 'name',
			'name' 			=> 'Название',
			'orderable'		=> true,
			'visible'		=> true,
			'filter'		=> [
				'type'		=> 'input',
				'custom'	=> false,
			],
			'searchable'	=> true,
		],
		[
			'data'			=> 'display',
			'name' 			=> 'Заголовок',
			'orderable'		=> true,
			'visible'		=> true,
			'filter'		=> [
				'type'		=> 'input',
				'custom'	=> false,
			],
			'searchable'	=> true,
		],
		[
			'data'			=> 'color',
			'name' 			=> 'Цвет',
			'orderable'		=> false,
			'visible'		=> true,
		],
		[
			'data'			=> 'action',
			'name'			=> '',
			'orderable'		=> false,
			'visible'		=> true
		]
	];

	public function setBaseCollection()
	{
		$this->baseCollection = (new Role);
	}

	public function mapItem($item)
	{
		return [
			'id'			=> $item->id,
			'name'			=> $item->name,
			'display'		=> $item->display,
			'color'			=> bladeBadge($item->display, $item->color),
			'action'		=> bladeTableButton('Изменить', route($this->routeEdit, $item->id), 'outline-info')
		];
	}

	public function loadDynamicFilters()
	{
		//
	}
}
