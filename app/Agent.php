<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Agent extends Model
{
	use \App\Traits\LocationIdentificator;

	public $incrementing = false;
	protected $primaryKey = 'id';

	use SoftDeletes;

	// protected $guarded = ['id'];
	protected $fillable = [
		'id',
		'name',
		'promocode',
		'discount',
		'sell_discount',
		'commission',
		'created_at',
		'updated_at',
		'deleted_at',
	];

	protected $dates = [
		'created_at',
		'updated_at'
	];

	public function orders()
	{
		return $this->hasMany(Order::class);
	}

	public function products()
	{
		return $this->belongsToMany(Product::class)->withPivot('discount');
	}
}
