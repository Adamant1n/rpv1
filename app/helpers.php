<?php

use App\User;

if (!function_exists('getSnakeFromModel'))
{
	function getSnakeFromModel($model)
	{
	    return strtolower(snake_case(str_plural(class_basename($model))));
	}
}

if (!function_exists('currentUser'))
{
	function currentUser() :? User
	{
	    return auth()->user();

	    throw new Exception('Coud not determine current user');
	}
}

if(!function_exists('getRouteBase'))
{
	function getRouteBase(): string
	{
	    $parts = explode('.', getRouteName());
	    array_pop($parts);

	    return implode('.', $parts);
	}
}

if(!function_exists('getActionName'))
{
	function getActionName($action): string
	{
	    return getRouteBase() . '.' . $action;
	}
}

if(!function_exists('getRouteName'))
{
	function getRouteName(): string
	{
	    return app()->router->getCurrentRoute()->getName();
	}
}

if(!function_exists('getRouteModel'))
{
	function getRouteModel(): string
	{
	    return explode('.', app()->router->getCurrentRoute()->getName())[0];
	}
}

/** template helpers **/

if(!function_exists('bladeBadge'))
{
	function bladeBadge($slot, $color): string
	{
	    return view('components.badge', compact('slot', 'color'))->render();
	}
}

if(!function_exists('bladeTableButton'))
{
	function bladeTableButton($slot, $href, $color): string
	{
	    return view('components.table_button', compact('slot', 'href', 'color'))->render();
	}
}

if(!function_exists('bladeThumbnail'))
{
	function bladeThumbnail($slot, $width, $height): string
	{
	    return view('components.thumbnail', compact('slot', 'width', 'height'))->render();
	}
}