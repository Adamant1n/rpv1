<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderGroup extends Model
{
    use \App\Traits\LocationIdentificator;

    public $incrementing = false;
    protected $primaryKey = 'id';

    // protected $guarded = ['id'];
    protected $fillable = [
        'id',
        'client_id',
        'status',
        'created_at',
        'updated_at',
    ];

    protected $appends = [
        'optional_id',
        'price',
        'pledge_amount',
        'active_reservations_count'
        /*
    	'price',
    	'starts_at',
    	'ends_at',
    	'duration',
    	'dimension'
        */
    ];

    protected $with = [
        'payments'
    ];

    protected $casts = [
        'price' => 'decimal:2'
    ];

    /** relationships **/

    public function payments()
    {
        return $this->hasManyThrough(Payment::class, Order::class, 'group_id');
    }

    public function orders()
    {
        return $this->hasMany(Order::class, 'group_id');
    }

    public function clients()
    {
        return $this->hasManyThrough(Client::class, Order::class, 'group_id');
    }

    public function reservations()
    {
        return $this->hasManyThrough(Reservation::class, Order::class, 'group_id');
    }

    /** attributes **/

    public function getOptionalIdAttribute()
    {
        return 'DG' . sprintf("%05d", $this->id);
    }

    public function getPriceAttribute()
    {
        if ($this->orders)
            return $this->castAttribute('price', $this->orders->sum('price'));
        else
            return $this->castAttribute('price', 0);
    }

    public function getPriceExtraAttribute()
    {
        return $this->orders()->sum('price_extra');
    }

    public function getStartsAtAttribute()
    {
        return $this->reservations()->min('reservations.starts_at');
    }

    public function getEndsAtAttribute()
    {
        return $this->reservations()->max('reservations.ends_at');
    }

    public function getDurationAttribute()
    {
        return $this->orders->first()->duration;
    }

    public function getDimensionAttribute()
    {
        return $this->orders->first()->dimension;
    }

    public function getPledgeAmountAttribute()
    {
        return $this->orders->sum('pledge_amount');
    }

    public function getActiveReservationsCountAttribute()
    {
        return $this->reservations()->whereNotNull('reservations.starts_at')->where('reservations.status', Reservation::STATUS_RENT)->count();
    }
}
