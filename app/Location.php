<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
	const MAIN = 1;

	// protected $guarded = ['id'];

	protected $fillable = [
		'id',
		'type',
		'address',
		'legal_name',
		'legal_address',
		'legal_phone',
		'price_weekdays',
		'price_weekends',
		'tariff_step',
		'hours_max',
		'free_time',
		'opens_at',
		'closes_at',
		'next_day_at',
		'return_by',
		'weekends',
		'day_is',
		'all_day',
		'active',
		'kkt',
		'payment_types',
		'rent_taxation_type',
		'rent_tax',
		'sale_taxation_type',
		'sale_tax',
		'created_at',
		'updated_at',
	];

	protected $dates = [
		'created_at',
		'updated_at'
	];

	protected $casts = [
		'active' 	=> 'boolean',
		'weekends'	=> 'array',
		'all_day'	=> 'boolean',
		'kkt'		=> 'boolean',
		'payment_types' => 'array',
	];

	/** CONSTANTS **/

	const STEP_MINUTE 	= '10m';
	const STEP_HOUR		= '1h';
	const STEP_DAY		= '1d';

	public static $steps = [
		self::STEP_MINUTE => '10 минут',
		self::STEP_HOUR => '1 час',
		self::STEP_DAY => '1 день',
	];

	const DAY_IS_OPENED	= 'opened';
	const DAY_IS_24		= '24';

	public const TYPE_BOTH = 'both';
	public const TYPE_INSTRUCTORS = 'instructors';
	public const TYPE_RENT = 'rent';

	public const TAXATION_TYPE_OSN = 'osn';
	public const TAXATION_TYPE_USNINCOME = 'usnIncome';
	public const TAXATION_TYPE_USNINCOMEOUTCOME = 'usnIncomeOutcome';
	public const TAXATION_TYPE_ENVD = 'envd';
	public const TAXATION_TYPE_PATENT = 'patent';

	public const TAX_NONE = 'none';
	public const TAX_VAT0 = 'vat0';
	public const TAX_VAT10 = 'vat10';
	public const TAX_VAT18 = 'vat18';
	public const TAX_VAT20 = 'vat20';

	/** relationships **/

	public function priceWeekdays()
	{
		return $this->belongsTo(PriceList::class, 'price_weekdays');
	}

	public function priceWeekends()
	{
		return $this->belongsTo(PriceList::class, 'price_weekends');
	}

	public function trainers()
	{
		return $this->belongsToMany(User::class, 'location_trainer', 'location_id', 'trainer_id');
	}

	public function users()
	{
		return $this->hasMany(User::class);
	}

	public function units()
	{
		return $this->hasMany(Unit::class);
	}

	public function products()
	{
		return $this->hasManyThrough(Unit::class, Product::class);
	}

	public function orders()
	{
		return $this->hasMany(Order::class);
	}

	public function price_lists()
	{
		return $this->hasMany(PriceList::class);
	}

	/** accessors **/

	public function getPaymentTypesAttribute($value)
	{
		return json_decode($value) ?? [];
	}

	public function getBaseIdAttribute()
	{
		$id = strval($this->id);
		$length = (strlen($id) === 1) ? 5 : 6;

		while (true) {
			$id .= '0';
			if (strlen($id) == $length) break;
		}

		return intval($id);
	}

	public function getMaxIdAttribute()
	{
		$id = strval($this->id + 1);
		$length = (strlen($id) === 1) ? 6 : 7;

		while (true) {
			$id .= '0';
			if (strlen($id) == $length) break;
		}

		return intval($id) - 1;
	}
}
