<?php

namespace App\Policies;

use App\User;
use Spatie\Permission\Models\Permission;
use Illuminate\Auth\Access\HandlesAuthorization;

class PermissionPolicy
{
    use HandlesAuthorization;

    public function index(User $user)
    {
        return $user->can('manage-roles');
    }

    public function view(User $user, Permission $permission)
    {
        return $user->can('manage-roles');
    }

    public function create(User $user)
    {
        return $user->can('manage-roles');
    }

    public function update(User $user, Permission $permission)
    {
        return $user->can('manage-roles');
    }

    public function delete(User $user, Permission $permission)
    {
        return $user->can('manage-roles');
    }

    public function restore(User $user, Permission $permission)
    {
        return $user->can('manage-roles');
    }

    public function forceDelete(User $user, Permission $permission)
    {
        return $user->can('manage-roles');
    }
}
