<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class SettingsPolicy
{
    use HandlesAuthorization;

    public function manageSettings(User $user)
    {
        return $user->can('manage-settings');
    }

    public function manageProperties(User $user)
    {
        return $user->can('manage-properties');
    }

    public function takePayment(User $user)
    {
        return $user->can('take-payment');
    }
}
