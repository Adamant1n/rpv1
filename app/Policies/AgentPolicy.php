<?php

namespace App\Policies;

use App\User;
use App\Agent;
use Illuminate\Auth\Access\HandlesAuthorization;

class AgentPolicy
{
    use HandlesAuthorization;

    public function index(User $user)
    {
        return $user->can('manage-agents');
    }

    public function view(User $user)
    {
        return $user->can('manage-agents');
    }

    public function create(User $user)
    {
        return $user->can('manage-agents');
    }

    public function update(User $user)
    {
        return $user->can('manage-agents');
    }

    public function delete(User $user, Agent $agent)
    {
        return $user->can('manage-agents');
    }

    public function restore(User $user, Agent $agent)
    {
        return $user->can('manage-agents');
    }

    public function forceDelete(User $user, Agent $agent)
    {
        return $user->can('manage-agents');
    }
}
