<?php

namespace App\Policies;

use App\User;
use App\Client;
use Illuminate\Auth\Access\HandlesAuthorization;

class ClientPolicy
{
    use HandlesAuthorization;

    public function index(User $user)
    {
        return $user->can('manage-clients');
    }

    public function view(User $user, Client $client)
    {
        return $user->can('manage-clients');
    }

    public function create(User $user)
    {
        return $user->can('manage-clients');
    }

    public function update(User $user, Client $client)
    {
        return $user->can('manage-clients');
    }

    public function delete(User $user, Client $client)
    {
        return $user->can('manage-clients');
    }

    public function restore(User $user, Client $client)
    {
        return $user->can('manage-clients');
    }

    public function forceDelete(User $user, Client $client)
    {
        return $user->can('manage-clients');
    }
}
