<?php

namespace App\Policies;

use App\User;
use App\Unit;

use Illuminate\Auth\Access\HandlesAuthorization;

class UnitPolicy
{
    use HandlesAuthorization;

    public function index(User $user)
    {
        return $user->can('manage-stock') || $user->can('manage-location-stock');
    }

    public function show(User $user, Unit $unit)
    {
        return $user->can('manage-stock') || 
            ($user->can('manage-location-stock') && $unit->location_id == $user->location_id);
    }

    public function create(User $user)
    {
        return $user->can('manage-stock') || $user->can('manage-location-stock');
    }

    public function delete(User $user, Unit $unit)
    {
        return $user->can('manage-stock') || 
            ($user->can('manage-location-stock') && $unit->location_id == $user->location_id);
    }

    public function update(User $user, Unit $unit)
    {
        return $user->can('manage-stock') || 
            ($user->can('manage-location-stock') && $unit->location_id == $user->location_id);
    }
}
