<?php

namespace App\Policies;

use App\User;
use App\PriceList;
use Illuminate\Auth\Access\HandlesAuthorization;

class PriceListPolicy
{
    use HandlesAuthorization;

    public function index(User $user)
    {
        return $user->can('manage-pricelists');
    }

    public function view(User $user, PriceList $priceList)
    {
        return $user->can('manage-pricelists');
    }

    public function create(User $user)
    {
        return $user->can('manage-pricelists');
    }

    public function update(User $user, PriceList $priceList)
    {
        return $user->can('manage-pricelists');
    }

    public function delete(User $user, PriceList $priceList)
    {
        return $user->can('manage-pricelists');
    }

    public function restore(User $user, PriceList $priceList)
    {
        return $user->can('manage-pricelists');
    }

    public function forceDelete(User $user, PriceList $priceList)
    {
        return $user->can('manage-pricelists');
    }
}
