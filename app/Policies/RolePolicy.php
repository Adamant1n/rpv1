<?php

namespace App\Policies;

use App\User;
use Spatie\Permission\Models\Role;
use Illuminate\Auth\Access\HandlesAuthorization;

class RolePolicy
{
    use HandlesAuthorization;

    public function index(User $user)
    {
        return $user->can('manage-roles');
    }

    public function view(User $user, Role $role)
    {
        return $user->can('manage-roles');
    }

    public function create(User $user)
    {
        return $user->can('manage-roles');
    }

    public function update(User $user, Role $role)
    {
        return $user->can('manage-roles');
    }

    public function delete(User $user, Role $role)
    {
        return $user->can('manage-roles');
    }

    public function restore(User $user, Role $role)
    {
        return $user->can('manage-roles');
    }

    public function forceDelete(User $user, Role $role)
    {
        return $user->can('manage-roles');
    }
}
