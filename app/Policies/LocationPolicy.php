<?php

namespace App\Policies;

use App\User;
use App\Location;
use Illuminate\Auth\Access\HandlesAuthorization;

class LocationPolicy
{
    use HandlesAuthorization;

    public function index(User $user)
    {
        return $user->can('manage-locations');
    }

    public function view(User $user, Location $location)
    {
        return $user->can('manage-locations');
    }

    public function create(User $user)
    {
        return $user->can('manage-locations');
    }

    public function update(User $user, Location $location)
    {
        return $user->can('manage-locations');
    }

    public function delete(User $user, Location $location)
    {
        return $user->can('manage-locations');
    }

    public function restore(User $user, Location $location)
    {
        return $user->can('manage-locations');
    }

    public function forceDelete(User $user, Location $location)
    {
        return $user->can('manage-locations');
    }
}
