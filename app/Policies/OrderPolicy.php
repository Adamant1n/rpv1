<?php

namespace App\Policies;

use App\User;
use App\Order;
use Illuminate\Auth\Access\HandlesAuthorization;

class OrderPolicy
{
    use HandlesAuthorization;

    public function index(User $user)
    {
        return $user->can('manage-orders') || $user->can('manage-location-orders');
    }

    public function view(User $user, Order $order)
    {
        return $user->can('manage-orders') || 
            ($user->can('manage-location-orders') && $user->location_id = $order->location_id);
    }

    public function create(User $user)
    {
        return $user->can('manage-orders') || $user->can('manage-location-orders');
    }

    public function update(User $user, Order $order)
    {
        return $user->can('manage-orders') || 
            ($user->can('manage-location-orders') && $user->location_id = $order->location_id);
    }

    public function delete(User $user, Order $order)
    {
        return $user->can('manage-orders') || 
            ($user->can('manage-location-orders') && $user->location_id = $order->location_id);
    }

    public function restore(User $user, Order $order)
    {
        return $user->can('manage-orders') || 
            ($user->can('manage-location-orders') && $user->location_id = $order->location_id);
    }

    public function forceDelete(User $user, Order $order)
    {
        return $user->can('manage-orders') || 
            ($user->can('manage-location-orders') && $user->location_id = $order->location_id);
    }
}
