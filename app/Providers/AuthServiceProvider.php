<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
        \App\Agent::class => \App\Policies\AgentPolicy::class,
        \App\Client::class => \App\Policies\ClientPolicy::class,
        \App\Location::class => \App\Policies\LocationPolicy::class,
        \App\Order::class => \App\Policies\OrderPolicy::class,
        \App\PriceList::class => \App\Policies\PriceListPolicy::class,
        \Spatie\Permission\Models\Permission::class => \App\Policies\PermissionPolicy::class,
        \Spatie\Permission\Models\Role::class => \App\Policies\RolePolicy::class,
        \App\User::class => \App\Policies\UserPolicy::class,
        \App\Unit::class => \App\Policies\UnitPolicy::class,
        \App\Report::class => \App\Policies\ReportPolicy::class,
        \App\Setting::class => \App\Policies\SettingsPolicy::class
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        //
    }
}
