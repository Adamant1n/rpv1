<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;

use App\Agent;
use App\Binding;
use App\Brand;
use App\Product;
use App\PriceList;
use App\Order;
use App\Client;
use App\Unit;
use App\OrderGroup;
use App\Reservation;

use Illuminate\Support\Facades\Blade;
use App\Observers\SynchronizableObserver;

class AppServiceProvider extends ServiceProvider
{
	/**
	 * Bootstrap any application services.
	 *
	 * @return void
	 */
	public function boot()
	{
		date_default_timezone_set('Asia/Aden');
		//date_default_timezone_set('America/Los_Angeles');
		
		setlocale(LC_TIME, 'ru_RU.utf8');

		/** mysql hook**/

		Schema::defaultStringLength(191);

		/** observers **/

		Agent::observe(SynchronizableObserver::class);
		Binding::observe(SynchronizableObserver::class);
		Brand::observe(SynchronizableObserver::class);
		Product::observe(SynchronizableObserver::class);
		PriceList::observe(SynchronizableObserver::class);
		Order::observe(SynchronizableObserver::class);
		Client::observe(SynchronizableObserver::class);
		Unit::observe(SynchronizableObserver::class);
		OrderGroup::observe(SynchronizableObserver::class);
		Reservation::observe(SynchronizableObserver::class);

		/** components **/

		Blade::component('components.badge', 'badge');
		Blade::component('components.card', 'card');
		Blade::component('components.trix', 'trix');
		Blade::component('components.datepicker', 'datepicker');
		Blade::component('components.timepicker', 'timepicker');
		Blade::component('components.tags', 'tags');
		Blade::component('components.dropzone', 'dropzone');
		Blade::component('components.accordion', 'accordion');
		Blade::component('components.accordion_element', 'accordionElement');
		Blade::component('components.modal', 'modal');
		Blade::component('components.progress', 'progress');
		Blade::component('components.select', 'select');
		Blade::component('components.form', 'form');
		Blade::component('components.flatpickr', 'flatpickr');
		Blade::component('components.dropify', 'dropify');
		Blade::component('components.heading', 'heading');
		Blade::component('components.avatar', 'avatar');
		Blade::component('components.delete_button', 'deleteButton');
        Blade::component('components.thumbnail', 'thumbnail');
	}

	/**
	 * Register any application services.
	 *
	 * @return void
	 */
	public function register()
	{
		//
	}
}
