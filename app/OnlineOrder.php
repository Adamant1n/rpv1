<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OnlineOrder extends Model
{
    /** attributes **/

    // protected $guarded = ['id'];

    protected $fillable = [
        'id',
        'client_id',
        'order_id',
        'agent_id',
        'location_id',
        'external_id',
        'date',
        'time',
        'delivery_type',
        'address',
        'duration',
        'dimension',
        'products',
        'additional_products',
        'promocode',
        'discount',
        'price',
        'total',
        'payment_type',
        'paid_amount',
        'paid_at',
        'starts_at',
        'ends_at',
        'created_at',
        'updated_at',
    ];

    protected $casts = [
        'paid_at'   => 'timestamp',
        'products'  => 'array',
        'additional_products'   => 'array',
        'price' => 'decimal:2',
        'total' => 'decimal:2',
        'paid_amount'   => 'decimal:2',
    ];

    public const PAYMENT_TYPE_FULL      = 'full';
    public const PAYMENT_TYPE_PARTIAL   = 'partial';

    protected $appends = [
        'assignedProductsHtml'
    ];

    /** relationships **/

    public function client()
    {
        return $this->belongsTo(Client::class);
    }

    public function order()
    {
        return $this->belongsTo(Order::class);
    }

    /** accessors **/

    public function getAssignedProductsHtmlAttribute()
    {
        $productsIds = $this->products;
        $additionalProductsIds = $this->additional_products;

        $products = Category::whereIn('id', $productsIds)->get();

        $html = '<ul>';
        foreach ($products as $product) {
            $html .= '<li>' . $product->name . '</li>';
        }

        foreach ($additionalProductsIds as $additionalProduct) {
            $product = Category::where('id', $additionalProduct['category_id'])->first();
            $html .= '<li>' . $product->name . ' – ' . $additionalProduct['size'] . '</li>';
        }

        $html .= '</ul>';

        return $html;
    }
}
