<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Unit extends Model
{
	use \App\Traits\LocationIdentificator;

	public $incrementing = false;
	protected $primaryKey = 'id';

	// protected $guarded = ['id'];
	protected $fillable = [
		'id',
		'location_id',
		'main_location',
		'product_id',
		'condition_id',
		'binding_id',
		'size',
		'code',
		'feature_1',
		'feature_2',
		'feature_3',
		'feature_4',
		'feature_5',
		'count',
		'reserved',
		'created_at',
		'updated_at',
	];

	protected $dates = [
		'created_at',
		'updated_at'
	];

	protected $casts = [
		'reserved' => 'boolean',
	];

	protected $appends = [
		// 'current_order'
	];

	const CONDITION_OK = 1;
	const CONDITION_BROKEN = 2;
	const CONDITION_LOST = 3;

	private static $sml_sizes = ['XXS', 'XS', 'S', 'M', 'L', 'XL', 'XXL'];

	/** RELATIONS **/

	public function location()
	{
		return $this->belongsTo(Location::class);
	}

	public function mainLocation()
	{
		return $this->belongsTo(Location::class, 'main_location');
	}

	public function product()
	{
		return $this->belongsTo(Product::class);
	}

	public function condition()
	{
		return $this->belongsTo(Condition::class);
	}

	public function orders()
	{
		return $this->belongsToMany(Order::class, 'reservations', 'unit_id', 'order_id')->using(Reservation::class);
		// return $this->belongsToMany(Order::class)->using(Reservation::class);
	}

	public function reservations()
	{
		return $this->hasMany(Reservation::class);
	}

	public function binding()
	{
		return $this->belongsTo(Binding::class);
	}

	/** ATTRIBUTES **/

	public function getIsReservedAttribute()
	{
		return $this->reservations()->rent()->exists();
	}

	public function getIsNotReservedAttribute()
	{
		return !$this->reservations()->rent()->exists();
	}

	public function getCurrentOrderAttribute()
	{
		// return $this->reservations()->rent()->first();
		// return $this->hasOne(Reservation::class)->latest()->rent();
		return $this->orders()->where('orders.status', Order::STATUS_DRAFT)->orWhere('orders.status', Order::STATUS_PAID)->latest()->first();
	}

	public function getIsUniqueAttribute()
	{
		// return Unit::where('code', $this->code)->count() === 1;
		return $this->count < 2;
	}

	public function getIsNotUniqueAttribute()
	{
		// return Unit::where('code', $this->code)->count() > 1;
		// return $this->count > 1;
		return $this->product->category->group;
	}

	/*
	public function getCodeCountAttribute()
	{
		$count = Unit::where('code', $this->code)->count();
		return $count > 1 ? $count : '';
	}
	*/

	/** ПЕРЕСМОТРЕТЬ ВСЕ ЧТО НИЖЕ **/

	public function scopeNotReserved($query)
	{
		return $query->where('reserved', false);
	}

	public function scopeReserved($query)
	{
		return $query->where('reserved', true);
	}

	public static function getSMLSizes()
	{
		return self::$sml_sizes;
	}

	public function getAgeAttribute()
	{
		return $this->created_at->diffInMonths(now());
	}

	public function getRentsAttribute()
	{
		// return $this->orders()->count();
		return $this->reservations()->whereNotNull('returned_at')->where('price', '<>', 0)->count();
	}

	public function getRevenueAttribute()
	{
		return $this->reservations()->whereNotNull('returned_at')->sum('price');
		// return $this->orders()->real()->sum('price');
		// return 0;
	}

	public function getLongStringAttribute()
	{
		return $this->optional_id . ' ' . $this->product->category->name . ' ' . $this->product->brand->name;
	}
}
