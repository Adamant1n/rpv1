<?php

namespace App;

use Dan\UploadImage\UploadImage;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
	use \App\Traits\LocationIdentificator;

	public $incrementing = false;
	protected $primaryKey = 'id';

	// protected $guarded = ['id'];
	protected $fillable = [
		'id',
		'category_id',
		'brand_id',
		'name',
		'photo',
		'for_sale',
		'price_buy',
		'price_sell',
		'created_at',
		'updated_at',
	];

	protected $dates = [
		'created_at',
		'updated_at'
	];

	protected $casts = [
		'for_sale'
	];

	protected $appends = [
		'photo_url',
	];

	/** RELATIONS **/

	public function brand()
	{
		return $this->belongsTo(Brand::class);
	}

	public function category()
	{
		return $this->belongsTo(Category::class);
	}

	public function units()
	{
		return $this->hasMany(Unit::class);
	}

	public function getPhotoUrlAttribute()
	{
		if ($this->photo) {
			if (strpos($this->photo, 'http://') !== false || strpos($this->photo, 'https://') !== false) {
				$photo = $this->photo;
			} else {
				$photo = asset(UploadImage::load('product') . $this->photo);
			}
		} else {
			$photo = '';
		}

		return $photo;
	}
}
