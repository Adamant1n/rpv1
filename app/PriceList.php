<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PriceList extends Model
{
	use \App\Traits\LocationIdentificator;

	public $incrementing = false;
	protected $primaryKey = 'id';

	// protected $guarded = ['id'];
	protected $fillable = [
		'id',
		'location_id',
		'name',
		'tariffs',
		'created_at',
		'updated_at',
	];

	protected $dates = [
		'created_at',
		'updated_at'
	];

	protected $casts = [
		'tariffs' => 'array'
	];

	/** CONSTANTS **/
	public static $timeline = [
		['duration' => 10, 'dimension' => 'm', 'ru' => 'минут'],
		['duration' => 20, 'dimension' => 'm', 'ru' => 'минут'],
		['duration' => 30, 'dimension' => 'm', 'ru' => 'минут'],
		['duration' => 1, 'dimension' => 'h', 'ru' => 'час'],
		['duration' => 2, 'dimension' => 'h', 'ru' => 'часа'],
		['duration' => 3, 'dimension' => 'h', 'ru' => 'часа'],
		['duration' => 4, 'dimension' => 'h', 'ru' => 'часа'],
		['duration' => 5, 'dimension' => 'h', 'ru' => 'часов'],
		['duration' => 6, 'dimension' => 'h', 'ru' => 'часов'],
		['duration' => 7, 'dimension' => 'h', 'ru' => 'часов'],
		['duration' => 1, 'dimension' => 'd', 'ru' => 'день'],
		['duration' => 2, 'dimension' => 'd', 'ru' => 'дня'],
		['duration' => 3, 'dimension' => 'd', 'ru' => 'дня'],
		['duration' => 4, 'dimension' => 'd', 'ru' => 'дня'],
		['duration' => 5, 'dimension' => 'd', 'ru' => 'дней'],
		['duration' => 6, 'dimension' => 'd', 'ru' => 'дней'],
		['duration' => 7, 'dimension' => 'd', 'ru' => 'дней'],
	];

	/** RELATIONS **/

	public function location()
	{
		return $this->belongsTo(Location::class);
	}
}
