<?php

namespace App\Observers;

use Illuminate\Database\Eloquent\Model;

class SynchronizableObserver
{
    public function creating($model)
    {
    	$model->id = $model->id ?? $model->makeId();
    }
}
