<?php

namespace App\Console;

use App\Console\Commands\GetAtolStatusCommand;
use App\Console\Commands\GetOrderReceiptStatusCommand;
use App\Console\Commands\SendAtolStatusCommand;
use App\Jobs\GetAtolStatusJob;
use App\Jobs\SendAtolStatusJob;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use App\User;
use App\Services\ShiftService;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->call(function () {
            dispatch(new GetAtolStatusJob);
            dispatch(new SendAtolStatusJob);
        })->everyMinute();

        // $schedule->command(GetAtolStatusCommand::class)->everyMinute();
        // $schedule->command(SendAtolStatusCommand::class)->everyMinute();
        // $schedule->command(GetOrderReceiptStatusCommand::class)->everyMinute();

        /*
        $schedule->call(function () {
            $users = User::whereHas('shifts', function ($query) {
                $query->where('opened_at', '<=', now()->subHours(20))
                    ->whereNull('closed_at');
            })->get();

            foreach ($users as $user) {
                $service = new ShiftService($user);
                $service->closePendingShift();
            }
        })->hourly();
        */
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__ . '/Commands');

        require base_path('routes/console.php');
    }
}
