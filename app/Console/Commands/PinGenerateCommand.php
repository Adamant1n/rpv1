<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Console\ConfirmableTrait;
use Illuminate\Encryption\Encrypter;

class PinGenerateCommand extends Command
{

    use ConfirmableTrait;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'pin:generate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate pin code for license';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $license = $this->ask('Enter license: ');

        $pinCode = $this->generateRandomKey();

        if (!$this->setKeyInEnvironmentFile($pinCode, $license)) {
            return;
        }

        $this->laravel['config']['app.license'] = $license;
        $this->laravel['config']['app.pin'] = $pinCode;

        $this->info('License pin code set successfully.');
    }

    protected function generateRandomKey()
    {
        return 'base64:'.base64_encode(
                Encrypter::generateKey($this->laravel['config']['app.cipher'])
            );
    }

    /**
     * Set the application key in the environment file.
     *
     * @param  string  $pinCode
     * @return bool
     */
    protected function setKeyInEnvironmentFile($pinCode, $license)
    {
        $currentLicense = $this->laravel['config']['app.license'];

        if (strlen($currentLicense) !== 0 && (!$this->confirmToProceed())) {
            return false;
        }

        $this->writeNewEnvironmentFileWith($pinCode, $license);

        return true;
    }

    /**
     * Write a new environment file with the given key.
     *
     * @param  string  $pinCode
     * @param  string  $license
     * @return void
     */
    protected function writeNewEnvironmentFileWith($pinCode, $license)
    {
        file_put_contents($this->laravel->environmentFilePath(), preg_replace(
            $this->keyReplacementPattern(),
            'APP_PIN='.$pinCode,
            file_get_contents($this->laravel->environmentFilePath())
        ));
        file_put_contents($this->laravel->environmentFilePath(), preg_replace(
            $this->licenseReplacementPattern(),
            'APP_LICENSE='.$license,
            file_get_contents($this->laravel->environmentFilePath())
        ));
    }

    /**
     * Get a regex pattern that will match env APP_KEY with any random key.
     *
     * @return string
     */
    protected function keyReplacementPattern()
    {
        $escaped = preg_quote('='.$this->laravel['config']['app.pin'], '/');

        return "/^APP_PIN{$escaped}/m";
    }

    protected function licenseReplacementPattern()
    {
        $escaped = preg_quote('='.$this->laravel['config']['app.license'], '/');

        return "/^APP_LICENSE{$escaped}/m";
    }
}
