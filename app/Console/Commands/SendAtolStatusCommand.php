<?php

namespace App\Console\Commands;

use App\AtolStatus;
use Illuminate\Console\Command;
use Illuminate\Support\Str;

class SendAtolStatusCommand extends Command
{
    protected $signature = 'atol:send';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        info('sendatolstatus handle');

        if (AtolStatus::whereNotReady()->first()) {
            return;
        }

        $command = AtolStatus::create([
            'uuid' => Str::uuid(),
        ]);

        $request = [
            'uuid' => $command->uuid,
            'request' => [
                'type' => 'getDeviceStatus',
            ],
        ];

        $client = new \GuzzleHttp\Client();
        $url = config('services.atol.url') . 'requests';

        try {
            $client->request('POST', $url, ['json' => $request]);
        } catch (\Exception $e) {
            $command->update([
                'status' => false,
            ]);
        }
    }
}
