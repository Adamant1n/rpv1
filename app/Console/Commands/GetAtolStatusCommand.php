<?php

namespace App\Console\Commands;

use App\AtolStatus;
use Exception;
use Illuminate\Console\Command;

class GetAtolStatusCommand extends Command
{
    protected $signature = 'atol:get';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        info('getatolstatus handle');

        $command = AtolStatus::whereNotReady()->first();
        if (!$command) {
            return;
        }

        $client = new \GuzzleHttp\Client();
        $url = config('services.atol.url') . 'requests/' . $command->uuid;

        try {
            $response = $client->get($url);
            $body = json_decode($response->getBody()->getContents());

            $deviceStatus = $body->results[0]->result->deviceStatus;

            $status = !$deviceStatus->blocked && $deviceStatus->fiscal && $deviceStatus->paperPresent;

            if ($status) {
                $command->update([
                    'status' => true,
                ]);
            } else {
                $command->update([
                    'status' => false,
                ]);
            }
        } catch (Exception $e) {
            $command->update([
                'status' => false,
            ]);
        }
    }
}
