<?php

namespace App\Console\Commands;

use App\Agent;
use App\AtolStatus;
use App\Binding;
use App\Booking;
use App\Brand;
use App\Category;
use App\Location;
use App\OnlineOrder;
use App\Order;
use App\OrderGroup;
use App\Payment;
use App\Product;
use App\Reservation;
use App\Shift;
use App\Stocktaking;
use App\StocktakingUnit;
use App\Synchronization;
use App\User;
use Illuminate\Console\Command;

class RefreshDatabaseLiteCommand extends Command
{
    protected $signature = 'data:fresh-lite';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        Agent::query()->delete();
        AtolStatus::query()->delete();
        Binding::query()->delete();
        Booking::query()->delete();
        Brand::query()->delete();
        Category::query()->delete();
        Order::query()->delete();
        OrderGroup::query()->delete();
        Location::where('id', '<>', 1)->delete();
        OnlineOrder::query()->delete();
        Payment::query()->delete();
        Product::query()->delete();
        Reservation::query()->delete();
        Shift::query()->delete();
        StocktakingUnit::query()->delete();
        Stocktaking::query()->delete();
        Synchronization::query()->delete();
        User::where('id', '<>', 1)->delete();
    }
}
