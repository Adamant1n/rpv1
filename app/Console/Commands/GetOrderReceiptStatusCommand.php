<?php

namespace App\Console\Commands;

use App\Order;
use Exception;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class GetOrderReceiptStatusCommand extends Command
{
    protected $signature = 'atol:order';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $orders = Order::whereNotNull('uuid')->whereNull('paid_at')->get();

        foreach ($orders as $order) {
            $this->checkOrder($order);
        }
    }

    public function checkOrder(Order $order)
    {
        $client = new \GuzzleHttp\Client();
        $url = config('services.atol.url') . 'requests/' . $order->uuid;

        try {
            $response = $client->get($url);
            $body = json_decode($response->getBody()->getContents());

            if ($body->results[0]->status == 'ready') {
                $order->markAsPaid();
            } else {
                Log::error('GetOrderReceiptStatus :: Not Ready', ['message' => $body]);
            }
        } catch (Exception $e) {
            Log::error('GetOrderReceiptStatus :: Exception', ['message' => $e->getMessage()]);
        }
    }
}
