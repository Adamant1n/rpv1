<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Shift extends Model
{
    // protected $guarded = ['id'];
    protected $fillable = [
        'id',
        'uuid',
        'user_id',
        'opened_at',
        'closed_at',
        'created_at',
        'updated_at',
    ];

    protected $appends = [
        'is_closed'
    ];

    protected $dates = [
        'opened_at',
        'closed_at'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function scopeOpened($query)
    {
        return $query->whereNull('closed_at');
    }

    public function getIsClosedAttribute()
    {
        return !is_null($this->closed_at);
    }
}
