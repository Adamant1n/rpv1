<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StocktakingUnit extends Model
{
    // protected $guarded = ['id'];
    protected $fillable = [
        'id',
        'stocktaking_id',
        'unit_id',
        'condition_id',
        'created_at',
        'updated_at',
    ];

    public function stocktaking()
    {
        return $this->belongsTo(Stocktaking::class);
    }

    public function unit()
    {
        return $this->belongsTo(Unit::class);
    }

    public function condition()
    {
        return $this->belongsTo(Condition::class);
    }
}
