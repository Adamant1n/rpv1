<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClientFeature extends Model
{
	// protected $guarded = ['id'];
	protected $fillable = [
		'id',
		'client_id',
		'height',
		'weight',
		'shoe_size',
		'created_at',
		'updated_at',
	];

	protected $casts = [
		'shoe_size' => 'decimal:1'
	];

	protected $dates = [
		'created_at',
		'updated_at'
	];

	protected $appends = [
		'foot_length'
	];

	private static $sizes = [
		19 =>	11.5,
		20 =>	12.5,
		21 =>	13,
		22 =>	13.5,
		23 =>	14.5,
		24 =>	15,
		25 =>	15.5,
		26 =>	16.5,
		27 => 	17,
		28 =>	17.5,
		29 =>	18.5,
		30 =>	19,
		31 =>	19.5,
		32 =>	20,
		33 =>	21,
		34 =>	21.5,
		35 =>	22,
		35.5 =>	22.5,
		36 =>	23,
		36.5 =>	23.5,
		37 =>	24,
		38 =>	24.5,
		39 =>	25,
		40 =>	25.5,
		41 =>	26,
		41.5 =>	26.5,
		42 =>	27,
		42.5 =>	27.5,
		43 =>	28,
		43.5 =>	28.5,
		44 =>	29,
		44.5 =>	29.5,
		45 =>	30,
		45.6 =>	30.5,
		46 =>	31,
		46.5 =>	31.5,
		47 =>	32,
		47.5 =>	32.5,
		48 =>	33
	];

	public function client()
	{
		return $this->belongsTo(Client::class);
	}

	public function getFootLengthAttribute()
	{
		return isset(self::$sizes[(float) $this->shoe_size]) ? self::$sizes[(float) $this->shoe_size] : 0;
	}
}
