<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
	use Notifiable;
	use HasRoles;
	use SoftDeletes;

	// protected $guarded = ['id'];
	protected $fillable = [
		'id',
		'atol_id',
		'atol_name',
		'category_id',
		'rate',
		'location_id',
		'first_name',
		'last_name',
		'mid_name',
		'barcode',
		'phone',
		'email',
		'password',
		'created_at',
		'updated_at',
	];

	protected $appends = [
		'full_name',
		'optional_id',
		'barcode',
		'role',
	];

	protected $hidden = [
		'password',
		'remember_token'
	];

	protected $dates = [
		'created_at',
		'updated_at',
		'deleted_at',
	];

	protected $casts = [
		'session_updated_at' => 'datetime'
	];

	const ROLE_TRAINER = 'trainer';

	/** RELATIONS **/

	public function shifts()
	{
		return $this->hasMany(Shift::class);
	}

	public function location()
	{
		return $this->belongsTo(Location::class);
	}

	public function locations()
	{
		return $this->belongsToMany(Location::class, 'location_trainer', 'trainer_id', 'location_id');
	}

	public function clients()
	{
		return $this->hasMany(Client::class);
	}

	public function taken()
	{
		return $this->hasMany(Reservation::class, 'taken_by');
	}

	public function given()
	{
		return $this->hasMany(Reservation::class, 'given_by');
	}

	/** ATTRIBUTES **/

	public function getOptionalIdAttribute()
	{
		return 'X' . sprintf("%06d", $this->id);
	}

	public function getBarcodeAttribute($value)
	{
		return $value ?? $this->optional_id;
	}

	public function getFullNameAttribute()
	{
		return $this->first_name . ' ' . $this->last_name;
	}

	public function getNameShortAttribute()
	{
		return $this->last_name . ' ' . mb_substr($this->first_name, 0, 1) . '.' . mb_substr($this->mid_name, 0, 1) . '.';
	}

	public function getRoleAttribute()
	{
		return $this->roles->first();
	}

	public function getRoleNameAttribute()
	{
		$role = $this->getRoleNames()[0];

		$roles = [
			'superadmin' => 'Суперадмин',
			'admin'		 => 'Админ',
			'manager'	 => 'Менеджер',
			'cashier'	 => 'Кассир',
			'trainer'	 => 'Тренер',
			'delivery'	 => 'Курьер'
		];

		return isset($roles[$role]) ? $roles[$role] : '–';
	}
}
