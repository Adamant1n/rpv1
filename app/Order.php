<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Order extends Model
{
	use \App\Traits\LocationIdentificator;

	public $incrementing = false;
	protected $primaryKey = 'id';

	// protected $guarded = ['id'];
	protected $fillable = [
		'id',
		'uuid',
		'group_id',
		'location_id',
		'user_id',
		'agent_id',
		'client_id',
		'payment_accepted_by',
		'status',
		'type',
		'pledge',
		'pledge_amount',
		'duration',
		'dimension',
		'price',
		'full_price',
		'discount',
		'price_extra',
		'paid_extra',
		'refunded',
		'price_manual',
		'address',
		'kkt',
		'description',
		'paid_by',
		'paid_at',
		'starts_at',
		'ends_at',
		'book_at',
		'created_at',
		'updated_at',
	];

	protected $appends = [
		'optional_id',
		'z_num',
		'current_price_list',
		'price_paid',
		'active_reservations_count',
		'term',
		//'pledge_amount',
		'refund',
	];

	protected $with = [
		'payments',
		'onlineOrder',
	];

	protected $dates = [
		'created_at',
		'updated_at',
		'starts_at',
		'ends_at',
		'paid_at',
		'book_at'
	];

	protected $casts = [
		'kkt' => 'boolean',
		'price'	=> 'decimal:2',
		'refunded' => 'decimal:2',
	];

	/*
	protected $with = [
		'group'
	];
	*/

	/** CONSTANTS **/

	const STATUS_DRAFT 		= 'draft';
	const STATUS_PAID 		= 'paid';
	const STATUS_ENDED 		= 'ended';
	const STATUS_CANCELLED	= 'cancelled';
	const STATUS_STAFFED	= 'staffed';

	private $statuses = [
		self::STATUS_DRAFT 		=> ['label' => 'Новый', 'badge' => 'badge badge-outline-info'],
		self::STATUS_PAID 		=> ['label' => 'Оплачен', 'badge' => 'badge badge-info'],
		self::STATUS_ENDED 		=> ['label' => 'Завершен', 'badge' => 'badge badge-success'],
		self::STATUS_CANCELLED	=> ['label' => 'Отменен', 'badge' => 'badge badge-warning'],
		self::STATUS_STAFFED	=> ['label' => 'Укомплектован', 'badge' => 'badge badge-primary'],
	];

	const PAID_CASH		= 'cash';
	const PAID_CARD 	= 'card';
	const PAID_ONLINE	= 'online';
	const PAID_INVOICE 	= 'invoice';

	const TYPE_RENT		= 'rent';
	const TYPE_BUY		= 'buy';
	const TYPE_BOOK		= 'book';
	const TYPE_DELIVERY	= 'delivery';

	const PLEDGE_CASH	= 'cash';
	const PLEDGE_DOC	= 'document';
	const PLEDGE_ONLINE	= 'online';

	public static $pledges = [
		self::PLEDGE_CASH => 'Наличные',
		self::PLEDGE_DOC => 'Документ',
	];

	const DIMENSION_MINUTE	= 'm';
	const DIMENSION_HOUR	= 'h';
	const DIMENSION_DAY		= 'd';

	/** RELATIONS **/

	public function onlineOrder()
	{
		return $this->hasOne(OnlineOrder::class);
	}

	public function payments()
	{
		return $this->hasMany(Payment::class);
	}

	public function client()
	{
		return $this->belongsTo(Client::class);
	}

	public function user()
	{
		return $this->belongsTo(User::class);
	}

	public function agent()
	{
		return $this->belongsTo(Agent::class);
	}

	public function units()
	{
		return $this->belongsToMany(Unit::class, 'reservations', 'order_id', 'unit_id')->using(Reservation::class);;
	}

	public function reservations()
	{
		return $this->hasMany(Reservation::class)->latest('id');
	}

	public function location()
	{
		return $this->belongsTo(Location::class);
	}

	public function group()
	{
		return $this->belongsTo(OrderGroup::class, 'group_id');
	}

	/** ATTRIBUTES **/

	public function getIsPaidAttribute()
	{
		return !is_null($this->paid_at);
	}

	public function getStatusLabelAttribute()
	{
		return $this->statuses[$this->status]['label'];
	}

	public function getStatusBadgeAttribute()
	{
		$status = $this->statuses[$this->status];

		return '<span class="' . $status['badge'] . '">' . $status['label'] . '</span>';
	}

	public function getOptionalIdAttribute()
	{
		if ($this->type === self::TYPE_RENT)			  $char = 'D';
		else if ($this->type === self::TYPE_BOOK)	  $char = 'R';
		else if ($this->type === self::TYPE_DELIVERY) $char = 'S';
		else return null;

		return $char . sprintf("%06d", $this->id);
	}

	public function getCurrentPriceListAttribute()
	{
		$dayOfWeek = (int) optional($this->starts_at)->format('N') ?? now()->format('N');
		$this->loadMissing('location');

		// выходной
		if (in_array($dayOfWeek, $this->location->weekends)) {
			$priceList = $this->location->priceWeekends;
		}
		// будний
		else {
			$priceList = $this->location->priceWeekdays;
		}

		return $priceList;
	}

	// число z
	public function getZNumAttribute()
	{
		$weights = [13, 17, 21, 25, 30, 35, 41, 48, 57, 66, 78, 94, 200];
		$heights = [0, 0, 0, 0, 0, 0, 0, 0, 157, 166, 178, 194, 250];
		$sizes = [
			250 => ['0.75', '1', '1.5', '1.75', '2.25', '2.75', '3.5', '', '', '', '', '', '', ''],
			270 => ['0.75', '1', '1.25', '1.5', '2', '2.5', '3', '3.5', '4.5', '5.5', '6.5', '7.5', '', ''],
			290 => ['', '0.75', '1', '1.5', '1.75', '2.25', '2.75', '3', '4', '5', '6', '7', '8.5', '10'],
			310 => ['', '', '', '1.25', '1.5', '2', '2.5', '3', '3.5', '4.5', '5.5', '6.5', '8', '9.5'],
			330 => ['', '', '', '', '1.5', '1.75', '2.25', '2.75', '3.5', '4', '5', '6', '7', '8.5'],
			600 => ['', '', '', '', '', '1.75', '2', '2.5', '3', '3.5', '4.5', '5.5', '6.5', '8']
		];

		$client = $this->client;
		$reservations = $this->reservations()->with('unit')->get();

		$znum = '–';

		foreach ($reservations as $reservation) {
			if (!is_null($reservation->unit->feature_4)) {
				if ($client->feature()->exists() && $client->feature->weight && $client->feature->height) {
					foreach ($weights as $w_key => $weight)
						if ($client->feature->weight <= $weight) break;

					foreach ($heights as $h_key => $height)
						if ($client->feature->height <= $height) break;

					foreach ($sizes as $size => $znums)
						if ($reservation->unit->feature_4 <= $size) break;

					$znum = $znums[min($w_key, $h_key)];
				}

				break;
			}
		}

		return $znum;
	}

	/** SCOPES **/

	public function scopeExpired($query)
	{
		return $query->where('ends_at', '<', now());
	}

	public function scopeReal($query)
	{
		return $query->where('status', self::STATUS_PAID)->orWhere('status', self::STATUS_ENDED);
	}

	public function scopePaid($query)
	{
		return $query->where('status', self::STATUS_PAID);
	}

	public function setStatusEnded()
	{
		return $this->update(['status' => self::STATUS_ENDED]);
	}

	public function scopeEnded($query)
	{
		return $query->where('status', self::STATUS_ENDED);
	}

	public function scopeActive($query)
	{
		return $query->where('status', '<>', self::STATUS_ENDED)->where('status', '<>', self::STATUS_CANCELLED);
	}

	public function scopeDraft($query)
	{
		return $query->where('status', self::STATUS_DRAFT);
	}

	public function scopeRent($query)
	{
		return $query->where('type', self::TYPE_RENT);
	}

	public function scopeBook($query)
	{
		return $query->where('type', self::TYPE_BOOK);
	}

	public function scopeDelivery($query)
	{
		return $query->where('type', self::TYPE_DELIVERY);
	}

	/** FUNCTIONS **/
	public function cancel()
	{
		$this->update(['status' => self::STATUS_CANCELLED]);

		foreach ($this->reservations as $reservation) {
			$reservation->cancel();
		}
	}

	/*
	public function getPriceAttribute($value)
	{
		return empty($this->price_manual) ? $value : $this->price_manual;
	}
	*/

	public function getRefundAttribute()
	{
		$this->loadMissing('reservations');

		return $this->reservations->sum('refund') - $this->refunded;
	}

	public function getPricePaidAttribute()
	{
		// return $this->castAttribute('price', $this->price + $this->paid_extra);
		return $this->castAttribute(
			'price',
			$this->payments()->sum('sum')
		);
	}

	public function getPriceExtraAttribute($value)
	{
		$price = $value - $this->paid_extra;
		$price = ($price < 0) ? 0 : $price;

		return $this->castAttribute('price', $price);
	}

	public function markAsPaid()
	{
		$this->pay();
		// если есть группа
		if (!is_null($this->group_id)) {
			foreach ($this->orders as $_order) {
				$_order->pay();
				foreach ($_order->reservations as $reservation) {
					if (is_null($reservation->starts_at)) $reservation->sold();
				}
			}

			$this->update(['status' => 'paid']);
		} else {
			foreach ($this->reservations as $reservation) {
				if (is_null($reservation->starts_at)) {
					$reservation->sold();
				}
			}
		}
	}

	public function pay()
	{
		if ($this->price_extra > 0) {
			$this->payments()->create([
				'sum'	=> $this->price_extra,
				'type'	=> Payment::TYPE_DEBT
			]);

			$this->update([
				'paid_extra' => $this->price_extra
			]);

			// если есть группа
			if (!is_null($this->group_id)) {
				foreach ($this->group->orders as $_order) {
					$_order->update([
						'paid_extra' => $this->price_extra
					]);

					if ($_order->id != $this->id) {
						$_order->payments()->create([
							'sum'	=> $this->price_extra,
							'type'	=> Payment::TYPE_DEBT
						]);
					}
				}
			}

			return;
		}

		if ($this->status != self::TYPE_RENT) {
			foreach ($this->reservations as $reservation) {
				$reservation->update([
					'created_at' => now()
				]);
			}

			$orderService = new \App\Services\OrderService($this);
			$orderService->calculateOrderTime();
			$orderService->calculateOrderPrice();
		}

		$this->payments()->create([
			'sum'	=> $this->price,
			'type'	=> Payment::TYPE_ORDER
		]);

		$this->update([
			'payment_accepted_by' => Auth::check() ? auth()->user()->id : $this->payment_accepted_by,
			'status' 	=> self::STATUS_PAID,
			'type'		=> self::TYPE_RENT,
			'paid_at' 	=> now()
		]);
	}

	public function end()
	{
		$this->update([
			'status'	=> self::STATUS_ENDED,
		]);
	}

	public function getActiveReservationsCountAttribute()
	{
		return $this->reservations()->whereNotNull('starts_at')->rent()->count();
	}

	public function getTermAttribute()
	{
		$timeline = PriceList::$timeline;
		$term = $this->duration;

		foreach ($timeline as $time) {
			if ($time['duration'] == $this->duration && $time['dimension'] == $this->dimension) {
				$term .= ' ' . $time['ru'];
			}
		}

		return $term ?? '–';
	}

	public function getPledgeAmountAttribute($value)
	{
		return $value ?? $this->units->sum(function ($unit) {
			return $unit->product->price_sell;
		});
	}
}
