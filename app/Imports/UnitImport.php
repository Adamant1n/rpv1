<?php

namespace App\Imports;

use App\Unit;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\Importable;

class UnitImport implements ToModel
{
	use Importable;

    public function model(array $row)
    {
        return new Unit([
            //
        ]);
    }

    public function collection(Collection $rows)
    {
        foreach ($rows as $row)
        {
            //User::create([
            //    'name' => $row[0],
            //]);
        }
    }

}
