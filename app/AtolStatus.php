<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class AtolStatus extends Model
{
    protected $guarded = [];

    protected $casts = [
        'status' => 'boolean',
    ];

    public const OK = 'ok';
    public const ERROR = 'error';
    public const UNKNOWN = 'unknown';

    public static function getStatus()
    {
        $atolStatus = self::whereReady()->latest()->first();

        if ($atolStatus) {
            return $atolStatus->status ? self::OK : self::ERROR;
        }

        return self::UNKNOWN;
    }

    public function scopeWhereNotReady(Builder $query)
    {
        $query->whereNull('status')->oldest();
    }

    public function scopeWhereReady(Builder $query)
    {
        $query->whereNotNull('status');
    }
}
