<?php

namespace App\Traits;

trait LocationIdentificator
{
	public static function makeId()
	{
		$location = \App\Location::find(config('app.location_id'));

		if (method_exists(self::class, 'bootSoftDeletes')) {
			$id = self::selectRaw('(MAX(id) + 1) as id')->whereRaw('id BETWEEN ' . $location->base_id . ' AND ' . $location->max_id)->withTrashed()->first();
		} else {
			$id = self::selectRaw('(MAX(id) + 1) as id')->whereRaw('id BETWEEN ' . $location->base_id . ' AND ' . $location->max_id)->first();
		}

		$id = $id->id ?? $location->base_id;

		return ($id);
	}
}
