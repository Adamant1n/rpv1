<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
	const SKI = 1;

	// protected $guarded = ['id'];
	protected $fillable = [
		'id',
		'name',
		'params',
		'group',
		'type',
		'fr_group',
		'created_at',
		'updated_at',
	];

	protected $dates = [
		'created_at',
		'updated_at'
	];

	protected $casts = [
		'params' => 'array',
		'group'	=> 'boolean'
	];

	const PARAM_LENGTH 		= 1;
	const PARAM_BINDING		= 2;
	const PARAM_SIZE 		= 3;
	const PARAM_SHOE 		= 4;
	const PARAM_SIZE_SML 	= 5;

	public const TYPE_RENT = 'rent';
	public const TYPE_SALE = 'sale';

	private static $available_params = [
		self::PARAM_LENGTH => 'Длина',
		self::PARAM_BINDING => 'Крепление',
		self::PARAM_SIZE => 'Размер',
		self::PARAM_SHOE => 'Колодка',
		self::PARAM_SIZE_SML => 'Размер (SLM)'
	];

	/** RELATIONS **/

	public function products()
	{
		return $this->hasMany(Product::class, 'category_id');
	}

	/** FUNCTIONS **/

	public static function getAvailableParams()
	{
		return self::$available_params;
	}

	public static function getSMLSizes()
	{
		return self::$sml_sizes;
	}
}
