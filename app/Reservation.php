<?php

namespace App;

//use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Pivot;

//class Reservation extends Model
class Reservation extends Pivot
{
    use \App\Traits\LocationIdentificator;
    use \App\Traits\CreateLock;

    public $incrementing = false;
    protected $primaryKey = 'id';

    protected $table = 'reservations';

    // protected $guarded = ['id'];
    protected $fillable = [
        'id',
        'order_id',
        'unit_id',
        'replaced',
        'duration',
        'dimension',
        'given_by',
        'taken_by',
        'status',
        'price',
        'price_manual',
        'full_price',
        'price_extra',
        'refund',
        'starts_at',
        'ends_at',
        'returned_at',
        'created_at',
        'updated_at',
    ];

    protected $dates = [
        'starts_at',
        'ends_at',
        'returned_at',
    ];

    protected $casts = [
        'replaced' => 'boolean',
        'price'    => 'decimal:2',
        'refund' => 'decimal:2',
    ];

    protected $appends = [
        'term'
    ];

    public static $timeline = [
        ['duration' => 10, 'dimension' => 'm', 'ru' => 'мин.'],
        ['duration' => 20, 'dimension' => 'm', 'ru' => 'мин.'],
        ['duration' => 30, 'dimension' => 'm', 'ru' => 'мин.'],
        ['duration' => 1, 'dimension' => 'h', 'ru' => 'ч.'],
        ['duration' => 2, 'dimension' => 'h', 'ru' => 'ч.'],
        ['duration' => 3, 'dimension' => 'h', 'ru' => 'ч.'],
        ['duration' => 4, 'dimension' => 'h', 'ru' => 'ч.'],
        ['duration' => 5, 'dimension' => 'h', 'ru' => 'ч.'],
        ['duration' => 6, 'dimension' => 'h', 'ru' => 'ч.'],
        ['duration' => 7, 'dimension' => 'h', 'ru' => 'ч.'],
        ['duration' => 1, 'dimension' => 'd', 'ru' => 'д.'],
        ['duration' => 2, 'dimension' => 'd', 'ru' => 'д.'],
        ['duration' => 3, 'dimension' => 'd', 'ru' => 'д.'],
        ['duration' => 4, 'dimension' => 'd', 'ru' => 'д.'],
        ['duration' => 5, 'dimension' => 'd', 'ru' => 'д.'],
        ['duration' => 6, 'dimension' => 'd', 'ru' => 'д.'],
        ['duration' => 7, 'dimension' => 'd', 'ru' => 'д.'],
    ];

    /** CONSTANTS **/

    const STATUS_RENT        = 'rent';
    const STATUS_RETURNED    = 'returned';
    const STATUS_SOLD        = 'sold';
    const STATUS_CANCELLED  = 'cancelled';

    /** RELATIONS **/

    public function order()
    {
        return $this->belongsTo(Order::class);
    }

    public function unit()
    {
        return $this->belongsTo(Unit::class);
    }

    public function given()
    {
        return $this->belongsTo(User::class, 'given_by');
    }

    public function taken()
    {
        return $this->belongsTo(User::class, 'taken_by');
    }

    /** SCOPES **/

    public function scopeRent($query)
    {
        $query->where('status', self::STATUS_RENT);
    }

    public function scopeIsSold($query)
    {
        $query->where('status', self::STATUS_SOLD);
    }

    public function scopeReturned($query)
    {
        $query->where('status', self::STATUS_CANCELLED);
    }

    /** FUNCTIONS **/

    public function cancel()
    {
        $this->update(['status' => self::STATUS_CANCELLED]);
    }

    public function sold()
    {
        $this->update(['status' => self::STATUS_SOLD]);
        $this->unit->decrement('count', 1);
    }

    /** accessors **/

    public function getPriceAttribute($value)
    {
        return $this->castAttribute('price', empty($this->price_manual) ? $value : $this->price_manual);
    }

    public function getExpiredAttribute()
    {
        $diff = now()->diffInDays($this->ends_at);
        return $this->ends_at > now() ? 0 : $diff;
    }

    public function getTermAttribute()
    {
        if (is_null($this->starts_at))
            return '–';

        // $timeline = PriceList::$timeline;
        $timeline = [
            'd' => 'д.',
            'h' => 'ч.',
            'm' => 'мин.',
        ];


        if (is_null($this->returned_at)) {
            $term = $this->duration;
        } elseif ($this->dimension == 'd') {
            $term = $this->starts_at->diffInDays($this->returned_at);
        } elseif ($this->dimension == 'h') {
            $term = $this->starts_at->diffInHours($this->returned_at);
        }

        $term .= ' ';
        $term .= $timeline[$this->dimension] ?? '???';

        return $term;
    }
}
