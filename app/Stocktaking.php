<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Stocktaking extends Model
{
    // protected $guarded = ['id'];
    protected $fillable = [
        'id',
        'location_id',
        'count_lost',
        'count_ok',
        'count_other',
        'created_at',
        'updated_at',
    ];

    public function units()
    {
        return $this->hasMany(StocktakingUnit::class, 'stocktaking_id');
    }

    public function location()
    {
        return $this->belongsTo(Location::class);
    }
}
