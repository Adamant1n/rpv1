<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    /** parameters **/

    // protected $guarded = ['id'];
    protected $fillable = [
        'id',
        'order_id',
        'sum',
        'type',
        'created_at',
        'updated_at',
    ];

    protected $casts = [
        'sum'    => 'decimal:2'
    ];

    protected $appends = [
        'type_info',
        'optional_id'
    ];

    const TYPE_ORDER = 'order';
    const TYPE_DEBT  = 'debt';

    private static $types = [
        self::TYPE_ORDER => 'Договор',
        self::TYPE_DEBT => 'Задолженность'
    ];

    /** relationships **/

    public function order()
    {
        return $this->belongsTo(Order::class);
    }

    public function getOptionalIdAttribute()
    {
        // return $this->order->optional_id;
        return 'D' . sprintf("%06d", $this->order_id);
    }

    /** accessors **/

    public function getTypeInfoAttribute()
    {
        return self::$types[$this->type];
    }
}
