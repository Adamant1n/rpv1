<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExpiredPassport extends Model
{
    protected $guarded = ['id'];
}
