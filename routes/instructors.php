<?php

Route::any('callback/yandex', 'YandexCallController@callback')->name('callback.webhook');
Route::get('callback/get', 'YandexCallController@getClient')->name('callback.get_client');

Route::middleware('auth')->group(function () {

    Route::get('/', function () {
        //return redirect()->route('calendar.index');
        return redirect('instructors/calendar');
    });

    Route::get('settings', 'SettingsController@edit')->name('settings.edit');
    Route::post('settings', 'SettingsController@update')->name('settings.update');

    Route::get('calendar', 'CalendarController@index');
    Route::get('calendar/{id}/event', 'CalendarController@event');
    Route::get('calendar/events_json', 'CalendarController@events_json');
    Route::get('calendar/resources_json', 'CalendarController@resources_json');

    Route::get('clients/clients_json', 'ClientController@clients_json');
    Route::resource('clients', 'ClientController');

    Route::get('event/events_json', 'EventController@events_json');
    Route::resource('event', 'EventController');
    Route::post('event/store', 'EventController@store');
    Route::post('event/update', 'EventController@update');
    Route::post('event/{event}/pay', 'EventController@pay')->name('event.pay');
    Route::get('event/{id}/delete', 'EventController@destroy');

    Route::resource('user', 'UserController');
    Route::post('user/store', 'UserController@store');
    Route::post('user/update', 'UserController@update');
    Route::get('user/{id}/delete', 'UserController@destroy');
    Route::get('user/{id}/restore', 'UserController@restore');

    Route::resource('category', 'CategoryController');
    Route::post('category/store', 'CategoryController@store');
    Route::post('category/update', 'CategoryController@update');
    Route::get('category/{id}/delete', 'CategoryController@destroy');

    Route::resource('tariff', 'TariffController');
    Route::post('tariff/store', 'TariffController@store');
    Route::post('tariff/update', 'TariffController@update');
    Route::get('tariff/{id}/delete', 'TariffController@destroy');

    Route::resource('service', 'ServiceController');
    Route::post('service/store', 'ServicefController@store');
    Route::post('service/update', 'ServiceController@update');
    Route::get('service/{id}/delete', 'ServiceController@destroy');

    Route::resource('location', 'LocationController');
    Route::post('location/store', 'LocationController@store');
    Route::post('location/update', 'LocationController@update');
    Route::get('location/{id}/delete', 'LocationController@destroy');

    Route::get('report/sales', 'ReportController@sales');
    Route::get('report/sales_json', 'ReportController@sales_json');
    Route::get('report/trainers', 'ReportController@trainers');
    Route::get('report/trainers_json', 'ReportController@trainers_json');
    Route::get('report/efficiency', 'ReportController@efficiency');
    Route::get('report/efficiency_json', 'ReportController@efficiency_json');

    Route::get('/download/{file}', 'HomeController@download');

    Route::get('logout', 'Auth\LoginController@logout');

});