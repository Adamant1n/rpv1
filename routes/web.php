<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// убрать на продакшне
// Artisan::call('view:clear');

/** synchronization **/

Route::get('sync/send', 'SynchronizationController@send');
Route::any('sync/receive', 'SynchronizationController@receive');
Route::get('sync/request', 'BackwardSyncController@request');
Route::get('sync/response', 'BackwardSyncController@response');

Route::middleware('maintenance')->group(function () {
	Route::prefix('instructors')
		->namespace('Instructors')
		->name('instructors.')
		->group(base_path('routes/instructors.php'));

	/** user public **/
	Route::get('rent', 'PublicController@index')->name('public.index');
	Route::post('rent/calculate', 'PublicController@calculate')->name('public.calculate');
	Route::get('rent/locations', 'PublicController@getLocations')->name('public.locations');
	Route::post('rent', 'PublicController@store')->name('public.store');
	Route::get('rent/{onlineOrder}/process', 'PublicController@process')->name('public.process');
	Route::get('rent/{onlineOrder}/success', 'PublicController@success')->name('public.success');

	Route::post('login/barcode', 'BarcodeLogin@handle')->name('login.barcode');
	Auth::routes();

	Route::prefix('test')->group(function () {
		Route::get('schedule', 'TestController@schedule');
		Route::get('api', 'TestController@api');
	});

	Route::resource('guest', 'GuestController');
	Route::post('guest/find', 'GuestController@find')->name('guest.find');
	Route::get('clients/{client}/barcode', 'ClientController@barcode')->name('clients.barcode');
	Route::post('clients/{client}/ajax', 'ClientController@ajaxUpdate')->name('clients.ajax.update');

	Route::get('/expired_passports/upload', 'ExpiredPassportController@upload')->name('expired_passports.upload');
	Route::post('/expired_passports/check', 'ExpiredPassportController@check')->name('expired_passports.check');


	// сканер штрихкодов
	Route::middleware('api_login')->group(function () {
		Route::get('/atol_status/request', 'AtolStatusController@request')->name('atol.status.request');
		Route::get('/atol_status/{uuid}', 'AtolStatusController@ping')->name('atol.status.ping');
		Route::get('/scan/refund', 'ScannerController@refund')->name('scanner.refund');
		Route::get('/scan/set_price', 'ScannerController@setPrice')->name('scanner.set_price');
		Route::get('/scan/set_price_extra', 'ScannerController@setPriceExtra')->name('scanner.set_price_extra');
		Route::get('/scan/client', 'ScannerController@client')->name('scanner.client');
		Route::get('/scan/unit', 'ScannerController@unit')->name('scanner.unit');
		Route::get('/scan/update', 'ScannerController@updateOrder')->name('scanner.update');
		Route::get('/scan/order', 'ScannerController@order')->name('scanner.order');
		Route::get('/scan/create_order', 'ScannerController@createOrder')->name('scanner.create_order');
		Route::get('/scan/cancel_order', 'ScannerController@cancelOrder')->name('scanner.cancel_order');
		Route::get('/scan/promocode', 'ScannerController@promocode')->name('scanner.promocode');
		Route::get('/scan/pay', 'ScannerController@pay')->name('scanner.pay');
		Route::get('/scan/force_pay', 'ScannerController@forcePay')->name('scanner.force_pay');
		Route::get('/scan/pay_extra', 'ScannerController@payExtra')->name('scanner.pay_extra');
		Route::get('/scan/print_receipt', 'ScannerController@printReceipt')->name('scanner.print_receipt');
		Route::get('/scan/end', 'ScannerController@end')->name('scanner.end');
		Route::get('/scan/group', 'ScannerController@addToGroup')->name('scanner.add_to_group');
		Route::get('/scan/ungroup', 'ScannerController@removeFromGroup')->name('scanner.remove_from_group');
		Route::get('/scan/replace', 'ScannerController@replace')->name('scanner.replace');
		Route::get('/scan/get_clients', 'ScannerController@getClients')->name('scanner.get_clients');
		Route::get('/scan/update_condition', 'ScannerController@updateCondition')->name('scanner.update_condition');
	});

	Route::middleware('auth')->group(function () {

		Route::get('/', function () {
			return redirect()->route('dashboard.index');
		});

		Route::resource('roles', 'RoleController');
		Route::resource('permissions', 'PermissionController');

		Route::middleware('location_active')->group(function () {
			// вебкамера
			Route::post('webcam', 'ClientController@webcam')->name('client.webcam');
			Route::post('clients/webcam/{client}', 'ClientController@webcamSave')->name('client.webcam_save');

			Route::get('synchronizations', 'SynchronizationController@index')->name('synchronizations.index');

			// смены
			Route::get('shifts/{id}/report', 'ShiftController@report')->name('shifts.report');
			Route::get('shifts', 'ShiftController@index')->name('shifts.index');
			Route::get('shifts/open', 'ShiftController@open')->name('shifts.open');
			Route::get('shifts/xreport', 'ShiftController@xreport')->name('shifts.xreport');
			Route::get('shifts/{id}', 'ShiftController@close')->name('shifts.close');

			Route::get('dashboard', 'DashboardController@dashboard')->name('dashboard.index');
			Route::get('analytics', 'DashboardController@analytics')->name('analytics.index');

			// настройки
			Route::get('settings', 'SettingsController@edit')->name('settings.edit');
			Route::post('settings', 'SettingsController@update')->name('settings.update');

			// категории
			Route::resource('categories', 'CategoryController');

			// бренды
			Route::resource('brands', 'BrandController');

			// крепления
			Route::resource('bindings', 'BindingController');

			// агенты
			Route::get('agents/{id}/restore', 'AgentController@restore')->name('agents.restore');
			Route::resource('agents', 'AgentController');

			// пользователи
			Route::get('users/{id}/restore', 'UserController@restore')->name('users.restore');
			Route::resource('users', 'UserController');
			Route::get('users/{id}/barcode', 'UserController@barcode')->name('users.barcode');

			// модели
			Route::resource('products', 'ProductController');

			// филиалы
			Route::resource('locations', 'LocationController');

			// прейскуранты
			Route::resource('price_lists', 'PriceListController');
			Route::post('price_lists/{price_list}/timeline', 'PriceListController@timeline')->name('price_lists.timeline');


			// клиенты
			Route::get('clients/json', 'ClientController@json')->name('clients.json');
			Route::resource('clients', 'ClientController');

			// заказы
			Route::resource('orders', 'OrderController');
			Route::get('orders/{order}/agreement', 'OrderController@agreement')->name('agreement');
			Route::get('orders/{order}/barcode', 'OrderController@barcode');
			Route::get('orders/{order}/sticker', 'OrderController@sticker');
			Route::get('orders/{order}/pledge', 'OrderController@pledgeAgreement');


			// склад
			Route::get('stock/json', 'StockController@json')->name('stock.json');
			Route::get('stock/import', 'StockController@import')->name('stock.import');
			Route::post('stock/import', 'StockController@import');
			Route::get('stock/is_unique', 'StockController@checkIfUnitUnique')->name('stock.is_unique');
			Route::resource('stock', 'StockController');

			// массовое редактирование
			Route::get('massediting', 'MassEditingController@index')->name('massediting.index');
			Route::post('massediting', 'MassEditingController@store')->name('massediting.store');
			Route::post('massediting/units', 'MassEditingController@getUnits')->name('massediting.get_units');
			Route::post('massediting/unit', 'MassEditingController@getUnit')->name('massediting.get_unit');

			// инвентаризация
			Route::get('stocktaking/perform', 'StocktakingController@perform')->name('stocktaking.perform');
			Route::get('stocktaking/{stocktaking}', 'StocktakingController@show')->name('stocktaking.show');
			Route::get('stocktaking', 'StocktakingController@index')->name('stocktaking.index');
			Route::post('stocktaking', 'StocktakingController@store')->name('stocktaking.store');
			Route::post('stocktaking/units', 'StocktakingController@getUnits')->name('stocktaking.get_units');
			Route::post('stocktaking/unit', 'StocktakingController@getUnit')->name('stocktaking.get_unit');

			// отчеты
			Route::get('reports/daily_revenue', 'ReportController@daily_revenue')->name('reports.financial');
			Route::get('reports/agreements', 'ReportController@agreements')->name('reports.agreements');
			Route::get('reports/agent_sales_common', 'ReportController@agent_sales_common')->name('reports.agent_sales_common');
			Route::get('reports/agent_sales_detailed', 'ReportController@agent_sales_detailed')->name('reports.agent_sales_detailed');

			Route::get('reports/inventory_rent', 'ReportController@inventory_rent')->name('reports.inventory_rent');
			Route::get('reports/inventory_stock', 'ReportController@inventory_stock')->name('reports.inventory_stock');
			Route::get('reports/inventory_stock_detailed', 'ReportController@inventory_stock_detailed')->name('reports.inventory_stock_detailed');
			Route::get('reports/inventory_sale', 'ReportController@inventory_sale')->name('reports.inventory_sale');
			Route::get('reports/inventory_history', 'ReportController@inventory_history')->name('reports.inventory_history');

			Route::get('reports/inventory_products', 'ReportController@inventory_products')->name('reports.inventory_products');
			Route::get('reports/products_sale', 'ReportController@products_sale')->name('reports.products_sale');
			Route::get('reports/bookings', 'ReportController@bookings')->name('reports.bookings');
			Route::get('reports/effectivity', 'ReportController@effectivity')->name('reports.effectivity');

			Route::get('reports/inventory', 'ReportController@inventory')->name('reports.inventory');
			Route::get('reports/general', 'ReportController@general')->name('reports.general');
			Route::get('reports/other', 'ReportController@other')->name('reports.other');

			Route::get('reports/debt', 'ReportController@debt')->name('reports.debt');
		});

		Route::get('logout', 'Auth\LoginController@logout');
	});
});
